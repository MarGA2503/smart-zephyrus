#!/usr/bin/env python
"""
Contains all functions related with the connection using web3py
"""

from web3 import Web3, HTTPProvider, IPCProvider
import json
import web3
import os
#from solc import compile_source
from web3.contract import ConciseContract

__author__ = "Mar Gimenez Aguilar"
__version__ = "1"
__maintainer__ = "Mar Gimenez Aguilar"
__email__ = "mar.gimeneza@gmail.com"


# def w3getCode (sourceCode, auto = True, IPCP = "", HTTP = ""):
#     with open(sourceCode, 'r') as myfile:
#         contract_source_code=myfile.read().replace('\n', '')
#     compiled_sol = compile_source(contract_source_code) # Compiled source code
#     contract_interface = compiled_sol[list(compiled_sol)[0]]
#     # # web3.py instance
#     # w3 = Web3(TestRPCProvider())
#     return contract_interface
def w3getgasPrice(w3):
    """
    Returns the current gas price
    """
    return w3.eth.gasPrice
def w3getChainId (w3):
    """
    Returns the w3getChainId
    """
    return w3.eth.chain_id

def w3getBlockGasLimit (w3, block_number= ""):
    """
    Returns the gas limit for the block
    """
    if block_number == "":
        try:
            block = w3.eth.getBlock("pending")
        except web3.exceptions.ValidationError:
            from web3.middleware import geth_poa_middleware
            w3.middleware_stack.inject(geth_poa_middleware, layer=0)
            block = w3.eth.getBlock("pending")
    else:
        try:
            block = w3.eth.getBlock(block_number)
        except web3.exceptions.ValidationError:
            from web3.middleware import geth_poa_middleware
            w3.middleware_stack.inject(geth_poa_middleware, layer=0)
            block = w3.eth.getBlock(block_number)
    return block['gasLimit']

def w3getBlocktransactionsbyNumber (block_number, key_to_extract, key_value, key_to_retrieve,w3,tocontract=False):
    """
    Returns the valid transactions in a block that match a certain criteria or key
    """
    data_to_retrieve = []
    nonces = []
    try:
        block = w3.eth.getBlock(block_number)
    except web3.exceptions.ValidationError:
        from web3.middleware import geth_poa_middleware
        w3.middleware_stack.inject(geth_poa_middleware, layer=0)
        block = w3.eth.getBlock(block_number)
    total_transactions = block['transactions']
    for tx_hash in total_transactions:
        tmp_transaction = w3.eth.getTransaction(tx_hash.hex())
        fullfill = True
        i = 0
        if tmp_transaction['to'] != None:
            if tocontract:
                if  w3.eth.getCode(w3.toChecksumAddress(tmp_transaction['to'])).hex() == '0x':
                    fullfill = False
            else:
                if  w3.eth.getCode(w3.toChecksumAddress(tmp_transaction['to'])).hex() != '0x':
                    fullfill = False
        while i < len(key_to_extract) and fullfill:
            if not(eval(str(eval("tmp_transaction[key_to_extract[i]]")) + str(eval("key_value[i]"))) or eval(str(eval("tmp_transaction[key_to_extract[i]].lower()")) +  eval("key_value[i].lower()"))):
                fullfill = False
            i += 1
        if fullfill:
            data_to_retrieve.append(tmp_transaction[key_to_retrieve])
            if type(data_to_retrieve[-1]) == str and data_to_retrieve[-1][0:2] == '0x':
                data_to_retrieve[-1] = data_to_retrieve[-1][2:]
            nonces.append(tmp_transaction['nonce'])
    return data_to_retrieve, nonces

def w3getAccountNonce (senderAddress,w3):
    """
    Returns the nonce of an account
    """
    nonce = w3.eth.getTransactionCount(w3.toChecksumAddress(senderAddress),block_identifier="pending")
    return nonce

def w3deployContract (senderAddress, senderKey, contract_interface,option="ipc", gas = 90000,  times = 1):
    """
    Deploys a contract into the blockchain
    """
    # Instantiate and deploy contract
    #Redundancy for deploying
    #TODO si el tiempo es mas de uno el resultado es un array de hashes
    for x in range(times):
        if option=="ipc":
            w3.geth.personal.unlockAccount(w3.toChecksumAddress(senderAddress), senderKey)
        contract = w3.eth.contract(abi=contract_interface['abi'], bytecode=contract_interface['bin'])
        # Get transaction hash from deployed contract
        tx_hash = contract.deploy(transaction={'from':w3.toChecksumAddress(senderAddress), 'gas': gas})
        tx_hash_hex = tx_hash.hex()
    return tx_hash_hex

def w3checkContractDeployment (tx_hash_hex, w3):
    """
    Retrieve the contract address from a contract deployment transaction hash
    """
    address = ""
    tx_receipt = w3.eth.getTransactionReceipt(tx_hash_hex)
    while tx_receipt is None:
        # Get tx receipt to get contract address
        tx_receipt = w3.eth.getTransactionReceipt(tx_hash_hex)
    contract_address = tx_receipt['contractAddress']
    return contract_address


def w3checkTransaction (tx_hash_hex):
    """
    Check if a transaction is mined
    """
    #TODO: maybe better waittransactionreceipt
    tx_receipt = w3.eth.getTransactionReceipt(tx_hash_hex)
    while tx_receipt is None:
        # Get tx receipt to get contract address
        tx_receipt = w3.eth.getTransactionReceipt(tx_hash_hex)
    return True

def w3sendRawTransaction(senderAddress, senderKey, receiverAddress, nonce, w3, gas = 90000, gasPrice = "", value = -1, data = "" , times = 1):
    """
    Signs and sends a raw transaction
    """
    transaction = {}
    transaction['from'] = w3.toChecksumAddress(senderAddress)
    transaction['to'] = w3.toChecksumAddress(receiverAddress)
    transaction['gas'] = gas
    transaction['nonce']= nonce
    if gasPrice == "":
        transaction['gasPrice'] = w3.eth.gasPrice
    else:
        transaction['gasPrice'] = '0x' + gasPrice
    try:
        transaction['chainId'] = int(w3.version.network)
    except AttributeError:
        transaction['chainId'] = int(w3.eth.chainId)
    if data != "":
        transaction['data'] = data
    if value != -1:
        transaction['value'] = '0x'+ value
    signed = w3.eth.account.signTransaction(transaction, senderKey)
    tx_hash = w3.eth.sendRawTransaction(signed.rawTransaction)
    return tx_hash.hex()

def w3sendTransaction(senderAddress, senderKey, receiverAddress, w3, option="ipc", gas = 90000, gasPrice = -1, value = -1, data = "", nonce = -1 , times = 1):
    """
    Sends a transaction
    """
    for x in range(times):
        if option=="ipc":
            w3.geth.personal.unlockAccount(w3.toChecksumAddress(senderAddress), senderKey)
        if gasPrice!= -1:
            tx_hash = w3.eth.sendTransaction({'to':w3.toChecksumAddress(receiverAddress), 'from': w3.toChecksumAddress(senderAddress), 'data': data, 'gas':gas, 'gasPrice':gasPrice })
        elif value != -1:
            tx_hash = w3.eth.sendTransaction({'to':w3.toChecksumAddress(receiverAddress), 'from': w3.toChecksumAddress(senderAddress), 'data': data, 'gas':gas, 'value': value })
        else:
            tx_hash = w3.eth.sendTransaction({'to':w3.toChecksumAddress(receiverAddress), 'from': w3.toChecksumAddress(senderAddress), 'data': data, 'gas':gas})
    return tx_hash.hex()

def w3estimateGasSendTransaction(senderAddress, senderKey, receiverAddress, w3, option="ipc",gas = 90000, gasPrice = -1, value = 0, data = "", nonce = -1 , times = 1):
    """
    Returns the estimated gas necessary to send a transaction
    """
    for x in range(times):
        if option=="ipc":
            w3.geth.personal.unlockAccount(w3.toChecksumAddress(senderAddress), senderKey)
        gas = w3.eth.estimateGas({'to':w3.toChecksumAddress(receiverAddress), 'from': w3.toChecksumAddress(senderAddress), 'data': data})
    return gas

def w3estimateGasdeployContractConstructor(senderAddress, senderKey, contract_interface,w3, option="ipc", gas = 90000, nonce = -1 , times = 1):
    """
    Returns the estimated gas necessary to deploy a contract
    """
    for x in range(times):
        if option=="ipc":
            w3.geth.personal.unlockAccount(w3.toChecksumAddress(senderAddress), senderKey)
        gas = w3.eth.estimateGas({ 'from': w3.toChecksumAddress(senderAddress), 'data': contract_interface['bin']})
    return gas

def w3deployRawContract(senderAddress, senderKey, contract_interface, nonce, w3, option="ipc", gas = 90000 , times = 1):
    """
    Signs and deploys a contract
    """
    transaction = {}
    transaction['from'] = w3.toChecksumAddress(senderAddress)
    transaction['to'] = b''
    transaction['gas'] = gas
    transaction['nonce']= nonce
    transaction['gasPrice'] = w3.eth.gasPrice
    try:
        transaction['chainId'] = int(w3.version.network)
    except AttributeError:
        transaction['chainId'] = int(w3.eth.chainId)
    transaction['data'] = contract_interface['bin']
    signed = w3.eth.account.signTransaction(transaction, senderKey)
    tx_hash = w3.eth.sendRawTransaction(signed.rawTransaction)
    return tx_hash.hex()

def w3deployContractConstructor(senderAddress, senderKey, contract_interface,w3, option="ipc", gas = 90000, nonce = -1 , times = 1):
    """
    Deploys a contract
    """
    for x in range(times):
        if option=="ipc":
            w3.geth.personal.unlockAccount(w3.toChecksumAddress(senderAddress), senderKey)
        tx_hash = w3.eth.sendTransaction({ 'from': w3.toChecksumAddress(senderAddress), 'data': contract_interface['bin'], 'gas':gas})
    return tx_hash.hex()

def w3returnFunctionhashes(contractAddress, contractAbi, w3):
    """
    Return the hashes for the contract functions
    """
    hashes = {}
    arguments = []
    i = 0
    while i < len(contractAbi):
        dic = contractAbi[i]
        if dic['type'] == 'function':
            functionName = dic['name']
            inputs = dic['inputs']
            argsaux = []
            sarguments = ""
            for x in inputs:
                argsaux.append(x['type'])
                sarguments += str(x['type']) + ","
            arguments.append(argsaux)
            parametersha3 = functionName + "(" + sarguments[:-1] +")"
            parametersha3_hex = ''.join(hex(ord(c))[2:] for c in parametersha3)
            #print(parametersha3, parametersha3_hex)
            function_indicator = w3.sha3(hexstr= '0x'+parametersha3_hex).hex()[2:10]
            hashes[function_indicator] = argsaux
        i += 1
    return hashes, arguments

def w3callRawFunction(senderAddress, senderKey, contractAddress, contractAbi,functionName, arguments, nonce, w3, option="ipc", gas = 90000, gasPrice = -1, value = 0, data = "", times = 1):
    """
    Signs and makes a function call
    """
    sarguments = ""
    for x in arguments:
        sarguments += str(x) + ","
    #print(sarguments)
    parametersha3 = functionName + "(" + sarguments[:-1] +")"
    parametersha3_hex = ''.join(hex(ord(c))[2:] for c in parametersha3)
    print(parametersha3, parametersha3_hex,arguments)
    function_indicator = w3.sha3(hexstr= '0x'+parametersha3_hex).hex()[0:10]
    #info = eval(call)
    #info = contract_instance.functions.add(5,8).buildTransaction()
    #print(function_indicator)
    data = function_indicator + data
    transaction = {}
    transaction['from'] = w3.toChecksumAddress(senderAddress)
    transaction['to'] = w3.toChecksumAddress(contractAddress)
    transaction['gas'] = gas
    transaction['nonce']= nonce
    transaction['gasPrice'] = w3.eth.gasPrice
    try:
        transaction['chainId'] = int(w3.version.network)
    except AttributeError:
        transaction['chainId'] = int(w3.eth.chainId)
    transaction['data'] = data
    signed = w3.eth.account.signTransaction(transaction, senderKey)
    tx_hash = w3.eth.sendRawTransaction(signed.rawTransaction)
    return tx_hash.hex()

def w3callFunction(senderAddress, senderKey, contractAddress, contractAbi,functionName, arguments,w3, option="ipc", gas = 90000, gasPrice = -1, value = 0, data = "", nonce = -1 , times = 1):
    """
    Makes a function call
    """
    contract_instance = w3.eth.contract(abi=contractAbi, address=w3.toChecksumAddress(contractAddress))
    sarguments = ""
    #print (arguments)
    for x in arguments:
        sarguments += str(x) + ","
    #print(sarguments)
    parametersha3 = functionName + "(" + sarguments[:-1] +")"
    parametersha3_hex = ''.join(hex(ord(c))[2:] for c in parametersha3)
    #print(parametersha3, parametersha3_hex)
    function_indicator = w3.sha3(hexstr= '0x'+parametersha3_hex).hex()[0:10]
    #info = eval(call)
    #info = contract_instance.functions.add(5,8).buildTransaction()
    #print(function_indicator)
    data = function_indicator + data
    for x in range(times):
        if option == "ipc":
            w3.geth.personal.unlockAccount(senderAddress, senderKey)
        tx_hash = w3.eth.sendTransaction({'to':w3.toChecksumAddress(contractAddress), 'from': w3.toChecksumAddress(senderAddress), 'data': data, 'gas':gas})
    return tx_hash.hex()

def w3estimateGascallFunction(senderAddress, senderKey, contractAddress, contractAbi,functionName, arguments, w3, option="ipc", gas = 90000, gasPrice = -1, value = 0, data = "", nonce = -1 , times = 1):
    """
    Estimates the gas necessary to execute a function call
    """
    contract_instance = w3.eth.contract(abi=contractAbi, address=w3.toChecksumAddress(contractAddress))
    sarguments = ""
    #print (arguments)
    for x in arguments:
        sarguments += str(x) + ","
    #print(sarguments)
    parametersha3 = functionName + "(" + sarguments[:-1] +")"
    parametersha3_hex = ''.join(hex(ord(c))[2:] for c in parametersha3)
    #print(parametersha3, parametersha3_hex)
    print(parametersha3, parametersha3_hex,arguments)
    function_indicator = w3.sha3(hexstr= '0x'+parametersha3_hex).hex()[0:10]
    #info = eval(call)
    #info = contract_instance.functions.add(5,8).buildTransaction()
    #print(function_indicator)
    data = function_indicator + data
    for x in range(times):
        if option == "ipc":
            w3.geth.personal.unlockAccount(senderAddress, senderKey)
        gas = w3.eth.estimateGas({'to':w3.toChecksumAddress(contractAddress), 'from': w3.toChecksumAddress(senderAddress), 'data':data })
    return gas

def w3waitForTransactionReceipt(tx_hashes, w3, timeout=120):
    """
    Waits a certain time for transactions to be mined and returns their receipts
    """
    tx_receipts = []
    for x in tx_hashes:
        tx_receipts.append(w3.eth.waitForTransactionReceipt(x, timeout))
    return tx_receipts

def w3getTransaction(tx_hashes, w3):
    """
    Returns the transactions information
    """
    tx_receipts = []
    #print(tx_hashes)
    for x in tx_hashes:
        #print(x)
        tx_receipts.append(w3.eth.getTransaction(x))
    #print(tx_receipts)
    return tx_receipts

def w3getTransactionReceipt(tx_hashes, w3):
    """
    Returns the transactions receipts
    """
    tx_receipts = []
    for x in tx_hashes:
        tx_receipts.append(w3.eth.getTransactionReceipt(x))
    return tx_receipts

def w3simpleconnection (http_url):
    """
    Connects with an http provider and return the handler
    """
    w3 = Web3(Web3.HTTPProvider(http_url))
    return w3

def w3connect (option):
    """
    Return the handler of the connection
    """
    if option == "ipc":
        print("Introduce ipc path\n")
        Correct = False
        while not Correct:
            ipc_path = input()
            if os.path.lexists(ipc_path):
                Correct = True
            else:
                print("The file does not exist\n")
        w3 = Web3(Web3.IPCProvider(ipc_path))
    elif option == "HTTP":
        print("Introduce url\n")
        http_url = input()
        w3 = Web3(Web3.HTTPProvider(http_url))
        if 'rinkeby' in http_url:
            from web3.middleware import geth_poa_middleware
            w3.middleware_stack.inject(geth_poa_middleware, layer=0)
        if "infura" in http_url:
            option = "infura"
    else:
        w3 = Web3()
    return w3, option
#TODO: if rpc unlock doesnt work
#w3 = Web3(IPCProvider('/home/mar/Documentos/node1/geth.ipc'))
# w3 = Web3()
# contract_interface = w3getCode ("sum.sol")
# print(contract_interface['bin'])
#tx_hash = w3sendTransaction("0xa78ced1fa08f3a696e7b1f8bfbe3c532b89a7de5", "elperrodesanroque", "0x4c6f72656d20697073756d20646f6c6f72207369")
# tx_hash_hex = w3deployContract ("0xa78ced1fa08f3a696e7b1f8bfbe3c532b89a7de5", "elperrodesanroque", contract_interface, 500000,  times = 1)
#tx_hash = w3deployContractConstructor("0xa78ced1fa08f3a696e7b1f8bfbe3c532b89a7de5", "", contract_interface,option="HTTP")
# print(tx_hash)
# #contract_address = w3checkTransaction (tx_hash)
# print(tx_hash_hex)
# Contract instance in concise mode
# Getters + Setters for web3.eth.contract object
#print('Contract value: {}'.format(contract_instance.add()))
# abi = [{'name': 'add', 'inputs': [{'name': '_a', 'type': 'uint256'}, {'name': '_b', 'type': 'uint256'}], 'stateMutability': 'nonpayable', 'outputs': [{'name': '', 'type': 'uint256'}], 'constant': False, 'type': 'function', 'payable': False}]
# tx_hash = w3callFunction("0x6c57661a49941121d71abc499afde6bb66c0d427", "elperrodesanroque", '0x72e6BD470d3cE03090c9962Fb38ae1f8DA1E0Ceb',  abi, "add", ["0x4c6f72656d20697073756d20646f6c6f722073697420616d657420616d65742e",8])
