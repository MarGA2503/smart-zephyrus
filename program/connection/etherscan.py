import json
from urllib.request import urlopen
from urllib import request, parse


def retrievecontractabi (contractAddress, ethscanKey, network):
    if network != "":
        network = '-' + network
    url = "https://api" + network + ".etherscan.io/api?module=contract&action=getabi&address="+ contractAddress + "&apikey=" + ethscanKey
    with urlopen(url) as r:
        abi = json.loads(r.read().decode(r.headers.get_content_charset('utf-8')))
    return abi['result']

def retrivecontractcode (contractAddress, ethscanKey, network):
    if network != "":
        network = '-' + network
    url = "https://api" + network + ".etherscan.io/api?module=contract&action=getsourcecode&address="+ contractAddress + "&apikey=" + ethscanKey
    with urlopen(url) as r:
        abi = json.loads(r.read().decode(r.headers.get_content_charset('utf-8')))
    return abi['result']

def retrievetransactionlist (senderAddress, ethscanKey, network, startblock = 0, endblock=99999999, page = 1, offset = 50):
    if network != "":
        network = '-' + network
    url = "https://api" + network +".etherscan.io/api?module=account&action=txlist&address="+ senderAddress + "&startblock=" + str(startblock) + "&endblock=" + str(endblock) + "&page="+ str(page) + "&offset=" + str(offset)+ "&sort=asc&apikey=" + ethscanKey
    with urlopen(url) as r:
        result = json.loads(r.read().decode(r.headers.get_content_charset('utf-8')))
    tx_list = result['result']
    return tx_list

# def verifycontracts (contractlist, senderAddress, ethscanKey, network, solcVersion):
#     #Contractlist should contain {['consarf']['cAddress'][cname]['sourceCode(running or construction?if runninggetcode())']
#     if contractlist > 100:
#         print("Currently only 100 contracts per user allowed. Verify by hand\n")
#         exit(1)
#     for contract in contractlist:
#         data = {}
#         data['apikey'] = ethscanKey
#         data['']
#         data = parse.urlencode(<your data dict>).encode()
#         req =  request.Request(<your url>, data=data) # this will make the method "POST"
#         resp = request.urlopen(req)

#Ropsten contract test
contractAddress = '0x384EB0e4416179047B82092B8741805935b2fEd8'
ethscanKey = '64X6R9ZCBFYZGYU2CR9Q6QFGUMP4IQKGHV'
network = 'ropsten'
abi = retrievecontractabi (contractAddress, ethscanKey, network)
print("Rospten abi", abi , "\n")
source =retrivecontractcode (contractAddress, ethscanKey, network)
print("Source ropsten", source)
#Rinkeby contract test
network = 'rinkeby'
contractAddress = '0x677d10c27275b07f6CDB64629408Ca4Af4dEB5d4'
abi = retrievecontractabi (contractAddress, ethscanKey, network)
print("Rinkeby abi", abi,"\n")
#Mainnet contract test
network = ""
contractAddress = '0xB8c77482e45F1F44dE1745F52C74426C631bDD52'
abi = retrievecontractabi (contractAddress, ethscanKey, network)
print("Mainnet abi", abi,"\n")
