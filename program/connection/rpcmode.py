#!/usr/bin/env python
"""
Contains all functions related with connection using RPC
"""

import subprocess
import json
import ast
import os
import time

__author__ = "Mar Gimenez Aguilar"
__version__ = "1"
__maintainer__ = "Mar Gimenez Aguilar"
__email__ = "mar.gimeneza@gmail.com"

def rpcgetgasPrice(rpcdir,ID):
    """
    Returns the current gas price
    """
    app = "Content-Type: application/json"
    call = '{"jsonrpc":"2.0","method":"eth_gasPrice","params":[],"id": '+ str(ID)+'}'
    FNULL = open(os.devnull, 'wb')
    result = subprocess.run(['curl',  '-H', app, '--data', call, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
    address = json.loads(result.stdout.decode('utf-8'))
    return address['result'][2:]

def rpcgetBlockGasLimit(rpcdir,ID, block_number = ""):
    """
    Returns the gas limit for the block
    """
    app = "Content-Type: application/json"
    if block_number == "":
        call = '{"jsonrpc":"2.0","method":"eth_getBlockByNumber","params":["pending",true],"id": '+ str(ID)+'}'
    else:
        call = '{"jsonrpc":"2.0","method":"eth_getBlockByNumber","params":["'+ block_number +'",true],"id": '+ str(ID)+'}'
    FNULL = open(os.devnull, 'wb')
    result = subprocess.run(['curl',  '-H', app, '--data', call, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
    address = json.loads(result.stdout.decode('utf-8'))
    return address['result']['gasLimit'][2:]

def rpcgetCode(contractAddress, rpcdir, ID, block_number = ""):
    app = "Content-Type: application/json"
    call = '{"jsonrpc":"2.0","method":"eth_getCode","params":["'+ contractAddress +'","latest"],"id": '+ str(ID)+'}'
    FNULL = open(os.devnull, 'wb')
    result = subprocess.run(['curl',  '-H', app, '--data', call, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
    address = json.loads(result.stdout.decode('utf-8'))
    #print(address)
    return address['result']

def rpcgetBlocktransactionsbyNumber (block_number, key_to_extract, key_value, key_to_retrieve, rpcdir,ID,tocontract=False):
    """
    Returns the valid transactions in a block that match a certain criteria or key
    """
    data_to_retrieve = []
    nonces = []
    app = "Content-Type: application/json"
    call = '{"jsonrpc":"2.0","method":"eth_getBlockByNumber","params":["'+ block_number +'",true],"id": '+ str(ID)+'}'
    FNULL = open(os.devnull, 'wb')
    result = subprocess.run(['curl',  '-H', app, '--data', call, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
    block = json.loads(result.stdout.decode('utf-8'))['result']
    total_transactions = block['transactions']
    ID += 1
    for tx_hash in total_transactions:
        fullfill = True
        i = 0
        if tx_hash['to'] != None:
            if tocontract:
                if  rpcgetCode(tx_hash['to'], rpcdir, ID) == '0x':
                    fullfill = False
            else:
                if  rpcgetCode(tx_hash['to'], rpcdir, ID) != '0x':
                    fullfill = False
        while i < len(key_to_extract) and fullfill:
            if not(eval(str(eval("tx_hash[key_to_extract[i]]")) + str(eval("key_value[i]"))) or eval(str(eval("tx_hash[key_to_extract[i]].lower()")) +  eval("key_value[i].lower()"))):
                fullfill = False
            i += 1
        if fullfill:
            data_to_retrieve.append(tx_hash[key_to_retrieve])
            if type(data_to_retrieve[-1]) == str and data_to_retrieve[-1][0:2] == '0x':
                data_to_retrieve[-1] = data_to_retrieve[-1][2:]
            nonces.append(int(tx_hash['nonce'],16))
        ID += 1
    return data_to_retrieve, nonces

def rpcgetAccountNonce (senderAddress, rpcdir, ID):
    """
    Retrieve the current account nonce
    """
    tx_receipts = []
    address = ""
    app = "Content-Type: application/json"
    call = '{"jsonrpc":"2.0","method":"eth_getTransactionCount","params":["'+ senderAddress +'", "pending"],"id": '+ str(ID)+'}'
    FNULL = open(os.devnull, 'wb')
    result = subprocess.run(['curl',  '-H', app, '--data', call, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
    address = json.loads(result.stdout.decode('utf-8'))
    # timeout_start = time.time()
    # while 'result' not in address and time.time() < (timeout_start + timeout):
    #     result = subprocess.run(['curl',  '-H', app, '--data', call, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
    #     address = json.loads(result.stdout.decode('utf-8'))
    tx_receipts.append(address)
    ID += 1
    #print(tx_receipts[0])
    return tx_receipts[0]['result'][2:]

def rpcdeployContract(senderAddress, bytecode, rpcdir, ID, gas = '0x7A120',  times = 1):
    """
    Deploys a given contract into blockchain
    """
    app = "Content-Type: application/json"
    #call = '{"jsonrpc":"2.0","method": "eth_sendTransaction", "params": [{"from": "'+ senderAddress+ '", "gas": "'+ hex(gas) + '", "data": "0x'+ bytecode +'"}], "id":'+str(ID)+'}'
    FNULL = open(os.devnull, 'wb')
    call = '{"jsonrpc":"2.0","method": "eth_sendTransaction", "params": [{"from": "'+ senderAddress +'", "gas": "'+ gas +'", "data": "'+bytecode+'"}], "id": '+str(ID)+'}'
    #print(call)
    result = subprocess.run(['curl', '--data', call,  '-H', app, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
    tx_hash = json.loads(result.stdout.decode('utf-8'))
    if not 'result' in tx_hash:
        print(tx_hash)
        exit(1)
    return tx_hash['result']

def rpcestimateGasdeployContract(senderAddress, bytecode, rpcdir, ID, gas = '0x7A120',  times = 1):
    """
    Estimates the necesary gas to deploy a given contract into the blockchain
    """
    app = "Content-Type: application/json"
    #call = '{"jsonrpc":"2.0","method": "eth_sendTransaction", "params": [{"from": "'+ senderAddress+ '", "gas": "'+ hex(gas) + '", "data": "0x'+ bytecode +'"}], "id":'+str(ID)+'}'
    FNULL = open(os.devnull, 'wb')
    call = '{"jsonrpc":"2.0","method": "eth_estimateGas", "params": [{"from": "'+ senderAddress +'",  "data": "'+bytecode+'"}], "id": '+str(ID)+'}'
    #print(call)
    result = subprocess.run(['curl', '--data', call,  '-H', app, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
    tx_hash = json.loads(result.stdout.decode('utf-8'))
    if not 'result' in tx_hash:
        print(tx_hash)
        exit(1)
    return tx_hash['result']

def rpccheckContractDeployment (tx_hash, rpcdir, ID):
    """
    Check if a contract is deployed and return it address
    """
    address = ""
    app = "Content-Type: application/json"
    call = '{"jsonrpc":"2.0","method":"eth_getTransactionReceipt","params":["'+ tx_hash +'"],"id": '+ str(ID)+'}'
    FNULL = open(os.devnull, 'wb')
    result = subprocess.run(['curl',  '-H', app, '--data', call, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
    address = json.loads(result.stdout.decode('utf-8'))
    while 'result' not in address:
        result = subprocess.run(['curl',  '-H', app, '--data', call, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
        address = json.loads(result.stdout.decode('utf-8'))
    address2= address['result']
    return address2['contractAddress']

def rpcestimateGasSendTransaction(senderAddress, receiverAddress,rpcdir,ID, gas = '0x7A120', gasPrice = -1, value = 0, data = "", nonce = -1 , times = 1):
    """
    Estimates the necessary gas to send a transaction
    """
    app = "Content-Type: application/json"
    call = '{"jsonrpc":"2.0","method": "eth_estimateGas", "params": [{"from": "'+ senderAddress+ '","to": "'+ receiverAddress +'",  "data": "'+ data +'"}], "id":'+str(ID)+'}'
    FNULL = open(os.devnull, 'wb')
    for x in range(times):
        result = subprocess.run(['curl',  '-H', app, '--data', call, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
        #print(result)
        tx_hash = json.loads(result.stdout.decode('utf-8'))
        #print(tx_hash)
    if not 'result' in tx_hash:
        print(tx_hash)
        exit(1)
    return tx_hash['result']

def rpcsendTransaction(senderAddress, receiverAddress,rpcdir,ID, gas = '0x7A120', gasPrice = -1, value = -1, data = "", nonce = -1 , times = 1):
    """
    Sends a transaction
    """
    app = "Content-Type: application/json"
    if gasPrice != -1:
        call = '{"jsonrpc":"2.0","method": "eth_sendTransaction", "params": [{"from": "'+ senderAddress+ '","to": "'+ receiverAddress +'", "gas": "'+ gas + '", "gasPrice": "'+ gasPrice + '","data": "'+ data +'"}], "id":'+str(ID)+'}'
    elif value != -1:
        call = '{"jsonrpc":"2.0","method": "eth_sendTransaction", "params": [{"from": "'+ senderAddress+ '","to": "'+ receiverAddress +'", "gas": "'+ gas + '", "value": "'+ value + '","data": "'+ data +'"}], "id":'+str(ID)+'}'
    else:
        call = '{"jsonrpc":"2.0","method": "eth_sendTransaction", "params": [{"from": "'+ senderAddress+ '","to": "'+ receiverAddress +'", "gas": "'+ gas + '", "data": "'+ data +'"}], "id":'+str(ID)+'}'
    FNULL = open(os.devnull, 'wb')
    for x in range(times):
        result = subprocess.run(['curl',  '-H', app, '--data', call, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
        tx_hash = json.loads(result.stdout.decode('utf-8'))
    if not 'result' in tx_hash:
        print(tx_hash)
        exit(1)
    return tx_hash['result']

def rpccheckTransaction (tx_hash, rpcdir, ID):
    """
    Check if a given trasnaction is mined
    """
    address = ""
    app = "Content-Type: application/json"
    call = '{"jsonrpc":"2.0","method":"eth_getTransactionReceipt","params":["'+ tx_hash +'"],"id": '+ str(ID)+'}'
    FNULL = open(os.devnull, 'wb')
    result = subprocess.run(['curl',  '-H', app, '--data', call, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
    address = json.loads(result.stdout.decode('utf-8'))
    while 'result' not in address:
        result = subprocess.run(['curl',  '-H', app, '--data', call, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
        address = json.loads(result.stdout.decode('utf-8'))
    return True

def rpcgetTransaction (tx_hashes, rpcdir, ID):
    """
    Retrieves a transaction from the blockchain
    """
    tx_receipts = []
    for tx_hash in tx_hashes:
        address = ""
        app = "Content-Type: application/json"
        call = '{"jsonrpc":"2.0","method":"eth_getTransactionByHash","params":["'+ tx_hash +'"],"id": '+ str(ID)+'}'
        FNULL = open(os.devnull, 'wb')
        result = subprocess.run(['curl',  '-H', app, '--data', call, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
        address = json.loads(result.stdout.decode('utf-8'))
        # timeout_start = time.time()
        # while 'result' not in address and time.time() < (timeout_start + timeout):
        #     result = subprocess.run(['curl',  '-H', app, '--data', call, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
        #     address = json.loads(result.stdout.decode('utf-8'))
        tx_receipts.append(address)
        ID += 1
        #print(tx_receipts[0]['result'])
    return tx_receipts

def rpcgetTransactionReceipt (tx_hashes, rpcdir, ID):
    """
    Retrieves the transaction receipt from the blockchain
    """
    tx_receipts = []
    for tx_hash in tx_hashes:
        address = ""
        app = "Content-Type: application/json"
        call = '{"jsonrpc":"2.0","method":"eth_getTransactionReceipt","params":["'+ tx_hash +'"],"id": '+ str(ID)+'}'
        FNULL = open(os.devnull, 'wb')
        result = subprocess.run(['curl',  '-H', app, '--data', call, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
        address = json.loads(result.stdout.decode('utf-8'))
        # timeout_start = time.time()
        # while 'result' not in address and time.time() < (timeout_start + timeout):
        #     result = subprocess.run(['curl',  '-H', app, '--data', call, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
        #     address = json.loads(result.stdout.decode('utf-8'))
        tx_receipts.append(address)
        ID += 1
    return tx_receipts['result']

def rpcwaitTransaction (tx_hashes, rpcdir, ID, timeout=120):
    """
    Waits for transactions to be mined a certain time and returns it receipts if
    they are
    """
    tx_receipts = []
    for tx_hash in tx_hashes:
        address = ""
        app = "Content-Type: application/json"
        call = '{"jsonrpc":"2.0","method":"eth_getTransactionReceipt","params":["'+ tx_hash +'"],"id": '+ str(ID)+'}'
        FNULL = open(os.devnull, 'wb')
        result = subprocess.run(['curl',  '-H', app, '--data', call, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
        address = json.loads(result.stdout.decode('utf-8'))
        timeout_start = time.perf_counter()
        while 'result' not in address and (time.perf_counter() - timeout_start) <= (timeout):
            result = subprocess.run(['curl',  '-H', app, '--data', call, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
            address = json.loads(result.stdout.decode('utf-8'))
            #print(result)
            # print((time.perf_counter() - timeout_start))
        tx_receipts.append(address)
        ID += 1
    return tx_receipts

#TODO:check if contract correctly deployed
def rpcdeployContractConstructor(senderAddress, bytecode, rpcdir, ID, gas = 500000, nonce = -1 , times = 1):
    """
    Deploys a contract
    """
    app = "Content-Type: application/json"
    call = '{"jsonrpc":"2.0","method": "eth_sendTransaction", "params": [{"from": "'+ senderAddress +'", "gas": "'+ hex(gas) +'", "data": "'+bytecode+'"}], "id": '+str(ID)+'}'
    #print(call)
    #FNULL = open(os.devnull, 'wb')
    for x in range(times):
        result = subprocess.run(['curl', '--data', call,  '-H', app, rpcdir ], stdout=subprocess.PIPE)
        tx_hash = json.loads(result.stdout.decode('utf-8'))
    return tx_hash['result']

def rpcreturnFunctionhashes(contractAddress, contractAbi, rpcdir, ID):
    """
    Returns the hashes for the function in the contract
    """
    app = "Content-Type: application/json"
    hashes = {}
    arguments = []
    i = 0
    while i < len(contractAbi):
        dic = contractAbi[i]
        if dic['type'] == 'function':
            functionName = dic['name']
            inputs = dic['inputs']
            argsaux = []
            sarguments = ""
            for x in inputs:
                argsaux.append(x['type'])
                sarguments += str(x['type']) + ","
            arguments.append(argsaux)
            parametersha3 = functionName + "(" + sarguments[:-1] +")"
            parametersha3_hex = ''.join(hex(ord(c))[2:] for c in parametersha3)
            #print(parametersha3, parametersha3_hex)
            call = '{"jsonrpc":"2.0","method":"web3_sha3","params":["0x'+ parametersha3_hex +'"],"id": ' + str(ID) +'}'
            FNULL = open(os.devnull, 'wb')
            result = subprocess.run(['curl',  '-H', app, '--data', call, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
            function_indicator = json.loads(result.stdout.decode('utf-8'))['result'][2:10]
            hashes[function_indicator] = argsaux
        i += 1
    return hashes, arguments

def estimateGasrpccallFunction(senderAddress, contractAddress, contractAbi,functionName, arguments,rpcdir, ID, gas = 500000, gasPrice = -1, value = 0, data = "", nonce = -1 , times = 1):
    """
    Estimates the gas necessary to make a function call
    """
    app = "Content-Type: application/json"
    #First get format for sha3
    #Asume that when formatting message we have already padded it and its en hex stringform
    # Find = False
    # i = 0
    # while not Find and i < len(contractAbi):
    #     dic = contractAbi[i]
    #     print(dic['inputs'])
    #     if functionName in dic.values():
    #         dic2 = dic['inputs']
    #         print(dic2)
    #         Find = True
    #     i += 1
    # parametersha3 = functionName + "("
    # for x in dic2:
    #     parametersha3 += x['type'] + ","
    # parametersha3 = parametersha3[:-1] + ")"
    sarguments = ""
    for x in arguments:
        sarguments += str(x) + ","
    #print(sarguments)
    parametersha3 = functionName + "(" + sarguments[:-1] +")"
    parametersha3_hex = ''.join(hex(ord(c))[2:] for c in parametersha3)
    #print(parametersha3)
    call = '{"jsonrpc":"2.0","method":"web3_sha3","params":["0x'+ parametersha3_hex +'"],"id": ' + str(ID) +'}'
    FNULL = open(os.devnull, 'wb')
    result = subprocess.run(['curl',  '-H', app, '--data', call, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
    function_indicator = json.loads(result.stdout.decode('utf-8'))['result'][0:10]
    #function_indicator = "0xc6888fa1"
    # data = function_indicator
    # for x in arguments:
    #     data += x
    data = function_indicator + data
    call = '{"jsonrpc":"2.0","method": "eth_estimateGas", "params": [{"from": "'+ senderAddress +'", "to": "'+ contractAddress + '", "data": "'+ data+'"}], "id": '+str(ID)+'}'
    #print(call)
    result = subprocess.run(['curl', '--data', call,  '-H', app, rpcdir ], stdout=subprocess.PIPE,stderr=FNULL)
    tx_hash = json.loads(result.stdout.decode('utf-8'))
    #print(result)
    return tx_hash['result']

def rpccallFunction(senderAddress, contractAddress, contractAbi,functionName, arguments,rpcdir, ID, gas = 500000, gasPrice = -1, value = 0, data = "", nonce = -1 , times = 1):
    """
    Calls a function with a given arguments
    """
    app = "Content-Type: application/json"
    #First get format for sha3
    #Asume that when formatting message we have already padded it and its en hex stringform
    # Find = False
    # i = 0
    # while not Find and i < len(contractAbi):
    #     dic = contractAbi[i]
    #     print(dic['inputs'])
    #     if functionName in dic.values():
    #         dic2 = dic['inputs']
    #         print(dic2)
    #         Find = True
    #     i += 1
    # parametersha3 = functionName + "("
    # for x in dic2:
    #     parametersha3 += x['type'] + ","
    # parametersha3 = parametersha3[:-1] + ")"
    sarguments = ""
    for x in arguments:
        sarguments += str(x) + ","
    #print(sarguments)
    parametersha3 = functionName + "(" + sarguments[:-1] +")"
    parametersha3_hex = ''.join(hex(ord(c))[2:] for c in parametersha3)
    #print(parametersha3)
    call = '{"jsonrpc":"2.0","method":"web3_sha3","params":["0x'+ parametersha3_hex +'"],"id": ' + str(ID) +'}'
    FNULL = open(os.devnull, 'wb')
    result = subprocess.run(['curl',  '-H', app, '--data', call, rpcdir ], stdout=subprocess.PIPE, stderr=FNULL)
    function_indicator = json.loads(result.stdout.decode('utf-8'))['result'][0:10]
    #function_indicator = "0xc6888fa1"
    # data = function_indicator
    # for x in arguments:
    #     data += x
    data = function_indicator + data
    call = '{"jsonrpc":"2.0","method": "eth_sendTransaction", "params": [{"from": "'+ senderAddress +'", "to": "'+ contractAddress + '", "gas": "'+ gas +'", "data": "'+ data+'"}], "id": '+str(ID)+'}'
    #print(call)
    result = subprocess.run(['curl', '--data', call,  '-H', app, rpcdir ], stdout=subprocess.PIPE,stderr=FNULL)
    tx_hash = json.loads(result.stdout.decode('utf-8'))
    #print(result)
    return tx_hash['result']

#TODO:check deployment with compiled via rpc and local
#TODO: check for gas in deployment (failure)
# bytecode = "0x6060604052341561000f57600080fd5b6040516040806101ec8339810160405280805190602001909190805190602001909190505081600081905550806001819055505050610199806100536000396000f30060606040526004361061004c576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff1680632f2f48591461005157806335e6a7af146100df575b600080fd5b341561005c57600080fd5b610064610108565b6040518080602001828103825283818151815260200191508051906020019080838360005b838110156100a4578082015181840152602081019050610089565b50505050905090810190601f1680156100d15780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b34156100ea57600080fd5b6100f261014b565b6040518082815260200191505060405180910390f35b610110610159565b6040805190810160405280600c81526020017f68656c6c6f20776f726c64210000000000000000000000000000000000000000815250905090565b600060015460005401905090565b6020604051908101604052806000815250905600a165627a7a723058200224c831d88d10cc98ce07feffd9fcf3b08170bf1206d3919870dc45c2e2504700294c6f72656d20697073756d20646f6c6f722073697420616d657420616d65742e067a80735636cd3f48eea4cbe3d4d9239e63006aa70e0830ac763fd429d00106"
# senderAddress = ""
# gas = 115486
# string = '[{"constant":false,"inputs":[{"name":"_a","type":"uint256"},{"name":"_b","type":"uint256"}],"name":"add","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"nonpayable","type":"function"}]'
# string2 = string.replace("false", "False")
# contractAbi = ast.literal_eval(string2)
# w3getCode ("./sum.sol",'localhost:8545', 1)
#tx_hash = rpcdeployContract("0xa78ced1fa08f3a696e7b1f8bfbe3c532b89a7de5", bytecode, 'localhost:8545', 1, gas = gas)
#tx_hash = rpccallFunction("0xa78ced1fa08f3a696e7b1f8bfbe3c532b89a7de5", "0x84f7bbdfddd171a43354c70a27fafb226d01d46b", contractAbi,"add", ["0000000000000000000000000000000000000000000000000000000000000006"],'localhost:8545', 1, gas = gas)
#tx_hash = rpcdeployContractConstructor("0xa78ced1fa08f3a696e7b1f8bfbe3c532b89a7de5", bytecode, 'localhost:8545', 1)
#tx_hash = rpcestimateGasSendTransaction("0xa78ced1fa08f3a696e7b1f8bfbe3c532b89a7de5","0x420db16d7acfc126d5a26de96c2f93bf18c049fa" ,'localhost:8545',1, data = "0x4c6f72656d20697073756d20646f6c6f722073697420616d657420616d65742e")
# call = '{"jsonrpc":"2.0","method": "eth_sendTransaction", "params": [{"from": "' + senderAddress +'", "gas": "'+ hex(gas)+'", "data": "0x'+bytecode+'"}], "id": ' + str(1) + '}'
# app = "Content-Type: application/json"
# rpcdir = 'localhost:8545'
#rpccallFunction("0xa78ced1fa08f3a696e7b1f8bfbe3c532b89a7de5","0x4764098ad188c015ec8f5740c476f6bb914dd5a6",contractAbi, "add", [1,2], 'localhost:8545', 1, gas = gas)
# c = subprocess.check_output(['curl', '-H' , app ,  '--data',  call, rpcdir])
#address = rpccheckContractDeployment (tx_hash,'localhost:8545', 2)
#rpccheckTransaction (tx_hash,'localhost:8545', 2)

#print (tx_hash)
