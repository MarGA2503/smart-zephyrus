import requests
import os
from urllib.request import urlopen
from urllib import request, parse
import json

chain ={
1 : "",
3 :	"-ropsten",
4 :	"-rinkeby",
5 : "-goerli",
42 : "-kovan",
11155111: "-sepolia"
}

evm={
15: "v0.8.15+commit.e14f2714",
14: "v0.8.14+commit.80d49f37",
13: "v0.8.13+commit.abaa5c0e",
12: "v0.8.12+commit.f00d7308",
11: "v0.8.11+commit.d7f03943",
10: "v0.8.10+commit.fc410830",
9: "v0.8.9+commit.e5eed63a",
8: "v0.8.8+commit.dddeac2f",
7: "v0.8.7+commit.e28d00a7",
6: "v0.8.6+commit.11564f7e",
5: "v0.8.5+commit.a4f2e591",
4: "v0.8.4+commit.c7e474f2",
3: "v0.8.3+commit.8d00100c",
2: "v0.8.2+commit.661d1103",
1: "v0.8.1+commit.df193b15",
0: "v0.8.0+commit.c7dfd78e"
}

def verify (url,contract_file, contract_address,api_key, evm_version,contract_name):
    headers = {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}
    f = open('./program/resources/verify.json' )
    data = f.read()
    data_json = eval(data)
    ff = open(contract_file )
    d = ff.read()
    data_json['sourceCode'] = d
    data_json['apikey'] = api_key
    data_json['contractaddress'] = contract_address
    data_json['contractname'] = contract_name
    data_json['compilerversion'] = evm_version
    #print(data_json)
    x = requests.post(url, data=data_json,headers=headers)
    guid = eval(x.text)
    guid = guid['result']
    while "Unable to locate ContractCode" in guid:
        x = requests.post(url, data=data_json,headers=headers)
        guid = eval(x.text)
        guid = guid['result']
    print(x.text,guid)
    return guid


def checkstatus (url, api_key, guid):
    headers = {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}
    data= {}
    data["apikey"]= api_key
    data["guid"]= guid
    data["module"]= "contract"
    data["action"]= "checkverifystatus"
    x = requests.get(url, data=data,headers=headers)
    print(x)
    result = eval(x.text)
    result = result['result']
    print(result)


def retrivecontractcode (contractAddress, ethscanKey, chainId):
    url = "https://api"  +chain[chainId] + ".etherscan.io/api?module=contract&action=getsourcecode&address="+ contractAddress + "&apikey=" + ethscanKey
    with urlopen(url) as r:
        abi = json.loads(r.read().decode(r.headers.get_content_charset('utf-8')))
    return abi['result']



def verifycontracts (chainId, pragmas, contracts_names, contract_addresses,api_keys):
    url = "https://api" +chain[chainId] + ".etherscan.io/api"
    z = 0
    for i in range(len(pragmas)):
        evm_version = evm[pragmas[i]]
        contract_name = contracts_names[i]
        contract_file =  contract_name + ".sol"
        contract_address = contract_addresses[i]
        api_key = api_keys[z]
        #print(url,evm_version,contract_name,contract_file,api_key)
        guid = verify (url,contract_file, contract_address,api_key, evm_version,contract_name)
        checkstatus (url, api_key, guid)


url = "https://api-rinkeby.etherscan.io/api"
contract_name = "RomanticisedMonocrats"
contract_address = "0x2F7D94337021e086FA86e8BCAF41A225Df18463C"
evm_version = "v0.8.6+commit.11564f7e"
contract_file = "/home/mar/Documentos/Third paper/RomanticisedMonocrats.sol"
#guid = verify (url,contract_file, contract_address,api_key, evm_version,contract_name)
#checkstatus (url, api_key, "jnn1rbkrhxs1xw6g2jpq51mrehs4v9qnyercyrtpgaaxmzhday")
