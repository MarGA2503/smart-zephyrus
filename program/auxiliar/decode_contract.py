import os
from os.path import isfile, join
import json
import os
import copy
import math
from natsort import natsorted
import itertools
from itertools import combinations
import random
from collections import OrderedDict



def readlines (file_name):
    f = open(file_name, "r")
    lines = ""
    lines += f.read()
    f.close()
    lines = lines.splitlines()
    return lines

def readlines_file (file):
    with open(file, encoding="utf-8") as f:
        lines=f.readlines()
    return lines

def retrievefolder (folder_name):
    sub_folders = os.listdir(folder_name)
    return natsorted(sub_folders)

def formatname(name):
    if '-' in name:
        name = name.split ('-')[1]
    if "." in name:
        name = name.split ('.')[0]
    return name

def examinecontract (lines):
    functions = []
    events = []
    modifiers = []
    types = []
    number_of_brackets = 0
    lines_final = []
    lines_def = []
    block_lines = []
    contracts_names = []
    contracts_line = {}
    function_indicator = False
    functiontmp=""
    modifier_indicator = False
    struct_indicator = False
    enum_indicator = False
    constructor_indicator = False
    modifiertmp = ""
    eventtmp = ""
    event_indicator = False
    block = False
    contracts_dic = {}
    contracts_len = {}
    has_herency_contract = {}
    herency_dic_contract = {}
    herency_to_contract = {}
    herency_len_contract = {}
    library_dic = {}
    interface_dic={}
    herency_relations_contract = {}
    is_library = False
    is_contract = False
    is_interface = False
    has_constructor = False
    has_herency = False
    is_start_block = False
    herencynames = []
    contractlentmp = 0
    librarylentmp = 0
    interfacelentmp = 0
    comment_block = False
    name = ""
    pragma_version = ""
    for i in range(len(lines)):
        #print("sig",lines[i])
        #print("fun",functions,function_indicator)
        i_split = lines[i].split()
        is_start_block = False
        #print(i_split)
        """ if len(i_split)>0:
            print(i_split[0]) """
        if "pragma solidity " in lines[i]:
            functions = []
            events = []
            modifiers = []
            types = []
            number_of_brackets = 0
            lines_final = []
            lines_def = []
            block_lines = []
            function_indicator = False
            functiontmp=""
            modifier_indicator = False
            struct_indicator = False
            enum_indicator = False
            constructor_indicator = False
            modifiertmp = ""
            eventtmp = ""
            event_indicator = False
            comment_block = False
            pragma_version = lines[i].split('0.8.')[-1][:-1]
            #print("New contract", number_of_brackets)
        if "*/" in lines[i]:
            comment_block= False
            #print("end comment")
        if not comment_block:
            if len(i_split)>0 and  ("/*" in lines[i] or "*/" in lines[i] or  i_split[0] == "\**" or i_split[0] == "/**" or i_split[0] == "*" or i_split[0] == "*/" or i_split[0] == "//" or i_split[0] == "///"):
                if  "/*" in lines[i]:
                    comment_block = True
                    #print("comment block")
                if "*/" in lines[i]:
                    comment_block= False
                    #print("end comment")
                continue
            elif lines[i].strip()[0:2] == "//" or "import " in lines[i]:
                continue
            elif len(i_split)>0 and i_split[0] == "library":
                is_library = True
                if i_split[1] in library_dic:
                    cant = library_dic[i_split[1]]
                    library_dic[i_split[1]] = cant +1
                else:
                    library_dic[i_split[1]] = 1
                name = i_split[1]
                is_start_block = True
                #print(library_dic)
            elif len(i_split)>0 and i_split[0] == "interface":
                is_interface = True
                if i_split[1] in interface_dic:
                    cant = interface_dic[i_split[1]]
                    interface_dic[i_split[1]] = cant +1
                else:
                    interface_dic[i_split[1]] = 1
                name = i_split[1]
                is_start_block = True
            elif len(i_split)>0 and i_split[0] == "contract":
                is_contract = True
                if i_split[1] in contracts_dic:
                    cant = contracts_dic[i_split[1]]
                    contracts_dic[i_split[1]] = cant +1
                else:
                    contracts_dic[i_split[1]] = 1
                name = i_split[1]
                #print(name)
                contracts_names.append(name)
                contracts_line[name] = i
                is_start_block = True
            elif len(i_split)>1 and i_split[1] == "contract":
                is_contract = True
                if i_split[2] in contracts_dic:
                    cant = contracts_dic[i_split[2]]
                    contracts_dic[i_split[2]] = cant +1
                else:
                    contracts_dic[i_split[2]] = 1
                name = i_split[2]
                #print(name)
                contracts_names.append(name)
                contracts_line[name] = i
                is_start_block = True
            if " is " in lines[i] and is_start_block:
                has_herency = True
                index_is = i_split.index("is")
                rest = i_split[(index_is +1):]
                for x in rest:
                    if "{" == x:
                        continue
                    elif "," in x:
                        element = x[:-1]
                        herencynames.append(element)
                    else:
                        herencynames.append(x)
                if is_contract:
                    if name in herency_relations_contract.keys():
                        ltmp = []
                        for x in herencynames:
                            ltmp.append(x)
                        herency_relations_contract[name] = ltmp
                    else:
                        ltmp = []
                        for x in herencynames:
                            ltmp.append(x)
                        herency_relations_contract[name] = ltmp
                    hn = []
                    #print('%%%%%%',contracts_names)
                    for x in contracts_names:
                        #print('$$$', x, hn)
                        if x in herencynames:
                            hn.append(x)
                    if name in herency_to_contract.keys():
                        herency_to_contract[name] = [herencynames,hn]
                    else:
                        herency_to_contract[name] = [herencynames,hn]

                if is_interface:
                    continue
                if is_library:
                    continue
                #print(herency_relations_contract)
            if len(i_split)>0 and  (i_split[0] == "\**" or i_split[0] == "*" or i_split[0] == "*/" or i_split[0] == "//" or i_split[0] == "///" or "//" in i_split):
                continue
            elif lines[i].strip()[0:2] == "//" or "import" in lines[i]:
                continue
            elif "{" in lines[i] and "function" not in lines[i] and not function_indicator and "modifier" not in lines[i] and not modifier_indicator and not "struct" in lines[i] and not struct_indicator:
                number_of_brackets += 1
                block = True
                lines_final.append(lines[i])
                #lines_def.append([lines[i]])
                #print("{", lines_final,lines_def, number_of_brackets)
            # if block == False:
            #     lines_final.append(lines[i])
            #     lines_def.append(lines[i])
            #     print("block", lines_final,lines_def)
            elif modifier_indicator and "}" not in lines[i]:
                modifiertmp += lines[i]
                modifiertmp += "\n"
                if "{" in lines[i]:
                    number_of_brackets += 1
            elif function_indicator and "{" in lines[i]:
                number_of_brackets += 1
            elif constructor_indicator and "{" in lines[i]:
                number_of_brackets += 1
            elif modifier_indicator and "{" in lines[i]:
                number_of_brackets += 1
            elif function_indicator and "}" not in lines[i]:
                functiontmp +=lines[i]
                functiontmp += "\n"
                if "{" in lines[i]:
                    number_of_brackets += 1
            elif (len(i_split)>0) and i_split[0] != "interface" and (i_split[0] == "bool" or i_split[0][0:3] == "int" or i_split[0][0:4] == "uint" or i_split[0][0:5] == "fixed" or  i_split[0][0:6] == "ufixed" or i_split[0][0:7] == "address" or i_split[0][0:5] == "bytes" or i_split[0][0:6] == "string"):
                types.append(lines[0])
                key = i_split[0]
                if struct_indicator:
                    key= "struct" + key
                if "=" in lines[i]:
                    if "=" in i_split:
                        j = i_split.index("=")
                        key2 = key + i_split[j-1]
                    else:
                        i_split2 = lines[i].split("=")[0]
                        i_split2 = i_split2.split()
                        key2 = key + i_split2[-1]
                else:
                    key2 = key + i_split[-1]
            elif "modifier" in lines[i] and ((len(lines[i].strip().split()) > 0 and lines[i].strip().split()[0] == "modifier" ) or  (len(lines[i].strip().split("(")) > 0 and lines[i].strip().split("(")[0] == "modifier")):
                if "{" in lines[i]:
                    modifiertmp += lines[i]
                    modifiertmp += "\n"
                    modifier_indicator = True
                    number_of_brackets += 1
                    #print("modifier{",modifiertmp,number_of_brackets)
                elif (len(i_split)==1):
                    modifiertmp += lines[i]
                    modifiertmp += "\n"
                    modifier_indicator = True
                else:
                    modifiers.append(lines[i])
                    lines_final.append(lines[i])
                    #print("modifier", modifiers)
            elif "mapping" in lines[i] and ((len(lines[i].strip().split()) > 0 and lines[i].strip().split()[0] == "mapping" ) or  (len(lines[i].strip().split("(")) > 0 and lines[i].strip().split("(")[0] == "mapping")) :
                count = lines[i].count("mapping")
                key = "mapping" + str(count)
                types.append(lines[i])
                if struct_indicator:
                    key= "struct" + key
                #get types of Mapping
                index_i = lines[i].find("(")
                index_e = lines[i].rfind(")")
                key = key + lines[i][index_i:(index_e+1)]
                key2 = key + i_split[-1]
            elif "struct " in lines[i] and "constructor" not in lines[i] and ((len(lines[i].strip().split()) > 0 and lines[i].strip().split()[0] == "struct" ) or  (len(lines[i].strip().split("(")) > 0 and lines[i].strip().split("(")[0] == "struct")):
                if "{" in lines[i]:
                    struct_indicator = True
                    number_of_brackets += 1
                    #print("struct{",number_of_brackets)
                types.append(lines[i])
                key2 = "struct" + i_split[1]
            elif "constructor" in lines[i] and ((len(lines[i].strip().split()) > 0 and lines[i].strip().split()[0] == "constructor" ) or  (len(lines[i].strip().split("(")) > 0 and lines[i].strip().split("(")[0] == "constructor")):
                if "//" in lines[i]:
                    if lines[i].index("//") > lines[i].index("constructor"):
                        has_constructor = True
                        if "{" in lines[i]:
                            constructor_indicator = True
                            number_of_brackets += 1
                            #print("constructor_indicator{",number_of_brackets)
                        elif ";" in lines[i]:
                            has_constructor = True
                        else:
                            constructor_indicator = True
                            #print("constructor_indicator{",number_of_brackets)
                else:
                    has_constructor = True
                    if "{" in lines[i]:
                        constructor_indicator = True
                        number_of_brackets += 1
                        #print("constructor_indicator{",number_of_brackets)
                    elif ";" in lines[i]:
                        has_constructor = True
                    else:
                        constructor_indicator = True
                        #print("constructor_indicator{",number_of_brackets)
            elif "enum" in lines[i] and ((len(lines[i].strip().split()) > 0 and lines[i].strip().split()[0] == "enum" ) or  (len(lines[i].strip().split("(")) > 0 and lines[i].strip().split("(")[0] == "enum")):
                if "{" in lines[i]:
                    enum_indicator = True
                    number_of_brackets += 1
                    #print("enum{",number_of_brackets)
                types.append(lines[i])
                key2 = "enum" + i_split[1]
            elif len(lines[i].strip().split()) > 0 and lines[i].strip().split()[0] == "function":
                if "{" in lines[i]:
                    functiontmp += lines[i]
                    functiontmp += "\n"
                    function_indicator = True
                    number_of_brackets += 1
                    #print("function{", functiontmp,number_of_brackets)
                elif ";" in lines[i]:
                    functions.append(lines[i])
                    lines_final.append(lines[i])
                    #print("function", functions)
                else:
                    function_indicator = True
                    functiontmp += lines[i]
                    functiontmp += "\n"
                    #print("fun",functions)
            elif len(lines[i].strip().split()) > 0 and lines[i].strip().split()[0] == "event" :
                events.append(lines[i])
                lines_final.append(lines[i])
                #print("event",events, lines_final)
            elif "{" in lines[i]:
                number_of_brackets += 1
                #print("num", number_of_brackets)
            if "}" in lines[i]:
                #print("numero", number_of_brackets)
                if struct_indicator and number_of_brackets > 1:
                    number_of_brackets -= 1
                    if number_of_brackets == 1:
                        struct_indicator =False
                elif enum_indicator and number_of_brackets > 1:
                    number_of_brackets -= 1
                    if number_of_brackets == 1:
                        enum_indicator =False
                elif constructor_indicator and number_of_brackets > 1:
                    number_of_brackets -= 1
                    if number_of_brackets == 1:
                        constructor_indicator =False
                elif function_indicator and number_of_brackets > 1:
                    number_of_brackets -= 1
                    #print(number_of_brackets)
                    if number_of_brackets == 1:
                        functiontmp += lines[i]
                        functiontmp += "\n"
                        functions.append(functiontmp)
                        lines_final.append(functiontmp)
                        functiontmp = ""
                    else:
                        functiontmp += lines[i]
                    if number_of_brackets == 1:
                        function_indicator = False
                elif modifier_indicator and number_of_brackets > 1:
                    number_of_brackets -= 1
                    if number_of_brackets == 1:
                        modifiertmp += lines[i]
                        modifiertmp += "\n"
                        modifiers.append(modifiertmp)
                        lines_final.append(modifiertmp)
                        modifiertmp = ""
                    else:
                        modifiertmp += lines[i]
                    if number_of_brackets ==1:
                        modifier_indicator = False
                else:
                    #ending block
                    #modify with perm later
                    j= 0
                    z = 0
                    k = 0
                    if is_contract:
                        for x in herencynames:
                            if x in herency_dic_contract:
                                cant = herency_dic_contract[x]
                                herency_dic_contract[x] = cant +1
                            else:
                                herency_dic_contract[x] = 1
                        herencynames_len = len(herencynames)
                        contractlentmp += 1
                        is_contract = False
                        if has_herency:
                            herencykey = "Yes"
                            has_herency = False
                        else:
                            herencykey = "No"
                        if herencykey in has_herency_contract:
                            cant = has_herency_contract[herencykey]
                            has_herency_contract[herencykey] = cant +1
                        else:
                            has_herency_contract[herencykey] = 1
                    if is_library:
                        librarylentmp += 1
                        is_library = False
                    if is_interface:
                        interfacelentmp += 1
                        is_interface = False
                    #print("******************************************************")
                    #print(contracts_dic,contracts_names,contracts_line,herency_relations_contract)
                    #perm = permutations(functions)
                    block = False
                    functions = []
                    functiontmp =""
                    function_indicator = False
                    events=[]
                    modifiers = []
                    number_of_brackets -= 1
                    lines_final = []
                    herencynames = []
            # if len(functions)>0 and functions[0]== '    }\n':
            #     print(lines[i])
            #     return 0
    return contracts_names,contracts_line,pragma_version,herency_relations_contract, herency_to_contract

def decodemessage (contracts_names,contracts_line,pragma_version,herency_relations_contract,herency_to_contract,lines):
    #print(contracts_names,contracts_line,pragma_version)
    message = ""
    message += bin(int(pragma_version))[2:].zfill(4)
    #print(message)
    folder_stealthy = './program/resources/stealhy'
    dictionary  =  './program/resources/dictionarycleaned.txt'
    contract_list = retrievefolder(folder_stealthy)
    contract_list = [formatname(x) for x in contract_list ]
    #print(contract_list)
    for i in contracts_names:
        if i in contract_list:
            index_chosen = contract_list.index(i)
            #print(i,index_chosen)
            del contract_list[index_chosen]
            message += bin(int(index_chosen))[2:].zfill(3)
            #print(message)
    # herency = herency_relations_contract[contracts_names[-1]]
    # print("***",herency)
    # #herency in order
    # herency_sorted = sorted(herency)
    herency_sorted = herency_to_contract[contracts_names[-1]][1]
    herency = herency_to_contract[contracts_names[-1]][0]
    #print("***",herency, herency_sorted)
    z = list(itertools.permutations(herency_sorted))
    length = int(math.log2(len(z)))
    for i in range(len(z)):
        if herency == list(z[i]) and length > 0:
            #print(i,herency,z[i])
            message+= bin(int(i))[2:].zfill(length)
            #print('+++++++++',int(i))
            break
    index_line = contracts_line[contracts_names[-1]]
    while index_line < len(lines):
        line = lines[index_line]
        if "constructor()" in line:
            tmp1 = line.split("ERC20(")[1]
            tmp1 = tmp1.split(")")[0]
            splitted = tmp1.split(",")
            token_name = eval(splitted[0])
            token_symbol = eval(splitted[1])
            #print(token_name,token_symbol)
            index_line = len(lines)
        index_line +=1
    first =token_name.split(" ")[0].lower()
    second = token_name.split(" ")[1].lower()
    lines_dict= readlines_file(dictionary)
    length = int(math.log2(len(lines_dict)))
    #print(length,len(lines_dict),first, second)
    for i in range(len(lines_dict)):
        if lines_dict[i].rstrip() == first:
            message+= bin(int(i))[2:].zfill(length)
            del lines_dict [i]
            break
    for i in range(len(lines_dict)):
        if lines_dict[i].rstrip() == second:
            message+= bin(int(i))[2:].zfill(length)
            del lines_dict [i]
            break
    length_symbol = len(token_symbol)
    if length_symbol ==3:
        message +='0'
    else:
        message += '1'
    symbol_list = [''.join(l) for l in combinations(first+second, length_symbol)]
    symbol_list = [x.lower() for x in symbol_list]
    symbol_list = list(OrderedDict.fromkeys(symbol_list))
    length = int(math.log2(len(symbol_list)))
    for i in range(len(symbol_list)):
        #print(i)
        if symbol_list[i].lower() == token_symbol.lower():
            message+= bin(int(i))[2:].zfill(length)
            break
    return message


def decodemessagestealthy (contracts_names,contracts_line,pragma_version,herency_relations_contract,herency_to_contract,lines):
    #print(contracts_names,contracts_line,pragma_version)
    message = ""
    message += bin(int(pragma_version))[2:].zfill(4)
    #print(message)
    folder_stealthy = './program/resources/stealhy'
    dictionary  =  './program/resources/little_dic.txt'
    contract_list = retrievefolder(folder_stealthy)
    contract_list = [formatname(x) for x in contract_list ]
    #print(contract_list)
    for i in contracts_names:
        if i in contract_list:
            index_chosen = contract_list.index(i)
            #print(i,index_chosen)
            del contract_list[index_chosen]
            message += bin(int(index_chosen))[2:].zfill(3)
            #print(message)
    # herency = herency_relations_contract[contracts_names[-1]]
    # print("***",herency)
    # #herency in order
    # herency_sorted = sorted(herency)
    # herency_sorted = herency_to_contract[contracts_names[-1]][1]
    # herency = herency_to_contract[contracts_names[-1]][0]
    # print("***",herency, herency_sorted)
    # z = list(itertools.permutations(herency_sorted))
    # length = int(math.log2(len(z)))
    # for i in range(len(z)):
    #     if herency == list(z[i]):
    #         print(i,herency,z[i])
    #         message+= bin(int(i))[2:].zfill(length)
    #         print('+++++++++',int(i))
    #         break
    # print (message)
    index_line = contracts_line[contracts_names[-1]]
    while index_line < len(lines):
        line = lines[index_line]
        if "constructor()" in line:
            tmp1 = line.split("ERC20(")[1]
            tmp1 = tmp1.split(")")[0]
            splitted = tmp1.split(",")
            token_name = eval(splitted[0])
            token_symbol = eval(splitted[1])
            #print(token_name,token_symbol)
            index_line = len(lines)
        index_line +=1
    first =token_name.split(" ")[0].lower()
    second = token_name.split(" ")[1].lower()
    lines_dict= readlines_file(dictionary)
    length = int(math.log2(len(lines_dict)))
    #print(length,len(lines_dict),first, second)
    for i in range(len(lines_dict)):
        if lines_dict[i].rstrip() == first:
            message+= bin(int(i))[2:].zfill(length)
            del lines_dict [i]
            break
    for i in range(len(lines_dict)):
        if lines_dict[i].rstrip() == second:
            message+= bin(int(i))[2:].zfill(length)
            del lines_dict [i]
            break
    length_symbol = len(token_symbol)
    if length_symbol ==3:
        message +='0'
    else:
        message += '1'
    # symbol_list = [''.join(l) for l in combinations(first+second, length_symbol)]
    # symbol_list = [x.lower() for x in symbol_list]
    # symbol_list = list(OrderedDict.fromkeys(symbol_list))
    # length = int(math.log2(len(symbol_list)))
    # for i in range(len(symbol_list)):
    #     print(i)
    #     if symbol_list[i].lower() == token_symbol.lower():
    #         message+= bin(int(i))[2:].zfill(length)
    #         break
    # print(symbol_list)
    return message


#lines = readlines ("/home/mar/Documentos/Third paper/RomanticisedMonocrats.sol")
# contracts_names,contracts_line,pragma_version,herency_relations_contract = examinecontract (lines)
# decodemessage(contracts_names,contracts_line,pragma_version,herency_relations_contract)
