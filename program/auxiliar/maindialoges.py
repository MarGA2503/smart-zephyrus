#!/usr/bin/env python
"""
Contains the program main dialogues with the user
"""

from getpass import getpass
import os

__author__ = "Mar Gimenez Aguilar"
__version__ = "1"
__maintainer__ = "Mar Gimenez Aguilar"
__email__ = "mar.gimeneza@gmail.com"

def extractcontractconstructor ():
    print("Load from used contracts(file names) or code (bin)?\n")
    print("1. Used contracts\n")
    print("2. Code\n")
    Correct = False
    used_path = ""
    contracts_path = ""
    while  not Correct:
        option = input()
        if option == "1":
            print("Please introduce the path to the contract abi or directory\n")
            Correct = False
            while  not Correct:
                origin = input()
                if os.path.lexists(origin):
                    Correct = True
                else:
                    print("Path not valid\n")
            print("Please introduce the path to the file with used abis\n")
            Correct = False
            while  not Correct:
                used_path = input()
                if os.path.lexists(used_path):
                    Correct = True
                else:
                    print("Path not valid\n")
            Correct = True
        elif option == "2":
            print("Please introduce the path to the directory with the contract abi and creation code(.bin)\n")
            Correct = False
            while  not Correct:
                contracts_path = input()
                if os.path.lexists(contracts_path):
                    Correct = True
                else:
                    print("Path not valid\n")
        else:
            print("Option not valid\n")
    return used_path, contracts_path

def askforvalueinone():
    print("Do you want to embed the message using the maximun space available or to divide by bytes per transaction?(y/n)\n")
    print("1.Try to embed using the maximun(9 bytes)\n")
    print("2.Divide in smaller quantities\n")
    Correct = False
    while not Correct:
        embed = input()
        length = 9
        if embed == "1":
            embed = "one"
            Correct = True
        elif embed == "2":
            embed = "smaller"
            print("Enter a value between 1 and 8\n")
            Correct = True
            length = int(input())
            while length< 1 and length > 8:
                print ("Option not valid\n")
                length = int(input())
        else:
            print ("Option not valid\n")
    return embed,length * 2
def askforgaspriceinone():
    print("Do you want to embed the message in one gas price or to make it similar  to the current one?(y/n)\n")
    print("1.Try to embed in one\n")
    print("2.Make it look similar\n")
    Correct = False
    while not Correct:
        embed = input()
        if embed == "1":
            embed = "one"
            Correct = True
        elif embed == "2":
            embed = "similar"
            Correct = True
        else:
            print ("Option not valid\n")
    return embed
def askfordestinatary():
    print("Please, introduce the path to the file with the destinatary/ies address/ess\n")
    Correct = False
    while not Correct:
        destinatary_add = input()
        if  os.path.lexists(destinatary_add):
            Correct = True
        else:
            print ("The file does not exist\n")
    return destinatary_add

def askforcontracts():
    print("Please introduce the directory with the .bin files or the .bin file\n")
    Correct = False
    while not Correct:
        origin = input()
        if  os.path.lexists(origin):
            Correct = True
        else:
            print ("The file does not exist\n")
    return origin

def askfortransactioncontracts ():
    print("Do you want to embed the message using normal transactions or contracts?\n")
    print("1.Normal transactions\n")
    print("2.Contracts\n")
    Correct = False
    while not Correct:
        option = input()
        if option == "1":
            option = "transaction"
            Correct = True
        elif option == "2":
            option = "contract"
            Correct = True
        else:
            print ("Option not valid\n")
    return option

def waitfortransaction():
    print("Do you want to wait for these transaction to be mined?(y/n)\n")
    Correct = False
    timeout = 0
    while not Correct:
        option = input()
        if option == "y" or option == "yes":
            print("Introduce the maximun time per transaction you want to wait in seconds(if none, 120s)\n")
            timeout = input()
            if timeout == "":
                timeout = 120
            else:
                timeout = int(timeout)
            option = True
            Correct = True
        elif option == "n" or option == "no":
            Correct = True
            option = False
        else:
            print ("Option not valid\n")
    return timeout, option

def modifygasbelow(estimatedGas, connection):
    print("Do you want to modify the estimated gas below?(y/n)\n")
    if connection != "w3" and type(estimatedGas[0]) is not int:
        print([int(x, 16) for x in estimatedGas])
    else:
        print(estimatedGas)
    Correct = False
    while not Correct:
        option = input()
        if option == "y" or option == "yes":
            print("Introduce a number to add\n")
            number = input()
            for i in range(len(estimatedGas)):
                if connection == "w3":
                    estimatedGas[i] += int(number)
                else:
                    estimatedGas[i] = hex(int(estimatedGas[i], 16) + int(number))
            Correct = True
        elif option == "n" or option == "no":
            Correct = True
        else:
            print ("Option not valid\n")
    return estimatedGas

def insertmessagefile ():
    print("Insert the message file path")
    Correct = False
    file_format = ""
    while not Correct:
        message_file = input()
        if os.path.lexists(message_file):
            Correct = True
            file_format = os.path.splitext(message_file)[-1].lower()
        else:
            print("The file does not exist\n")
    return message_file, file_format

def choosedefault():
    print("Do you want to execute the program with the default options?(y/n)\n")
    Correct = False
    while not Correct:
        option = input()
        if option == "y" or option == "yes":
            default = True
            Correct = True
        elif option == "n" or option == "no":
            default = False
            Correct = True
        else:
            print ("Option not valid\n")
    return default

def messageingas():
    print("Do you want to insert message in the Gas Limit too?(y/n)\n")
    Correct = False
    while not Correct:
        option = input()
        if option == "y" or option == "yes":
            gas = True
            Correct = True
        elif option == "n" or option == "no":
            gas = False
            Correct = True
        else:
            print ("Option not valid\n")
    return gas

def choosecipher(default):
    if not default:
        print("Do you want to cipher/decipher the message?(y/n)\n")
    Correct = False
    while not Correct:
        if default:
            print("Introduce the password\n")
            AESkey = getpass()
            print("Introduce the a nonce\n")
            nonce = getpass()
            Correct = True
        else:
            option = input()
            if option == "y" or option == "yes":
                print("Introduce the password\n")
                AESkey = getpass()
                print("Introduce the a nonce\n")
                nonce = getpass()
                Correct = True
            elif option == "n" or option == "no":
                AESkey = ""
                nonce = ""
                Correct = True
            else:
                print ("Option not valid\n")
    return AESkey, nonce

def getapi_key ():
    print("Introduce the file with the etherscan api keys")
    file = input()
    with open(file, encoding="utf-8") as f:
        lines=f.readlines()
    lines = [i.rstrip() for i in lines]
    return lines

def chooseorder():
    print("Do you want to disorder/order the message?(y/n)\n")
    Correct = False
    while not Correct:
        option = input()
        if option == "y" or option == "yes":
            print("Introduce a password\n")
            password = getpass()
            Correct = True
        elif option == "n" or option == "no":
            password = ""
            Correct = True
        else:
            print ("Option not valid\n")
    return password

def choosegethconnectionmethod(default):
    if not default:
        print("Choose a geth connection method\n")
        print("1. Use web3.py\n")
        print("2. Use rpc with curl\n")
        Correct = False
        while not Correct:
            connection = input()
            if connection == "1":
                connection = "w3"
                Correct = True
            elif connection == "2":
                connection= "rcp"
                Correct = True
            else:
                print ("Option not valid\n")
    else:
        connection = "rcp"
    return connection

def choosemethodextract():
    print("Choose a method\n")
    print("1. Receiver address\n")
    print("2. Swarm hash\n")
    print("3. Contract bytecode without execution\n")
    print("4. Contract constructor\n")
    print("5. Functions calls\n")
    print("6. Gas Limit\n")
    print("7. Gas Price\n")
    print("8. Value\n")
    print("9. Solidity capacity")
    print("10. Solidity stealthy")
    Correct = False
    type_hex = False
    type_bin = False
    while not Correct:
        method = input()
        if method == "1":
            method = "receiveraddressmodule.extractReceiverAddressMethod(tx_hashes,connection, ChaCha20key, password, default, ntimes, timeV)"
            Correct = True
            type_hex = True
        elif method == "2":
            method = "swarmhashmodule.extractSwarmHashMethod(tx_hashes,connection, ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            type_hex = True
        elif method == "3":
            method = "bytecodemodule.extractContractBytecodeMethod(tx_hashes,connection, ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            #type_bin = True
            type_hex = True
        elif method == "4":
            method = "constructormodule.extractContractConstructorMethod(tx_hashes,connection, ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            type_hex = True
        elif method == "5":
            method = "functioncallmodule.extractContractFunctionMethod(tx_hashes,connection, ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            type_hex = True
        elif method == "6":
            method = "gaslimitmodule.extractGasLimitMethod (tx_hashes,connection,ChaCha20key, password, default,ntimes, timeV )"
            Correct = True
            type_hex = True
        elif method == "7":
            method = "gaspricemodule.extractGasPriceMethod(tx_hashes,connection,ChaCha20key, password, default,ntimes, timeV )"
            Correct = True
            type_hex = True
        elif method == "8":
            method = "valuemodule.extractvalueMethod(tx_hashes,connection,ChaCha20key, password, default ,ntimes, timeV)"
            Correct = True
            type_hex = True
        elif method == "9":
            method = "soliditystealthymodule.extractSolidityStealthyMethod(tx_hashes,connection,ChaCha20key, password, default ,ntimes, timeV)"
            Correct = True
            type_hex = True
        elif method == "10":
            method = "soliditysuperstealthymodule.extractSolidityStealthyMethod(tx_hashes,connection,ChaCha20key, password, default ,ntimes, timeV)"
            Correct = True
            type_hex = True
        else:
            print ("Option not valid\n")
    return method, type_hex, type_bin


def choosemethodextract_old():
    print("Choose a method\n")
    print("1. Receiver address\n")
    print("2. Transaction data\n")
    print("3. Swarm hash\n")
    print("4. Contract bytecode without execution\n")
    print("5. Contract bytecode with execution\n")
    print("6. Contract constructor\n")
    print("7. Functions calls\n")
    print("8. Gas Limit\n")
    print("9. Gas Price\n")
    print("10. Value\n")
    Correct = False
    type_hex = False
    type_bin = False
    while not Correct:
        method = input()
        if method == "1":
            method = "receiveraddressmodule.extractReceiverAddressMethod(tx_hashes,connection, ChaCha20key, password, default, ntimes, timeV)"
            Correct = True
            type_hex = True
        elif method == "2":
            method = "transactiondatamodule.extractTransactionDataMethod(tx_hashes,connection, ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            type_hex = True
        elif method == "3":
            method = "swarmhashmodule.extractSwarmHashMethod(tx_hashes,connection, ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            type_hex = True
        elif method == "4":
            method = "bytecodemodule.extractContractBytecodeMethod(tx_hashes,connection, ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            #type_bin = True
            type_hex = True
        elif method == "5":
            method = "bytecodeexecutionmodule.extractContractBytecodeExecutionMethod(tx_hashes,connection, ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            type_bin = True
        elif method == "6":
            method = "constructormodule.extractContractConstructorMethod(tx_hashes,connection, ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            type_hex = True
        elif method == "7":
            method = "functioncallmodule.extractContractFunctionMethod(tx_hashes,connection, ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            type_hex = True
        elif method == "8":
            method = "gaslimitmodule.extractGasLimitMethod (tx_hashes,connection,ChaCha20key, password, default,ntimes, timeV )"
            Correct = True
            type_hex = True
        elif method == "9":
            method = "gaspricemodule.extractGasPriceMethod(tx_hashes,connection,ChaCha20key, password, default,ntimes, timeV )"
            Correct = True
            type_hex = True
        elif method == "10":
            method = "valuemodule.extractvalueMethod(tx_hashes,connection,ChaCha20key, password, default ,ntimes, timeV)"
            Correct = True
            type_hex = True
        else:
            print ("Option not valid\n")
    return method, type_hex, type_bin

def choosemethodinsert ():
    print("Choose a method\n")
    print("1. Receiver address\n")
    print("2. Swarm hash\n")
    print("3. Contract bytecode without execution\n")
    print("4. Contract constructor\n")
    print("5. Functions calls\n")
    print("6. Gas Limit\n")
    print("7. Gas Price\n")
    print("8. Value\n")
    print("9. Solidity capacity\n")
    print("10. Solidity stealthy\n")
    Correct = False
    type_hex = False
    type_bin = False
    while not Correct:
        method = input()
        if method == "1":
            method = "receiveraddressmodule.ReceiverAddressMethod(data,connection, ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            type_hex = True
        elif method == "2":
            method = "swarmhashmodule.SwarmHashMethod(data,connection, ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            type_hex = True
        elif method == "3":
            method = "bytecodemodule.ContractBytecodeMethod(data,connection, ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            #type_bin = True
            type_hex = True
        elif method == "4":
            method = "constructormodule.ContractConstructorMethod(data,connection, ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            type_hex = True
        elif method == "5":
            method = "functioncallmodule.ContractFunctionMethod(data,connection, ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            type_hex = True
        elif method == "6":
            method = "gaslimitmodule.GasLimitMethod(data,connection,ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            type_hex = True
        elif method == "7":
            method = "gaspricemodule.GasPriceMethod(data,connection,ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            type_hex = True
        elif method == "8":
            method = "valuemodule.valueMethod(data,connection,ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            type_hex = True
        elif method == "9":
            method = "soliditystealthymodule.SolidityStealthyMethod(data,connection,ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            type_hex = True
        elif method == "10":
            method = "soliditysuperstealthymodule.SolidityStealthyMethod(data,connection,ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            type_hex = True
        else:
            print ("Option not valid\n")
    return method, type_hex, type_bin


def choosemethodinsert_old ():
    print("Choose a method\n")
    print("1. Receiver address\n")
    print("2. Transaction data\n")
    print("3. Swarm hash\n")
    print("4. Contract bytecode without execution\n")
    print("5. Contract bytecode with execution\n")
    print("6. Contract constructor\n")
    print("7. Functions calls\n")
    print("8. Gas Limit\n")
    print("9. Gas Price\n")
    print("10. Value\n")
    Correct = False
    type_hex = False
    type_bin = False
    while not Correct:
        method = input()
        if method == "1":
            method = "receiveraddressmodule.ReceiverAddressMethod(data,connection, ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            type_hex = True
        elif method == "2":
            method = "transactiondatamodule.TransactionDataMethod(data,connection, ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            type_hex = True
        elif method == "3":
            method = "swarmhashmodule.SwarmHashMethod(data,connection, ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            type_hex = True
        elif method == "4":
            method = "bytecodemodule.ContractBytecodeMethod(data,connection, ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            #type_bin = True
            type_hex = True
        elif method == "5":
            method = "bytecodeexecutionmodule.ContractBytecodeExecutionMethod(TextVBin,connection, ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            type_bin = True
        elif method == "6":
            method = "constructormodule.ContractConstructorMethod(data,connection, ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            type_hex = True
        elif method == "7":
            method = "functioncallmodule.ContractFunctionMethod(data,connection, ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            type_hex = True
        elif method == "8":
            method = "gaslimitmodule.GasLimitMethod(data,connection,ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            type_hex = True
        elif method == "9":
            method = "gaspricemodule.GasPriceMethod(data,connection,ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            type_hex = True
        elif method == "10":
            method = "valuemodule.valueMethod(data,connection,ChaCha20key, password, default,ntimes, timeV)"
            Correct = True
            type_hex = True
        else:
            print ("Option not valid\n")
    return method, type_hex, type_bin
