#!/usr/bin/env python
"""
Contains all functions related with reading and writing to a file
"""

import binascii
import os

__author__ = "Mar Gimenez Aguilar"
__version__ = "1"
__maintainer__ = "Mar Gimenez Aguilar"
__email__ = "mar.gimeneza@gmail.com"

def readbytes(file_to):
    """
    Read a file and return it in bytes and binary form
    """
    with open(file_to, "rb") as binary_file:
        # Read the whole file at once
        data = binary_file.read()
        #print(data)

        # Seek position and read N bytes
        binary_file.seek(0)  # Go to beginning
        couple_bytes = binary_file.read(2)
        #print(couple_bytes)
        TextVBin = ''.join('{:08b}'.format(x) for x in data)
        #TextV = bytearray([int(TextVBin[i:i+8], 2) for i in range(0, len(TextVBin), 8)])
        #print(TextVBin)
    return data.hex(), TextVBin
        # with open("./sol2.sol", "wb") as outfile:
        #     outfile.write(data)
        # with open("./sol3.sol", "wb") as outfile:
        #     outfile.write(TextV)

def writeThash(destination, tx_hashes, times=0):
    """
    Write the transactions hashes to a file named "hashes.txt"
    """
    #print(times)
    if times==0:
        with open(os.path.join(destination, "hashes.txt"), "w") as outfile:
            for x in tx_hashes:
                outfile.write(x+ "\n")
    else:
        for n in range(times):
            tx_tmp = tx_hashes[(n-1)]
            #print(tx_tmp)
            with open(os.path.join(destination, "hashes" + str(n) + ".txt"), "w") as outfile:
                for x in tx_tmp:
                    #print(x)
                    outfile.write(x+ "\n")
    outfile.close()


def readThash(origin):
    """
    Read a file and return its value and if it contains addresses or hashes
    """
    address = False
    if os.path.isdir(origin):
        content = []
        address = []
        for file in os.listdir(origin):
            #print(os.path.join(origin,file))
            if file[0] != '.':
                with open(os.path.join(origin,file)) as f:
                    if os.path.isfile(os.path.join(origin,file)):
                        content_tmp = (line.rstrip() for line in f)
                        #print(content_tmp)
                        content_tmp = list(line for line in content_tmp if line)
                    if len(content_tmp[0]) == 40 or len(content_tmp[0]) == 42:
                        address.append(True)
                        if len(content_tmp[0]) == 40:
                            content_tmp = ['0x'+ x for x in content]
                    elif len(content_tmp[0]) == 66 or len(content_tmp[0]) == 64:
                        if len(content_tmp[0]) == 64:
                            content_tmp = ['0x'+ x for x in content_tmp]
                    else:
                        print("The file does not contain valid hashes or addresses\n")
                        exit(1)
                    if len(content_tmp) > 0:
                        content.append(content_tmp)
    else:
        with open(origin) as f:
            content = (line.rstrip() for line in f)
            content = list(line for line in content if line)
        if len(content[0]) == 40 or len(content[0]) == 42:
            address = True
            if len(content[0]) == 40:
                content = ['0x'+ x for x in content]
        elif len(content[0]) == 66 or len(content[0]) == 64:
            if len(content[0]) == 64:
                content = ['0x'+ x for x in content]
        else:
            print("The file does not contain valid hashes or addresses\n")
            exit(1)
    return content, address

def savemessage(destination, message, hexa = True):
    """
    Save the message into a file
    """
    #first we extract the extension
    #TODO Windows users, habilitate this option
    # if hexa:
    #     #check if first is . else add
    #     extension = bytes.fromhex(message[0:8]).decode("utf-8")
    #     if extension[0] != ".":
    #         extension = "." + extension
    #     data = bytes.fromhex(message[8:])
    #     print("File extension",extension)
    # else:
    #     extension = (message[0:8]).decode("utf-8")
    #     if extension[0] != ".":
    #         extension = "." + extension
    #     data = (message[8:])
    #     print("File extension",extension)
    # with open (os.path.join(destination, "message" + extension), "wb") as outfile:
    #     outfile.write(data)
    if hexa:
        data = bytes.fromhex(message[:])
    else:
        data = (message[:])
    with open (os.path.join(destination, "message"), "wb") as outfile:
        outfile.write(data)

# def compress(uncompressed):
#     """Compress a string to a list of output symbols."""
#
#     dict_size = 2
#     dictionary = {
#     '0': '0',
#     '1': '1'
#     }
#     # dict_size = 256
#     # dictionary = {chr(i): chr(i) for i in range(dict_size)}
#
#     w = ""
#     result = []
#     for c in uncompressed:
#         wc = w + c
#         #print(wc)
#         if wc in dictionary:
#             w = wc
#         else:
#             # print("w")
#             # print (w)
#             result.append(dictionary[w])
#             # Add wc to the dictionary.
#             dictionary[wc] = dict_size
#             dict_size += 1
#             w = c
#
#     # Output the code for w.
#     if w:
#         result.append(dictionary[w])
#     return result
#
# def decompress(compressed):
#
#     dict_size = 2
#     dictionary = {
#     '0': '0',
#     '1': '1'
#     }
#
#     w = result = compressed.pop(0)
#     for k in compressed:
#         if k in dictionary:
#             entry = dictionary[k]
#         elif k == dict_size:
#             entry = w + w[0]
#         else:
#             raise ValueError('Bad compressed k: %s' % k)
#         result += entry
#
#         # Add w+entry[0] to the dictionary.
#         dictionary[dict_size] = w + entry[0]
#         dict_size += 1
#
#         w = entry
#     return result
# def text_to_bits_u(text, encoding='utf-8', errors='surrogatepass'):
#     bits = bin(int.from_bytes(text.encode(encoding, errors), 'big'))[2:]
#     return bits.zfill(8 * ((len(bits) + 7) // 8))
#
# def text_from_bits_u(bits, encoding='utf-8', errors='surrogatepass'):
#     n = int(bits, 2)
#     return n.to_bytes((n.bit_length() + 7) // 8, 'big').decode(encoding, errors) or '\0'
#
# def text_to_bits_a(text, encoding='ascii', errors='surrogatepass'):
#     bits = bin(int.from_bytes(text.encode(encoding, errors), 'big'))[2:]
#     return bits.zfill(8 * ((len(bits) + 7) // 8))
#
# def text_from_bits_a(bits, encoding='ascii', errors='surrogatepass'):
#     n = int(bits, 2)
#     return n.to_bytes((n.bit_length() + 7) // 8, 'big').decode(encoding, errors) or '\0'
#
# def compress2(uncompressed):
#     """Compress a string to a list of output symbols."""
#
#     # Build the dictionary.
#     dict_size = 256
#     dictionary = {chr(i): chr(i) for i in range(dict_size)}
#
#     w = ""
#     result = []
#     for c in uncompressed:
#         wc = w + str(c)
#         if wc in dictionary:
#             w = wc
#         else:
#             result.append(dictionary[w])
#             # Add wc to the dictionary.
#             dictionary[wc] = dict_size
#             dict_size += 1
#             w = str(c)
#
#     # Output the code for w.
#     if w:
#         result.append(dictionary[w])
#     return result
#
#
#
# def decompress2(compressed):
#     """Decompress a list of output ks to a string."""
#
#     # Build the dictionary.
#     dict_size = 256
#     dictionary = dict((chr(i), chr(i)) for i in xrange(dict_size))
#     dictionary = {chr(i): chr(i) for i in range(dict_size)}
#
#     w = result = compressed.pop(0)
#     for k in compressed:
#         if k in dictionary:
#             entry = dictionary[k]
#         elif k == dict_size:
#             entry = w + w[0]
#         else:
#             raise ValueError('Bad compressed k: %s' % k)
#         result += entry
#
#         # Add w+entry[0] to the dictionary.
#         dictionary[dict_size] = w + entry[0]
#         dict_size += 1
#
#         w = entry
#     return result
# data, TextVBin= readbytes("./cas.png")
# com = gzip.compress(data, compresslevel=9)
# print(com)
# print(len(data), len(com))
# assert(len(data) >= len(com))
# bin_compress = str(compress(TextVBin))
# b = bytearray(bin_compress, "ascii")
# b = bytes.fromhex(bin_compress.encode("ascii").hex())
# x=binascii.hexlify(bytearray(bin_compress, "ascii"))
# print (x)
# with open("./sol3", "wb") as outfile:
#     outfile.write(x)
# Tex3 = text_to_bits_a(data_compress)
# data2 = text_to_bits_a(data)
# Tex = text_to_bits_a(bin_compress)
# Tex2 = text_to_bits_u(bin_compress)
# # for x in bin_compress:
# #     print( '{:08b}'.format(x))
# #print(Tex)
# print(len(TextVBin), len(Tex), len(Tex2))
# assert(len(data2)>= len(Tex3))
# a = "['0', '1', 3, '0', 5, 5, 3, '1', 7, 7, 9, 6, 9, 4, 2, 4, 17, 14, 16, 6, 2, 10, 21, 16, 12, 18, 25, 14, 29, 13, 19, 22, 15, 12, 20, 26, 25, 22, 11, 34, 23, 21, 29, 15, 20, 24, 44, 31, 39, 10, 8, 49, 23, 34, 26, 27, 13, 50, 51, 11, 49, 18, 27, 52, 38, 35, 65, 56, 43, 46, 36, 17, 70, 24, 59, 60, 72, 19, 30, 73, 76, 28, 71, 33, 47, 50, 35, 74, 43, 69, 66, 33, 48, 71, 63, 87, 32, 80, 52, 96, 42, 53, 51, 37, 61, 59, 88, 58, 75, 32, 109, 90, 79, 36, 104, 38, 92, 112, 89, 58, 8, 57, 54, 84, 108, 102, 40, 85, 81, 99, 56, 111, 82, 44, 123, 67, 75, 82, 85, 39, 126, 134, 41, 46, 79, 81, 92, 94, 69, 68, 55, 127, 77, 119, 128, 146, 141, 78, 156, 120, 155, 77, 105, 113, 30, 121, 162, 139, 142, 126, 68, 106, 146, 111, 100, 93, 105, 101, 134, 28, 166, 124, 110, 93, 153, 135, 182, 67, 123, 140, 169, 110, 122, 107, 141, 194, 88, 19]"
# print("text")
# text= text_to_bits_a(a)
# print(text)
