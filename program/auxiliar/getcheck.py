#!/usr/bin/env python
"""
Contains functions that extract some information or allows the program to check something
"""
import os
import ast
import cbor
import copy
import json
from ..methods import calling
from ..methods import swarm

__author__ = "Mar Gimenez Aguilar"
__version__ = "1"
__maintainer__ = "Mar Gimenez Aguilar"
__email__ = "mar.gimeneza@gmail.com"


def getpatterjson (name):
    existing = False
    current_directory = os.getcwd()
    #print("currentdirectory", current_directory)
    resources = "/program/resources/" + name + ".json"
    complete_path = current_directory + resources
    existing = os.path.isfile(complete_path)
    pattern_file = {}
    if existing:
        with open(complete_path, 'r') as f:
            pattern_file = json.load(f)
    return existing, pattern_file

def saveresourcepattern(name,resource):
    current_directory = os.getcwd()
    resources = "/program/resources/" + name + ".json"
    complete_path = current_directory + resources
    with open(complete_path, 'w') as f:
        json.dump(resource, f)

def getargumentsfromused(origin, used_path):
    """
    Retrieve the type of arguments used in a contract constructor of contracts
    contained in "origin" and named in a text file "used_path"
    """
    with open(used_path) as f:
        used = f.readlines()
        used = [x.strip() for x in used]
    arguments = []
    sizes = []
    dynamic = []
    #First for each used we have to get the contract abi
    for x in used:
        if os.path.isfile(os.path.join(origin, x)):
            abi = getContracts((os.path.join(origin, x)),bytecode=False)
            #print(abi)
            arguments.append(calling.constructorarguments(abi[0]['abi']))
            size, dy, arguments_f = calling.checklentotalmessage(arguments[-1],'constructor')
            arguments[-1] = arguments_f
            sizes.append(size)
            dynamic.append(dy)
        elif os.path.isfile(os.path.join(origin, x+ ".abi")):
            abi = getContracts (os.path.join(origin, x+ ".abi"),bytecode=False)
            arguments.append(calling.constructorarguments(abi[0]['abi']))
            size, dy, arguments_f = calling.checklentotalmessage(arguments[-1], 'constructor')
            arguments[-1] = arguments_f
            sizes.append(size)
            dynamic.append(dy)
        else:
            print("Used names don't match\n")
            exit(1)
    return arguments, sizes, dynamic

def getargumentsfromcode(contracts_path, data_in_transactions,messages):
    """
    Retrieve the type of arguments used in a contract constructor  using the
    data in the transaction
    """
    contracts = getContracts(contracts_path)
    arguments = []
    sizes = []
    dynamic = []
    used = {}
    #print(contracts)
    #First for each used we have to get the contract abi
    #TODO: pass if no message (combined)
    for x in range(len(data_in_transactions)):
        if data_in_transactions[x][:-len(messages[x])] in used.keys():
            abi = used[data_in_transactions[x][:-len(messages[x])]]
            arguments.append(calling.constructorarguments(abi))
            size, dy,arguments_f = calling.checklentotalmessage(arguments[-1], 'constructor')
            arguments[-1] = arguments_f
            sizes.append(size)
            dynamic.append(dy)
        else:
            notFound = True
            i = 0
            while i< len(contracts) and notFound:
                if data_in_transactions[x] in contracts[i].values():
                    abi = contracts[i]['abi']
                    #print("abii", abi)
                    used[data_in_transactions[x][:-len(messages[x])]] = abi
                    arguments.append(calling.constructorarguments(abi))
                    size, dy, arguments_f = calling.checklentotalmessage(arguments[-1],'constructor')
                    arguments[-1] = arguments_f
                    sizes.append(size)
                    dynamic.append(dy)
                    notFound = False
                elif data_in_transactions[x][:-len(messages[x])] in contracts[i].values():
                    abi = contracts[i]['abi']
                    #print("abii", abi)
                    used[data_in_transactions[x][:-len(messages[x])]] = abi
                    arguments.append(calling.constructorarguments(abi))
                    size, dy, arguments_f  = calling.checklentotalmessage(arguments[-1],'constructor')
                    arguments[-1] = arguments_f
                    sizes.append(size)
                    dynamic.append(dy)
                    notFound = False
                i += 1
                if i >= len(contracts) and notFound:
                    print("Contracts don't match\n")
                    exit(1)
    return arguments, sizes, dynamic

# def getendswarmhash(bytecode):
#     """
#     Return the position where the swarm hash ends
#     """
#     count = bytecode.count("627a7a72")
#     if count == 1:
#         first, metadata = swarm.findswarm (bytecode)
#         metadata = cbor.dumps(metadata).hex()
#         end = first + len(metadata) + 4
#         #end = first + 86
#     else:
#         bytecodeaux = copy.deepcopy(bytecode)
#         found = False
#         while count > 0 and not found:
#             metakey = False
#             first = bytecodeaux.find("627a7a72")
#             first -= 4
#             metadata = cbor.loads(bytes.fromhex(bytecodeaux[first:]))
#             #check if metadata
#             for key in metadata:
#                 if key.startswith('bzzr'):
#                         metakey = True
#             #rest = bytecodeaux[(first + 82)]
#             if metakey:
#                 found = True
#                 #end = first + 86
#                 metadata = cbor.dumps(metadata).hex()
#                 end = first + len(metadata) + 4
#             else:
#                 bytecodeaux = bytecodeaux[(first+8):]
#             count -= 1
#     return end


def getendswarmhash(bytecode):
    """
    Return the position where the swarm hash ends
    """
    count = bytecode.count("627a7a72")
    end = -1
    if count == 1:
        first, metadata = swarm.findswarm (bytecode)
        metadata = cbor.dumps(metadata).hex()
        end = first + len(metadata) + 4
        #end = first + 86
    else:
        bytecodeaux = copy.deepcopy(bytecode)
        found = False
        while count > 0 and not found:
            metakey = False
            first = bytecodeaux.rfind("627a7a72")
            first -= 4
            try:
                metadata = cbor.loads(bytes.fromhex(bytecodeaux[first:]))
                #check if metadata
                if metadata!= 0:
                    for key in metadata:
                        if key.startswith('bzzr'):
                                metakey = True
                    #rest = bytecodeaux[(first + 82)]
                    if metakey:
                        found = True
                        #end = first + 86
                        metadata = cbor.dumps(metadata).hex()
                        end = first + len(metadata) + 4
                else:
                    bytecodeaux = bytecodeaux[:first]
            except ValueError:
                bytecodeaux = bytecodeaux[:first]
            count -= 1
    if end == -1:
        count = bytecode.count("69706673")
        if count == 1:
            first, metadata = swarm.findipfs (bytecode)
            metadata = cbor.dumps(metadata).hex()
            end = first + len(metadata) + 4
            #end = first + 86
        else:
            bytecodeaux = copy.deepcopy(bytecode)
            found = False
            while count > 0 and not found:
                metakey = False
                first = bytecodeaux.rfind("69706673")
                first -= 4
                try:
                    metadata = cbor.loads(bytes.fromhex(bytecodeaux[first:]))
                    #check if metadata
                    if metadata!= 0:
                        for key in metadata:
                            if key.startswith('ipfs'):
                                    metakey = True
                        #rest = bytecodeaux[(first + 82)]
                        if metakey:
                            found = True
                            #end = first + 86
                            metadata = cbor.dumps(metadata).hex()
                            end = first + len(metadata) + 4
                    else:
                        bytecodeaux = bytecodeaux[:first]
                except ValueError:
                    bytecodeaux = bytecodeaux[:first]
                count -= 1
    return end

def getabi (origin):
    """
    Get the contract ABI from a file and replace the boolean keywords with Python ones
    """
    with open(origin, "r") as abi_file:
        # Read the whole file at once
        abi = abi_file.read().replace("\n","")
        abi = abi.replace("false", "False")
        abi = abi.replace("true", "True")
    return abi

def getbin (origin):
    """
    Get the contract binary code from a file
    """
    with open(origin, "r") as bin_file:
        bytecode = bin_file.read().replace("\n","")
    return bytecode

def getContracts(origin,abi=True,bytecode=True):
    """
    Get the contract ABI and/or bytecode from a file or get all the contracts
    ABI and/or bytecode from a directory if origin is one
    """
    contracts = []
    if os.path.isfile(origin) or os.path.isfile(origin + ".bin") or os.path.isfile(origin + ".abi"):
        dic = {}
        if abi:
            if origin[-4:] == ".abi":
                abi = getabi (origin)
            else:
                abi = getabi (origin + ".abi")
            dic['abi'] = ast.literal_eval(abi)
        if bytecode:
            if origin[-4:] == ".bin":
                bytecode = getbin(origin)
            else:
                bytecode = getbin(origin + ".bin")
            dic['bin'] = bytecode
        contracts.append(dic)
    elif os.path.isdir(origin):
        directory = sorted(os.listdir(origin))
        i = 0
        while i < len(directory):
            dic = {}
            if abi and bytecode:
                if directory[i][-4:] == ".abi" and directory[(i+1)][-4:] == ".bin":
                    abi = getabi (os.path.join(origin, directory[i]))
                    i += 1
                    dic['abi'] = ast.literal_eval(abi)
                    bytecode = getbin(os.path.join(origin, directory[i]))
                    dic['bin'] = bytecode
                    i += 1
                    contracts.append(dic)
                else:
                    i += 1
            elif abi and not bytecode:
                #Check that the extensions are correct
                if directory[i][-4:] == ".abi":
                    abi = getabi (os.path.join(origin, directory[i]))
                    dic['abi'] = ast.literal_eval(abi)
                    contracts.append(dic)
                i += 1
            elif not abi and bytecode:
                if directory[i][-4:] == ".bin":
                    bytecode = getbin(os.path.join(origin, directory[i]))
                    dic['bin'] = bytecode
                    contracts.append(dic)
                i += 1
    return contracts

def checkfunctioncalls_old(contracts,default):
    """
    Get the type of arguments from a function or functions of a contract
    and controls that function with arguments too small are not used
    """
    arguments = []
    sizes = []
    dynamic = []
    found = False
    i = 0
    x = 0
    dymaxsize = 0
    option = ""
    function_index = -1
    #print(contracts)
    functionName = []
    if not default:
        print("Do you want to use only one function?(y/n)\n")
        Correct = False
        while not Correct:
            option = input()
            if option == "y" or option == "yes":
                print("Introduce the functionName\n")
                functionName = [input()]
                Correct = True
            elif option == "n" or option == "no":
                functionName = []
                Correct = True
            else:
                print ("Option not valid\n")
    arguments, functionName = calling.functionarguments(contracts[0]['abi'], functionName)
    #print(arguments)
    original_arguments = copy.deepcopy(arguments)
    while i < len(arguments) and not found:
        if dymaxsize == 0:
            size, dy, final_arguments = calling.checklentotalmessage(arguments[i])
            arguments[i] = copy.deepcopy(final_arguments)
            #print(x, len(dy), default)
        if x == 0 and len(dy) > 0 and not default:
            print("This function has dynamic(s) type(s) as arguments. Do you want to limit their size?(y/n)\n")
            Correct = False
            x += 1
            while not Correct:
                option = input()
                if option == "y" or option == "yes":
                    print("Introduce a size in bytes\n")
                    correct = False
                    while not correct:
                        dymaxsize = int(input())
                        if dymaxsize>=32 and dymaxsize%32 == 0:
                            correct= True
                        else:
                            print("Introduce a number bigger than 32 and multiple of 32")
                    #dymaxsize = int(input())
                    size, dy, final_arguments = calling.checklentotalmessage(arguments[-1], maxsize = (dymaxsize * 2))
                    arguments[-1] =  copy.deepcopy(final_arguments)
                    Correct = True
                    option = "yes"
                elif option == "n" or option == "no":
                    Correct = True
                    option = "no"
                else:
                    print ("Option not valid\n")
        if option == "no" or default:
            arguments = [arguments[i]]
            original_arguments = [original_arguments[i]]
            sizes = [size]
            dynamic = [dy]
            function_index = i
            found = True
        else:
            #print(size, dy)
            size, dy, final_arguments = calling.checklentotalmessage(arguments[i], maxsize = (dymaxsize * 2))
            arguments[i] = copy.deepcopy(final_arguments)
            #print(size, dy)
            if sum(size) < 12:
                del arguments[i]
                del functionName[i]
                del original_arguments[i]
                i -=1
            else:
                sizes.append(size)
                dynamic.append(dy)
        i += 1
    print("arg", arguments, sizes)
    return arguments, sizes, dynamic, function_index, functionName, original_arguments

def checkfunctioncalls(contracts,default):
    """
    Get the type of arguments from a function or functions of a contract
    and controls that function with arguments too small are not used
    """
    arguments = []
    sizes = []
    dynamic = []
    found = False
    i = 0
    x = 0
    dymaxsize = 0
    option = ""
    function_index = -1
    #print(contracts)
    functionName = []
    if not default:
        print("Do you want to use only one function?(y/n)\n")
        Correct = False
        while not Correct:
            option = input()
            if option == "y" or option == "yes":
                print("Introduce the functionName\n")
                functionName = [input()]
                Correct = True
            elif option == "n" or option == "no":
                functionName = []
                Correct = True
            else:
                print ("Option not valid\n")
    arguments, functionName = calling.functionarguments(contracts[0]['abi'], functionName)
    #print(arguments)
    original_arguments = copy.deepcopy(arguments)
    while i < len(arguments) and not found:
        if dymaxsize == 0:
            size, dy, final_arguments = calling.checklentotalmessage(arguments[i], 'function')
            arguments[i] = copy.deepcopy(final_arguments)
            #print(x, len(dy), default)
        if x == 0 and len(dy) > 0 and not default:
            print("This function has dynamic(s) type(s) as arguments. Do you want to limit their size?(y/n)\n")
            Correct = False
            x += 1
            while not Correct:
                option = input()
                if option == "y" or option == "yes":
                    print("Introduce a size in bytes\n")
                    correct = False
                    while not correct:
                        dymaxsize = int(input())
                        if dymaxsize>=32 and dymaxsize%32 == 0:
                            correct= True
                        else:
                            print("Introduce a number bigger than 32 and multiple of 32")
                    #dymaxsize = int(input())
                    size, dy, final_arguments = calling.checklentotalmessage(arguments[-1], 'function',maxsize = (dymaxsize * 2))
                    arguments[-1] =  copy.deepcopy(final_arguments)
                    Correct = True
                    option = "yes"
                elif option == "n" or option == "no":
                    Correct = True
                    option = "no"
                else:
                    print ("Option not valid\n")
        if option == "no" or default:
            arguments = [arguments[i]]
            original_arguments = [original_arguments[i]]
            sizes = [size]
            dynamic = [dy]
            function_index = i
            found = True
        else:
            #print(size, dy)
            size, dy, final_arguments = calling.checklentotalmessage(arguments[i], 'function',  maxsize = (dymaxsize * 2))
            arguments[i] = copy.deepcopy(final_arguments)
            #print(size, dy)
            # if sum(size) < 12:
            #     del arguments[i]
            #     del functionName[i]
            #     del original_arguments[i]
            #     i -=1
            # else:
            #     sizes.append(size)
            #     dynamic.append(dy)
            sizes.append(size)
            dynamic.append(dy)
        i += 1
    #print("arg", arguments, sizes)
    return arguments, sizes, dynamic, function_index, functionName, original_arguments

def checkcontractsconstructor(contracts, default, check = False):
    """
    Get the type of arguments for a contract or contracts constructor
    """
    arguments = []
    sizes = []
    dynamic = []
    found = False
    i = 0
    x = 0
    dymaxsize = 0
    option = ""
    contract_index = -1
    while i < len(contracts) and not found:
        arguments.append(calling.constructorarguments(contracts[i]['abi']))
        original_arguments = copy.deepcopy(arguments)
        if arguments[-1] == -1 and not check:
            print("This constract has no constructor\n")
            exit(1)
        elif arguments[-1] == -1 and check:
            sizes.append(-1)
            dynamic.append(-1)
        else:
            if dymaxsize == 0:
                size, dy, final_arguments = calling.checklentotalmessage(arguments[-1], 'constructor')
                arguments[i] = copy.deepcopy(final_arguments)
            if x == 0 and len(dy) > 0 and not default:
                print("This contructor has dynamic(s) type(s) as arguments. Do you want to limit their size?(y/n)\n")
                Correct = False
                x += 1
                while not Correct:
                    option = input()
                    if option == "y" or option == "yes":
                        print("Introduce a size in bytes\n")
                        correct = False
                        while not correct:
                            dymaxsize = int(input())
                            if dymaxsize>=32 and dymaxsize%32 == 0:
                                correct= True
                            else:
                                print("Introduce a number bigger than 32 and multiple of 32")

                        size, dy, final_arguments = calling.checklentotalmessage(arguments[-1], 'constructor', maxsize = (dymaxsize * 2))
                        arguments[-1] =  copy.deepcopy(final_arguments)
                        Correct = True
                        option = "yes"
                    elif option == "n" or option == "no":
                        Correct = True
                        option = "no"
                    else:
                        print ("Option not valid\n")
            if option == "no" or default:
                arguments = [arguments[-1]]
                original_arguments = [original_arguments[-1]]
                sizes = [size]
                dynamic = [dy]
                contract_index = i
                found = True
            else:
                size, dy,final_arguments = calling.checklentotalmessage(arguments[-1], 'constructor',maxsize = (dymaxsize * 2))
                arguments[i] = copy.deepcopy(final_arguments)
                sizes.append(size)
                dynamic.append(dy)
        i += 1
    return arguments, sizes, dynamic, contract_index,original_arguments

def checkcontractsconstructor_old(contracts, default, check = False):
    """
    Get the type of arguments for a contract or contracts constructor
    """
    arguments = []
    sizes = []
    dynamic = []
    found = False
    i = 0
    x = 0
    dymaxsize = 0
    option = ""
    contract_index = -1
    while i < len(contracts) and not found:
        arguments.append(calling.constructorarguments(contracts[i]['abi']))
        original_arguments = copy.deepcopy(arguments)
        if arguments[-1] == -1 and not check:
            print("This constract has no constructor\n")
            exit(1)
        elif arguments[-1] == -1 and check:
            sizes.append(-1)
            dynamic.append(-1)
        else:
            if dymaxsize == 0:
                size, dy, final_arguments = calling.checklentotalmessage(arguments[-1])
                arguments[i] = copy.deepcopy(final_arguments)
            if x == 0 and len(dy) > 0 and not default:
                print("This contructor has dynamic(s) type(s) as arguments. Do you want to limit their size?(y/n)\n")
                Correct = False
                x += 1
                while not Correct:
                    option = input()
                    if option == "y" or option == "yes":
                        print("Introduce a size in bytes\n")
                        correct = False
                        while not correct:
                            dymaxsize = int(input())
                            if dymaxsize>=32 and dymaxsize%32 == 0:
                                correct= True
                            else:
                                print("Introduce a number bigger than 32 and multiple of 32")

                        size, dy, final_arguments = calling.checklentotalmessage(arguments[-1], maxsize = (dymaxsize * 2))
                        arguments[-1] =  copy.deepcopy(final_arguments)
                        Correct = True
                        option = "yes"
                    elif option == "n" or option == "no":
                        Correct = True
                        option = "no"
                    else:
                        print ("Option not valid\n")
            if option == "no" or default:
                arguments = [arguments[-1]]
                original_arguments = [original_arguments[-1]]
                sizes = [size]
                dynamic = [dy]
                contract_index = i
                found = True
            else:
                size, dy,final_arguments = calling.checklentotalmessage(arguments[-1], maxsize = (dymaxsize * 2))
                arguments[i] = copy.deepcopy(final_arguments)
                sizes.append(size)
                dynamic.append(dy)
        i += 1
    return arguments, sizes, dynamic, contract_index,original_arguments

def getcontractcoderead(origin):
    """
    Return the alphabetically sorted code in a solidity code file
    """
    data = []
    if os.path.isfile(origin) and origin[-4:] == ".sol":
        data_aux, c = code.readCodeFile(origin)
        data.append(data_aux)
    else:
        directory = sorted(os.listdir(origin))
        i = 0
        while i < len(directory):
            if directory[i][-4:] == ".sol":
                data_aux, c = code.readCodeFile(os.path.join(origin, directory[i]))
                data.append(data_aux)
            i += 1
    #print(sorted(data, key=len))
    return data

def getcontractcode(origin):
    """
    Retrieve the Solidity code from contracts in origin ordered by contract length
    """
    data = []
    contract_to_use = []
    aux = {}
    if os.path.isfile(origin) and origin[-4:] == ".sol":
        data_aux, contract_to_use_aux = code.readCodeFile(origin)
        data.append(data_aux)
        aux [data_aux] = contract_to_use_aux + ".bin"
        #contract_to_use.append(contract_to_use_aux + ".bin")
    else:
        directory = sorted(os.listdir(origin))
        i = 0
        while i < len(directory):
            if directory[i][-4:] == ".sol":
                data_aux, contract_to_use_aux =code.readCodeFile(os.path.join(origin, directory[i]))
                data.append(data_aux)
                aux [data_aux] = contract_to_use_aux + ".bin"
                #contract_to_use.append(contract_to_use_aux+".bin")
            i += 1
    #print(sorted(data, key=len))
    data_ordered = sorted(data, key=len)[::-1]
    for x in data_ordered:
        contract_to_use.append(aux[x])
    return data_ordered, contract_to_use

def getContractaddresses(tx_hashes, connection, x):
    """
    Retrieve the contract address from a contract deployment transaction hash
    """
    contract_addresses = []
    for i in tx_hashes:
        if connection == "w3":
            from ..connection import w3mode
            contract_addresses.append(w3mode.w3checkContractDeployment(i, x))
        else:
            from ..connection import rpcmode
            contract_addresses.append(rpcmode.rpccheckContractDeployment(i, x, 0))
    return contract_addresses

# def getlengthscombined(data, contract_to_use, contracts, default):
#     len_spaces = []
#     #len_constructor = []
#     arguments = []
#     sizes = []
#     dynamic = []
#     contract_index = []
#     for i in range(len(data)):
#         len_spaces.append(code.countspaces(data[i]))
#     #Check from contructor arguments
#     #print("hey", len(contracts))
#     # for i in range(len(contracts)):
#     #     print("hey2")
#     #     print(contracts[i])
#     arguments, sizes, dynamic, contract_index = checkcontractsconstructor(contracts, default, check = True)
#         # arguments.append(argu)
#         # sizes.append(siz)
#         # dynamic.append(dy)
#         # contract_index.append(c)
#     return  arguments, sizes, dynamic, contract_index , len_spaces
