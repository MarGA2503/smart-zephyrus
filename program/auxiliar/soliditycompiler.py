import subprocess
import json
import ast
import os



def installcompilerversion (pragma):
    call = "solc-select install 0.8." + str(pragma)
    FNULL = open(os.devnull, 'wb')
    result = subprocess.run(call , shell=True, check=True, stdout=subprocess.PIPE, stderr=FNULL)
    #installed = json.loads(result.stdout.decode('utf-8'))
    print(result.stdout.decode('ascii'))

def selectcompilerverison (pragma):
    call = "solc-select use 0.8." + str(pragma)
    FNULL = open(os.devnull, 'wb')
    result = subprocess.run(call , shell=True, check=True,stdout=subprocess.PIPE, stderr=FNULL)
    #installed = json.loads(result.stdout.decode('utf-8'))
    print(result.stdout.decode('ascii'))

def checkcompilerverison (pragma):
    call = "solc-select versions"
    FNULL = open(os.devnull, 'wb')
    result = subprocess.run(call , shell=True, check=True,stdout=subprocess.PIPE, stderr=FNULL)
    #installed = json.loads(result.stdout.decode('utf-8'))
    r = result.stdout.decode('ascii')
    if '0.8.' + str(pragma) in r:
        return True
    else:
        return False

def compilecontract (contract_name):
    call = "solc " + contract_name+ ".sol --bin -o '" + contract_name+"' --overwrite"
    FNULL = open(os.devnull, 'wb')
    result = subprocess.run(call , shell=True, check=True, stdout=subprocess.PIPE, stderr=FNULL)
    #installed = json.loads(result.stdout.decode('utf-8'))
    print(result.stdout.decode('ascii'))

def compilecontracts (contract_names, pragmas):
    for i in range(len(pragmas)):
        installed = checkcompilerverison(pragmas[i])
        if not installed:
            installcompilerversion (pragmas[i])
        selectcompilerverison(pragmas[i])
        #print("****",i,pragmas[i])
        compilecontract (contract_names[i])
