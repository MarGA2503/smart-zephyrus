#!/usr/bin/env python
"""
Contains all functions related with ciphering and decrypting the message
"""
import random
#TODO check if in crypto o cryptodome
from Crypto.Cipher import AES
from Crypto.Cipher import ARC4
from Crypto.Hash import SHA512
from Crypto.Protocol.KDF import PBKDF2
from Crypto.Cipher import ChaCha20

__author__ = "Mar Gimenez Aguilar"
__version__ = "1"
__maintainer__ = "Mar Gimenez Aguilar"
__email__ = "mar.gimeneza@gmail.com"

nonce = 0
nonces = []
nonce_stretched = b''

def generateKeyNonce (key):
    """
    Generate a key from an existing key
    """
    z = int.from_bytes( (key), byteorder='little')
    q = z ** 2
    x = hex(z ^ q)[2:]
    if len(x) % 2 != 0:
        x = x + '0'
    return bytes.fromhex(x)

def strechKey (key, nonce="",length=32):
    """
    Stretch a given key, using a given nonce
    Returns a key of given length
    """
    random.seed(key)
    if type(key) == str:
        key_bytes =  str.encode(key)
    elif type(key) == int:
        key_bytes = bytes(key)
    else:
        key_bytes = key
    if nonce == "":
        nonce = bytes(bytearray.fromhex(''.join([hex(random.randint(0,15))[2:] for i in range(32)])))
    else:
        noncehex= hex(nonce)[2:]
        if len(noncehex) % 2 != 0:
            noncehex = '0' + noncehex
        #rest = 32 - len(noncehex)
        if len(noncehex) > 32:
            completenonce = noncehex[-32:]
        # elif len(noncehex) % 32 != 0:
        #     completenonce = '0' * rest + noncehex
        else:
            rest = 32 - len(noncehex)
            completenonce = noncehex + ''.join([hex(random.randint(0,15))[2:] for i in range(32)])
        nonce = bytes(bytearray.fromhex( completenonce))
    key_streched =  PBKDF2(key_bytes, nonce, dkLen=length, hmac_hash_module=SHA512)
    return key_streched

def generateRC4key (key):
    """
    Generates a RC4 key from a given key
    """
    key_for_nonce = generateKeyNonce(key)
    #print("keyf",key_for_nonce)
    #key_for_cipher = strechKey(key_for_nonce, nonce = nonce)
    return key_for_nonce

def generateChaCha20key (key):
    """
    Generates a Chacha20 key from a given key
    """
    keytmp = strechKey(key,nonce="")
    key_for_nonce = generateKeyNonce(keytmp)
    #print("keyf",key_for_nonce)
    #key_for_cipher = strechKey(key_for_nonce, nonce = nonce)
    return key_for_nonce

def upgradeRC4key (key):
    """
    Upgrade the current RC4 key using the nonce of the transaction
    """
    global nonce
    print("nonce RC4", nonce)
    if type(nonce) == int:
        key_for_cipher = strechKey(key, nonce = nonce)
        print("RC4 key",key_for_cipher)
        nonce += 1
    else:
        key_for_cipher = strechKey(key, nonce = nonce[0])
        print("RC4 key",key_for_cipher)
        nonce.pop(0)
    return key_for_cipher

def upgradeChaCha20key (key):
    """
    Upgrade the current ChaCha20 key using the nonce of the transaction
    """
    global nonce
    global nonce_stretched
    print("nonce transaction", nonce)
    if type(nonce) == int:
        key_for_cipher = strechKey(key, nonce = nonce)
        nonce_stretched = strechKey(nonce, nonce = "",length = 12 )
        print("ChaCha20 key",key_for_cipher)
        print("ChaCha20 nonce",nonce_stretched)
        nonce += 1
    else:
        key_for_cipher = strechKey(key, nonce = nonce[0])
        nonce_stretched = strechKey(nonce[0], nonce = "",length = 12 )
        print("ChaCha20 key",key_for_cipher)
        print("ChaCha20 nonce",nonce_stretched)
        nonce.pop(0)
    return key_for_cipher

def cipherRC4 (plaintext, key, type):
    """
    Cipher a plaintext using a key and RC4 algorithm
    """
    cipher = ARC4.new(key)
    if type == "hex":
        plaintext = bytes.fromhex(plaintext)
    else:
        plaintext = str.encode(plaintext)
    ciphertext = cipher.encrypt(plaintext)
    if type == "hex":
        ciphertext = ciphertext.hex()
    else:
        ciphertext = ciphertext.decode()
    return ciphertext

def cipherChaCha20 (plaintext, key, type):
    """
    Cipher a plaintext using a key and ChaCha20 algorithm
    """
    global nonce_stretched
    cipher = ChaCha20.new(key=key, nonce=nonce_stretched)
    if type == "hex":
        plaintext = bytes.fromhex(plaintext)
    else:
        plaintext = str.encode(plaintext)
    ciphertext = cipher.encrypt(plaintext)
    if type == "hex":
        ciphertext = ciphertext.hex()
    else:
        ciphertext = ciphertext.decode()
    return ciphertext

def ecipherChaCha20 (ciphertext, key, type):
    """
    Decrypt a ciphertext encrypted with ChaCha20 with a given key
    """
    global nonce_stretched
    cipher = ChaCha20.new(key=key, nonce=nonce_stretched)
    if type == "hex":
        ciphertext = bytes.fromhex(ciphertext)
    else:
        ciphertext = str.encode(ciphertext)
    plaintext = cipher.decrypt(ciphertext)
    if type == "hex":
        plaintext = plaintext.hex()
    else:
        plaintext = plaintext.decode()
    return plaintext

def ecipherRC4 (ciphertext, key, type):
    """
    Decrypt a ciphertext encrypted with RC4 with a given key
    """
    cipher = ARC4.new(key)
    if type == "hex":
        ciphertext = bytes.fromhex(ciphertext)
    else:
        ciphertext = str.encode(ciphertext)
    plaintext = cipher.decrypt(ciphertext)
    if type == "hex":
        plaintext = plaintext.hex()
    else:
        plaintext = plaintext.decode()
    return plaintext

def cipherAES (plaintext, key, nonceAES, type):
    """
    Cipher a plaintext using AES with a given key and nonce
    """
    nonce = strechKey(nonceAES, nonce="", length = 8)
    #print("nonce", nonce, key)
    #key = strechKey(key,nonce="")
    if type == "hex":
        plaintext = bytes.fromhex(plaintext)
    else:
        plaintext = str.encode(plaintext)
    cipher = AES.new(key, AES.MODE_CTR, nonce=nonce)
    ciphertext = cipher.encrypt(plaintext)
    if type == "hex":
        ciphertext = ciphertext.hex()
    else:
        ciphertext = ciphertext.decode()
    return ciphertext

def ecipherAES(ciphertext, key, nonceAES, type):
    """
    Decrypt a ciphertext encrypted using AES and a given key and nonce
    """
    nonce = strechKey(nonceAES,nonce="", length = 8)
    #key = strechKey(key,nonce="")
    if type == "hex":
        ciphertext = bytes.fromhex(ciphertext)
    else:
        ciphertext = str.encode(ciphertext)
    cipher = AES.new(key, AES.MODE_CTR, nonce=nonce)
    plaintext = cipher.decrypt(ciphertext)
    if type == "hex":
        plaintext = plaintext.hex()
    else:
        plaintext = plaintext.decode()
    return plaintext
