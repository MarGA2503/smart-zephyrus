#!/usr/bin/env python
"""
Contains functions that interacts with the message and modifies it
"""

import random
import binascii
import copy
import math
import sys
from . import cipherfunctions
from . import globalsettings
from . import getcheck
from ..methods import calling

__author__ = "Mar Gimenez Aguilar"
__version__ = "1"
__maintainer__ = "Mar Gimenez Aguilar"
__email__ = "mar.gimeneza@gmail.com"

#Global variables declarations
max_size_value_bits = 3
#value_lens= {17:2157122, 18:2926517}
value_lens= {17:2157122}
function_uint256_lens = {22:1020206}
function_uint256_zeros_lens = {22:{18:1}}
constructor_uint256_lens = {10:439}
constructor_uint256_zeros_lens = {10:{2:1}}
#value_zeros_lens = {17:{10:1}, 18:{10:1}}
value_zeros_lens = {17:{10:1}}
max_number_bits_value = 22
from_gas_price_transaction = {
1000000000:'0000',
20000000000:'0001',
10000000000:'0010',
4000000000:'0011',
21000000000:'0100',
5000000000:'0101',
50000000000:'0110',
30000000000:'0111',
2000000000:'1000',
3000000000:'1001',
40000000000:'1010',
15000000000:'1011',
6000000000:'1100',
25000000000:'1101',
8000000000:'1110',
100000000:'1111'
}
to_gas_price_transaction = {
'0000':1000000000,
'0001':20000000000,
'0010':10000000000,
'0011':4000000000,
'0100':21000000000,
'0101':5000000000,
'0110':50000000000,
'0111':30000000000,
'1000':2000000000,
'1001':3000000000,
'1010':40000000000,
'1011':15000000000,
'1100':6000000000,
'1101':25000000000,
'1110':8000000000,
'1111':100000000,
}

to_gas_limit_transaction = {
'000':1600000,
'001':210000,
'010':1000000,
'011':400000,
'100':285839,
'101':1200000,
'110':280000,
'111':3000000
}

from_gas_limit_transaction = {
1600000:'000',
210000:'001',
1000000:'010',
400000:'011',
285839:'100',
1200000:'101',
280000:'110',
3000000:'111'
}

def preparegasprice(data, current, length, ChaCha20key, password = ""):
    message_prepared = []
    #print(data)
    data_length_hex = hex(len(data))[2:]
    if len(data_length_hex)% 2 != 0:
        data_length_hex = '0' + data_length_hex
    len_data_length = hex(len(data_length_hex))[2:]
    if len(len_data_length) %2 != 0:
        len_data_length = '0' + len_data_length
    #print(data_length_hex, len_data_length)
    if ChaCha20key != "":
        auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
        len_data_length = cipherfunctions.cipherChaCha20(len_data_length, auxChaCha20key, "hex")
        data_length_hex =  cipherfunctions.cipherChaCha20(data_length_hex, auxChaCha20key, "hex")
        #print(data_length_hex, len_data_length)
        data = len_data_length + data_length_hex + data
    else:
        data = len_data_length + data_length_hex + data
    #TODO: ask number od max divisions, for now 15 (can be 255 but dificult to keep track)
    # 2--> f and f
    #print(data, current, len(data),len(current), length)
    blocks, r = divmod (len(data),(length - len(hex(globalsettings.max_number_transactions)[2:]*2)))
    #print(blocks,r)
    # If the rest is not 0, another block and padding
    #print(blocks, r)
    rando = ""
    if r != 0:
        blocks += 1
        rest = (length -len(hex(globalsettings.max_number_transactions)[2:]*2)) - r
        random.seed()
        for x in range(rest):
            rando += hex(random.randint(0, 15))[2:]
    #print(data)
    data += rando
    #print(data)
    #print(data)
    #TODO: for now only 15 blocks permited
    if blocks > globalsettings.max_number_transactions:
        print("Please, reduce the size of the message and try again\n")
        exit(1)
    total_blocks_hex = hex(blocks)[2:]
    if len(total_blocks_hex) %2 != 0:
        total_blocks_hex = '0' + total_blocks_hex
    x = 0
    i = 0
    for i in range(blocks):
        aux = hex((i+1))[2:]
        if len(aux)%2 != 0:
            aux = '0' + aux
        if ChaCha20key != "":
            auxtotal =  cipherfunctions.cipherChaCha20(aux + total_blocks_hex, auxChaCha20key, "hex")
            message_prepared.append(current[0] + auxtotal + data[(i * (length - len(hex(globalsettings.max_number_transactions)[2:])*2)):((length - len(hex(globalsettings.max_number_transactions)[2:])*2)*(i+1))])
            #print("auxChaCha20key", auxChaCha20key)
            #print(current[0],auxtotal, (i * (length - len(hex(globalsettings.max_number_transactions)[2:])*2)), ((length - len(hex(globalsettings.max_number_transactions)[2:])*2)*(i+1)),  data[(i * (length - len(hex(globalsettings.max_number_transactions)[2:])*2)):((length - len(hex(globalsettings.max_number_transactions)[2:])*2)*(i+1))])
            auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
        else:
            message_prepared.append(current[0] + aux + total_blocks_hex + data[(i * (length - len(hex(globalsettings.max_number_transactions)[2:])*2)):((length - len(hex(globalsettings.max_number_transactions)[2:])*2)*(i+1))])
        if int(message_prepared[-1],16) < int(current,16):
            #print(message_prepared[-1], current)
            message_prepared[-1]= hex(int(message_prepared[-1][0],16)+1)[2:] + message_prepared[-1][1:]
            #print(message_prepared[-1], current)
    return message_prepared
# def preparegasprice(data, current, length, ChaCha20key, password = ""):
#     message_prepared = []
#     print(data)
#     data_length_hex = hex(len(data))[2:]
#     if len(data_length_hex)% 2 != 0:
#         data_length_hex = '0' + data_length_hex
#     len_data_length = hex(len(data_length_hex))[2:]
#     if len(len_data_length) %2 != 0:
#         len_data_length = '0' + len_data_length
#     #print(data_length_hex, len_data_length)
#     if ChaCha20key != "":
#         auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
#         len_data_length = cipherfunctions.cipherChaCha20(len_data_length, auxChaCha20key, "hex")
#         data_length_hex =  cipherfunctions.cipherChaCha20(data_length_hex, auxChaCha20key, "hex")
#         print(data_length_hex, len_data_length)
#         data = len_data_length + data_length_hex + data
#     else:
#         data = len_data_length + data_length_hex + data
#     #TODO: ask number od max divisions, for now 15 (can be 255 but dificult to keep track)
#     # 2--> f and f
#     print(data, current, len(data),len(current))
#     blocks, r = divmod (len(data),(length - 2))
#     print(blocks,r)
#     #If the rest is not 0, another block and padding
#     #print(blocks, r)
#     # rando = ""
#     # if r != 0:
#     #     blocks += 1
#     #     rest = (length -2) - r
#     #     random.seed()
#     #     for x in range(rest):
#     #         rando += hex(random.randint(0, 15))[2:]
#     # #print(data)
#     # data += rando
#     print(data)
#     #print(data)
#     #TODO: for now only 15 blocks permited
#     if blocks > 15:
#         print("Please, reduce the size of the message and try again\n")
#         exit(1)
#     total_blocks_hex = hex(blocks)[2:]
#     x = 0
#     i = 0
#     while i < 15 and data != "":
#         aux = hex((i+1))[2:]
#         print(data)
#         if ChaCha20key != "":
#             auxtotal =  cipherfunctions.cipherChaCha20(aux + total_blocks_hex, auxChaCha20key, "hex")
#             #message_prepared.append(auxtotal + data[(i * (length -2)):((length -2)*(i+1))])
#             message_prepared.append( data[:(length -2)][::-1] + auxtotal)
#             data = data[(length -2):]
#             if int(message_prepared[-1],16)< int(current,16) and data !="":
#                 message_prepared[-1] = data[0] + message_prepared[-1]
#                 data = data[1:]
#             while message_prepared[-1][0]== '0':
#                 print(message_prepared[-1][0])
#                 if data != "" and len(message_prepared[-1])!= 64 and data[0] != '0':
#                     message_prepared[-1] = data[0] + message_prepared[-1]
#                     data = data[1:]
#                 elif len(message_prepared[-1]) < 64 and data == "":
#                     message_prepared[-1] = + hex(random.randint(0, 15))[2:] + message_prepared[-1]
#                 else:
#                     message_prepared[-1] = message_prepared[-1][1:]
#                     data = message_prepared[-1][0] + data
#             # while int(message_prepared[-1],16) > block_limit:
#             #     message_prepared[-1] = message_prepared[-1][1:]
#             #     data = message_prepared[-1][0] + data
#
#             #print("auxChaCha20key", auxChaCha20key)
#             auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
#         else:
#             message_prepared.append(aux + total_blocks_hex + data[:(length -2)])
#             message_prepared.append( data[:(length -2)][::-1] + auxtotal)
#             data = data[(length -2):]
#             if int(message_prepared[-1],16)< int(current,16) and data !="":
#                 message_prepared[-1] = data[0] + message_prepared[-1]
#                 data = data[1:]
#             while message_prepared[-1][0]== '0':
#                 if data != "" and len(message_prepared[-1])!= 64 and data[0] != '0':
#                     message_prepared[-1] = data[0] + message_prepared[-1]
#                     data = data[1:]
#                 elif len(message_prepared[-1]) < 64 and data == "":
#                     message_prepared[-1] =  hex(random.randint(0, 15))[2:] + message_prepared[-1]
#                 else:
#                     message_prepared[-1] = message_prepared[-1][1:]
#                     data = message_prepared[-1][0] + data
#         #x += (x+(length - 2))
#         #print(x)
#         i += 1
#     if data != "":
#         print("Please, reduce the size of the message and try again\n")
#         exit(1)
#     while int(message_prepared[-1],16)<int(current,16):
#         message_prepared[-1] = hex(random.randint(0, 15))[2:] + message_prepared[-1]
#     return message_prepared

def gaspriceorderblocks(data_in_transactions, ChaCha20key):
    """
    Orders the transactions passed with hex information
    """
    completed = False
    data_ordered = copy.deepcopy(data_in_transactions)
    cont = 0
    # if ChaCha20key != "":
    #     totalaux = cipherfunctions.ecipherChaCha20 (data_in_transactions[0][:2], ChaCha20key, "hex")
    #     print(totalaux)
    #     total = int(totalaux[1], 16)
    # else:
    #     total = int(data_in_transactions[0][1], 16)
    for x in data_in_transactions:
        if ChaCha20key != "":
            totalaux = cipherfunctions.ecipherChaCha20 (x[1:(len(hex(globalsettings.max_number_transactions)[2:])*2 +1)], cipherfunctions.upgradeChaCha20key(ChaCha20key), "hex")
            index = int(totalaux[:len(hex(globalsettings.max_number_transactions)[2:])], 16)
            total = int(totalaux[len(hex(globalsettings.max_number_transactions)[2:]):], 16)
            print("Transaction {} of {}".format(index,total))
        else:
            index = int(x[1:(len(hex(globalsettings.max_number_transactions)[2:])+1)], 16)
            total = int(x[(len(hex(globalsettings.max_number_transactions)[2:])+1):(len(hex(globalsettings.max_number_transactions)[2:])*2 +1)], 16)
        data_ordered[(index-1)] = x
        cont += 1
    if cont != total:
        print("Some transactions are missing\n")
        exit(1)
    return data_ordered

def checkifpartofmessagegasprice (data_to_retrieve, nonces_tx, data_in_transactions, cumulative_nonces, ChaCha20key):
    i = 0
    added= 0
    while i < len(data_to_retrieve):
        if type(data_to_retrieve[i]) == int:
            data_to_retrieve[i] = hex(data_to_retrieve[i])[2:]
        if ChaCha20key != "":
            totalaux = cipherfunctions.ecipherChaCha20 (data_to_retrieve[i][1:(len(hex(globalsettings.max_number_transactions)[2:])*2 +1)], cipherfunctions.upgradeChaCha20key(ChaCha20key), "hex")
            index = int(totalaux[:len(hex(globalsettings.max_number_transactions)[2:])], 16)
            total = int(totalaux[len(hex(globalsettings.max_number_transactions)[2:]):], 16)
        else:
            index = int(data_to_retrieve[i][1:(len(hex(globalsettings.max_number_transactions)[2:])+1)], 16)
            total = int(data_to_retrieve[i][len(hex(globalsettings.max_number_transactions)[2:]):], 16)
        if total == len(data_in_transactions) and index > 0 and index <= len(data_in_transactions):
            data_in_transactions[(index-1)] = data_to_retrieve[i]
            cumulative_nonces[(index-1)] = nonces_tx[i]
            print("Transaction {} of {}".format(index,total))
            added += 1
        i += 1
    return data_in_transactions ,cumulative_nonces, added

def checkifsinglegasprice (data_in_transactions, ChaCha20key):
    """
    Check if the hash passed is part of a single-transaction message,
    is one of the transactions that contain the message or it does not
    seem to contain a message
    """
    single = False
    withmessage = True
    data_array = copy.deepcopy(data_in_transactions)
    total = 0
    # if ChaCha20key != "":
    #     totalaux = cipherfunctions.ecipherChaCha20 (data_in_transactions[0][:2], ChaCha20key, "hex")
    #     print(totalaux)
    #     total = int(totalaux[1], 16)
    # else:
    #     total = int(data_in_transactions[0][1], 16
    if ChaCha20key != "":
        totalaux = cipherfunctions.ecipherChaCha20 (data_in_transactions[0][1:(len(hex(globalsettings.max_number_transactions)[2:])*2 +1)], cipherfunctions.upgradeChaCha20key(ChaCha20key), "hex")
        index = int(totalaux[:len(hex(globalsettings.max_number_transactions)[2:])], 16)
        total = int(totalaux[len(hex(globalsettings.max_number_transactions)[2:]):], 16)
    else:
        index = int(data_in_transactions[0][1:(len(hex(globalsettings.max_number_transactions)[2:])+1)], 16)
        total = int(data_in_transactions[0][len(hex(globalsettings.max_number_transactions)[2:]):], 16)
    #print(index,total)
    if index < 1 or index > total or total < 1:
        withmessage = False
        print("This transaction hash does not seem to contain a message\n")
    else:
        if total == 1:
            single = True
            data_array[(index-1)] = data_in_transactions[0]
        else:
            data_array = ['0'] * total
            data_array[(index-1)] = data_in_transactions[0]
        print("Transaction {} of {}".format(index,total))
    return single, data_array, total, index, withmessage


# def gaspriceextract(data_in_transactions, to_skip, len_to_read, ChaCha20key, password = ""):
#     message = ""
#     total_length = 0
#     for x in range(len(data_in_transactions)):
#         #Extract total length of the message:
#         aux = data_in_transactions[x][:-to_skip]
#         message += aux
#     if ChaCha20key!= "":
#         auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
#         len_of_length = cipherfunctions.ecipherChaCha20(message[0:len_to_read], auxChaCha20key, "hex")
#         len_of_length = int(len_of_length,16)
#         total_length = cipherfunctions.ecipherChaCha20(message[len_to_read:(len_to_read+len_of_length)], auxChaCha20key, "hex")
#         total_length = int(total_length,16)
#     else:
#         len_of_length = int(message[0:len_to_read],16)
#         total_length = int(message[len_to_read:(len_to_read+len_of_length)], 16)
#     message = message[(len_to_read+len_of_length):]
#     message = message[:total_length]
#     return message

def gasorderblocks(data_in_transactions, ChaCha20key, length):
    """
    Orders the transactions passed with hex information
    """
    completed = False
    data_ordered = copy.deepcopy(data_in_transactions)
    cont = 0
    # if ChaCha20key != "":
    #     totalaux = cipherfunctions.ecipherChaCha20 (data_in_transactions[0][:2], ChaCha20key, "hex")
    #     print(totalaux)
    #     total = int(totalaux[1], 16)
    # else:
    #     total = int(data_in_transactions[0][1], 16)
    for x in data_in_transactions:
        if len(x) == length:
            x = x[1:]
        elif len(x) == (length - 2):
            x = '0' + x
        if ChaCha20key != "":
            totalaux = cipherfunctions.ecipherChaCha20 (x[:len(hex(globalsettings.max_number_transactions)[2:])*2], cipherfunctions.upgradeChaCha20key(ChaCha20key), "hex")
            index = int(totalaux[:len(hex(globalsettings.max_number_transactions)[2:])], 16)
            total = int(totalaux[len(hex(globalsettings.max_number_transactions)[2:]):], 16)
            print("Transaction {} of {}".format(index,total))
        else:
            index = int(x[:len(hex(globalsettings.max_number_transactions)[2:])], 16)
            total = int(x[len(hex(globalsettings.max_number_transactions)[2:]):len(hex(globalsettings.max_number_transactions)[2:])*2], 16)
        data_ordered[(index-1)] = x
        cont += 1
    if cont != total:
        print("Some transactions are missing\n")
        exit(1)
    return data_ordered

def checkifpartofmessagegas (data_to_retrieve, nonces_tx, data_in_transactions, cumulative_nonces, ChaCha20key,length):
    i = 0
    added= 0
    while i < len(data_to_retrieve):
        if type(data_to_retrieve[i]) == int:
            data_to_retrieve[i] = hex(data_to_retrieve[i])[2:]
        if len(data_to_retrieve[i]) == length:
            data_to_retrieve[i] = data_to_retrieve[i][1:]
        elif len((data_to_retrieve[i]))%2 == (length - 2):
            data_to_retrieve[i] = '0'+ data_to_retrieve[i]
        if ChaCha20key != "":
            totalaux = cipherfunctions.ecipherChaCha20 (data_to_retrieve[i][:len(hex(globalsettings.max_number_transactions)[2:])*2], cipherfunctions.upgradeChaCha20key(ChaCha20key), "hex")
            index = int(totalaux[:len(hex(globalsettings.max_number_transactions)[2:])], 16)
            total = int(totalaux[len(hex(globalsettings.max_number_transactions)[2:]):], 16)
        else:
            index = int(data_to_retrieve[0][:len(hex(globalsettings.max_number_transactions)[2:])], 16)
            total = int(data_to_retrieve[0][len(hex(globalsettings.max_number_transactions)[2:]):len(hex(globalsettings.max_number_transactions)[2:])*2], 16)
        if total == len(data_in_transactions) and index > 0 and index <= len(data_in_transactions):
            data_in_transactions[(index-1)] = data_to_retrieve[i]
            cumulative_nonces[(index-1)] = nonces_tx[i]
            print("Transaction {} of {}".format(index,total))
            added += 1
        i += 1
    return data_in_transactions ,cumulative_nonces, added


def checkifsinglegas (data_in_transactions, ChaCha20key,length):
    """
    Check if the hash passed is part of a single-transaction message,
    is one of the transactions that contain the message or it does not
    seem to contain a message
    """
    single = False
    withmessage = True
    data_array = copy.deepcopy(data_in_transactions)
    total = 0
    # if ChaCha20key != "":
    #     totalaux = cipherfunctions.ecipherChaCha20 (data_in_transactions[0][:2], ChaCha20key, "hex")
    #     print(totalaux)
    #     total = int(totalaux[1], 16)
    # else:
    #     total = int(data_in_transactions[0][1], 16
    if len(data_in_transactions[0]) == length:
        data_in_transactions[0] = data_in_transactions[0][1:]
    elif len(data_in_transactions[0]) == (length - 2):
        data_in_transactions[0] = '0' + data_in_transactions[0]
    if ChaCha20key != "":
        totalaux = cipherfunctions.ecipherChaCha20 (data_in_transactions[0][:len(hex(globalsettings.max_number_transactions)[2:])*2], cipherfunctions.upgradeChaCha20key(ChaCha20key), "hex")
        index = int(totalaux[:len(hex(globalsettings.max_number_transactions)[2:])], 16)
        total = int(totalaux[len(hex(globalsettings.max_number_transactions)[2:]):], 16)
    else:
        index = int(data_in_transactions[0][:len(hex(globalsettings.max_number_transactions)[2:])], 16)
        total = int(data_in_transactions[0][len(hex(globalsettings.max_number_transactions)[2:]):len(hex(globalsettings.max_number_transactions)[2:])*2], 16)
    #print(index,total)
    if index < 1 or index > total or total < 1:
        withmessage = False
        print("This transaction hash does not seem to contain a message\n")
    else:
        if total == 1:
            single = True
            data_array[(index-1)] = data_in_transactions[0]
        else:
            data_array = ['0'] * total
            data_array[(index-1)] = data_in_transactions[0]
        print("Transaction {} of {}".format(index,total))
    return single, data_array, total, index, withmessage

def gasLimitAdjustment (message_prepared, estimateGas):
    """
    Check that the transaction would not run out of gas
    """
    for i in range(len(message_prepared)):
        if message_prepared[i][0] == '0':
            message_prepared[i] = '1'+ message_prepared[i]
        message_int = int(message_prepared[i],16)
        if type(estimateGas[i]) != int:
            estimateGas_int = int(estimateGas[i],16)
        else:
            estimateGas_int = estimateGas[i]
        #We check if the transaction has enough gas
        # print(message_int, estimateGas_int)
        if message_int < estimateGas_int:
            if type(estimateGas[i]) != int:
                if len(message_prepared[i]) == len(estimateGas[i][2:]):
                    message_prepared[i] = '1' + message_prepared[i]
                else:
                    message_prepared[i] = estimateGas[i][2] + message_prepared[i]
                    message_int = int(message_prepared[i],16)
                    if message_int < estimateGas_int:
                        new = int(estimateGas[i][2],16) +1
                        message_prepared[i] = hex(new)[2:] + message_prepared[i]
            else :
                if len(message_prepared[i]) == len(hex(estimateGas[i])[2:]):
                    message_prepared[i] = '1' + message_prepared[i]
                else:
                    message_prepared[i] = hex(estimateGas[i])[2] + message_prepared[i]
                    message_int = int(message_prepared[i],16)
                    if message_int < estimateGas_int:
                        new = int(hex(estimateGas[i])[2],16) +1
                        message_prepared[i] = hex(new)[2:] + message_prepared[i]
    return message_prepared


def insertextensiondata (data, file_format):
    """
    Inserts the original file extension in the message
    """
    if len(file_format) > 4:
        file_format = file_format[1:]
    file_format_hex = file_format.encode("utf-8").hex()
    data = file_format_hex + data
    return data

def insertextensionbin (TextVBin, file_format):
    """
    Inserts the original file extension in the message
    """
    if len(file_format) > 4:
        file_format = file_format[1:]
    file_format_hex = file_format.encode("utf-8").hex()
    file_fomat_byte = binascii.unhexlify(file_format_hex)
    file_fomat_bits = ''.join('{:08b}'.format(x) for x in file_fomat_byte)
    TextVBin = file_fomat_bits + TextVBin
    return TextVBin

#TODO:differentiate in transactiondata
def disordermessage(message_prepared, key):
    """
    Disorders the message using a key as seed
    """
    # random.seed(key)
    # orderMessage = random.sample(list(range(0,len(message_prepared[0]))),len(message_prepared[0]))
    # print(orderMessage)
    for x in range(len(message_prepared)):
        random.seed(key)
        orderMessage = random.sample(list(range(0,len(message_prepared[x]))),len(message_prepared[x]))
        newmessage = ""
        for i in orderMessage:
            newmessage += message_prepared[x][i]
        message_prepared[x] = newmessage
    return message_prepared

def ordermessage(message_disordered, key):
    """
    Returns the message to its original order using a key
    """
    # random.seed(key)
    # orderMessage = random.sample(list(range(0,len(message_disordered[0]))),len(message_disordered[0]))
    for x in range(len(message_disordered)):
        random.seed(key)
        orderMessage = random.sample(list(range(0,len(message_disordered[x]))),len(message_disordered[x]))
        newmessage = list(copy.deepcopy(message_disordered[x]))
        z = 0
        # print(newmessage)
        # print(orderMessage)
        for i in orderMessage:
            newmessage[i] = message_disordered[x][z]
            z += 1
            #print(newmessage)
        message_disordered[x] = ''.join(newmessage)
    return message_disordered
# def calculateBlocksCombined(len_spaces, sizes, dynamic,constructor, message, ChaCha20key, default, divided = 0):
#     number = 0
#     message_code = []
#     message_swarm = []
#     message_gas = []
#     message_constructor_lengths= []
#     message_constructor_data = []
#     message_gas = []
#     lengthsmin = []
#     form = '00'
#     i = 0
#     if constructor:
#         if  len(dynamic[-1]) > 0:
#             blocks = 1
#             if default:
#                 form = '00'
#                 lengthsmin = []
#             else:
#                 print("Do you want to distribute the message weight between the arguments?(y/n)\n")
#                 Correct = False
#                 while not Correct:
#                     option = input()
#                     if option == "y" or option == "yes":
#                         form = '01'
#                         Correct = True
#                     elif option == "n" or option == "no":
#                         form = '00'
#                         Correct = True
#                     else:
#                         print ("Option not valid\n")
#         else:
#             if default:
#                  form = '10'
#             else:
#                 print("Do you want to distribute the message weight between the arguments?(y/n)\n")
#                 Correct = False
#                 while not Correct:
#                     option = input()
#                     if option == "y" or option == "yes":
#                         form = '11'
#                         Correct = True
#                     elif option == "n" or option == "no":
#                         form = '10'
#                         Correct = True
#                     else:
#                         print ("Option not valid\n")
#     while number < 15 and message != "":
#         auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
#         number += 1
#         print("message", message)
#         TextVBin = ''.join('{:08b}'.format(x) for x in bytes.fromhex(message))
#         #Keeping them even in case of constructor
#         len_data = math.floor(len_spaces[i%len(len_spaces)]/(8))
#         #Keeping them even in case of constructor
#         # if len_data %2 != 0:
#         #     len_data -= 1
#         if ChaCha20key != "":
#             len_message = hex(len_data)[2:]
#             if len(len_message) % 2 != 0:
#                 len_message = '0' + len_message
#             len_len_message = hex(len(len_message))[2:]
#             lens = []
#             if len(len_len_message) % 2 != 0:
#                 len_len_message = '0' + len_len_message
#             extra = (len_len_message) + (len_message) + 'ff'
#             extra_bin = binascii.unhexlify(extra)
#             extra_bits = ''.join('{:08b}'.format(z) for z in extra_bin)
#             print(len_data, len(extra_bits), extra_bits)
#             if (len_data * 8) - len(extra_bits) > len(TextVBin):
#                 aux = bytearray([int(TextVBin[i:i+8], 2) for i in range(0, len(TextVBin), 8)]).hex()
#                 len_message = hex(len(aux))[2:]
#                 print("aqui", len_message)
#                 if len(len_message) % 2 != 0:
#                     len_message = '0' + len_message
#                 len_len_message = hex(len(len_message))[2:]
#                 lens = []
#                 if len(len_len_message) % 2 != 0:
#                     len_len_message = '0' + len_len_message
#                 textcopy = TextVBin
#             else:
#                 rest = (len_data *8) - len(extra_bits)
#                 textcopy = TextVBin[:rest]
#                 aux = bytearray([int(textcopy[i:i+8], 2) for i in range(0, len(textcopy), 8)]).hex()
#                 len_message = hex(len(aux))[2:]
#                 TextVBin = TextVBin[rest:]
#                 print("alli", len_message)
#                 if len(len_message) % 2 != 0:
#                     len_message = '0' + len_message
#                 len_len_message = hex(len(len_message))[2:]
#                 lens = []
#                 if len(len_len_message) % 2 != 0:
#                     len_len_message = '0' + len_len_message
#             print("len", len_message,len_len_message)
#             len_len_message = cipherfunctions.cipherChaCha20(len_len_message, auxChaCha20key, "hex")
#             len_message = cipherfunctions.cipherChaCha20(len_message, auxChaCha20key, "hex")
#             block_bin = binascii.unhexlify(len_len_message+len_message)
#             print(block_bin)
#         else:
#             len_message = hex(len_data)[2:]
#             if len(len_message) % 2 != 0:
#                 len_message = '0' + len_message
#             len_len_message = hex(len(len_message))[2:]
#             lens = []
#             if len(len_len_message) % 2 != 0:
#                 len_len_message = '0' + len_len_message
#             extra = (len_len_message) + (len_message) + 'ff'
#             extra_bin = binascii.unhexlify(extra)
#             extra_bits = ''.join('{:08b}'.format(z) for z in extra_bin)
#             if (len_data *8) - len(extra_bits) >= TextVBin:
#                 len_message = hex(len(bytearray([int(TextVBin[i:i+8], 2) for i in range(0, len(TextVBin), 8)]).hex()))[2:]
#                 if len(len_message) % 2 != 0:
#                     len_message = '0' + len_message
#                 len_len_message = hex(len(len_message))[2:]
#                 lens = []
#                 if len(len_len_message) % 2 != 0:
#                     len_len_message = '0' + len_len_message
#                 textcopy = TextVBin
#                 TextVBin = ""
#             else:
#                 rest = (len_data *8) - len(extra_bits)
#                 textcopy = TextVBin[:rest]
#                 TextVBin = TextVBin[rest:]
#                 aux = bytearray([int(textcopy[i:i+8], 2) for i in range(0, len(textcopy), 8)]).hex()
#                 len_message = hex(len(aux))[2:]
#                 if len(len_message) % 2 != 0:
#                     len_message = '0' + len_message
#                 len_len_message = hex(len(len_message))[2:]
#                 lens = []
#                 if len(len_len_message) % 2 != 0:
#                     len_len_message = '0' + len_len_message
#             block_bin = binascii.unhexlify(len_len_message+len_message)
#         print(block_bin)
#         block_bits = ''.join('{:08b}'.format(z) for z in block_bin)
#         print(block_bits)
#         message_code.append(block_bits + textcopy)
#         message = bytearray([int(TextVBin[i:i+8], 2) for i in range(0, len(TextVBin), 8)]).hex()
#         if message != "":
#             len_message = hex(64)[2:]
#             if len(len_message) % 2 != 0:
#                 len_message = '0' + len_message
#             # len_len_message = hex(len(len_message))[2:]
#             lens = []
#             # if len(len_len_message) % 2 != 0:
#             #     len_len_message = '0' + len_len_message
#             extra = (len_message) + 'ff'
#             messageaux = message[:(64-len(extra))]
#             message = message[(64-len(extra)):]
#             data_length_hex = hex(len(messageaux))[2:]
#             if len(data_length_hex)% 2 != 0:
#                 data_length_hex = '0' + data_length_hex
#             # len_data_length = hex(len(data_length_hex))[2:]
#             # if len(len_data_length) %2 != 0:
#             #     len_data_length = '0' + len_data_length
#             blocks, r = divmod (len(messageaux),(1))
#             #If the rest is not 0, another block and padding
#             #print(blocks, r)
#             rando = ""
#             if r != 0:
#                 blocks += 1
#                 rest = (length -2) - r
#                 random.seed()
#                 rando = ''.join([hex(random.randint(0,15))[2:] for i in range(rest)])
#             messageaux += rando
#             #print(data)
#             if ChaCha20key != "":
#                 #len_data_length = cipherfunctions.cipherChaCha20(len_data_length, auxChaCha20key, "hex")
#                 data_length_hex =  cipherfunctions.cipherChaCha20(data_length_hex, auxChaCha20key, "hex")
#             message_swarm.append(data_length_hex + messageaux)
#         else:
#             #Generating fake message
#             messageux = ""
#             blocks, r = divmod (len(messageaux)-2,(1))
#             #If the rest is not 0, another block and padding
#             #print(blocks, r)
#             rando = ""
#             if r != 0:
#                 blocks += 1
#                 rest = (length -2) - r
#                 random.seed()
#                 for x in range(rest):
#                     rando += hex(random.randint(0, 15))[2:]
#             messageaux += rando
#             len_data_length = '00'
#             if ChaCha20key != "":
#                 len_data_length = cipherfunctions.cipherChaCha20('00', auxChaCha20key, "hex")
#             message_swarm.append(len_data_length + messageaux)
#         print(i,sizes,sizes[i%(len(sizes))], message )
#         if constructor:
#             if message == "" and sizes[i%(len(sizes))] != -1:
#                 if  len(dynamic[-1]) > 0:
#                     if message == "":
#                         rest = 40
#                         random.seed()
#                         rando = ''.join([hex(random.randint(0,15))[2:] for i in range(rest)])
#                         message = rando
#                     lengthsmin = []
#                     if ChaCha20key != "":
#                         #form = cipherChaCha20(form, ChaCha20key, "hex")
#                         auxm =  form + message
#                         # len_message = hex(data)[2:]
#                         # if len(len_message) % 2 != 0:
#                         #     len_message = '0' + len_message
#                         # len_len_message = hex(len(len_message))[2:]
#                         # if len(len_len_message) % 2 != 0:
#                         #     len_len_message = '0' + len_len_message
#                         # message_prepared.append(  aux + aux + len_len_message + len_message + form + data)
#                         len_message = hex(len(auxm))[2:]
#                         if len(len_message) % 2 != 0:
#                             len_message = '0' + len_message
#                         len_len_message = hex(len(len_message))[2:]
#                         if len(len_len_message) % 2 != 0:
#                             len_len_message = '0' + len_len_message
#                         lengthsmin.append(len(len_len_message) + len(len_message) + 2)
#                         len_message_form =  cipherfunctions.cipherChaCha20( form, auxChaCha20key, "hex")
#                         len_len_message =  cipherfunctions.cipherChaCha20( '00', auxChaCha20key, "hex")
#                         message_constructor_lengths.append(len_len_message + len_message_form)
#                         message_constructor_data.append(message)
#
#                     else:
#                         auxm = form + 'ff' + message
#                         # len_message = hex(data)[2:]
#                         # if len(len_message) % 2 != 0:
#                         #     len_message = '0' + len_message
#                         # len_len_message = hex(len(len_message))[2:]
#                         # if len(len_len_message) % 2 != 0:
#                         #     len_len_message = '0' + len_len_message
#                         # message_prepared.append(  aux + aux + len_len_message + len_message + form + data)
#                         len_message = hex(len(auxm))[2:]
#                         if len(len_message) % 2 != 0:
#                             len_message = '0' + len_message
#                         len_len_message = hex(len(len_message))[2:]
#                         if len(len_len_message) % 2 != 0:
#                             len_len_message = '0' + len_len_message
#                         message_constructor_lengths.append('00' +  form)
#                         message_constructor_data.append(message)
#                         lengthsmin.append(len(len_len_message) + len(len_message) + 2)
#                 elif sizes[i%(len(sizes))] != -1:
#                     size = sizes[i%(len(sizes))]
#                     if message == "":
#                         rest = math.floor(sum(size)/4)
#                         random.seed()
#                         for x in range(rest):
#                             rando += hex(random.randint(0, 15))[2:]
#                         message = rando
#                     message_prepared_aux = []
#                     size = sizes[i%(len(sizes))]
#                     len_message = hex(sum(size))[2:]
#                     if len(len_message) % 2 != 0:
#                         len_message = '0' + len_message
#                     len_len_message = hex(len(len_message))[2:]
#                     if len(len_len_message) % 2 != 0:
#                         len_len_message = '0' + len_len_message
#                     rest = sum(size)  - len(len_len_message) - len(len_message) - 2 - 2
#                     dat = message[:rest]
#                     message = message[rest:]
#                     if ChaCha20key != "":
#                         auxm = form + 'ff' + dat
#                         len_message = hex(len(auxm))[2:]
#                         if len(len_message) % 2 != 0:
#                             len_message = '0' + len_message
#                         len_len_message = hex(len(len_message))[2:]
#                         if len(len_len_message) % 2 != 0:
#                             len_len_message = '0' + len_len_message
#                         lengthsmin.append(len(len_len_message) + len(len_message) + 2)
#                         len_message_form =  cipherfunctions.cipherChaCha20( form, auxChaCha20key, "hex")
#                         len_len_message =  cipherfunctions.cipherChaCha20( '00', auxChaCha20key, "hex")
#                         message_constructor_lengths.append(len_len_message + len_message + form)
#                         if len(dat)%2 != 0:
#                             dat += hex(random.randint(0, 15))[2:]
#                         message_constructor_data.append(dat)
#                     else:
#                         auxm = form + 'ff' + message
#                         len_message = hex(len(auxm))[2:]
#                         if len(len_message) % 2 != 0:
#                             len_message = '0' + len_message
#                         len_len_message = hex(len(len_message))[2:]
#                         if len(len_len_message) % 2 != 0:
#                             len_len_message = '0' + len_len_message
#                         message_constructor_lengths.append('00'+ form)
#                         message_constructor_data.append(dat)
#                         lengthsmin.append(len(len_len_message) + len(len_message) + 2)
#                 message = ""
#             elif message != "" and sizes[i%(len(sizes))] != -1:
#                 print("entro en esta rama", dynamic,len(dynamic[-1]) > 0)
#                 if  len(dynamic[-1]) > 0:
#                     lengthsmin = []
#                     if ChaCha20key != "":
#                         #form = cipherChaCha20(form, ChaCha20key, "hex")
#                         auxm =  form + message
#                         # len_message = hex(data)[2:]
#                         # if len(len_message) % 2 != 0:
#                         #     len_message = '0' + len_message
#                         # len_len_message = hex(len(len_message))[2:]
#                         # if len(len_len_message) % 2 != 0:
#                         #     len_len_message = '0' + len_len_message
#                         # message_prepared.append(  aux + aux + len_len_message + len_message + form + data)
#                         len_message = hex(len(auxm))[2:]
#                         if len(len_message) % 2 != 0:
#                             len_message = '0' + len_message
#                         len_len_message = hex(len(len_message))[2:]
#                         if len(len_len_message) % 2 != 0:
#                             len_len_message = '0' + len_len_message
#                         lengthsmin.append(len(len_len_message) + len(len_message) + 2)
#                         len_message_form =  cipherfunctions.cipherChaCha20( len_message + form, auxChaCha20key, "hex")
#                         len_len_message =  cipherfunctions.cipherChaCha20( len_len_message, auxChaCha20key, "hex")
#                         message_constructor_lengths.append(len_len_message + len_message_form)
#                         # if len(message)%2 != 0:
#                         #     message += hex(random.randint(0, 15))[2:]
#                         message_constructor_data.append(message)
#                     else:
#                         auxm = form + message
#                         # len_message = hex(data)[2:]
#                         # if len(len_message) % 2 != 0:
#                         #     len_message = '0' + len_message
#                         # len_len_message = hex(len(len_message))[2:]
#                         # if len(len_len_message) % 2 != 0:
#                         #     len_len_message = '0' + len_len_message
#                         # message_prepared.append(  aux + aux + len_len_message + len_message + form + data)
#                         len_message = hex(len(auxm))[2:]
#                         if len(len_message) % 2 != 0:
#                             len_message = '0' + len_message
#                         len_len_message = hex(len(len_message))[2:]
#                         if len(len_len_message) % 2 != 0:
#                             len_len_message = '0' + len_len_message
#                         message_constructor_lengths.append(len_len_message + len_message + form)
#                         if len(message)%2 != 0:
#                             message += hex(random.randint(0, 15))[2:]
#                         message_constructor_data.append(message)
#                         lengthsmin.append(len(len_len_message) + len(len_message) + 2)
#                     message = ""
#                     print("message after constructor", message)
#                 elif sizes[i%(len(sizes))] != -1:
#                     size = sizes[i%(len(sizes))]
#                     size = sizes[i%(len(sizes))]
#                     len_message = hex(sum(size))[2:]
#                     if len(len_message) % 2 != 0:
#                         len_message = '0' + len_message
#                     len_len_message = hex(len(len_message))[2:]
#                     if len(len_len_message) % 2 != 0:
#                         len_len_message = '0' + len_len_message
#                     rest = sum(size)  - len(len_len_message) - len(len_message) - 2
#                     dat = message[:rest]
#                     message = message[rest:]
#                     if ChaCha20key != "":
#                         auxm = form + dat
#                         len_message = hex(len(auxm))[2:]
#                         if len(len_message) % 2 != 0:
#                             len_message = '0' + len_message
#                         len_len_message = hex(len(len_message))[2:]
#                         if len(len_len_message) % 2 != 0:
#                             len_len_message = '0' + len_len_message
#                         lengthsmin.append(len(len_len_message) + len(len_message) + 2)
#                         len_message_form =  cipherfunctions.cipherChaCha20( len_message + form, auxChaCha20key, "hex")
#                         len_len_message =  cipherfunctions.cipherChaCha20( len_len_message, auxChaCha20key, "hex")
#                         message_constructor_lengths.append(len_len_message + len_message_form)
#                         # if len(dat)%2 != 0:
#                         #     dat += hex(random.randint(0, 15))[2:]
#                         message_constructor_data.append(dat)
#                     else:
#                         auxm = form + message
#                         len_message = hex(len(auxm))[2:]
#                         if len(len_message) % 2 != 0:
#                             len_message = '0' + len_message
#                         len_len_message = hex(len(len_message))[2:]
#                         if len(len_len_message) % 2 != 0:
#                             len_len_message = '0' + len_len_message
#                         message_constructor_lengths.append(len_len_message + len_message + form)
#                         message_constructor_data.append(dat)
#                         # if len(dat)%2 != 0:
#                         #     dat += hex(random.randint(0, 15))[2:]
#                         lengthsmin.append(len(len_len_message) + len(len_message) + 2)
#         elif not constructor:
#             message_constructor_lengths.append("")
#             message_constructor_data.append("")
#
#         i += 1
#     if number >=15 and message != "":
#         print("The message is too large\n")
#         exit(1)
#     return number, message_code, message_swarm, message_constructor_lengths,message_constructor_data,lengthsmin, form
#
# def combinedSequential(number, message_code ,message_swarm, message_constructor_lengths, message_constructor_data,ChaCha20key):
#     """Combine different methods filling completely"""
#     i = 0
#     results = []
#     message_constructor_final = []
#     while i < number:
#         auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
#         number_hex = hex(number)[2:]
#         aux = hex((i+1))[2:]
#         if ChaCha20key != "":
#             block_bin_ci_au = (cipherfunctions.cipherChaCha20(aux+number_hex, auxChaCha20key, "hex"))
#             block_bin = binascii.unhexlify(block_bin_ci_au)
#         else:
#             block_bin = binascii.unhexlify(aux+number_hex)
#         block_bits = ''.join('{:08b}'.format(z) for z in block_bin)
#         message_code[i] = block_bits + message_code[i]
#         message_swarm[i] = block_bin_ci_au + message_swarm[i]
#         if message_constructor_lengths[i] != "":
#             message_constructor_final.append(message_constructor_lengths[i] + message_constructor_data[i])
#         else:
#             message_constructor_final.append("")
#         i+= 1
#     return message_code, message_swarm, message_constructor_final
#
# def extractmessagecombinedSequential(codes, swarms, constructor, to_skip, len_to_read, ChaCha20key, password = ""):
#     message = ""
#     for x in range(len(codes)):
#         auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
#         aux_codes = codes[x][(to_skip*4):]
#         aux_swarm = swarms[x][to_skip:]
#         aux_constructor = constructor[x]
#         if password != "":
#             aux_constructor = ordermessage([aux_constructor], password)[0]
#         if ChaCha20key!= "":
#             len_of_length = bytearray([int(aux_codes[i:i+8], 2) for i in range(0, (len_to_read*4), 8)]).hex()
#             len_of_length = cipherfunctions.ecipherChaCha20(len_of_length, auxChaCha20key, "hex")
#             aux_codes = aux_codes[len_to_read*4:]
#             len_of_length = int(len_of_length,16)
#             total_length = bytearray([int(aux_codes[i:i+8], 2) for i in range(0, (len_of_length*4), 8)]).hex()
#             total_length = cipherfunctions.ecipherChaCha20(total_length, auxChaCha20key, "hex")
#             total_length = int(total_length,16)
#             aux_codes = aux_codes[len_of_length*4:(len_of_length*4)+total_length*4]
#             #aux_codes = aux_codes[(len_to_read*4+len_of_length*4):(len_to_read*4+len_of_length*4+ total_length*4)]
#             print(len_of_length, total_length, aux_codes)
#             aux_codes = bytearray([int(aux_codes[i:i+8], 2) for i in range(0, len(aux_codes), 8)]).hex()
#             print(aux_codes)
#             message += aux_codes
#             # len_of_length = cipherfunctions.ecipherChaCha20(aux_swarm[0:len_to_read], auxChaCha20key, "hex")
#             # len_of_length = int(len_of_length,16)
#             total_length = cipherfunctions.ecipherChaCha20(aux_swarm[0:2], auxChaCha20key, "hex")
#             total_length = int(total_length,16)
#             print(len_of_length, total_length, aux_swarm[2:(total_length+2)] )
#             message += aux_swarm[2:(total_length+2)]
#             print(aux_codes, aux_swarm, aux_constructor)
#             message += aux_constructor
#         else:
#             len_of_length = int(aux_codes[0:len_to_read],16)
#             total_length = int(aux_codes[len_to_read:(len_to_read+len_of_length)], 16)
#             message += aux_codes[(len_to_read+len_of_length):]
#             total_length = int(aux_swarm[0:2], 16)
#             message += aux_swarm[2:(total_length+2)]
#             message += aux_constructor
#     return message
#

# def preparemessagehex(data, length, method, ChaCha20key, password = ""):
#     """
#     Insert the extra information in the hex message and divides it into
#     different transaction with a maximun of 15
#     """
#     message_prepared = []
#     if method == "ReceiverAddressMethod" or method == "SwarmHashMethod"  or method == "GasLimitMethod":
#         data_length_hex = hex(len(data))[2:]
#         if len(data_length_hex)% 2 != 0:
#             data_length_hex = '0' + data_length_hex
#         len_data_length = hex(len(data_length_hex))[2:]
#         if len(len_data_length) %2 != 0:
#             len_data_length = '0' + len_data_length
#         #print(data_length_hex, len_data_length)
#         if ChaCha20key != "":
#             auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
#             len_data_length = cipherfunctions.cipherChaCha20(len_data_length, auxChaCha20key, "hex")
#             data_length_hex =  cipherfunctions.cipherChaCha20(data_length_hex, auxChaCha20key, "hex")
#             #print(data_length_hex, len_data_length)
#             data = len_data_length + data_length_hex + data
#         else:
#             data = len_data_length + data_length_hex + data
#         blocks, r = divmod (len(data),(length - len(hex(globalsettings.max_number_transactions)[2:])*2))
#         #If the rest is not 0, another block and padding
#         #print(blocks, r)
#         rando = ""
#         if r != 0:
#             blocks += 1
#             rest = (length -len(hex(globalsettings.max_number_transactions)[2:])*2) - r
#             random.seed()
#             for x in range(rest):
#                 rando += hex(random.randint(0, 15))[2:]
#         #print(data)
#         data += rando
#         #print(data)
#         #TODO: for now only 15 blocks permited
#         if blocks > globalsettings.max_number_transactions:
#             print("Please, reduce the size of the message and try again\n")
#             exit(1)
#         total_blocks_hex = hex(blocks)[2:]
#         if len(total_blocks_hex) %2 != 0:
#             total_blocks_hex = '0' + total_blocks_hex
#         x = 0
#         for i in range(blocks):
#             aux = hex((i+1))[2:]
#             if len(aux)%2 != 0:
#                 aux = '0' + aux
#             if ChaCha20key != "":
#                 auxtotal =  cipherfunctions.cipherChaCha20(aux + total_blocks_hex, auxChaCha20key, "hex")
#                 message_prepared.append(auxtotal + data[(i * (length - len(hex(globalsettings.max_number_transactions)[2:])*2)):((length - len(hex(globalsettings.max_number_transactions)[2:])*2)*(i+1))])
#                 #print("auxChaCha20key", auxChaCha20key)
#                 auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
#             else:
#                 message_prepared.append(aux + total_blocks_hex + data[(i * (length - len(hex(globalsettings.max_number_transactions)[2:])*2)):((length - len(hex(globalsettings.max_number_transactions)[2:])*2)*(i+1))])
#             #x += (x+(length - 2))
#             #print(x)
#     elif method == "TransactionDataMethod" or method == "valueMethod":
#         #If length == 0, dont divide the message
#         #Only necesary to add the message number
#         if length!= 0:
#             blocks, r = divmod (len(data),(length - len(hex(globalsettings.max_number_transactions)[2:])*2))
#             if r!= 0:
#                 blocks += 1
#             if blocks > globalsettings.max_number_transactions:
#                 print("Please, reduce the size of the message or increase the size per transaction and try again\n")
#                 exit(1)
#             total_blocks_hex = hex(blocks)[2:]
#             if len(total_blocks_hex) %2 != 0:
#                 total_blocks_hex = '0' + total_blocks_hex
#             x = 0
#             for i in range(blocks):
#                 aux = hex((i+1))[2:]
#                 if len(aux)%2 != 0:
#                     aux = '0' + aux
#                 if ChaCha20key != "":
#                     auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
#                     auxtotal =  cipherfunctions.cipherChaCha20(aux + total_blocks_hex, auxChaCha20key, "hex")
#                     message_prepared.append(auxtotal + data[(i * (length - len(hex(globalsettings.max_number_transactions)[2:])*2)):((length - len(hex(globalsettings.max_number_transactions)[2:])*2)*(i+1))])
#                 else:
#                     message_prepared.append(aux + total_blocks_hex + data[(i * (length - len(hex(globalsettings.max_number_transactions)[2:])*2)):((length - len(hex(globalsettings.max_number_transactions)[2:])*2)*(i+1))])
#                 #x += (x+(length - 2))
#         else:
#             blocks = 1
#             total_blocks_hex = hex(blocks)[2:]
#             if len(total_blocks_hex) %2 != 0:
#                 total_blocks_hex = '0' + total_blocks_hex
#             x = 0
#             for i in range(blocks):
#                 aux = hex((i+1))[2:]
#                 if len(aux)%2 != 0:
#                     aux = '0' + aux
#                 if ChaCha20key != "":
#                     auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
#                     auxtotal =  cipherfunctions.cipherChaCha20(aux + total_blocks_hex, auxChaCha20key, "hex")
#                     message_prepared.append(auxtotal + data)
#                 else:
#                     message_prepared.append(aux + total_blocks_hex + data)
#                 #x += (x+(length - 2))
#
#     elif method == "ContractBytecodeMethod":
#         #If length == 0, dont divide the message
#         #Only necesary to add the message number
#         if length!= 0:
#             blocks, r = divmod ((len(data)),(length - len(hex(globalsettings.max_number_transactions)[2:])*8))
#             #print(blocks,r)
#             if r!= 0:
#                 blocks += 1
#             if blocks > globalsettings.max_number_transactions:
#                 print("Please, reduce the size of the message or increase the size per transaction and try again\n")
#                 exit(1)
#             total_blocks_hex = hex(blocks)[2:]
#             if len(total_blocks_hex) %2 != 0:
#                 total_blocks_hex = '0' + total_blocks_hex
#             #x = 0
#             for i in range(blocks):
#                 aux = hex((i+1))[2:]
#                 if len(aux)%2 != 0:
#                     aux = '0' + aux
#                 if ChaCha20key != "":
#                     auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
#                     block_bin_ci = cipherfunctions.cipherChaCha20(aux+total_blocks_hex, auxChaCha20key, "hex")
#                     block_bin_ci = binascii.unhexlify(block_bin_ci)
#                     block_bits = ''.join('{:08b}'.format(z) for z in block_bin_ci)
#                 else:
#                     block_bin= binascii.unhexlify(aux+total_blocks_hex)
#                     block_bits = ''.join('{:08b}'.format(z) for z in block_bin)
#                 message_prepared.append(block_bits+ data[(i * (length -  len(hex(globalsettings.max_number_transactions)[2:])*8)):((length - len(hex(globalsettings.max_number_transactions)[2:])*8)*(i+1))])
#                 #x += (x+(length - 2))
#         else:
#             blocks = 1
#             aux = hex((1))[2:]
#             if len(aux)%2 != 0:
#                 aux = '0' + aux
#             total_blocks_hex = hex(blocks)[2:]
#             if len(total_blocks_hex) %2 != 0:
#                 total_blocks_hex = '0' + total_blocks_hex
#             if ChaCha20key != "":
#                 auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
#                 block_bin_ci = cipherfunctions.cipherChaCha20(aux+total_blocks_hex, auxChaCha20key, "hex")
#                 block_bin_ci = binascii.unhexlify(block_bin_ci)
#                 block_bits = ''.join('{:08b}'.format(z) for z in block_bin_ci)
#             else:
#                 block_bin= binascii.unhexlify(aux+total_blocks_hex)
#                 block_bits = ''.join('{:08b}'.format(z) for z in block_bin)
#             message_prepared.append(block_bits+ data)
#
#     elif method == "ContractBytecodeExecutionMethod":
#         #If length == 0, dont divide the message
#         #Only necesary to add the message number
#         if ChaCha20key != "":
#             data_length_hex = hex(len(data))[2:]
#             if len(data_length_hex)% 2 != 0:
#                 data_length_hex = '0' + data_length_hex
#             len_data_length = hex(len(data_length_hex))[2:]
#             if len(len_data_length) %2 != 0:
#                 len_data_length = '0' + len_data_length
#             data_length_b = binascii.unhexlify(data_length_hex)
#             data_length_bin = ''.join('{:08b}'.format(z) for z in data_length_b)
#             len_data_length_b = binascii.unhexlify(len_data_length)
#             len_data_length_bin = ''.join('{:08b}'.format(z) for z in len_data_length_b)
#             data_for_len = len_data_length_bin + data_length_bin + data
#             #print("data",data_for_len, data_length_hex,data,len_data_length)
#         else:
#             data_length_bin = bin(len(data))[2:]
#             len_data_length = bin(len(data_length_bin))[2:]
#             while len(len_data_length) < 4:
#                 len_data_length = '0' + len_data_length
#             #print("lensbin", data_length_bin, len_data_length)
#             data_for_len = len_data_length + data_length_bin + data
#             #print(data_for_len)
#         if length!= 0:
#             blocks, r = divmod ((len(data_for_len)),(length - len(hex(globalsettings.max_number_transactions)[2:])*8))
#             #print(blocks,r,len(data_for_len), length,len(hex(globalsettings.max_number_transactions)[2:])*8 )
#             if r!= 0:
#                 blocks += 1
#             length_per_block, r = divmod ((len(data_for_len)+ len(hex(globalsettings.max_number_transactions)[2:])*8 * blocks),blocks)
#             #print("per block",length_per_block)
#             lens=[length_per_block] * (blocks - 1)
#             #print(lens)
#             #DEcoment this one to save gas
#             lens.append(length_per_block + r)
#             #print("lens",lens)
#             #Comment the next two to save gas
#             #lens.append(r)
#             if blocks > globalsettings.max_number_transactions:
#                 print("Please, reduce the size of the message or increase the size per transaction and try again\n")
#                 exit(1)
#         else:
#             blocks = 1
#             lens = [len(data_for_len) + len(hex(globalsettings.max_number_transactions)[2:])*8]
#             #print("lens", lens)
#         total_blocks_hex = hex(blocks)[2:]
#         if len(total_blocks_hex) %2 != 0:
#             total_blocks_hex = '0' + total_blocks_hex
#         x = 0
#         for i in range(blocks):
#             aux = hex((i+1))[2:]
#             if len(aux)%2 != 0:
#                 aux = '0' + aux
#             print(aux, total_blocks_hex)
#             if ChaCha20key != "":
#                 auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
#                 block_bin_ci = cipherfunctions.cipherChaCha20(aux+total_blocks_hex, auxChaCha20key, "hex")
#                 #print(block_bin_ci)
#                 block_bin_ci = binascii.unhexlify(block_bin_ci)
#                 block_bits = ''.join('{:08b}'.format(z) for z in block_bin_ci)
#                 #print(block_bits)
#             else:
#                 block_bin= binascii.unhexlify(aux+total_blocks_hex)
#                 block_bits = ''.join('{:08b}'.format(z) for z in block_bin)
#             if i == 0:
#                 if ChaCha20key != "":
#                     #print("h")
#                     data_length_bin = cipherfunctions.cipherChaCha20(data_length_hex, auxChaCha20key, "hex")
#                     #print (data_length_bin)
#                     data_length_bin = binascii.unhexlify(data_length_bin)
#                     #print (data_length_bin)
#                     data_length_bin = ''.join('{:08b}'.format(z) for z in data_length_bin)
#                     #print (data_length_bin)
#                     len_data_length = cipherfunctions.cipherChaCha20(len_data_length, auxChaCha20key, "hex")
#                     #print (len_data_length)
#                     len_data_length = binascii.unhexlify(len_data_length)
#                     #print (len_data_length)
#                     len_data_length = ''.join('{:08b}'.format(z) for z in len_data_length )
#                     #print (len_data_length)
#                 size = data_length_bin + len_data_length
#                 #print("size",size)
#                 dat = data[:(lens[i]-len(size)-len(block_bits))]
#                 #TODO: include the size when disordering
#                 if password != "":
#                     dat = disordermessage(dat, password)
#                 #print(dat)
#                 # message_prepared.append(data[(i * (length -8 - len(size))):((length -8 - len(size))*(i+1))] + size + block_bits )
#                 message_prepared.append(dat+  size + block_bits)
#                 #print(dat)
#                 data = data[(lens[i]-len(size)-len(block_bits)):]
#             else:
#                 dat = data[:(lens[i]-len(block_bits))]
#                 if password != "":
#                     dat = disordermessage(dat, password)
#                 message_prepared.append(dat + block_bits)
#                 data = data[(lens[i]-len(block_bits)):]
#     elif method == "ContractConstructorMethod":
#         #First number of message, then len (in case of padded)
#         #For calculate be have to add the len first
#         #TODO: como usamos padding no hay necesidadde hacer a 2?Max size 480
#         #3
#         # data_length_hex = hex(len(data))[2:]
#         # if len(data_length_hex) % 2 != 0:
#         #     data_length_hex = '0' + data_length_hex
#         # len_data_length = hex(len(data_length_hex))[2:]
#         # if len(len_data_length) % 2 != 0:
#         #     len_data_length = '0' + len_data_length
#         # print(data_length_hex, len_data_length)
#         #data = len_data_length + data_length_hex + data
#         #TODO: ask number od max divisions, for now 15 (can be 255 but dificult to keep track)
#         # 2--> f and f
#         if length != 0:
#             max_len_arguments = hex(length)[2:]
#             if len(max_len_arguments) % 2 != 0:
#                 max_len_arguments = '0' + max_len_arguments
#             len_max_len_arguments = hex(len(max_len_arguments))[2:]
#             #TODO: dont allow more than 2 of len
#             if len(len_max_len_arguments) % 2 != 0:
#                 len_max_len_arguments = '0' + len_max_len_arguments
#             total_len_size = len(len_max_len_arguments) + len(max_len_arguments)
#             blocks, r = divmod (len(data),(length - 4  - total_len_size) )
#             #print(blocks, r)
#             if r != 0:
#                 blocks += 1
#             #print(data)
#             #TODO: for now only 15 blocks permited
#             if blocks > 15:
#                 print("Please, reduce the size of the message and try again\n")
#                 exit(1)
#             print("Do you want to distribute the message weight between the arguments?(y/n)\n")
#             Correct = False
#             while not Correct:
#                 option = input()
#                 if option == "y" or option == "yes":
#                     readjust = '01'
#                     Correct = True
#                 elif option == "n" or option == "no":
#                     readjust = '10'
#                     Correct = True
#                 else:
#                     print ("Option not valid\n")
#             total_blocks_hex = hex(blocks)[2:]
#             x = 0
#             print("Do you want to disorder the message?(y/n)\n")
#             Correct = False
#             while not Correct:
#                 option = input()
#                 if option == "y" or option == "yes":
#                     print("Introduce a password\n")
#                     password = getpass()
#                     #print("hola", password)
#                     Correct = True
#                 elif option == "n" or option == "no":
#                     password = ""
#                     Correct = True
#                 else:
#                     print ("Option not valid\n")
#             for i in range(blocks):
#                 data_aux = data[(i * (length - 4  - total_len_size)):((length - 4  - total_len_size)*(i+1))]
#                 if password != "":
#                     data_aux = disordermessage(data_aux, password)[0]
#                 aux = hex((i+1))[2:]
#                 message_prepared.append(readjust+ aux + total_blocks_hex + data_aux )
#                 len_message = hex(len(message_prepared[-1]))[2:]
#                 if len(len_message) % 2 != 0:
#                     len_message = '0' + len_message
#                 len_len_message = hex(len(len_message))[2:]
#                 if len(len_len_message) % 2 != 0:
#                     len_len_message = '0' + len_len_message
#                 message_prepared[-1] = len_len_message + len_message + message_prepared[-1]
#         else:
#             blocks = 1
#             print("Do you want to distribute the message weight between the arguments?(y/n)\n")
#             Correct = False
#             while not Correct:
#                 option = input()
#                 if option == "y" or option == "yes":
#                     readjust = '01'
#                     Correct = True
#                 elif option == "n" or option == "no":
#                     readjust = '10'
#                     Correct = True
#                 else:
#                     print ("Option not valid\n")
#             print("Do you want to disorder the message?(y/n)\n")
#             Correct = False
#             while not Correct:
#                 option = input()
#                 if option == "y" or option == "yes":
#                     print("Introduce a password\n")
#                     password = getpass()
#                     #print("hola", password)
#                     Correct = True
#                 elif option == "n" or option == "no":
#                     password = ""
#                     Correct = True
#                 else:
#                     print ("Option not valid\n")
#             for i in range(blocks):
#                 aux = hex((i+1))[2:]
#                 if password != "":
#                     data = disordermessage(data, password)[0]
#                 message_prepared.append(readjust+ aux + total_blocks_hex + data)
#                 len_message = hex(message_prepared[-1])[2:]
#                 if len(len_message) % 2 != 0:
#                     len_message = '0' + len_message
#                 len_len_message = hex(len(len_message))[2:]
#                 if len(len_len_message) % 2 != 0:
#                     len_len_message = '0' + len_len_message
#                 message_prepared[-1] = len_len_message + len_message + message_prepared[-1]
#
#     return message_prepared

def preparelistpatternhigher36 (dict_field, zeros_field):
    dict_field = eval(dict_field)
    zeros_field = eval(zeros_field)
    dict_valuespossible = {}
    bits_per_length = {}
    for key in dict_field:
        zeros_tmp = {}
        bits_tmp = {}
        for zeros_value in zeros_field[key]:
            print("Executing", key, zeros_value)
            # first_element = int('1' + (key - zeros_value -2) * '0' +'1')
            # last_element = int('9'*len(str(first_element)))
            # for element in list_all:
            #     if str(element)[-1]=='0':
            #         list_all.remove(element)
            name = "length" + str(key)+ "zeros" + str(zeros_value)
            existing, list_all = getcheck.getpatterjson(name)
            #print("exist", existing)
            if not existing:
                first_element = int('1' + (key - zeros_value -2) * '0' +'1')
                last_element = int('9'*len(str(first_element)))
                list_all = list(range(first_element,last_element+1))
                list_all = [element for element in list_all if str(element)[-1] != '0']
                getcheck.saveresourcepattern(name, list_all)
            zeros_tmp[zeros_value] = copy.deepcopy(list_all)
            bits_tmp[zeros_value] = math.floor(math.log2(81*10**(key-zeros_value-2)))
        dict_valuespossible[key] = zeros_tmp
        bits_per_length[key] = bits_tmp
    return dict_valuespossible, bits_per_length

def preparemessagetopx (dict_field, data, ChaCha20key, password=''):
    topx = eval(dict_field)
    size = len(list(topx.keys())[0])
    #print("size", size)
    message_prepared = []
    data_length_hex_bytes = hex(int(len(data)/2))[2:]
    if len(data_length_hex_bytes) % 2 != 0:
        data_length_hex_bytes = '0' + data_length_hex_bytes
    # if len(to_read) % 2 != 0:
    #     to_read = '0' + to_read
    #len_bits = ''.join('{:04b}'.format(x) for x in bytes.fromhex(data_length_hex_bytes))
    len_bits = bin(int(len(data)/2))[2:]
    while len(len_bits) % 4 != 0:
        len_bits = '0' + len_bits
    #print("antes de nada", data_length_hex_bytes, len_bits)
    len_bits_hex = hex(int(len(len_bits)/4))[2:]
    if len(len_bits_hex) % 2 != 0:
        len_bits_hex = '0' + len_bits_hex
    to_read =  ''.join('{:04b}'.format(x) for x in bytes.fromhex(len_bits_hex))
    if ChaCha20key != "":
        if len(len_bits) % 8 == 0 :
            len_bits = '0' * 4 + len_bits
            len_bits_hex = hex(int(len(len_bits)/4))[2:]
            if len(len_bits_hex) % 2 != 0:
                len_bits_hex = '0' + len_bits_hex
            to_read =  ''.join('{:04b}'.format(x) for x in bytes.fromhex(len_bits_hex))
        length_control_info = to_read + len_bits
        #print(to_read, len_bits, length_control_info)
        #print(cipherfunctions.nonce)
        auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
        #print(cipherfunctions.nonce)
        #auxChaCha20key = ChaCha20key
        len_data_length = hex(int(length_control_info[:8],2))[2:]
        if len(len_data_length) % 2 != 0:
            len_data_length = '0' + len_data_length
        #print(cipherfunctions.nonce,auxChaCha20key )
        len_data_length = cipherfunctions.cipherChaCha20(len_data_length, auxChaCha20key, "hex")
        #print(cipherfunctions.nonce,auxChaCha20key )
        if length_control_info[8:] != '':
            data_length_hex = hex(int(length_control_info[8:],2))[2:]
            if len(data_length_hex)% 2 != 0:
                data_length_hex = '0' + data_length_hex
            #print("control", length_control_info,length_control_info[8:],data_length_hex)
            #print(cipherfunctions.nonce,auxChaCha20key )
            data_length_hex =  cipherfunctions.cipherChaCha20(data_length_hex, auxChaCha20key, "hex")
            if len(data_length_hex)% 2 != 0:
                data_length_hex = '0' + data_length_hex
            #print(cipherfunctions.nonce,auxChaCha20key )
            #print("control", length_control_info,length_control_info[8:],data_length_hex,len_data_length + data_length_hex)
            data = len_data_length + data_length_hex + data
        else:
            data = len_data_length + data
    else:
        length_control_info = to_read + len_bits
        #print("control", length_control_info, to_read , len_bits,  hex(int(length_control_info,2))[2:])
        hex_control_info =  hex(int(length_control_info,2))[2:]
        if len(hex_control_info)% 2 != 0:
            hex_control_info = '0' + hex_control_info
        data =  hex_control_info + data
    #to bits:
    data = ''.join('{:08b}'.format(x) for x in bytes.fromhex(data))
    data_old = copy.deepcopy(data)
    #sizes_number = [sizes[i%len(sizes)] for i in range(blocks)]
    #sum_sizes = [sum(x) for x in sizes_number]
    completed = False
    i = 0
    while i < 255 and not completed:
        while len(data) < size:
            rest = size - len(data)
            data += random.choice(['0', '1'])
        tmp = data[:size]
        message = topx[tmp]
        message_hex = hex(message)[2:]
        #print("message",data[:size], message,message_hex )
        message_prepared.append(message_hex)
        data = data[size:]
        if len(data) == 0:
            completed = True
        i+=1
    #print("Completed", data, completed)
    if len(message_prepared)>255 or not completed:
        print("Please, reduce the size of the message and try again\n")
        exit(1)
    return message_prepared

def checkifsingletopx(dict_field,data_in_transactions,len_to_read,ChaCha20key, field = ""):
    topx = eval(dict_field)
    size = len(list(topx.values())[0])
    #print("size", size)
    single = False
    withmessage = True
    data_array = copy.deepcopy(data_in_transactions)
    total = 0
    aux_total = ''
    total_length = 0
    in_next_transaction = False
    first_low_length = False
    tmp= data_in_transactions[0]
    if type(tmp) == str:
        tmp = int(tmp,16)
    aux = topx[tmp]
    #print("aux", aux)
    message = ''
    if len(aux) >=8:
        # if ChaCha20key != "":
        #     totalaux = cipherfunctions.ecipherChaCha20 (data_in_transactions[0][:2], ChaCha20key, "hex")
        #     print(totalaux)
        #     total = int(totalaux[1], 16)
        # else:
        #     total = int(data_in_transactions[0][1], 16
        #     data_in_transactions[0] = copy.deepcopy(hex(element)[2:])
        # if len(data_in_transactions[0])%2 !=0:
        #     data_in_transactions[0] = '0'+ data_in_transactions[0]
        if ChaCha20key != "":
            toread = aux[:4]
            #Number in bytes
            #print(aux, toread)
            # toread = int(toread,2)
            # to_read_bits = toread * 8
            aux_hex = hex(int(aux[:8],2))[2:]
            if len(aux_hex) %2 != 0:
                aux_hex = '0' + aux_hex
            #len
            #print("aux",aux_hex)
            auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
            #recover the whole byte to decipher
            #print("aux2",aux_hex[0:(len_to_read+1)])
            len_of_length_tmp = cipherfunctions.ecipherChaCha20(aux_hex[0:(len_to_read+1)], auxChaCha20key, "hex")
            #print("after",len_of_length_tmp)
            len_of_length_tmp = '{:08b}'.format(int(len_of_length_tmp,16))
            #print("bits",len_of_length_tmp)
            len_of_length = len_of_length_tmp[0:(len_to_read *4)]
            #print(len_of_length)
            #print(len_of_length)
            #len_of_length = ''.join('{:08b}'.format(x) for x in bytes.fromhex(len_of_length))
            len_of_length = int(len_of_length,2)
            len_of_length_bits = len_of_length * 4
            #print(len_of_length, len_of_length_bits)
            #print("len_of_length",len_of_length )
            #print(aux, (aux[len_to_read:(len_to_read+len_of_length)]), len(aux[len_to_read:(len_to_read+len_of_length)]))
            #print(aux_hex[len_to_read:(len_to_read+len_of_length)])
            if (len(aux_hex[len_to_read:(len_to_read+len_of_length)]) == len_of_length) or len(aux) >= (len_of_length_bits +4):
                counter = len_of_length
                total_length = ''
                #print("aqui", counter)
                retrieved = False
                while counter != 0 and not retrieved:
                    if counter % 2 != 0:
                        total_length += len_of_length_tmp[(len_to_read*4):]
                        counter -= 1
                        #print("h",len_to_read, len_of_length_tmp,total_length, len_of_length*4, len(total_length),(len(total_length)+(len_to_read *4)) )
                    elif counter %2 == 0 and counter != 0 and len(total_length) != (len_of_length * 4):
                        aux_hex = hex(int(aux[(len(total_length)+(len_to_read *4)):((len_of_length*4)+len(total_length))],2))[2:]
                        if len(aux_hex) %2 != 0:
                            aux_hex = '0' + aux_hex
                        #print(aux_hex,aux,aux[(len(total_length)+(len_to_read *4)):((len_of_length*4)+len(total_length))])
                        total_length_tmp = cipherfunctions.ecipherChaCha20(aux_hex, auxChaCha20key, "hex")
                        #print("y", total_length_tmp)
                        total_length_tmp = '{:08b}'.format(int(total_length_tmp,16))
                        while len(total_length_tmp) % 4 != 0:
                            total_length_tmp = '0' + total_length_tmp
                        total_length += total_length_tmp
                        counter -= 2
                        #print("o", total_length)
                    if len(total_length) == (len_of_length * 4):
                        print("Retrieved")
                        retrieved = True
                total_length = int(total_length,2)
                total_length_bits = total_length * 8
                #print(total_length,total_length_bits)
            else:
                #print("here")
                total_length = ''
                total_length = len_of_length_tmp[
                (len_to_read*4):] + aux[8:]
                #print(total_length,aux_hex[(len_to_read +1):(len_to_read+1+len_of_length)])
                in_next_transaction = True

        else:
            len_of_length = aux[0:len_to_read]
            len_of_length = '{:04b}'.format(int(len_of_length,16))
            #len_of_length = ''.join('{:08b}'.format(x) for x in bytes.fromhex(len_of_length))
            len_of_length = int(len_of_length,2)
            len_of_length_bits = len_of_length * 4
            if (len(aux[len_to_read:(len_to_read+len_of_length)]) == len_of_length):
                total_length = int(aux[len_to_read:(len_to_read+len_of_length)], 2)
                total_length_bits = total_length * 8
            else:
                total_length = aux[len_to_read:(len_to_read+len_of_length)]
                in_next_transaction = True
    else:
        len_of_length = 0
        total_length = aux
        in_next_transaction = True
        first_low_length = True
    #print("lens", len_of_length, total_length)
    if not first_low_length:
        message += aux[((len_to_read*4)+len_of_length_bits):]
    #print(message, len_of_length, total_length,in_next_transaction,first_low_length )
    return len_of_length, total_length, message, in_next_transaction,first_low_length

def checkextracttopx(dict_field,data_in_transactions,len_to_read,len_of_length, total_length, message, in_next_transaction, first_low_length,ChaCha20key,field = ""):
    topx = eval(dict_field)
    size = len(list(topx.values())[0])
    #print("size", size)
    rest = 0
    all_extracted = False
    if ChaCha20key != "":
        auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
    for x in range(len(data_in_transactions)):
        #Extract total length of the message:
        tmp= data_in_transactions[x]
        if type(tmp) == str:
            tmp = int(tmp,16)
        aux = topx[tmp]
        #print("aux", aux, message,first_low_length,in_next_transaction, data_in_transactions)
        if first_low_length and len(total_length + aux)>= 8:
            #print("First minor 8")
            aux = total_length + aux
            #print("aux", aux, message,data_in_transactions)
            if ChaCha20key != "":
                toread = aux[:4]
                #Number in bytes
                #print(aux, toread)
                # toread = int(toread,2)
                # to_read_bits = toread * 8
                aux_hex = hex(int(aux[:8],2))[2:]
                if len(aux_hex) %2 != 0:
                    aux_hex = '0' + aux_hex
                #len
                #print("aux",aux_hex)
                #recover the whole byte to decipher
                #print("aux2",aux_hex[0:(len_to_read+1)])
                len_of_length_tmp = cipherfunctions.ecipherChaCha20(aux_hex[0:(len_to_read+1)], auxChaCha20key, "hex")
                #print("after",len_of_length_tmp)
                len_of_length_tmp = '{:08b}'.format(int(len_of_length_tmp,16))
                #print("bits",len_of_length_tmp)
                len_of_length = len_of_length_tmp[0:(len_to_read *4)]
                #print(len_of_length)
                #print(len_of_length)
                #len_of_length = ''.join('{:08b}'.format(x) for x in bytes.fromhex(len_of_length))
                len_of_length = int(len_of_length,2)
                len_of_length_bits = len_of_length * 4
                #print(len_of_length, len_of_length_bits)
                #print("len_of_length",len_of_length )
                #print(aux, (aux[len_to_read:(len_to_read+len_of_length)]), len(aux[len_to_read:(len_to_read+len_of_length)]))
                #print(aux_hex[len_to_read:(len_to_read+len_of_length)])
                if (len(aux_hex[len_to_read:(len_to_read+len_of_length)]) == len_of_length) or len(aux) >= (len_of_length_bits +4):
                    counter = len_of_length
                    total_length = ''
                    #print("aqui", counter)
                    retrieved = False
                    while counter != 0 and not retrieved:
                        if counter % 2 != 0:
                            total_length += len_of_length_tmp[(len_to_read*4):]
                            counter -= 1
                            #print("h",len_to_read, len_of_length_tmp,total_length, len_of_length*4, len(total_length),(len(total_length)+(len_to_read *4)) )
                        elif counter %2 == 0 and counter != 0 and len(total_length) != (len_of_length * 4):
                            aux_hex = hex(int(aux[(len(total_length)+(len_to_read *4)):((len_of_length*4)+len(total_length))],2))[2:]
                            if len(aux_hex) %2 != 0:
                                aux_hex = '0' + aux_hex
                            #print(aux_hex,aux,aux[(len(total_length)+(len_to_read *4)):((len_of_length*4)+len(total_length))])
                            total_length_tmp = cipherfunctions.ecipherChaCha20(aux_hex, auxChaCha20key, "hex")
                            total_length_tmp = '{:08b}'.format(int(total_length_tmp,16))
                            total_length += total_length_tmp
                            counter -= 2
                            #print("o", total_length)
                        if len(total_length) == (len_of_length * 4):
                            print("Retrieved")
                            retrieved = True
                    #print("Firstafter", len_of_length,total_length)
                    total_length = int(total_length,2)
                    total_length_bits = total_length * 8
                    rest = (len_of_length*4) +(len_to_read *4)
                    #print("rest", rest, (len_of_length*4), (len_to_read *4))
                    in_next_transaction = False
                    first_low_length =False
                    #print(total_length,total_length_bits)
                else:
                    #print("here")
                    total_length = ''
                    total_length = len_of_length_tmp[
                    (len_to_read*4):] + aux[8:]
                    #print(total_length,aux_hex[(len_to_read +1):(len_to_read+1+len_of_length)])
                    aux = ''
                    in_next_transaction = True
                    first_low_length =False
        elif in_next_transaction and not first_low_length:
            #print("next", len_of_length,total_length)
            if (len_of_length*4) <= len(total_length):
                rest = (len_of_length*4) - len(total_length)
                to_add = total_length[rest:]
                aux = to_add + aux
                if len(total_length) > 4:
                    tmp = total_length[4:rest]
                rest = 0
            else:
                rest = (len_of_length*4) - len(total_length)
                tmp = aux[:rest]
                #print(rest, aux)
                #tmp = aux[:(rest*4)]
                tmp = aux[:rest]
                #print(tmp,tmp[:8])
                if len(total_length) > 4:
                    tmp = total_length[4:]+ tmp
            #print(tmp)
            #aux = copy.deepcopy(tmp)
            #New three delete if not working
            #aux = copy.deepcopy(tmp[8:])
            #rest = 0
            #print(aux)
            tmp2 = int(tmp[:8],2)
            #print(tmp2)
            aux_hex = hex(tmp2)[2:]
            if len(aux_hex) %2 != 0:
                aux_hex = '0' + aux_hex
            #total_length += aux_hex[:rest]
            #print("next2", aux_hex)
            if len(aux_hex)== 2 and len(tmp)>=8:
                if ChaCha20key!= "":
                    total_length_tmp = cipherfunctions.ecipherChaCha20(aux_hex, auxChaCha20key, "hex")
                    if len(total_length) > 4:
                        total_length = total_length[:4] + '{:08b}'.format(int(total_length_tmp,16))
                    else:
                        total_length += '{:08b}'.format(int(total_length_tmp,16))
                    #print(total_length_tmp, total_length)
                    total_length = int(total_length,2)
                    total_length_bits = total_length * 8
                    #print(total_length_tmp, total_length)
                else:
                    total_length += aux[:int((rest/2)*8)]
                    total_length = int(total_length,2)
                    total_length_bits = total_length * 8
                in_next_transaction = False
            else:
                total_length += aux
                in_next_transaction = True
                aux = ''
            #print(total_length, message)
        #print(rest)
        if not first_low_length and not in_next_transaction:
            if rest != 0:
                #print("ups", aux,aux[8:], aux[rest:] )
                #message += aux[8:]
                message += aux[rest:]
                #print("message", message)
                rest = 0
            else:
                message += aux
                #print(message)
        else:
            total_length += aux
            print("still collecting", total_length)
        #print(message)
    #print(message)
    if type(total_length) != str and len(message)>= (total_length * 8):
        all_extracted = True
    #message_bits = ''.join('{:08b}'.format(x) for x in bytes.fromhex(message))
    # message_bits = message[:total_length_bits]
    if all_extracted:
        total_length_bits = total_length * 8
        message = message[:total_length_bits]
        print("all",total_length,total_length_bits)
    # message = bytearray([int(message_bits[i:i+8], 2) for i in range(0, len(message_bits), 8)]).hex()
    #print(message)
    return message,len_of_length, total_length, message, in_next_transaction,all_extracted,first_low_length

def preparelistpatternbelow36 (dict_field, zeros_field):
    dict_field = eval(dict_field)
    zeros_field = eval(zeros_field)
    list_sample = []
    for key in dict_field:
        value_list = [key] * dict_field[key]
        list_sample += value_list
    list_zeros = {}
    dict_valuespossible = {}
    bits_per_length = {}
    for key in dict_field:
        zeros_tmp = {}
        list_zeros_tmp = []
        bits_tmp = {}
        for zeros_value in zeros_field[key]:
            zeros_list = [zeros_value] * zeros_field[key][zeros_value]
            list_zeros_tmp += zeros_list
            # first_element = int('1' + (key - zeros_value -2) * '0' +'1')
            # last_element = int('9'*len(str(first_element)))
            name = "length" + str(key)+ "zeros" + str(zeros_value)
            existing, list_all = getcheck.getpatterjson(name)
            print("Exist", existing)
            if not existing:
                first_element = int('1' + (key - zeros_value -2) * '0' +'1')
                last_element = int('9'*len(str(first_element)))
                list_all = list(range(first_element,last_element+1))
                # for element in list_all:
                #     if str(element)[-1]=='0':
                #         list_all.remove(element)
                list_all = [element for element in list_all if str(element)[-1] != '0']
                getcheck.saveresourcepattern(name, list_all)
            zeros_tmp[zeros_value] = copy.deepcopy(list_all)
            bits_tmp[zeros_value] = math.floor(math.log2(81*10**(key-zeros_value-2)))
        dict_valuespossible[key] = zeros_tmp
        bits_per_length[key] = bits_tmp
        list_zeros[key] = list_zeros_tmp
    return list_sample,list_zeros , dict_valuespossible, bits_per_length

def preparemessagevalue(data,dict_field, zeros_field, max_size, list_sample,list_zeros , dict_valuespossible, bits_per_length, ChaCha20key, password = ""):
    dict_field = eval(dict_field)
    zeros_field = eval(zeros_field)
    max_size = eval(max_size)
    # bits_to_block_counter =int(len(hex(globalsettings.max_number_transactions)[2:])*2/2) *8
    # max_size = max_size * globalsettings.max_number_transactions
    # max_data_length_hex_bytes = hex(int(max_size/8))[2:]
    # if len(max_data_length_hex_bytes) % 2 != 0:
    #     max_data_length_hex_bytes = '0' + max_data_length_hex_bytes
    # max_data_length_hex_bytes_bits = ''.join('{:04b}'.format(x) for x in bytes.fromhex(max_data_length_hex_bytes))
    # if len(data) > (max_size - (bits_to_block_counter*255)-4-len(max_data_length_hex_bytes_bits)):
    #     print("Please, reduce the size of the message and try again\n")
    #     exit(1)
    data_length_hex_bytes = hex(int(len(data)/2))[2:]
    #to_read = hex(len(data_length_hex_bytes))[2:]
    if len(data_length_hex_bytes) % 2 != 0:
        data_length_hex_bytes = '0' + data_length_hex_bytes
    # if len(to_read) % 2 != 0:
    #     to_read = '0' + to_read
    len_bits = ''.join('{:04b}'.format(x) for x in bytes.fromhex(data_length_hex_bytes))
    #if len(len_bits) % 4 != 0:
    while len(len_bits) % 4 != 0:
        len_bits = '0' + len_bits
    #print("antes de nada", data_length_hex_bytes, len_bits)
    len_bits_hex = hex(int(len(len_bits)/4))[2:]
    if len(len_bits_hex) % 2 != 0:
        len_bits_hex = '0' + len_bits_hex
    to_read =  ''.join('{:04b}'.format(x) for x in bytes.fromhex(len_bits_hex))
    if ChaCha20key != "":
        if len(len_bits) % 8 == 0 :
            len_bits = '0' * 4 + len_bits
            len_bits_hex = hex(int(len(len_bits)/4))[2:]
            if len(len_bits_hex) % 2 != 0:
                len_bits_hex = '0' + len_bits_hex
            to_read =  ''.join('{:04b}'.format(x) for x in bytes.fromhex(len_bits_hex))
        length_control_info = to_read + len_bits
        #print(to_read, len_bits, length_control_info)
        #print(cipherfunctions.nonce)
        auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
        #print(cipherfunctions.nonce)
        #auxChaCha20key = ChaCha20key
        len_data_length = hex(int(length_control_info[:8],2))[2:]
        if len(len_data_length) % 2 != 0:
            len_data_length = '0' + len_data_length
        #print(cipherfunctions.nonce,auxChaCha20key )
        len_data_length = cipherfunctions.cipherChaCha20(len_data_length, auxChaCha20key, "hex")
        #print(cipherfunctions.nonce,auxChaCha20key )
        if length_control_info[8:] != '':
            data_length_hex = hex(int(length_control_info[8:],2))[2:]
            if len(data_length_hex)% 2 != 0:
                data_length_hex = '0' + data_length_hex
            #print("control", length_control_info,length_control_info[8:],data_length_hex)
            #print(cipherfunctions.nonce,auxChaCha20key )
            data_length_hex =  cipherfunctions.cipherChaCha20(data_length_hex, auxChaCha20key, "hex")
            if len(data_length_hex)% 2 != 0:
                data_length_hex = '0' + data_length_hex
            #print(cipherfunctions.nonce,auxChaCha20key )
            #print("control", length_control_info,length_control_info[8:],data_length_hex,len_data_length + data_length_hex)
            data = len_data_length + data_length_hex + data
        else:
            data = len_data_length + data
    else:
        length_control_info = to_read + len_bits
        #print("control", length_control_info, to_read , len_bits,  hex(int(length_control_info,2))[2:])
        hex_control_info =  hex(int(length_control_info,2))[2:]
        if len(hex_control_info)% 2 != 0:
            hex_control_info = '0' + hex_control_info
        data =  hex_control_info + data
    #to bits:
    data = ''.join('{:08b}'.format(x) for x in bytes.fromhex(data))
    data_old = copy.deepcopy(data)
    #Now we have to chose the length, first we check if we can use the choices
    message = []
    chosen_lengths = []
    chosen_zeros = []
    while data != '':
        #print(data)
        if sys.version_info >= (3, 6):
            length = random.choices(list(dict_field.keys()), list(dict_field.values()))[0]
            tmp_dict = zeros_field[length]
            zeros = random.choices(list(tmp_dict.keys()), list(tmp_dict.values()))[0]
        else:
            length = random.choice(list_sample)
            tmp_list = list_zeros[length]
            zeros = random.choice(tmp_list)
        chosen_lengths.append(length)
        chosen_zeros.append(zeros)
        bits_to_read_total = bits_per_length[length][zeros]
        #bits_to_read = bits_to_read_total - bits_to_block_counter
        bits_to_read = bits_to_read_total
        if len(data) < bits_to_read:
            while (len(data) < bits_to_read):
                data = data + str(random.randint(0,1))
        message.append(data[:bits_to_read])
        data = data[bits_to_read:]
    #Now we add the block numbers:
    if len(message) > globalsettings.max_number_transactions:
        print("Please, reduce the size of the message or try again\n")
        exit(1)
    # total_blocks_hex = hex(len(message))[2:]
    # if len(total_blocks_hex) %2 != 0:
    #     total_blocks_hex = '0' + total_blocks_hex
    #Now we complete the message and chose the element
    final_message = []
    for i in range(len(message)):
        # block = (i+1)
        # block = hex(block)[2:]
        # if len(block) %2 != 0:
        #     block = '0' + block
        # if ChaCha20key != "":
        #     print(cipherfunctions.nonce,auxChaCha20key )
        #     auxtotal =  cipherfunctions.cipherChaCha20(block + total_blocks_hex, auxChaCha20key, "hex")
        #     print("blocks", auxtotal, block + total_blocks_hex)
        #     block_bits = ''.join('{:08b}'.format(x) for x in bytes.fromhex(auxtotal))
        #     auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
        # else:
        #     block_bits = ''.join('{:08b}'.format(x) for x in bytes.fromhex(block+total_blocks_hex))
        #     print("block", block, block_bits)
        #message[i] = block_bits + message[i]
        length = chosen_lengths[i]
        zeros = chosen_zeros[i]
        values = dict_valuespossible[length][zeros]
        #print(int(message[i],2), values[int(message[i],2)] )
        final_value = hex(int(str(values[int(message[i],2)]) + '0'*zeros))[2:]
        #print(int(str(values[int(message[i],2)]) + '0'*zeros), final_value, message[i])
        final_message.append(final_value)
    return final_message

def preparemessagevalue_old(data,dict_field, zeros_field, max_size, list_sample,list_zeros , dict_valuespossible, bits_per_length, ChaCha20key, password = ""):
    dict_field = eval(dict_field)
    zeros_field = eval(zeros_field)
    max_size = eval(max_size)
    bits_to_block_counter =int(len(hex(globalsettings.max_number_transactions)[2:])*2/2) *8
    max_size = max_size * globalsettings.max_number_transactions
    max_data_length_hex_bytes = hex(int(max_size/8))[2:]
    if len(max_data_length_hex_bytes) % 2 != 0:
        max_data_length_hex_bytes = '0' + max_data_length_hex_bytes
    max_data_length_hex_bytes_bits = ''.join('{:04b}'.format(x) for x in bytes.fromhex(max_data_length_hex_bytes))
    if len(data) > (max_size - (bits_to_block_counter*255)-4-len(max_data_length_hex_bytes_bits)):
        print("Please, reduce the size of the message and try again\n")
        exit(1)
    data_length_hex_bytes = hex(int(len(data)/2))[2:]
    #to_read = hex(len(data_length_hex_bytes))[2:]
    if len(data_length_hex_bytes) % 2 != 0:
        data_length_hex_bytes = '0' + data_length_hex_bytes
    # if len(to_read) % 2 != 0:
    #     to_read = '0' + to_read
    len_bits = ''.join('{:04b}'.format(x) for x in bytes.fromhex(data_length_hex_bytes))
    #if len(len_bits) % 4 != 0:
    while len(len_bits) % 4 != 0:
        len_bits = '0' + len_bits
    #print("antes de nada", data_length_hex_bytes, len_bits)
    len_bits_hex = hex(int(len(len_bits)/4))[2:]
    if len(len_bits_hex) % 2 != 0:
        len_bits_hex = '0' + len_bits_hex
    to_read =  ''.join('{:04b}'.format(x) for x in bytes.fromhex(len_bits_hex))
    if ChaCha20key != "":
        if len(len_bits) % 8 == 0 :
            len_bits = '0' * 4 + len_bits
            len_bits_hex = hex(int(len(len_bits)/4))[2:]
            if len(len_bits_hex) % 2 != 0:
                len_bits_hex = '0' + len_bits_hex
            to_read =  ''.join('{:04b}'.format(x) for x in bytes.fromhex(len_bits_hex))
        length_control_info = to_read + len_bits
        #print(to_read, len_bits, length_control_info)
        #print(cipherfunctions.nonce)
        auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
        #print(cipherfunctions.nonce)
        #auxChaCha20key = ChaCha20key
        len_data_length = hex(int(length_control_info[:8],2))[2:]
        if len(len_data_length) % 2 != 0:
            len_data_length = '0' + len_data_length
        #print(cipherfunctions.nonce,auxChaCha20key )
        len_data_length = cipherfunctions.cipherChaCha20(len_data_length, auxChaCha20key, "hex")
        #print(cipherfunctions.nonce,auxChaCha20key )
        if length_control_info[8:] != '':
            data_length_hex = hex(int(length_control_info[8:],2))[2:]
            if len(data_length_hex)% 2 != 0:
                data_length_hex = '0' + data_length_hex
            #print("control", length_control_info,length_control_info[8:],data_length_hex)
            #print(cipherfunctions.nonce,auxChaCha20key )
            data_length_hex =  cipherfunctions.cipherChaCha20(data_length_hex, auxChaCha20key, "hex")
            if len(data_length_hex)% 2 != 0:
                data_length_hex = '0' + data_length_hex
            #print(cipherfunctions.nonce,auxChaCha20key )
            #print("control", length_control_info,length_control_info[8:],data_length_hex,len_data_length + data_length_hex)
            data = len_data_length + data_length_hex + data
        else:
            data = len_data_length + data
    else:
        length_control_info = to_read + len_bits
        #print("control", length_control_info, to_read , len_bits,  hex(int(length_control_info,2))[2:])
        hex_control_info =  hex(int(length_control_info,2))[2:]
        if len(hex_control_info)% 2 != 0:
            hex_control_info = '0' + hex_control_info
        data =  hex_control_info + data
    #to bits:
    data = ''.join('{:08b}'.format(x) for x in bytes.fromhex(data))
    data_old = copy.deepcopy(data)
    #Now we have to chose the length, first we check if we can use the choices
    message = []
    chosen_lengths = []
    chosen_zeros = []
    while data != '':
        #print(data)
        if sys.version_info >= (3, 6):
            length = random.choices(list(dict_field.keys()), list(dict_field.values()))[0]
            tmp_dict = zeros_field[length]
            zeros = random.choices(list(tmp_dict.keys()), list(tmp_dict.values()))[0]
        else:
            length = random.choice(list_sample)
            tmp_list = list_zeros[length]
            zeros = random.choice(tmp_list)
        chosen_lengths.append(length)
        chosen_zeros.append(zeros)
        bits_to_read_total = bits_per_length[length][zeros]
        bits_to_read = bits_to_read_total - bits_to_block_counter
        if len(data) < bits_to_read:
            while (len(data) < bits_to_read):
                data = data + str(random.randint(0,1))
        message.append(data[:bits_to_read])
        data = data[bits_to_read:]
    #Now we add the block numbers:
    if len(message) > globalsettings.max_number_transactions:
        print("Please, reduce the size of the message or try again\n")
        exit(1)
    total_blocks_hex = hex(len(message))[2:]
    if len(total_blocks_hex) %2 != 0:
        total_blocks_hex = '0' + total_blocks_hex
    #Now we complete the message and chose the element
    final_message = []
    for i in range(len(message)):
        block = (i+1)
        block = hex(block)[2:]
        if len(block) %2 != 0:
            block = '0' + block
        if ChaCha20key != "":
            #print(cipherfunctions.nonce,auxChaCha20key )
            auxtotal =  cipherfunctions.cipherChaCha20(block + total_blocks_hex, auxChaCha20key, "hex")
            #print("blocks", auxtotal, block + total_blocks_hex)
            block_bits = ''.join('{:08b}'.format(x) for x in bytes.fromhex(auxtotal))
            auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
        else:
            block_bits = ''.join('{:08b}'.format(x) for x in bytes.fromhex(block+total_blocks_hex))
            #print("block", block, block_bits)
        message[i] = block_bits + message[i]
        length = chosen_lengths[i]
        zeros = chosen_zeros[i]
        values = dict_valuespossible[length][zeros]
        #print(int(message[i],2), values[int(message[i],2)] )
        final_value = hex(int(str(values[int(message[i],2)]) + '0'*zeros))[2:]
        #print(int(str(values[int(message[i],2)]) + '0'*zeros), final_value, message[i])
        final_message.append(final_value)
    return final_message







def preparemessagehex(data, length, method, ChaCha20key, password = ""):
    """
    Insert the extra information in the hex message and divides it into
    different transaction with a maximun of 15
    """
    message_prepared = []
    if method == "ReceiverAddressMethod" or method == "SwarmHashMethod"  or method == "GasLimitMethod":
        data_length_hex = hex(len(data))[2:]
        if len(data_length_hex)% 2 != 0:
            data_length_hex = '0' + data_length_hex
        len_data_length = hex(len(data_length_hex))[2:]
        if len(len_data_length) %2 != 0:
            len_data_length = '0' + len_data_length
        #print(data_length_hex, len_data_length)
        if ChaCha20key != "":
            auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
            len_data_length = cipherfunctions.cipherChaCha20(len_data_length, auxChaCha20key, "hex")
            data_length_hex =  cipherfunctions.cipherChaCha20(data_length_hex, auxChaCha20key, "hex")
            #print(data_length_hex, len_data_length)
            data = len_data_length + data_length_hex + data
        else:
            data = len_data_length + data_length_hex + data
        blocks, r = divmod (len(data),(length - len(hex(globalsettings.max_number_transactions)[2:])*2))
        #If the rest is not 0, another block and padding
        #print(blocks, r)
        rando = ""
        if r != 0:
            blocks += 1
            rest = (length -len(hex(globalsettings.max_number_transactions)[2:])*2) - r
            random.seed()
            for x in range(rest):
                rando += hex(random.randint(0, 15))[2:]
        #print(data)
        data += rando
        #print(data)
        #TODO: for now only 15 blocks permited
        if blocks > globalsettings.max_number_transactions:
            print("Please, reduce the size of the message and try again\n")
            exit(1)
        total_blocks_hex = hex(blocks)[2:]
        if len(total_blocks_hex) %2 != 0:
            total_blocks_hex = '0' + total_blocks_hex
        x = 0
        for i in range(blocks):
            aux = hex((i+1))[2:]
            if len(aux)%2 != 0:
                aux = '0' + aux
            if ChaCha20key != "":
                auxtotal =  cipherfunctions.cipherChaCha20(aux + total_blocks_hex, auxChaCha20key, "hex")
                message_prepared.append(auxtotal + data[(i * (length - len(hex(globalsettings.max_number_transactions)[2:])*2)):((length - len(hex(globalsettings.max_number_transactions)[2:])*2)*(i+1))])
                #print("auxChaCha20key", auxChaCha20key)
                auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
            else:
                message_prepared.append(aux + total_blocks_hex + data[(i * (length - len(hex(globalsettings.max_number_transactions)[2:])*2)):((length - len(hex(globalsettings.max_number_transactions)[2:])*2)*(i+1))])
            #x += (x+(length - 2))
            #print(x)
    elif method == "TransactionDataMethod":
        #If length == 0, dont divide the message
        #Only necesary to add the message number
        if length!= 0:
            blocks, r = divmod (len(data),(length - len(hex(globalsettings.max_number_transactions)[2:])*2))
            if r!= 0:
                blocks += 1
            if blocks > globalsettings.max_number_transactions:
                print("Please, reduce the size of the message or increase the size per transaction and try again\n")
                exit(1)
            total_blocks_hex = hex(blocks)[2:]
            if len(total_blocks_hex) %2 != 0:
                total_blocks_hex = '0' + total_blocks_hex
            x = 0
            for i in range(blocks):
                aux = hex((i+1))[2:]
                if len(aux)%2 != 0:
                    aux = '0' + aux
                if ChaCha20key != "":
                    auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
                    auxtotal =  cipherfunctions.cipherChaCha20(aux + total_blocks_hex, auxChaCha20key, "hex")
                    message_prepared.append(auxtotal + data[(i * (length - len(hex(globalsettings.max_number_transactions)[2:])*2)):((length - len(hex(globalsettings.max_number_transactions)[2:])*2)*(i+1))])
                else:
                    message_prepared.append(aux + total_blocks_hex + data[(i * (length - len(hex(globalsettings.max_number_transactions)[2:])*2)):((length - len(hex(globalsettings.max_number_transactions)[2:])*2)*(i+1))])
                #x += (x+(length - 2))
        else:
            blocks = 1
            total_blocks_hex = hex(blocks)[2:]
            if len(total_blocks_hex) %2 != 0:
                total_blocks_hex = '0' + total_blocks_hex
            x = 0
            for i in range(blocks):
                aux = hex((i+1))[2:]
                if len(aux)%2 != 0:
                    aux = '0' + aux
                if ChaCha20key != "":
                    auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
                    auxtotal =  cipherfunctions.cipherChaCha20(aux + total_blocks_hex, auxChaCha20key, "hex")
                    message_prepared.append(auxtotal + data)
                else:
                    message_prepared.append(aux + total_blocks_hex + data)
                #x += (x+(length - 2))

    elif method == "ContractBytecodeMethod":
        #If length == 0, dont divide the message
        #Only necesary to add the message number
        if length!= 0:
            blocks, r = divmod ((len(data)),(length - len(hex(globalsettings.max_number_transactions)[2:])*8))
            #print(blocks,r)
            if r!= 0:
                blocks += 1
            if blocks > globalsettings.max_number_transactions:
                print("Please, reduce the size of the message or increase the size per transaction and try again\n")
                exit(1)
            total_blocks_hex = hex(blocks)[2:]
            if len(total_blocks_hex) %2 != 0:
                total_blocks_hex = '0' + total_blocks_hex
            #x = 0
            for i in range(blocks):
                aux = hex((i+1))[2:]
                if len(aux)%2 != 0:
                    aux = '0' + aux
                if ChaCha20key != "":
                    auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
                    block_bin_ci = cipherfunctions.cipherChaCha20(aux+total_blocks_hex, auxChaCha20key, "hex")
                    block_bin_ci = binascii.unhexlify(block_bin_ci)
                    block_bits = ''.join('{:08b}'.format(z) for z in block_bin_ci)
                else:
                    block_bin= binascii.unhexlify(aux+total_blocks_hex)
                    block_bits = ''.join('{:08b}'.format(z) for z in block_bin)
                message_prepared.append(block_bits+ data[(i * (length -  len(hex(globalsettings.max_number_transactions)[2:])*8)):((length - len(hex(globalsettings.max_number_transactions)[2:])*8)*(i+1))])
                #x += (x+(length - 2))
        else:
            blocks = 1
            aux = hex((1))[2:]
            if len(aux)%2 != 0:
                aux = '0' + aux
            total_blocks_hex = hex(blocks)[2:]
            if len(total_blocks_hex) %2 != 0:
                total_blocks_hex = '0' + total_blocks_hex
            if ChaCha20key != "":
                auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
                block_bin_ci = cipherfunctions.cipherChaCha20(aux+total_blocks_hex, auxChaCha20key, "hex")
                block_bin_ci = binascii.unhexlify(block_bin_ci)
                block_bits = ''.join('{:08b}'.format(z) for z in block_bin_ci)
            else:
                block_bin= binascii.unhexlify(aux+total_blocks_hex)
                block_bits = ''.join('{:08b}'.format(z) for z in block_bin)
            message_prepared.append(block_bits+ data)

    elif method == "ContractBytecodeExecutionMethod":
        #If length == 0, dont divide the message
        #Only necesary to add the message number
        if ChaCha20key != "":
            data_length_hex = hex(len(data))[2:]
            if len(data_length_hex)% 2 != 0:
                data_length_hex = '0' + data_length_hex
            len_data_length = hex(len(data_length_hex))[2:]
            if len(len_data_length) %2 != 0:
                len_data_length = '0' + len_data_length
            data_length_b = binascii.unhexlify(data_length_hex)
            data_length_bin = ''.join('{:08b}'.format(z) for z in data_length_b)
            len_data_length_b = binascii.unhexlify(len_data_length)
            len_data_length_bin = ''.join('{:08b}'.format(z) for z in len_data_length_b)
            data_for_len = len_data_length_bin + data_length_bin + data
            #print("data",data_for_len, data_length_hex,data,len_data_length)
        else:
            data_length_bin = bin(len(data))[2:]
            len_data_length = bin(len(data_length_bin))[2:]
            while len(len_data_length) < 4:
                len_data_length = '0' + len_data_length
            #print("lensbin", data_length_bin, len_data_length)
            data_for_len = len_data_length + data_length_bin + data
            #print(data_for_len)
        if length!= 0:
            blocks, r = divmod ((len(data_for_len)),(length - len(hex(globalsettings.max_number_transactions)[2:])*8))
            #print(blocks,r,len(data_for_len), length,len(hex(globalsettings.max_number_transactions)[2:])*8 )
            if r!= 0:
                blocks += 1
            length_per_block, r = divmod ((len(data_for_len)+ len(hex(globalsettings.max_number_transactions)[2:])*8 * blocks),blocks)
            #print("per block",length_per_block)
            lens=[length_per_block] * (blocks - 1)
            #print(lens)
            #DEcoment this one to save gas
            lens.append(length_per_block + r)
            #print("lens",lens)
            #Comment the next two to save gas
            #lens.append(r)
            if blocks > globalsettings.max_number_transactions:
                print("Please, reduce the size of the message or increase the size per transaction and try again\n")
                exit(1)
        else:
            blocks = 1
            lens = [len(data_for_len) + len(hex(globalsettings.max_number_transactions)[2:])*8]
            #print("lens", lens)
        total_blocks_hex = hex(blocks)[2:]
        if len(total_blocks_hex) %2 != 0:
            total_blocks_hex = '0' + total_blocks_hex
        x = 0
        for i in range(blocks):
            aux = hex((i+1))[2:]
            if len(aux)%2 != 0:
                aux = '0' + aux
            print(aux, total_blocks_hex)
            if ChaCha20key != "":
                auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
                block_bin_ci = cipherfunctions.cipherChaCha20(aux+total_blocks_hex, auxChaCha20key, "hex")
                #print(block_bin_ci)
                block_bin_ci = binascii.unhexlify(block_bin_ci)
                block_bits = ''.join('{:08b}'.format(z) for z in block_bin_ci)
                #print(block_bits)
            else:
                block_bin= binascii.unhexlify(aux+total_blocks_hex)
                block_bits = ''.join('{:08b}'.format(z) for z in block_bin)
            if i == 0:
                if ChaCha20key != "":
                    #print("h")
                    data_length_bin = cipherfunctions.cipherChaCha20(data_length_hex, auxChaCha20key, "hex")
                    #print (data_length_bin)
                    data_length_bin = binascii.unhexlify(data_length_bin)
                    #print (data_length_bin)
                    data_length_bin = ''.join('{:08b}'.format(z) for z in data_length_bin)
                    #print (data_length_bin)
                    len_data_length = cipherfunctions.cipherChaCha20(len_data_length, auxChaCha20key, "hex")
                    #print (len_data_length)
                    len_data_length = binascii.unhexlify(len_data_length)
                    #print (len_data_length)
                    len_data_length = ''.join('{:08b}'.format(z) for z in len_data_length )
                    #print (len_data_length)
                size = data_length_bin + len_data_length
                #print("size",size)
                dat = data[:(lens[i]-len(size)-len(block_bits))]
                #TODO: include the size when disordering
                if password != "":
                    dat = disordermessage(dat, password)
                #print(dat)
                # message_prepared.append(data[(i * (length -8 - len(size))):((length -8 - len(size))*(i+1))] + size + block_bits )
                message_prepared.append(dat+  size + block_bits)
                #print(dat)
                data = data[(lens[i]-len(size)-len(block_bits)):]
            else:
                dat = data[:(lens[i]-len(block_bits))]
                if password != "":
                    dat = disordermessage(dat, password)
                message_prepared.append(dat + block_bits)
                data = data[(lens[i]-len(block_bits)):]
    elif method == "ContractConstructorMethod":
        #First number of message, then len (in case of padded)
        #For calculate be have to add the len first
        #TODO: como usamos padding no hay necesidadde hacer a 2?Max size 480
        #3
        # data_length_hex = hex(len(data))[2:]
        # if len(data_length_hex) % 2 != 0:
        #     data_length_hex = '0' + data_length_hex
        # len_data_length = hex(len(data_length_hex))[2:]
        # if len(len_data_length) % 2 != 0:
        #     len_data_length = '0' + len_data_length
        # print(data_length_hex, len_data_length)
        #data = len_data_length + data_length_hex + data
        #TODO: ask number od max divisions, for now 15 (can be 255 but dificult to keep track)
        # 2--> f and f
        if length != 0:
            max_len_arguments = hex(length)[2:]
            if len(max_len_arguments) % 2 != 0:
                max_len_arguments = '0' + max_len_arguments
            len_max_len_arguments = hex(len(max_len_arguments))[2:]
            #TODO: dont allow more than 2 of len
            if len(len_max_len_arguments) % 2 != 0:
                len_max_len_arguments = '0' + len_max_len_arguments
            total_len_size = len(len_max_len_arguments) + len(max_len_arguments)
            blocks, r = divmod (len(data),(length - 4  - total_len_size) )
            #print(blocks, r)
            if r != 0:
                blocks += 1
            #print(data)
            #TODO: for now only 15 blocks permited
            if blocks > 15:
                print("Please, reduce the size of the message and try again\n")
                exit(1)
            print("Do you want to distribute the message weight between the arguments?(y/n)\n")
            Correct = False
            while not Correct:
                option = input()
                if option == "y" or option == "yes":
                    readjust = '01'
                    Correct = True
                elif option == "n" or option == "no":
                    readjust = '10'
                    Correct = True
                else:
                    print ("Option not valid\n")
            total_blocks_hex = hex(blocks)[2:]
            x = 0
            print("Do you want to disorder the message?(y/n)\n")
            Correct = False
            while not Correct:
                option = input()
                if option == "y" or option == "yes":
                    print("Introduce a password\n")
                    password = getpass()
                    #print("hola", password)
                    Correct = True
                elif option == "n" or option == "no":
                    password = ""
                    Correct = True
                else:
                    print ("Option not valid\n")
            for i in range(blocks):
                data_aux = data[(i * (length - 4  - total_len_size)):((length - 4  - total_len_size)*(i+1))]
                if password != "":
                    data_aux = disordermessage(data_aux, password)[0]
                aux = hex((i+1))[2:]
                message_prepared.append(readjust+ aux + total_blocks_hex + data_aux )
                len_message = hex(len(message_prepared[-1]))[2:]
                if len(len_message) % 2 != 0:
                    len_message = '0' + len_message
                len_len_message = hex(len(len_message))[2:]
                if len(len_len_message) % 2 != 0:
                    len_len_message = '0' + len_len_message
                message_prepared[-1] = len_len_message + len_message + message_prepared[-1]
        else:
            blocks = 1
            print("Do you want to distribute the message weight between the arguments?(y/n)\n")
            Correct = False
            while not Correct:
                option = input()
                if option == "y" or option == "yes":
                    readjust = '01'
                    Correct = True
                elif option == "n" or option == "no":
                    readjust = '10'
                    Correct = True
                else:
                    print ("Option not valid\n")
            print("Do you want to disorder the message?(y/n)\n")
            Correct = False
            while not Correct:
                option = input()
                if option == "y" or option == "yes":
                    print("Introduce a password\n")
                    password = getpass()
                    #print("hola", password)
                    Correct = True
                elif option == "n" or option == "no":
                    password = ""
                    Correct = True
                else:
                    print ("Option not valid\n")
            for i in range(blocks):
                aux = hex((i+1))[2:]
                if password != "":
                    data = disordermessage(data, password)[0]
                message_prepared.append(readjust+ aux + total_blocks_hex + data)
                len_message = hex(message_prepared[-1])[2:]
                if len(len_message) % 2 != 0:
                    len_message = '0' + len_message
                len_len_message = hex(len(len_message))[2:]
                if len(len_len_message) % 2 != 0:
                    len_len_message = '0' + len_len_message
                message_prepared[-1] = len_len_message + len_message + message_prepared[-1]

    return message_prepared

def extractmessagevalue (data_in_transactions, len_to_read, ChaCha20key, to_skip=len(hex(globalsettings.max_number_transactions)[2:])*8, password = ""):
    message = ""
    #print("n")
    total_length = 0
    first_minor_eight_bits = False
    in_next_transaction = False
    rest = 0
    for x in range(len(data_in_transactions)):
        #Extract total length of the message:
        aux = data_in_transactions[x][to_skip:]
        if in_next_transaction:
            #print("next", len_of_length,total_length)
            rest = (len_of_length*4) - len(total_length)
            #print(rest, aux)
            #tmp = aux[:(rest*4)]
            tmp = aux[:rest]
            #print(tmp,tmp[:8])
            if len(total_length) > 4:
                tmp = total_length[4:]+ tmp
            #print(tmp)
            #aux = copy.deepcopy(tmp)
            #New three delete if not working
            #aux = copy.deepcopy(tmp[8:])
            #rest = 0
            #print(aux)
            tmp = int(tmp[:8],2)
            #print(tmp)
            aux_hex = hex(tmp)[2:]
            if len(aux_hex) %2 != 0:
                aux_hex = '0' + aux_hex
            #total_length += aux_hex[:rest]
            #print("next2", aux_hex)
            if len(aux_hex)== 2:
                if ChaCha20key!= "":
                    total_length_tmp = cipherfunctions.ecipherChaCha20(aux_hex, auxChaCha20key, "hex")
                    if len(total_length) > 4:
                        total_length = total_length[:4] + '{:08b}'.format(int(total_length_tmp,16))
                    else:
                        total_length += '{:08b}'.format(int(total_length_tmp,16))
                    #print(total_length_tmp, total_length)
                    total_length = int(total_length,2)
                    total_length_bits = total_length * 8
                    #print(total_length_tmp, total_length)
                else:
                    total_length += aux[:int((rest/2)*8)]
                    total_length = int(total_length,2)
                    total_length_bits = total_length * 8
                in_next_transaction = False
            else:
                in_next_transaction = True
                aux = ''
            #print(total_length, message)
        if first_minor_eight_bits:
            necessary = 8 - len(first_byte)
            #print("first",first_byte,aux)
            first_byte += aux[:necessary]
            aux = copy.deepcopy(aux[necessary:])
            #print(first_byte,aux)
            aux_hex = hex(int(first_byte[:8],2))[2:]
            if len(aux_hex) %2 != 0:
                aux_hex = '0' + aux_hex
            #len
            #print("aux",aux_hex)
            if ChaCha20key!= "":
                auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
                #recover the whole byte to decipher
                #print("aux2",aux_hex[0:(len_to_read+1)])
                len_of_length_tmp = cipherfunctions.ecipherChaCha20(aux_hex[0:(len_to_read+1)], auxChaCha20key, "hex")
                #print("after",len_of_length_tmp)
                len_of_length_tmp = '{:08b}'.format(int(len_of_length_tmp,16))
                #print("bits",len_of_length_tmp)
                len_of_length = len_of_length_tmp[0:(len_to_read *4)]
                #print(len_of_length)
                #print(len_of_length)
                #len_of_length = ''.join('{:08b}'.format(x) for x in bytes.fromhex(len_of_length))
                len_of_length = int(len_of_length,2)
                len_of_length_bits = len_of_length * 4
                #print(len_of_length, len_of_length_bits)
                #print("len_of_length",len_of_length )
                #print(aux, (aux[len_to_read:(len_to_read+len_of_length)]), len(aux[len_to_read:(len_to_read+len_of_length)]))
                #print(aux_hex[len_to_read:(len_to_read+len_of_length)])
                first_minor_eight_bits = False
                if (len(aux_hex[len_to_read:(len_to_read+len_of_length)]) == len_of_length):
                    counter = len_of_length
                    total_length = ''
                    #print("aqui")
                    while counter != 0:
                        if counter % 2 != 0:
                            total_length += len_of_length_tmp[(len_to_read*4):]
                            counter -= 1
                            #print("h",len_to_read, len_of_length_tmp,total_length )
                        elif counter %2 == 0 and counter != 0:
                            total_length = cipherfunctions.ecipherChaCha20(aux_hex[(len_to_read +1):(len_to_read+1+len_of_length)], auxChaCha20key, "hex")
                            total_length += total_length
                            counter -= 2
                    #print(total_length)
                    total_length = int(total_length,2)
                    total_length_bits = total_length * 8
                    #print("total",total_length,total_length_bits)
                else:
                    #print("here")
                    total_length = ''
                    total_length = len_of_length_tmp[(len_to_read*4):] + aux
                    #print(total_length,len_of_length_tmp[(len_to_read*4):], aux)
                    aux= ''
                    in_next_transaction = True
            else:
                len_of_length = aux[0:len_to_read]
                len_of_length = '{:04b}'.format(int(len_of_length,16))
                #len_of_length = ''.join('{:08b}'.format(x) for x in bytes.fromhex(len_of_length))
                len_of_length = int(len_of_length,2)
                len_of_length_bits = len_of_length * 4
                if (len(aux[len_to_read:(len_to_read+len_of_length)]) == len_of_length):
                    total_length = int(aux[len_to_read:(len_to_read+len_of_length)], 2)
                    total_length_bits = total_length * 8
                else:
                    total_length = aux[len_to_read:(len_to_read+len_of_length)]
                    in_next_transaction = True
        if x == 0:
            if len(aux) >= 8:
                if ChaCha20key!= "":
                    #tmp_bits = ''.join('{:08b}'.format(x) for x in bytes.fromhex(aux[:2]))
                    #the first 4 bits contains how much to keep reading
                    toread = aux[:4]
                    #Number in bytes
                    #print(aux, toread)
                    # toread = int(toread,2)
                    # to_read_bits = toread * 8
                    aux_hex = hex(int(aux[:8],2))[2:]
                    if len(aux_hex) %2 != 0:
                        aux_hex = '0' + aux_hex
                    #len
                    #print("aux",aux_hex)
                    auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
                    #recover the whole byte to decipher
                    #print("aux2",aux_hex[0:(len_to_read+1)])
                    len_of_length_tmp = cipherfunctions.ecipherChaCha20(aux_hex[0:(len_to_read+1)], auxChaCha20key, "hex")
                    #print("after",len_of_length_tmp)
                    len_of_length_tmp = '{:08b}'.format(int(len_of_length_tmp,16))
                    #print("bits",len_of_length_tmp)
                    len_of_length = len_of_length_tmp[0:(len_to_read *4)]
                    #print(len_of_length)
                    #print(len_of_length)
                    #len_of_length = ''.join('{:08b}'.format(x) for x in bytes.fromhex(len_of_length))
                    len_of_length = int(len_of_length,2)
                    len_of_length_bits = len_of_length * 4
                    #print(len_of_length, len_of_length_bits)
                    #print("len_of_length",len_of_length )
                    #print(aux, (aux[len_to_read:(len_to_read+len_of_length)]), len(aux[len_to_read:(len_to_read+len_of_length)]))
                    #print(aux_hex[len_to_read:(len_to_read+len_of_length)])
                    if (len(aux_hex[len_to_read:(len_to_read+len_of_length)]) == len_of_length):
                        counter = len_of_length
                        total_length = ''
                        #print("aqui")
                        while counter != 0:
                            if counter % 2 != 0:
                                total_length += len_of_length_tmp[(len_to_read*4):]
                                counter -= 1
                                #print("h",len_to_read, len_of_length_tmp )
                            elif counter %2 == 0 and counter != 0:
                                # aux_hex = hex(int(aux[(len_to_read +1):(len_to_read+1+len_of_length)],2))[2:]
                                # if len(aux_hex) %2 != 0:
                                #     aux_hex = '0' + aux_hex
                                # total_length_t2 = cipherfunctions.ecipherChaCha20(aux_hex[(len_to_read +1):(len_to_read+1+len_of_length)], auxChaCha20key, "hex")
                                # total_length += total_length_t2
                                total_length = cipherfunctions.ecipherChaCha20(aux_hex[(len_to_read +1):(len_to_read+1+len_of_length)], auxChaCha20key, "hex")
                                total_length += total_length
                                counter -= 2
                        total_length = int(total_length,2)
                        total_length_bits = total_length * 8
                        #print(total_length,total_length_bits)
                    else:
                        #print("here")
                        total_length = ''
                        total_length = len_of_length_tmp[(len_to_read*4):] + aux[8:]
                        #print(total_length,aux_hex[(len_to_read +1):(len_to_read+1+len_of_length)])
                        in_next_transaction = True
                else:
                    len_of_length = aux[0:len_to_read]
                    len_of_length = '{:04b}'.format(int(len_of_length,16))
                    #len_of_length = ''.join('{:08b}'.format(x) for x in bytes.fromhex(len_of_length))
                    len_of_length = int(len_of_length,2)
                    len_of_length_bits = len_of_length * 4
                    if (len(aux[len_to_read:(len_to_read+len_of_length)]) == len_of_length):
                        total_length = int(aux[len_to_read:(len_to_read+len_of_length)], 2)
                        total_length_bits = total_length * 8
                    else:
                        total_length = aux[len_to_read:(len_to_read+len_of_length)]
                        in_next_transaction = True
                #print("lens", len_of_length, total_length)
                message += aux[((len_to_read*4)+len_of_length_bits):]
                #print(message)
            else:
                #print("menor")
                first_byte = aux
                first_minor_eight_bits = True
        else:
            #print(rest)
            if rest != 0:
                #print("ups", aux,aux[8:], aux[rest:] )
                #message += aux[8:]
                message += aux[rest:]
                #print("message", message)
                rest = 0
            else:
                message += aux
                #print(message)
        #print(message)
    #print(message)
    #print(total_length,total_length_bits)
    #message_bits = ''.join('{:08b}'.format(x) for x in bytes.fromhex(message))
    message_bits = message[:total_length_bits]
    message = bytearray([int(message_bits[i:i+8], 2) for i in range(0, len(message_bits), 8)]).hex()
    #print(message, message_bits)
    return message

def extractmessagehex(data_in_transactions, len_to_read, method, ChaCha20key, to_skip=len(hex(globalsettings.max_number_transactions)[2:]*2), password = ""):
    """
    Extract the message from the transactions.
    to_skip are the the hex characters that contains the transaction order information
    len_to_read are the number of characters to read to figure out the actual number
    of characters that indicates the length of the message
    """
    if method == "extractReceiverAddressMethod" or method == "extractSwarmHashMethod" or method=="extractGasPriceMethod":
        message = ""
        total_length = 0
        in_next_transaction = False
        rest = 0
        for x in range(len(data_in_transactions)):
            #Extract total length of the message:
            aux = data_in_transactions[x][to_skip:]
            #print(data_in_transactions[x], aux)
            if in_next_transaction:
                rest = len_of_length - len(total_length)
                total_length += aux[:rest]
                #print("next", rest, total_length)
                if len(total_length)== len_of_length:
                    if ChaCha20key!= "":
                        total_length = cipherfunctions.ecipherChaCha20(total_length, auxChaCha20key, "hex")
                        total_length = int(total_length,16)
                    else:
                        total_length = int(total_length,16)
                    in_next_transaction = False
                else:
                    in_next_transaction = True
                #print(total_length, message)
            if x == 0:
                if ChaCha20key!= "":
                    auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
                    len_of_length = cipherfunctions.ecipherChaCha20(aux[0:len_to_read], auxChaCha20key, "hex")
                    len_of_length = int(len_of_length,16)
                    #print("len_of_length",len_of_length )
                    #print(aux, (aux[len_to_read:(len_to_read+len_of_length)]), len(aux[len_to_read:(len_to_read+len_of_length)]))
                    if (len(aux[len_to_read:(len_to_read+len_of_length)]) == len_of_length):
                        total_length = cipherfunctions.ecipherChaCha20(aux[len_to_read:(len_to_read+len_of_length)], auxChaCha20key, "hex")
                        total_length = int(total_length,16)
                    else:
                        #print("here")
                        total_length = aux[len_to_read:(len_to_read+len_of_length)]
                        #print(total_length)
                        in_next_transaction = True
                else:
                    len_of_length = int(aux[0:len_to_read],16)
                    if (len(aux[len_to_read:(len_to_read+len_of_length)]) == len_of_length):
                        total_length = int(aux[len_to_read:(len_to_read+len_of_length)], 16)
                    else:
                        total_length = aux[len_to_read:(len_to_read+len_of_length)]
                        in_next_transaction = True
                message += aux[(len_to_read+len_of_length):]
                #print(message)
            else:
                if rest != 0:
                    message += aux[rest:]
                    rest = 0
                else:
                    message += aux
            #print(message)
        #print(message)
        #print(total_length)
        message = message[:total_length]
    elif method == "extractGasLimitMethod":
        message = ""
        total_length = 0
        for x in range(len(data_in_transactions)):
            #Extract total length of the message:
            aux = data_in_transactions[x][to_skip:]
            message += aux
        if ChaCha20key!= "":
            auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
            len_of_length = cipherfunctions.ecipherChaCha20(message[0:len_to_read], auxChaCha20key, "hex")
            len_of_length = int(len_of_length,16)
            total_length = cipherfunctions.ecipherChaCha20(message[len_to_read:(len_to_read+len_of_length)], auxChaCha20key, "hex")
            total_length = int(total_length,16)
        else:
            len_of_length = int(message[0:len_to_read],16)
            total_length = int(message[len_to_read:(len_to_read+len_of_length)], 16)
        message = message[(len_to_read+len_of_length):]
        message = message[:total_length]
    elif method == "contractCodeMethod":
        message = ""
        total_length = 0
        for x in range(len(data_in_transactions)):
            #Extract total length of the message:
            aux = data_in_transactions[x][to_skip:]
            if x == 0:
                if ChaCha20key!= "":
                    len_of_length = cipherfunctions.ecipherChaCha20(aux[0:len_to_read], ChaCha20key, "hex")
                    len_of_length = int(len_of_length,16)
                    total_length = cipherfunctions.ecipherChaCha20(aux[len_to_read:(len_to_read+len_of_length)], ChaCha20key, "hex")
                    total_length = int(total_length,16)
                else:
                    len_of_length = int(aux[0:len_to_read],16)
                    total_length = int(aux[len_to_read:(len_to_read+len_of_length)], 16)
                message += aux[(len_to_read+len_of_length):]
            else:
                message += aux
        #print(message)
        #print(total_length)
        message = message[:total_length]
    elif method == "extractTransactionDataMethod" or method == "extractContractBytecodeMethod" or method == "extractContractConstructorMethod" or method == "extractvalueMethod":
        message = ""
        #print("n")
        for x in range(len(data_in_transactions)):
            #Extract total length of the message:
            aux = data_in_transactions[x][to_skip:]
            if password != "":
                aux = ordermessage([aux], password)[0]
            message += aux
        #print(message)
    elif method == "extractContractBytecodeExecutionMethod":
        # print("Is the message disorder?(y/n)\n")
        # option = input()
        # Correct = False
        # while not Correct:
        #     if option == "y" or option == "yes":
        #         print("Introduce a password\n")
        #         password = getpass()
        #         print("hola", password)
        #         Correct = True
        #     elif option == "n" or option == "no":
        #         password = ""
        #         Correct = True
        #     else:
        #         print ("Option not valid\n")
        message = ""
        if ChaCha20key != "":
            auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
            length_of_len = int(data_in_transactions[0][(-8 - (to_skip)):(- to_skip)],2)
            length_of_len = hex(length_of_len)[2:]
            #print(length_of_len, data_in_transactions[0],data_in_transactions[0][(-8 - (to_skip)):(- to_skip)])
            if len(length_of_len) % 2 != 0:
                length_of_len = '0' + length_of_len
            length_of_len = int(cipherfunctions.ecipherChaCha20 (length_of_len, auxChaCha20key, "hex"),16)
            length_of_len *= 4
            #print(length_of_len)
            data_length = int(data_in_transactions[0][((-8 - (to_skip))-length_of_len):(-8 - (to_skip))],2)
            data_length = hex(data_length)[2:]
            #print(data_length)
            if len(data_length) %2 != 0:
                data_length = '0' + data_length
            #print(data_length)
            data_length = int(cipherfunctions.ecipherChaCha20 (data_length, auxChaCha20key, "hex"),16)
            #print(data_length)
            #This 8 is the length of length_of_len
            data_for_len = length_of_len + data_length + 8
            #print(data_for_len)
        else:
            length_of_len = int(data_in_transactions[0][(-4 - (to_skip)):(- to_skip)],2)
            #print(length_of_len)
            data_length = int(data_in_transactions[0][((-4 - (to_skip))-length_of_len):(-4 - (to_skip))],2)
            #print(data_length)
            data_for_len = 4 + length_of_len + data_length
        #print(length_of_len)
        #print(data_length)
        #print(data_for_len)
        length_per_block, r = divmod (((data_for_len)+  to_skip * len(data_in_transactions)),len(data_in_transactions))
        #print("per",length_per_block, r)
        lens=[length_per_block] * (len(data_in_transactions)-1)
        lens.append(length_per_block + r)
        #print(lens)
        for x in range(len(data_in_transactions)):
            #Extract total length of the message:
            if x == 0:
                #print("hh")
                aux = data_in_transactions[x][-lens[x]:]
                #print (aux)
                if ChaCha20key != "":
                    aux = aux[:((-8 - (to_skip))-length_of_len)]
                else:
                    aux = aux[:((-4 - (to_skip))-length_of_len)]
                #print(aux)
                if password != "":
                    aux = ordermessage(aux, password)
                message += aux
            else:
                aux = data_in_transactions[x][-lens[x]:]
                #print(aux)
                aux = aux[:(- to_skip)]
                #print(aux)
                if password != "":
                    aux = ordermessage(aux, password)
                message += aux

        #print(message)
    return message

def preparemessageconstruccall(data, sizes, dynamic,password,ChaCha20key, default, form="", number = 0):
    """
    Insert the extra transaction information and prepare the message acording to
    the arguments sizes
    """
    #print(sizes, dynamic)
    message_prepared = []
    data_length_hex_bytes = hex(int(len(data)/2))[2:]
    lengthsmin = []
    #to_read = hex(len(data_length_hex_bytes))[2:]
    if len(data_length_hex_bytes) % 2 != 0:
        data_length_hex_bytes = '0' + data_length_hex_bytes
    # if len(to_read) % 2 != 0:
    #     to_read = '0' + to_read
    #len_bits = ''.join('{:04b}'.format(x) for x in bytes.fromhex(data_length_hex_bytes))
    len_bits = bin(int(len(data)/2))[2:]
    while len(len_bits) % 4 != 0:
        len_bits = '0' + len_bits
    #print("antes de nada", data_length_hex_bytes, int(len(data)/2), len_bits)
    len_bits_hex = hex(int(len(len_bits)/4))[2:]
    if len(len_bits_hex) % 2 != 0:
        len_bits_hex = '0' + len_bits_hex
    to_read =  ''.join('{:04b}'.format(x) for x in bytes.fromhex(len_bits_hex))
    if ChaCha20key != "":
        if len(len_bits) % 8 == 0 :
            len_bits = '0' * 4 + len_bits
            len_bits_hex = hex(int(len(len_bits)/4))[2:]
            if len(len_bits_hex) % 2 != 0:
                len_bits_hex = '0' + len_bits_hex
            to_read =  ''.join('{:04b}'.format(x) for x in bytes.fromhex(len_bits_hex))
        length_control_info = to_read + len_bits
        #print(to_read, len_bits, length_control_info)
        #print(cipherfunctions.nonce)
        auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
        #print(cipherfunctions.nonce)
        #auxChaCha20key = ChaCha20key
        len_data_length = hex(int(length_control_info[:8],2))[2:]
        if len(len_data_length) % 2 != 0:
            len_data_length = '0' + len_data_length
        #print(cipherfunctions.nonce,auxChaCha20key )
        len_data_length = cipherfunctions.cipherChaCha20(len_data_length, auxChaCha20key, "hex")
        #print(cipherfunctions.nonce,auxChaCha20key )
        if length_control_info[8:] != '':
            data_length_hex = hex(int(length_control_info[8:],2))[2:]
            if len(data_length_hex)% 2 != 0:
                data_length_hex = '0' + data_length_hex
            #print("control", length_control_info,length_control_info[8:],data_length_hex)
            #print(cipherfunctions.nonce,auxChaCha20key )
            data_length_hex =  cipherfunctions.cipherChaCha20(data_length_hex, auxChaCha20key, "hex")
            if len(data_length_hex)% 2 != 0:
                data_length_hex = '0' + data_length_hex
            #print(cipherfunctions.nonce,auxChaCha20key )
            #print("control", length_control_info,length_control_info[8:],data_length_hex,len_data_length + data_length_hex)
            data = len_data_length + data_length_hex + data
        else:
            data = len_data_length + data
    else:
        length_control_info = to_read + len_bits
        #print("control", length_control_info, to_read , len_bits,  hex(int(length_control_info,2))[2:])
        hex_control_info =  hex(int(length_control_info,2))[2:]
        if len(hex_control_info)% 2 != 0:
            hex_control_info = '0' + hex_control_info
        data =  hex_control_info + data
    #to bits:
    data = ''.join('{:08b}'.format(x) for x in bytes.fromhex(data))
    data_old = copy.deepcopy(data)
    #sizes_number = [sizes[i%len(sizes)] for i in range(blocks)]
    #sum_sizes = [sum(x) for x in sizes_number]
    completed = False
    i = 0
    while i < 255 and not completed:
        sizes_number = sizes[i%len(sizes)]
        sum_sizes = sum(sizes_number)
        #print("sizes and sum", sizes_number, sum_sizes)
        while len(data) < sum_sizes:
            rest = sum_sizes - len(data)
            data += random.choice(['0', '1'])
        message_prepared.append(data[:sum_sizes])
        data = data[sum_sizes:]
        if len(data) == 0:
            completed = True
        i+=1
    if len(message_prepared)>255 or not completed:
        print("Please, reduce the size of the message and try again\n")
        exit(1)
    return message_prepared, form, lengthsmin

#TODO: change the readbytes of length to max size 2
def preparemessageconstruccall_old(data, sizes, dynamic,password,ChaCha20key, default, form="", number = 0):
    """
    Insert the extra transaction information and prepare the message acording to
    the arguments sizes
    """
    #print(sizes, dynamic)
    message_prepared = []
    if  len(dynamic[-1]) > 0:
        blocks = 1
        if form == "":
            if default:
                form = '00'
                lengthsmin = []
            else:
                print("Do you want to distribute the message weight between the arguments?(y/n)\n")
                Correct = False
                while not Correct:
                    option = input()
                    if option == "y" or option == "yes":
                        form = '01'
                        Correct = True
                    elif option == "n" or option == "no":
                        form = '00'
                        Correct = True
                    else:
                        print ("Option not valid\n")
        lengthsmin = []
        for i in range(blocks):
            aux = hex((i+1))[2:]
            if len(aux)%2 != 0:
                aux = '0' + aux
            if password != "":
                #print("dataç",data)
                data = disordermessage([data], password)[0]
            if ChaCha20key != "":
                auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
                #print(aux)
                auxtotal =  cipherfunctions.cipherChaCha20(aux + aux, auxChaCha20key, "hex")
                #form = cipherChaCha20(form, ChaCha20key, "hex")
                auxm =  form + auxtotal + data
                # len_message = hex(data)[2:]
                # if len(len_message) % 2 != 0:
                #     len_message = '0' + len_message
                # len_len_message = hex(len(len_message))[2:]
                # if len(len_len_message) % 2 != 0:
                #     len_len_message = '0' + len_len_message
                # message_prepared.append(  aux + aux + len_len_message + len_message + form + data)
                #print(aux, auxtotal)
                len_message = hex(len(auxm))[2:]
                if len(len_message) % 2 != 0:
                    len_message = '0' + len_message
                len_len_message = hex(len(len_message))[2:]
                if len(len_len_message) % 2 != 0:
                    len_len_message = '0' + len_len_message
                #lengthsmin.append(len(len_len_message) + len(len_message) + 2)
                lengthsmin.append(len(len_len_message) + len(len_message) + 2)
                #print(lengthsmin,len_len_message,len_message,form,auxm)
                len_message_form =  cipherfunctions.cipherChaCha20( len_message + form, auxChaCha20key, "hex")
                len_len_message =  cipherfunctions.cipherChaCha20( len_len_message, auxChaCha20key, "hex")
                #print(len_len_message,len_message_form,auxtotal,data)
                message_prepared.append(len_len_message + len_message_form + auxtotal + data)

            else:
                message_prepared.append( form + aux + aux + data)
                # len_message = hex(data)[2:]
                # if len(len_message) % 2 != 0:
                #     len_message = '0' + len_message
                # len_len_message = hex(len(len_message))[2:]
                # if len(len_len_message) % 2 != 0:
                #     len_len_message = '0' + len_len_message
                # message_prepared.append(  aux + aux + len_len_message + len_message + form + data)
                len_message = hex(len(message_prepared[-1]))[2:]
                if len(len_message) % 2 != 0:
                    len_message = '0' + len_message
                len_len_message = hex(len(len_message))[2:]
                if len(len_len_message) % 2 != 0:
                    len_len_message = '0' + len_len_message
                message_prepared[-1] = len_len_message + len_message + message_prepared[-1]
                lengthsmin.append(len(len_len_message) + len(len_message) + 2)


    else:
        #print("aqui")
        if default:
             form = '10'
        else:
            if form == "":
                print("Do you want to distribute the message weight among contracts/functions?(y/n)\n")
                Correct = False
                while not Correct:
                    option = input()
                    if option == "y" or option == "yes":
                        form = '0'
                        Correct = True
                    elif option == "n" or option == "no":
                        form = '1'
                        Correct = True
                    else:
                        print ("Option not valid\n")
                if form == '0':
                    print("Introduce the approximate size (bytes)to use per contract/function to use\n")
                    Correct = False
                    while not Correct:
                        number = int(input())*2
                        if number <= globalsettings.max_number_transactions and number >=2:
                            Correct = True
                        else:
                            print ("Option not valid. It must be a number between 2 and 255 \n")
                print("Do you want to distribute the message weight between the arguments?(y/n)\n")
                Correct = False
                while not Correct:
                    option = input()
                    if option == "y" or option == "yes":
                        form += '1'
                        Correct = True
                    elif option == "n" or option == "no":
                        form += '0'
                        Correct = True
                    else:
                        print ("Option not valid\n")
        lengthsmin = []
        if form[0] == '0':
            #len in each form. 2 from form + number of transactions:
            #message_per_block, r = divmod ((len(data) + ((2 + len(hex(globalsettings.max_number_transactions)[2:])*2) * number )), number)
            blocks, r = divmod (len(data), number-(2 + len(hex(globalsettings.max_number_transactions)[2:])*2))
            #print(number,blocks,r,number-(2 + len(hex(globalsettings.max_number_transactions)[2:])*2),len(data))
            if r!= 0:
                blocks += 1
            message_per_block, r = divmod ((len(data) + ((2 + len(hex(globalsettings.max_number_transactions)[2:])*2) * blocks )), blocks)
            while r > message_per_block:
                blocks += 1
                message_per_block, r = divmod ((len(data) + ((2 + len(hex(globalsettings.max_number_transactions)[2:])*2) * blocks )), blocks)
            #print(len(data),((2 + len(hex(globalsettings.max_number_transactions)[2:])*2) * blocks), blocks, number,message_per_block, r)
            if r!= 0:
                blocks += 1
            while (message_per_block < 14 ) and blocks > 0:
                blocks -= 1
                message_per_block, r = divmod ((len(data) + ((2 + len(hex(globalsettings.max_number_transactions)[2:])*2) * blocks )), blocks)
            len_message = hex(message_per_block)[2:]
            #print(message_per_block, r,len(data), (2 + len(hex(globalsettings.max_number_transactions)[2:])*2) * blocks)
            if len(len_message) % 2 != 0:
                len_message = '0' + len_message
            len_len_message = hex(len(len_message))[2:]
            if len(len_len_message) % 2 != 0:
                len_len_message = '0' + len_len_message
            lens = [(len(len_len_message) + len(len_message) + message_per_block)] * (blocks - 1)
            if r == 0:
                lens = [(len(len_len_message) + len(len_message) + message_per_block)] * (blocks)
            else:
                lens = [(len(len_len_message) + len(len_message) + message_per_block)] * (blocks -1)
                if r < 14:
                    for x in range(r):
                        len_message = hex(message_per_block +1 )[2:]
                        #print(message_per_block, r,len(data), (2 + len(hex(globalsettings.max_number_transactions)[2:])*2) * blocks)
                        if len(len_message) % 2 != 0:
                            len_message = '0' + len_message
                        len_len_message = hex(len(len_message))[2:]
                        if len(len_len_message) % 2 != 0:
                            len_len_message = '0' + len_len_message
                        lens[x] = (len(len_len_message) + len(len_message) + (message_per_block+1))
                        #print(lens)
                    #lens.pop()
                    blocks -= 1
                else:
                    # print("here")
                    # print(lens,r)
                    #lens.pop()
                    len_message_last = hex(r + (2 + len(hex(globalsettings.max_number_transactions)[2:])*2))[2:]
                    if len(len_message_last) % 2 != 0:
                        len_message_last = '0' + len_message_last
                    len_len_message_last = hex(len(len_message_last))[2:]
                    if len(len_len_message_last) % 2 != 0:
                        len_len_message_last = '0' + len_len_message_last
                    #print(lens)
                    #blocks += 1
                    lens.append((len(len_len_message_last) + len(len_message_last) + r + (2 + len(hex(globalsettings.max_number_transactions)[2:])*2)))
            #print(data,lens,blocks)

            #print(lens,blocks)
            # if r > message_per_block:
            #     lens.append((len(len_len_message) + len(len_message) + message_per_block))
            #     blocks += 1
            #     while r > message_per_block and (r- message_per_block) > 10:
            #         lens.append((len(len_len_message) + len(len_message) + message_per_block))
            #         r -= message_per_block
            #         blocks += 1
            #     len_message_last = hex(r)[2:]
            #     if len(len_message_last) % 2 != 0:
            #         len_message_last = '0' + len_message_last
            #     len_len_message_last = hex(len(len_message_last))[2:]
            #     if len(len_len_message_last) % 2 != 0:
            #         len_len_message_last = '0' + len_len_message_last
            #     lens.append((len(len_len_message_last) + len(len_message_last) + r))
            # else:
            #     len_message_last = hex(message_per_block + r)[2:]
            #     if len(len_message_last) % 2 != 0:
            #         len_message_last = '0' + len_message_last
            #     len_len_message_last = hex(len(len_message_last))[2:]
            #     if len(len_len_message_last) % 2 != 0:
            #         len_len_message_last = '0' + len_len_message_last
            #     lens.append((len(len_len_message_last) + len(len_message_last) + message_per_block + r))
            sizes_number = [sizes[i%len(sizes)] for i in range(blocks)]
            sum_sizes = [sum(x) for x in sizes_number]
            #print("J",sum_sizes, sizes, lens,message_per_block, r )
            while(sum(lens) > sum(sum_sizes)) and blocks <= globalsettings.max_number_transactions:
                blocks += 1
                message_per_block, r = divmod ((len(data) + ((2 + len(hex(globalsettings.max_number_transactions)[2:])*2) * blocks )), blocks)
                # while r > message_per_block:
                #     blocks += 1
                #     message_per_block, r = divmod ((len(data) + ((2 + len(hex(globalsettings.max_number_transactions)[2:])*2) * blocks )), blocks)
                #print(len(data), ((2 + len(hex(globalsettings.max_number_transactions)[2:])*2) * blocks ), blocks,message_per_block, r)
                if r!= 0:
                    blocks += 1
                # while (message_per_block < 14 ) and blocks > 0:
                #     blocks -= 1
                #     message_per_block, r = divmod ((len(data) + ((2 + len(hex(globalsettings.max_number_transactions)[2:])*2) * blocks )), blocks)
                len_message = hex(message_per_block)[2:]
                #print(message_per_block, r,len(data), (2 + len(hex(globalsettings.max_number_transactions)[2:])*2) * blocks)
                if len(len_message) % 2 != 0:
                    len_message = '0' + len_message
                len_len_message = hex(len(len_message))[2:]
                if len(len_len_message) % 2 != 0:
                    len_len_message = '0' + len_len_message
                #lens = [(len(len_len_message) + len(len_message) + message_per_block)] * (blocks - 1)
                if r == 0:
                    lens = [(len(len_len_message) + len(len_message) + message_per_block)] * (blocks)
                else:
                    lens = [(len(len_len_message) + len(len_message) + message_per_block)] * (blocks -1)
                    # print("here")
                    # print(lens,r)
                    #lens.pop()
                    len_message_last = hex(r + (2 + len(hex(globalsettings.max_number_transactions)[2:])*2))[2:]
                    if len(len_message_last) % 2 != 0:
                        len_message_last = '0' + len_message_last
                    len_len_message_last = hex(len(len_message_last))[2:]
                    if len(len_len_message_last) % 2 != 0:
                        len_len_message_last = '0' + len_len_message_last
                    #print(lens)
                    #blocks += 1
                    lens.append((len(len_len_message_last) + len(len_message_last) + r + (2 + len(hex(globalsettings.max_number_transactions)[2:])*2)))
                sizes_number = [sizes[i%len(sizes)] for i in range(blocks)]
                sum_sizes = [sum(x) for x in sizes_number]
                # message_per_block, r = divmod ((len(data) + ((2 + len(hex(globalsettings.max_number_transactions)[2:])*2) * blocks )), blocks)
                # len_message = hex(message_per_block)[2:]
                # if len(len_message) % 2 != 0:
                #     len_message = '0' + len_message
                # len_len_message = hex(len(len_message))[2:]
                # if len(len_len_message) % 2 != 0:
                #     len_len_message = '0' + len_len_message
                # len_message_last = hex(message_per_block + r)[2:]
                # if len(len_message_last) % 2 != 0:
                #     len_message_last = '0' + len_message_last
                # len_len_message_last = hex(len(len_message_last))[2:]
                # if len(len_len_message_last) % 2 != 0:
                #     len_len_message_last = '0' + len_len_message_last
                # lens = [(len(len_len_message) + len(len_message) + message_per_block)] * (blocks - 1)
                # lens.append((len(len_len_message_last) + len(len_message_last) + message_per_block + r))
                # sizes_number = [sizes[i%len(sizes)] for i in range(blocks)]
                # sum_sizes = [sum(x) for x in sizes_number]
                # print(lens, blocks)
                # print(sum_sizes, lens )
            if blocks > globalsettings.max_number_transactions:
                print("The message is too large\n")
                exit(1)
            else:
                lens = calling.reajustf(sum_sizes, lens)
                lens = calling.makeeven (sum_sizes, lens)
                #print(lens)
            total_n = hex(blocks)[2:]
            if len(total_n) %2 != 0:
                total_n = '0' + total_n
            for i in range(blocks):
                #print("block", i, lens[i])
                aux = hex((i+1))[2:]
                if len(aux)%2 != 0:
                    aux = '0' + aux
                # if password != "":
                #     data = disordermessage(data, password)[0]
                len_message = hex(lens[i])[2:]
                if len(len_message) % 2 != 0:
                    len_message = '0' + len_message
                len_len_message = hex(len(len_message))[2:]
                if len(len_len_message) % 2 != 0:
                    len_len_message = '0' + len_len_message
                total = len(len_len_message) + len(len_message)
                rest = lens[i] - total - len(aux) - len(total_n) - 2
                if rest >=0:
                    #print(rest)
                    #TODO: could add block number to message disorder
                    dat = data[:rest]
                    if password != "":
                        dat = disordermessage([dat], password)[0]
                    if ChaCha20key != "":
                        auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
                        auxtotal =  cipherfunctions.cipherChaCha20(aux + total_n, auxChaCha20key, "hex")
                        auxm = form + auxtotal + dat
                        data = data[rest:]
                        #print("data", dat)
                        len_message = hex(len(auxm))[2:]
                        if len(len_message) % 2 != 0:
                            len_message = '0' + len_message
                        len_len_message = hex(len(len_message))[2:]
                        if len(len_len_message) % 2 != 0:
                            len_len_message = '0' + len_len_message
                        lengthsmin.append(len(len_len_message) + len(len_message) + 2)
                        #print("len_",len_message,len_len_message)
                        len_message_form =  cipherfunctions.cipherChaCha20( len_message + form, auxChaCha20key, "hex")
                        len_len_message =  cipherfunctions.cipherChaCha20( len_len_message, auxChaCha20key, "hex")
                        #print("len_",len_message_form,len_len_message,dat)
                        message_prepared.append(len_len_message + len_message_form + auxtotal + dat)
                    else:
                        message_prepared.append( form + aux + total_n + dat )
                        data = data[rest:]
                        len_message = hex(len(message_prepared[-1]))[2:]
                        if len(len_message) % 2 != 0:
                            len_message = '0' + len_message
                        len_len_message = hex(len(len_message))[2:]
                        if len(len_len_message) % 2 != 0:
                            len_len_message = '0' + len_len_message
                        message_prepared[-1] = len_len_message + len_message + message_prepared[-1]
                        lengthsmin.append(len(len_len_message) + len(len_message) + 2)
                else:
                    if i == (blocks -1):
                        blocks += 1
                        lens.append(lens[i] + total + len(aux) + len(total_n) + 2 + abs(rest))
                        # sizes_number = [sizes[i%len(sizes)] for i in range(blocks)]
                        # sum_sizes = [sum(x) for x in sizes_number]
                    else:
                        lens[(i+1)] += lens[i]
                    lens = calling.reajustf(sum_sizes, lens)
                    lens = calling.makeeven (sum_sizes, lens)
                    if ChaCha20key != "":
                        auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
                        auxtotal =  cipherfunctions.cipherChaCha20(aux + total_n, auxChaCha20key, "hex")
                        auxm = form + auxtotal
                        len_message = hex(len(auxm))[2:]
                        if len(len_message) % 2 != 0:
                            len_message = '0' + len_message
                        len_len_message = hex(len(len_message))[2:]
                        if len(len_len_message) % 2 != 0:
                            len_len_message = '0' + len_len_message
                        lengthsmin.append(len(len_len_message) + len(len_message) + 2)
                        #print("len_",len_message,len_len_message)
                        len_message_form =  cipherfunctions.cipherChaCha20( len_message + form, auxChaCha20key, "hex")
                        len_len_message =  cipherfunctions.cipherChaCha20( len_len_message, auxChaCha20key, "hex")
                        message_prepared.append(len_len_message + len_message_form + auxtotal)
                #TODO: Choose
                #len in total
                # message_per_block, r = divmod ((len(data) + (2 * number )), number)
                # while message_per_block < 10 and number > 0:
                #     number -= 1
                #     message_per_block, r = divmod ((len(data) + (2 * number )), number)
                # len_message = hex(data)[2:]
                # if len(len_message) % 2 != 0:
                #     len_message = '0' + len_message
                # len_len_message = hex(len(len_message))[2:]
                # if len(len_len_message) % 2 != 0:
                #     len_len_message = '0' + len_len_message
                # lens = [len(len_len_message) + len(len_message) + message_per_block + 2]
                # lens.append([message_per_block] * (number - 2))
                # lens.append(message_per_block + r))
                # sizes_number = [sizes[i%len(sizes)] for i in range(number)]
                # sum_sizes = [sum(x) for x in sizes_number]
                # while(sum(lens) > sum(sum_sizes)) and number <= 15:
                #     number += 1
                #     message_per_block, r = divmod ((len(data) + (2 * number )), number)
                #     lens = [len(len_len_message) + len(len_message) + message_per_block + 2]
                #     lens.append([message_per_block] * (number - 2))
                #     lens.append([message_per_block + r)])
                #     sizes_number = [sizes[i%len(sizes)] for i in range(number)]
                #     sum_sizes = [sum(x) for x in sizes_number]
                # if number > 15:
                #     print("The message is too large\n")
                #     exit(1)
                # else:
                #     lens = calling.reajustf(sum_sizes, lens)
                #     lens = calling.makeeven (sum_sizes, lens)
                # total_n = hex(number)[2:]
                # for i in range(number):
                #     aux = hex((i+1))[2:]
                #     # if password != "":
                #     #     data = disordermessage(data, password)[0]
                #     if i == 0:
                #         rest = lens[i] - len(len_len_message) - len(len_message) - len(aux) - len(total_n) -2
                #         dat = data[:rest]
                #         if password != "":
                #             dat = disordermessage(dat, password)[0]
                #         message_prepared.append( aux + total_n + len_len_message + len_message+ form+ dat )
                #         data = data[rest:]
                #     else:
                #         rest = lens[i] -  len(aux) - len(total_n)
                #         dat = data[:rest]
                #         if password != "":
                #             dat = disordermessage(dat, password)[0]
                #         message_prepared.append( aux + total_n + dat )
                #         data = data[rest:]
        else:
            #len in each form:
            blocks = 0
            message_prepared_aux = []
            #print("heyyyy")
            while data != "" and blocks < globalsettings.max_number_transactions:
                size = sizes[blocks%(len(sizes))]
                len_message = hex(sum(size))[2:]
                if len(len_message) % 2 != 0:
                    len_message = '0' + len_message
                len_len_message = hex(len(len_message))[2:]
                if len(len_len_message) % 2 != 0:
                    len_len_message = '0' + len_len_message
                rest = sum(size)  - len(len_len_message) - len(len_message) - 2 - len(hex(globalsettings.max_number_transactions)[2:])*2
                dat = data[:rest]
                if password != "":
                    dat = disordermessage([dat], password)[0]
                message_prepared_aux.append( dat )
                data = data[rest:]
                #print(data)
                blocks += 1
            if data != "":
                print("The message is too large\n")
                exit(1)
            total_n = hex((blocks))[2:]
            if len(total_n) %2 != 0:
                total_n = '0' + total_n
            for i in range(blocks):
                aux = hex((i+1))[2:]
                if len(aux)%2 != 0:
                    aux = '0' + aux
                size = sizes[i%(len(sizes))]
                len_message = hex(sum(size))[2:]
                if len(len_message) % 2 != 0:
                    len_message = '0' + len_message
                len_len_message = hex(len(len_message))[2:]
                if len(len_len_message) % 2 != 0:
                    len_len_message = '0' + len_len_message
                dat = message_prepared_aux[i]
                if ChaCha20key != "":
                    auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
                    auxtotal =  cipherfunctions.cipherChaCha20(aux + total_n, auxChaCha20key, "hex")
                    auxm = form + auxtotal + dat
                    len_message = hex(len(auxm))[2:]
                    if len(len_message) % 2 != 0:
                        len_message = '0' + len_message
                    len_len_message = hex(len(len_message))[2:]
                    if len(len_len_message) % 2 != 0:
                        len_len_message = '0' + len_len_message
                    lengthsmin.append(len(len_len_message) + len(len_message) + 2)
                    len_message_form =  cipherfunctions.cipherChaCha20( len_message + form, auxChaCha20key, "hex")
                    len_len_message =  cipherfunctions.cipherChaCha20( len_len_message, auxChaCha20key, "hex")
                    message_prepared.append(len_len_message + len_message_form + auxtotal + dat)
                else:
                    message_prepared.append( form + aux + total_n + dat )
                    len_message = hex(len(message_prepared[-1]))[2:]
                    if len(len_message) % 2 != 0:
                        len_message = '0' + len_message
                    len_len_message = hex(len(len_message))[2:]
                    if len(len_len_message) % 2 != 0:
                        len_len_message = '0' + len_len_message
                    message_prepared[-1] = len_len_message + len_message + message_prepared[-1]
                    lengthsmin.append(len(len_len_message) + len(len_message) + 2)
                #TODO: Choose
                # #len in total
                # number = 0
                # message_prepared_aux = []
                # while data != "" and number < 15:
                #     size = sizes[number%(len(sizes))]
                #     if number == 0:
                #         len_message = hex(len(data))[2:]
                #         if len(len_message) % 2 != 0:
                #             len_message = '0' + len_message
                #         len_len_message = hex(len(len_message))[2:]
                #         if len(len_len_message) % 2 != 0:
                #             len_len_message = '0' + len_len_message
                #         rest = sum(size)  - len(len_len_message) - len(len_message) - 2 - 2
                #         dat = data[:rest]
                #     else:
                #         rest = sum(size) - 2
                #         dat = data[:rest]
                #     if password != "":
                #         dat = disordermessage(dat, password)[0]
                #     message_prepared_aux.append( dat )
                #     number += 1
                #     data = data[rest:]
                # if data != "":
                #     print("The message is too large\n")
                #     exit(1)
                # total_n = hex((number))[2:]
                # for i in range(number):
                #     aux = hex((i+1))[2:]
                #     size = sizes[i%(len(sizes))]
                #     dat = message_prepared_aux[i]
                #     if i == 0:
                #         message_prepared.append( aux + total_n + len_len_message + len_message + form + dat )
                #     else:
                #         message_prepared.append( aux + total_n + dat )

    return message_prepared, form, lengthsmin

def orderblocks (data_in_transactions, ChaCha20key, nonces):
    """
    Orders the transactions passed with hex information
    """
    completed = False
    data_ordered = copy.deepcopy(data_in_transactions)
    nonces_ordered = copy.deepcopy(nonces)
    cont = 0
    # if ChaCha20key != "":
    #     totalaux = cipherfunctions.ecipherChaCha20 (data_in_transactions[0][:2], ChaCha20key, "hex")
    #     print(totalaux)
    #     total = int(totalaux[1], 16)
    # else:
    #     total = int(data_in_transactions[0][1], 16)
    for x in data_in_transactions:
        if ChaCha20key != "":
            totalaux = cipherfunctions.ecipherChaCha20 (x[:len(hex(globalsettings.max_number_transactions)[2:])*2], cipherfunctions.upgradeChaCha20key(ChaCha20key), "hex")
            index = int(totalaux[:len(hex(globalsettings.max_number_transactions)[2:])], 16)
            total = int(totalaux[len(hex(globalsettings.max_number_transactions)[2:]):], 16)
            print("Transaction {} of {}".format(index,total))
        else:
            index = int(x[:len(hex(globalsettings.max_number_transactions)[2:])], 16)
            total = int(x[len(hex(globalsettings.max_number_transactions)[2:]):len(hex(globalsettings.max_number_transactions)[2:])*2], 16)
        data_ordered[(index-1)] = x
        nonces_ordered[(index-1)] = nonces[cont]
        cont += 1
    if cont != total:
        print("Some transactions are missing\n")
        exit(1)
    return data_ordered,nonces_ordered

def checkifsingle (data_in_transactions, ChaCha20key):
    """
    Check if the hash passed is part of a single-transaction message,
    is one of the transactions that contain the message or it does not
    seem to contain a message
    """
    single = False
    withmessage = True
    data_array = copy.deepcopy(data_in_transactions)
    total = 0
    # if ChaCha20key != "":
    #     totalaux = cipherfunctions.ecipherChaCha20 (data_in_transactions[0][:2], ChaCha20key, "hex")
    #     print(totalaux)
    #     total = int(totalaux[1], 16)
    # else:
    #     total = int(data_in_transactions[0][1], 16)

    if ChaCha20key != "":
        totalaux = cipherfunctions.ecipherChaCha20 (data_in_transactions[0][:len(hex(globalsettings.max_number_transactions)[2:])*2], cipherfunctions.upgradeChaCha20key(ChaCha20key), "hex")
        index = int(totalaux[:len(hex(globalsettings.max_number_transactions)[2:])], 16)
        total = int(totalaux[len(hex(globalsettings.max_number_transactions)[2:]):], 16)
    else:
        index = int(data_in_transactions[0][:len(hex(globalsettings.max_number_transactions)[2:])], 16)
        total = int(data_in_transactions[0][len(hex(globalsettings.max_number_transactions)[2:]):len(hex(globalsettings.max_number_transactions)[2:])*2], 16)
    if index < 1 or index > total or total < 1:
        withmessage = False
        print("This transaction hash does not seem to contain a message\n")
    else:
        if total == 1:
            single = True
            data_array[(index-1)] = data_in_transactions[0]
        else:
            data_array = ['0'] * total
            data_array[(index-1)] = data_in_transactions[0]
        print("Transaction {} of {}".format(index,total))
    return single, data_array, total, index, withmessage

def checkifpartofmessage (data_to_retrieve, nonces_tx, data_in_transactions, cumulative_nonces, ChaCha20key):
    i = 0
    added= 0
    while i < len(data_to_retrieve):
        if ChaCha20key != "":
            totalaux = cipherfunctions.ecipherChaCha20 (data_to_retrieve[i][:len(hex(globalsettings.max_number_transactions)[2:])*2], cipherfunctions.upgradeChaCha20key(ChaCha20key), "hex")
            index = int(totalaux[:len(hex(globalsettings.max_number_transactions)[2:])], 16)
            total = int(totalaux[len(hex(globalsettings.max_number_transactions)[2:]):], 16)
        else:
            index = int(data_to_retrieve[0][:len(hex(globalsettings.max_number_transactions)[2:])], 16)
            total = int(data_to_retrieve[0][len(hex(globalsettings.max_number_transactions)[2:]):len(hex(globalsettings.max_number_transactions)[2:])*2], 16)
        if total == len(data_in_transactions) and index > 0 and index <= len(data_in_transactions):
            data_in_transactions[(index-1)] = data_to_retrieve[i]
            cumulative_nonces[(index-1)] = nonces_tx[i]
            print("Transaction {} of {}".format(index,total))
            added += 1
        i += 1
    return data_in_transactions ,cumulative_nonces, added

def orderblocksvalueMethod (data_in_transactions, dict_valuespossible,bits_per_length, ChaCha20key, field = ""):
    """
    Orders information of blocks for value method
    """
    completed = False
    data_ordered = copy.deepcopy(data_in_transactions)
    cont = 0
    # if ChaCha20key != "":
    #     totalaux = cipherfunctions.ecipherChaCha20 (data_in_transactions[0][:2], ChaCha20key, "hex")
    #     print(totalaux)
    #     total = int(totalaux[1], 16)
    # else:
    #     total = int(data_in_transactions[0][1], 16)
    for x in data_in_transactions:
        #print(x)
        tmp_full = str(int(x,16))
        #print(tmp_full)
        length = len(tmp_full)
        tmp = tmp_full.rstrip('0')
        zeros = len(tmp_full) - len(tmp)
        #print(length, zeros,tmp)
        values = dict_valuespossible[length][zeros]
        #print(values)
        number_bits = bits_per_length[length][zeros]
        #print(number_bits)
        element = values.index(int(tmp))
        #print(element)
        x = copy.deepcopy(hex(element)[2:])
        if len(x)%2 !=0:
            x = '0'+ x
        if ChaCha20key != "":
            #print(x,cipherfunctions.nonce, ChaCha20key)
            # auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
            # print(cipherfunctions.nonce, auxChaCha20key)
            xtmp = '{0:08b}'.format(int(element))
            #print(number_bits)
            if len(xtmp) < number_bits:
                necessary = number_bits -len(xtmp)
                xtmp = '0' * necessary + xtmp
            #print(xtmp)
            xhex = xtmp[:len(hex(globalsettings.max_number_transactions)[2:])*8]
            xhex= hex(int(xhex, 2))[2:]
            #print(xhex)
            if len(xhex)%2 !=0:
                xhex = '0'+ xhex
            #print(xhex)
            if len(xhex) < len(hex(globalsettings.max_number_transactions)[2:])*2:
                xhex = '00'+ xhex
            totalaux = cipherfunctions.ecipherChaCha20 (xhex, cipherfunctions.upgradeChaCha20key(ChaCha20key), "hex")
            #totalaux = cipherfunctions.ecipherChaCha20 (x[:len(hex(globalsettings.max_number_transactions)[2:])*2], auxChaCha20key , "hex")
            #print(totalaux)
            index = int(totalaux[:len(hex(globalsettings.max_number_transactions)[2:])], 16)
            total = int(totalaux[len(hex(globalsettings.max_number_transactions)[2:]):], 16)
            print("Transaction {} of {}".format(index,total))
        else:
            xtmp = '{0:08b}'.format(int(element))
            #print(number_bits)
            if len(xtmp) < number_bits:
                necessary = number_bits -len(xtmp)
                xtmp = '0' * necessary + xtmp
            #print(xtmp)
            xhex = xtmp[:len(hex(globalsettings.max_number_transactions)[2:])*8]
            xhex= hex(int(xhex, 2))[2:]
            #print(xhex)
            if len(xhex)%2 !=0:
                xhex = '0'+ xhex
            #print(xhex)
            if len(xhex) < len(hex(globalsettings.max_number_transactions)[2:])*2:
                xhex = '00'+ xhex
            index = int(xhex[:len(hex(globalsettings.max_number_transactions)[2:])], 16)
            total = int(xhex[len(hex(globalsettings.max_number_transactions)[2:]):len(hex(globalsettings.max_number_transactions)[2:])*2], 16)
        data_ordered[(index-1)] = xtmp
        cont += 1
    if cont != total:
        print("Some transactions are missing\n")
        exit(1)
    return data_ordered

def orderblocksvalue (data_in_transactions, ChaCha20key, field = ""):
    """
    Orders the transactions passed with hex information
    """
    completed = False
    data_ordered = copy.deepcopy(data_in_transactions)
    cont = 0
    # if ChaCha20key != "":
    #     totalaux = cipherfunctions.ecipherChaCha20 (data_in_transactions[0][:2], ChaCha20key, "hex")
    #     print(totalaux)
    #     total = int(totalaux[1], 16)
    # else:
    #     total = int(data_in_transactions[0][1], 16)
    for x in data_in_transactions:
        if len(x)%2 !=0:
            x = '0'+ x
        if ChaCha20key != "":
            # print(x,cipherfunctions.nonce, ChaCha20key)
            # auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
            # print(cipherfunctions.nonce, auxChaCha20key)
            totalaux = cipherfunctions.ecipherChaCha20 (x[:len(hex(globalsettings.max_number_transactions)[2:])*2], cipherfunctions.upgradeChaCha20key(ChaCha20key), "hex")
            #totalaux = cipherfunctions.ecipherChaCha20 (x[:len(hex(globalsettings.max_number_transactions)[2:])*2], auxChaCha20key , "hex")
            print(totalaux)
            index = int(totalaux[:len(hex(globalsettings.max_number_transactions)[2:])], 16)
            total = int(totalaux[len(hex(globalsettings.max_number_transactions)[2:]):], 16)
            print("Transaction {} of {}".format(index,total))
        else:
            index = int(x[:len(hex(globalsettings.max_number_transactions)[2:])], 16)
            total = int(x[len(hex(globalsettings.max_number_transactions)[2:]):len(hex(globalsettings.max_number_transactions)[2:])*2], 16)
        data_ordered[(index-1)] = x
        cont += 1
    if cont != total:
        print("Some transactions are missing\n")
        exit(1)
    return data_ordered

def checkextractbytecode (data_in_transactions,len_to_read,len_of_length, total_length, message, in_next_transaction, first_low_length,ChaCha20key,field = ""):
    rest = 0
    all_extracted = False
    for x in range(len(data_in_transactions)):
        #Extract total length of the message:
        aux = data_in_transactions[x]
        #print("aux", aux, message,data_in_transactions)
        if first_low_length:
            #print("First minor 8")
            aux = total_length + aux
            #print("aux", aux, message,data_in_transactions)
            if ChaCha20key != "":
                toread = aux[:4]
                #Number in bytes
                #print(aux, toread)
                # toread = int(toread,2)
                # to_read_bits = toread * 8
                aux_hex = hex(int(aux[:8],2))[2:]
                if len(aux_hex) %2 != 0:
                    aux_hex = '0' + aux_hex
                #len
                #print("aux",aux_hex)
                auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
                #recover the whole byte to decipher
                #print("aux2",aux_hex[0:(len_to_read+1)])
                len_of_length_tmp = cipherfunctions.ecipherChaCha20(aux_hex[0:(len_to_read+1)], auxChaCha20key, "hex")
                #print("after",len_of_length_tmp)
                len_of_length_tmp = '{:08b}'.format(int(len_of_length_tmp,16))
                #print("bits",len_of_length_tmp)
                len_of_length = len_of_length_tmp[0:(len_to_read *4)]
                #print(len_of_length)
                #print(len_of_length)
                #len_of_length = ''.join('{:08b}'.format(x) for x in bytes.fromhex(len_of_length))
                len_of_length = int(len_of_length,2)
                len_of_length_bits = len_of_length * 4
                #print(len_of_length, len_of_length_bits)
                #print("len_of_length",len_of_length )
                #print(aux, (aux[len_to_read:(len_to_read+len_of_length)]), len(aux[len_to_read:(len_to_read+len_of_length)]))
                #print(aux_hex[len_to_read:(len_to_read+len_of_length)])
                if (len(aux_hex[len_to_read:(len_to_read+len_of_length)]) == len_of_length) or len(aux) >= (len_of_length_bits +4):
                    counter = len_of_length
                    total_length = ''
                    #print("aqui", counter)
                    retrieved = False
                    while counter != 0 and not retrieved:
                        if counter % 2 != 0:
                            total_length += len_of_length_tmp[(len_to_read*4):]
                            counter -= 1
                            #print("h",len_to_read, len_of_length_tmp,total_length, len_of_length*4, len(total_length),(len(total_length)+(len_to_read *4)) )
                        elif counter %2 == 0 and counter != 0 and len(total_length) != (len_of_length * 4):
                            aux_hex = hex(int(aux[(len(total_length)+(len_to_read *4)):((len_of_length*4)+len(total_length))],2))[2:]
                            if len(aux_hex) %2 != 0:
                                aux_hex = '0' + aux_hex
                            #print(aux_hex,aux,aux[(len(total_length)+(len_to_read *4)):((len_of_length*4)+len(total_length))])
                            total_length_tmp = cipherfunctions.ecipherChaCha20(aux_hex, auxChaCha20key, "hex")
                            total_length_tmp = '{:08b}'.format(int(total_length_tmp,16))
                            total_length += total_length_tmp
                            counter -= 2
                            #print("o", total_length)
                        if len(total_length) == (len_of_length * 4):
                            print("Retrieved")
                            retrieved = True
                    #print("Firstafter", len_of_length,total_length)
                    total_length = int(total_length,2)
                    total_length_bits = total_length * 8
                    rest = (len_of_length*4) +(len_to_read *4)
                    #print("rest", rest, (len_of_length*4), (len_to_read *4))
                    in_next_transaction = False
                    first_low_length =False
                    #print(total_length,total_length_bits)
                else:
                    #print("here")
                    total_length = ''
                    total_length = len_of_length_tmp[
                    (len_to_read*4):] + aux[8:]
                    #print(total_length,aux_hex[(len_to_read +1):(len_to_read+1+len_of_length)])
                    in_next_transaction = True
                    first_low_length =False
        elif in_next_transaction:
            #print("next", len_of_length,total_length)
            rest = (len_of_length*4) - len(total_length)
            #print(rest, aux)
            #tmp = aux[:(rest*4)]
            tmp = aux[:rest]
            #print(tmp,tmp[:8])
            if len(total_length) > 4:
                tmp = total_length[4:]+ tmp
            #print(tmp)
            #aux = copy.deepcopy(tmp)
            #New three delete if not working
            #aux = copy.deepcopy(tmp[8:])
            #rest = 0
            #print(aux)
            tmp = int(tmp[:8],2)
            #print(tmp)
            aux_hex = hex(tmp)[2:]
            if len(aux_hex) %2 != 0:
                aux_hex = '0' + aux_hex
            #total_length += aux_hex[:rest]
            #print("next2", aux_hex)
            if len(aux_hex)== 2:
                if ChaCha20key!= "":
                    auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
                    total_length_tmp = cipherfunctions.ecipherChaCha20(aux_hex, auxChaCha20key, "hex")
                    if len(total_length) > 4:
                        total_length = total_length[:4] + '{:08b}'.format(int(total_length_tmp,16))
                    else:
                        total_length += '{:08b}'.format(int(total_length_tmp,16))
                    #print(total_length_tmp, total_length)
                    total_length = int(total_length,2)
                    total_length_bits = total_length * 8
                    #print(total_length_tmp, total_length)
                else:
                    total_length += aux[:int((rest/2)*8)]
                    total_length = int(total_length,2)
                    total_length_bits = total_length * 8
                in_next_transaction = False
            else:
                in_next_transaction = True
                aux = ''
            #print(total_length, message)
        #print(rest)
        if rest != 0:
            #print("ups", aux,aux[8:], aux[rest:] )
            #message += aux[8:]
            message += aux[rest:]
            #print("message", message)
            rest = 0
        else:
            message += aux
            #print(message)
        #print(message)
    #print(message)
    if len(message)>= (total_length * 8):
        all_extracted = True
    #message_bits = ''.join('{:08b}'.format(x) for x in bytes.fromhex(message))
    # message_bits = message[:total_length_bits]
    if all_extracted:
        total_length_bits = total_length * 8
        message = message[:total_length_bits]
        print("all",total_length,total_length_bits)
    # message = bytearray([int(message_bits[i:i+8], 2) for i in range(0, len(message_bits), 8)]).hex()
    #print(message)
    return message,len_of_length, total_length, message, in_next_transaction,all_extracted,first_low_length

def checkifsinglebytecodeMethod (data_in_transactions,len_to_read,ChaCha20key, field = ""):
    """
    Check if the hash passed is part of a single-transaction message,
    is one of the transactions that contain the message or it does not
    seem to contain a message
    """
    single = False
    withmessage = True
    data_array = copy.deepcopy(data_in_transactions)
    total = 0
    aux_total = ''
    total_length = 0
    in_next_transaction = False
    first_low_length = False
    aux = data_in_transactions[0]
    #print("aux", aux)
    message = ''
    if len(aux) >=8:
        # if ChaCha20key != "":
        #     totalaux = cipherfunctions.ecipherChaCha20 (data_in_transactions[0][:2], ChaCha20key, "hex")
        #     print(totalaux)
        #     total = int(totalaux[1], 16)
        # else:
        #     total = int(data_in_transactions[0][1], 16
        #     data_in_transactions[0] = copy.deepcopy(hex(element)[2:])
        # if len(data_in_transactions[0])%2 !=0:
        #     data_in_transactions[0] = '0'+ data_in_transactions[0]
        if ChaCha20key != "":
            toread = aux[:4]
            #Number in bytes
            #print(aux, toread)
            # toread = int(toread,2)
            # to_read_bits = toread * 8
            aux_hex = hex(int(aux[:8],2))[2:]
            if len(aux_hex) %2 != 0:
                aux_hex = '0' + aux_hex
            #len
            #print("aux",aux_hex)
            auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
            #recover the whole byte to decipher
            #print("aux2",aux_hex[0:(len_to_read+1)])
            len_of_length_tmp = cipherfunctions.ecipherChaCha20(aux_hex[0:(len_to_read+1)], auxChaCha20key, "hex")
            #print("after",len_of_length_tmp)
            len_of_length_tmp = '{:08b}'.format(int(len_of_length_tmp,16))
            #print("bits",len_of_length_tmp)
            len_of_length = len_of_length_tmp[0:(len_to_read *4)]
            #print(len_of_length)
            #print(len_of_length)
            #len_of_length = ''.join('{:08b}'.format(x) for x in bytes.fromhex(len_of_length))
            len_of_length = int(len_of_length,2)
            len_of_length_bits = len_of_length * 4
            #print(len_of_length, len_of_length_bits)
            #print("len_of_length",len_of_length )
            #print(aux, (aux[len_to_read:(len_to_read+len_of_length)]), len(aux[len_to_read:(len_to_read+len_of_length)]))
            #print(aux_hex[len_to_read:(len_to_read+len_of_length)])
            if (len(aux_hex[len_to_read:(len_to_read+len_of_length)]) == len_of_length) or len(aux) >= (len_of_length_bits +4):
                counter = len_of_length
                total_length = ''
                #print("aqui", counter)
                retrieved = False
                while counter != 0 and not retrieved:
                    if counter % 2 != 0:
                        total_length += len_of_length_tmp[(len_to_read*4):]
                        counter -= 1
                        #print("h",len_to_read, len_of_length_tmp,total_length, len_of_length*4, len(total_length),(len(total_length)+(len_to_read *4)) )
                    elif counter %2 == 0 and counter != 0 and len(total_length) != (len_of_length * 4):
                        aux_hex = hex(int(aux[(len(total_length)+(len_to_read *4)):((len_of_length*4)+len(total_length))],2))[2:]
                        if len(aux_hex) %2 != 0:
                            aux_hex = '0' + aux_hex
                        #print(aux_hex,aux,aux[(len(total_length)+(len_to_read *4)):((len_of_length*4)+len(total_length))])
                        total_length_tmp = cipherfunctions.ecipherChaCha20(aux_hex, auxChaCha20key, "hex")
                        #print("y", total_length_tmp)
                        total_length_tmp = '{:08b}'.format(int(total_length_tmp,16))
                        while len(total_length_tmp) % 4 != 0:
                            total_length_tmp = '0' + total_length_tmp
                        total_length += total_length_tmp
                        counter -= 2
                        #print("o", total_length)
                    if len(total_length) == (len_of_length * 4):
                        #print("Retrieved")
                        retrieved = True
                total_length = int(total_length,2)
                total_length_bits = total_length * 8
                #print(total_length,total_length_bits)
            else:
                #print("here")
                total_length = ''
                total_length = len_of_length_tmp[
                (len_to_read*4):] + aux[8:]
                #print(total_length,aux_hex[(len_to_read +1):(len_to_read+1+len_of_length)])
                in_next_transaction = True

        else:
            len_of_length = aux[0:len_to_read]
            len_of_length = '{:04b}'.format(int(len_of_length,16))
            #len_of_length = ''.join('{:08b}'.format(x) for x in bytes.fromhex(len_of_length))
            len_of_length = int(len_of_length,2)
            len_of_length_bits = len_of_length * 4
            if (len(aux[len_to_read:(len_to_read+len_of_length)]) == len_of_length):
                total_length = int(aux[len_to_read:(len_to_read+len_of_length)], 2)
                total_length_bits = total_length * 8
            else:
                total_length = aux[len_to_read:(len_to_read+len_of_length)]
                in_next_transaction = True
    else:
        len_of_length = 0
        total_length = aux
        in_next_transaction = True
        first_low_length = True
    #print("lens", len_of_length, total_length)
    if not first_low_length:
        message += aux[((len_to_read*4)+len_of_length_bits):]
    #print(message, len_of_length, total_length,in_next_transaction,first_low_length )
    return len_of_length, total_length, message, in_next_transaction,first_low_length

def checkifsinglevalueMethod (data_in_transactions, dict_valuespossible, bits_per_length, len_to_read,ChaCha20key, field = ""):
    """
    Check if the hash passed is part of a single-transaction message,
    is one of the transactions that contain the message or it does not
    seem to contain a message
    """
    single = False
    withmessage = True
    data_array = copy.deepcopy(data_in_transactions)
    single = False
    withmessage = True
    data_array = copy.deepcopy(data_in_transactions)
    total = 0
    aux_total = ''
    total_length = 0
    in_next_transaction = False
    first_low_length = False
    message = ''
    # if ChaCha20key != "":
    #     totalaux = cipherfunctions.ecipherChaCha20 (data_in_transactions[0][:2], ChaCha20key, "hex")
    #     print(totalaux)
    #     total = int(totalaux[1], 16)
    # else:
    #     total = int(data_in_transactions[0][1], 16
    if type(data_in_transactions[0]) == int:
        data_in_transactions[0] = hex(data_in_transactions[0])[2:]
    tmp_full = str(int(data_in_transactions[0],16))
    length = len(tmp_full)
    tmp = tmp_full.rstrip('0')
    zeros = len(tmp_full) - len(tmp)
    #print(length, zeros,tmp)
    values = dict_valuespossible[length][zeros]
    #print(values)
    number_bits = bits_per_length[length][zeros]
    #print(number_bits)
    element = values.index(int(tmp))
    #print(element)
    aux = '{0:08b}'.format(int(element))
    if len(aux) < number_bits:
        necessary = number_bits -len(aux)
        aux = '0' * necessary + aux
    if len(aux) >=8:
        # if ChaCha20key != "":
        #     totalaux = cipherfunctions.ecipherChaCha20 (data_in_transactions[0][:2], ChaCha20key, "hex")
        #     print(totalaux)
        #     total = int(totalaux[1], 16)
        # else:
        #     total = int(data_in_transactions[0][1], 16
        #     data_in_transactions[0] = copy.deepcopy(hex(element)[2:])
        # if len(data_in_transactions[0])%2 !=0:
        #     data_in_transactions[0] = '0'+ data_in_transactions[0]
        if ChaCha20key != "":
            toread = aux[:4]
            #Number in bytes
            #print(aux, toread)
            # toread = int(toread,2)
            # to_read_bits = toread * 8
            aux_hex = hex(int(aux[:8],2))[2:]
            if len(aux_hex) %2 != 0:
                aux_hex = '0' + aux_hex
            #len
            #print("aux",aux_hex)
            auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
            #recover the whole byte to decipher
            #print("aux2",aux_hex[0:(len_to_read+1)])
            len_of_length_tmp = cipherfunctions.ecipherChaCha20(aux_hex[0:(len_to_read+1)], auxChaCha20key, "hex")
            #print("after",len_of_length_tmp)
            len_of_length_tmp = '{:08b}'.format(int(len_of_length_tmp,16))
            #print("bits",len_of_length_tmp)
            len_of_length = len_of_length_tmp[0:(len_to_read *4)]
            #print(len_of_length)
            #print(len_of_length)
            #len_of_length = ''.join('{:08b}'.format(x) for x in bytes.fromhex(len_of_length))
            len_of_length = int(len_of_length,2)
            len_of_length_bits = len_of_length * 4
            #print(len_of_length, len_of_length_bits)
            #print("len_of_length",len_of_length )
            #print(aux, (aux[len_to_read:(len_to_read+len_of_length)]), len(aux[len_to_read:(len_to_read+len_of_length)]))
            #print(aux_hex[len_to_read:(len_to_read+len_of_length)])
            if (len(aux_hex[len_to_read:(len_to_read+len_of_length)]) == len_of_length) or len(aux) >= (len_of_length_bits +4):
                counter = len_of_length
                total_length = ''
                #print("aqui", counter)
                retrieved = False
                while counter != 0 and not retrieved:
                    if counter % 2 != 0:
                        total_length += len_of_length_tmp[(len_to_read*4):]
                        counter -= 1
                        #print("h",len_to_read, len_of_length_tmp,total_length, len_of_length*4, len(total_length),(len(total_length)+(len_to_read *4)) )
                    elif counter %2 == 0 and counter != 0 and len(total_length) != (len_of_length * 4):
                        aux_hex = hex(int(aux[(len(total_length)+(len_to_read *4)):((len_of_length*4)+len(total_length))],2))[2:]
                        if len(aux_hex) %2 != 0:
                            aux_hex = '0' + aux_hex
                        #print(aux_hex,aux,aux[(len(total_length)+(len_to_read *4)):((len_of_length*4)+len(total_length))])
                        total_length_tmp = cipherfunctions.ecipherChaCha20(aux_hex, auxChaCha20key, "hex")
                        #print("y", total_length_tmp)
                        total_length_tmp = '{:08b}'.format(int(total_length_tmp,16))
                        while len(total_length_tmp) % 4 != 0:
                            total_length_tmp = '0' + total_length_tmp
                        total_length += total_length_tmp
                        counter -= 2
                        #print("o", total_length)
                    if len(total_length) == (len_of_length * 4):
                        print("Retrieved")
                        retrieved = True
                total_length = int(total_length,2)
                total_length_bits = total_length * 8
                #print(total_length,total_length_bits)
            else:
                #print("here")
                total_length = ''
                total_length = len_of_length_tmp[
                (len_to_read*4):] + aux[8:]
                #print(total_length,aux_hex[(len_to_read +1):(len_to_read+1+len_of_length)])
                in_next_transaction = True

        else:
            len_of_length = aux[0:len_to_read]
            len_of_length = '{:04b}'.format(int(len_of_length,16))
            #len_of_length = ''.join('{:08b}'.format(x) for x in bytes.fromhex(len_of_length))
            len_of_length = int(len_of_length,2)
            len_of_length_bits = len_of_length * 4
            if (len(aux[len_to_read:(len_to_read+len_of_length)]) == len_of_length):
                total_length = int(aux[len_to_read:(len_to_read+len_of_length)], 2)
                total_length_bits = total_length * 8
            else:
                total_length = aux[len_to_read:(len_to_read+len_of_length)]
                in_next_transaction = True
    else:
        len_of_length = 0
        total_length = aux
        in_next_transaction = True
        first_low_length = True
    #print("lens", len_of_length, total_length)
    if not first_low_length:
        message += aux[((len_to_read*4)+len_of_length_bits):]
    #print(message, len_of_length, total_length,in_next_transaction,first_low_length )
    return len_of_length, total_length, message, in_next_transaction,first_low_length

    #     data_in_transactions[0] = copy.deepcopy(hex(element)[2:])
    # if len(data_in_transactions[0])%2 !=0:
    #     data_in_transactions[0] = '0'+ data_in_transactions[0]
    # if ChaCha20key != "":
    #     #xtmp = '{0:08b}'.format(int(element))
    #     print(number_bits)
    #     if len(xtmp) < number_bits:
    #         necessary = number_bits -len(xtmp)
    #         xtmp = '0' * necessary + xtmp
    #     print(xtmp)
    #     xhex = xtmp[:len(hex(globalsettings.max_number_transactions)[2:])*8]
    #     xhex= hex(int(xhex, 2))[2:]
    #     print(xhex)
    #     if len(xhex)%2 !=0:
    #         xhex = '0'+ xhex
    #     print(xhex)
    #     if len(xhex) < len(hex(globalsettings.max_number_transactions)[2:])*2:
    #         xhex = '00'+ xhex
    #     totalaux = cipherfunctions.ecipherChaCha20 (xhex, cipherfunctions.upgradeChaCha20key(ChaCha20key), "hex")
    #     print(totalaux)
    #     index = int(totalaux[:len(hex(globalsettings.max_number_transactions)[2:])], 16)
    #     total = int(totalaux[len(hex(globalsettings.max_number_transactions)[2:]):], 16)
    # else:
    #     xtmp = '{0:08b}'.format(int(element))
    #     print(number_bits)
    #     if len(xtmp) < number_bits:
    #         necessary = number_bits -len(xtmp)
    #         xtmp = '0' * necessary + xtmp
    #     print(xtmp)
    #     xhex = xtmp[:len(hex(globalsettings.max_number_transactions)[2:])*8]
    #     xhex= hex(int(xhex, 2))[2:]
    #     print(xhex)
    #     if len(xhex)%2 !=0:
    #         xhex = '0'+ xhex
    #     print(xhex)
    #     if len(xhex) < len(hex(globalsettings.max_number_transactions)[2:])*2:
    #         xhex = '00'+ xhex
    #     index = int(xhex[:len(hex(globalsettings.max_number_transactions)[2:])], 16)
    #     total = int(xhex[len(hex(globalsettings.max_number_transactions)[2:]):len(hex(globalsettings.max_number_transactions)[2:])*2], 16)
    # if index < 1 or index > total or total < 1:
    #     withmessage = False
    #     print("This transaction hash does not seem to contain a message\n")
    # else:
    #     if total == 1:
    #         single = True
    #         data_array[(index-1)] = xtmp
    #     else:
    #         data_array = ['0'] * total
    #         data_array[(index-1)] = xtmp
    #     print("Transaction {} of {}".format(index,total))
    # print("single", data_array)
    #return single, data_array, total, index, withmessage

def checkifsinglevalueMethod_old (data_in_transactions, dict_valuespossible, bits_per_length,ChaCha20key, field = ""):
    """
    Check if the hash passed is part of a single-transaction message,
    is one of the transactions that contain the message or it does not
    seem to contain a message
    """
    single = False
    withmessage = True
    data_array = copy.deepcopy(data_in_transactions)
    total = 0
    # if ChaCha20key != "":
    #     totalaux = cipherfunctions.ecipherChaCha20 (data_in_transactions[0][:2], ChaCha20key, "hex")
    #     print(totalaux)
    #     total = int(totalaux[1], 16)
    # else:
    #     total = int(data_in_transactions[0][1], 16
    if type(data_in_transactions[0]) == int:
        data_in_transactions[0] = hex(data_in_transactions[0])[2:]
    tmp_full = str(int(data_in_transactions[0],16))
    length = len(tmp_full)
    tmp = tmp_full.rstrip('0')
    zeros = len(tmp_full) - len(tmp)
    print(length, zeros,tmp)
    values = dict_valuespossible[length][zeros]
    #print(values)
    number_bits = bits_per_length[length][zeros]
    print(number_bits)
    element = values.index(int(tmp))
    print(element)
    #     data_in_transactions[0] = copy.deepcopy(hex(element)[2:])
    # if len(data_in_transactions[0])%2 !=0:
    #     data_in_transactions[0] = '0'+ data_in_transactions[0]
    if ChaCha20key != "":
        xtmp = '{0:08b}'.format(int(element))
        print(number_bits)
        if len(xtmp) < number_bits:
            necessary = number_bits -len(xtmp)
            xtmp = '0' * necessary + xtmp
        print(xtmp)
        xhex = xtmp[:len(hex(globalsettings.max_number_transactions)[2:])*8]
        xhex= hex(int(xhex, 2))[2:]
        print(xhex)
        if len(xhex)%2 !=0:
            xhex = '0'+ xhex
        print(xhex)
        if len(xhex) < len(hex(globalsettings.max_number_transactions)[2:])*2:
            xhex = '00'+ xhex
        totalaux = cipherfunctions.ecipherChaCha20 (xhex, cipherfunctions.upgradeChaCha20key(ChaCha20key), "hex")
        print(totalaux)
        index = int(totalaux[:len(hex(globalsettings.max_number_transactions)[2:])], 16)
        total = int(totalaux[len(hex(globalsettings.max_number_transactions)[2:]):], 16)
    else:
        xtmp = '{0:08b}'.format(int(element))
        print(number_bits)
        if len(xtmp) < number_bits:
            necessary = number_bits -len(xtmp)
            xtmp = '0' * necessary + xtmp
        print(xtmp)
        xhex = xtmp[:len(hex(globalsettings.max_number_transactions)[2:])*8]
        xhex= hex(int(xhex, 2))[2:]
        print(xhex)
        if len(xhex)%2 !=0:
            xhex = '0'+ xhex
        print(xhex)
        if len(xhex) < len(hex(globalsettings.max_number_transactions)[2:])*2:
            xhex = '00'+ xhex
        index = int(xhex[:len(hex(globalsettings.max_number_transactions)[2:])], 16)
        total = int(xhex[len(hex(globalsettings.max_number_transactions)[2:]):len(hex(globalsettings.max_number_transactions)[2:])*2], 16)
    if index < 1 or index > total or total < 1:
        withmessage = False
        print("This transaction hash does not seem to contain a message\n")
    else:
        if total == 1:
            single = True
            data_array[(index-1)] = xtmp
        else:
            data_array = ['0'] * total
            data_array[(index-1)] = xtmp
        print("Transaction {} of {}".format(index,total))
    print("single", data_array)
    return single, data_array, total, index, withmessage

def checkextractfullfilled (data_in_transactions,len_to_read,len_of_length, total_length, message, in_next_transaction, first_low_length,ChaCha20key,field = ""):
    rest = 0
    all_extracted = False
    for x in range(len(data_in_transactions)):
        #Extract total length of the message:
        aux = ''.join('{:08b}'.format(x) for x in bytes.fromhex(data_in_transactions[x]))
        #print("aux", aux, message,data_in_transactions)
        if first_low_length:
            #print("First minor 8")
            aux = total_length + aux
            #print("aux", aux, message,data_in_transactions)
            if ChaCha20key != "":
                toread = aux[:4]
                #Number in bytes
                #print(aux, toread)
                # toread = int(toread,2)
                # to_read_bits = toread * 8
                aux_hex = hex(int(aux[:8],2))[2:]
                if len(aux_hex) %2 != 0:
                    aux_hex = '0' + aux_hex
                #len
                #print("aux",aux_hex)
                auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
                #recover the whole byte to decipher
                #print("aux2",aux_hex[0:(len_to_read+1)])
                len_of_length_tmp = cipherfunctions.ecipherChaCha20(aux_hex[0:(len_to_read+1)], auxChaCha20key, "hex")
                #print("after",len_of_length_tmp)
                len_of_length_tmp = '{:08b}'.format(int(len_of_length_tmp,16))
                #print("bits",len_of_length_tmp)
                len_of_length = len_of_length_tmp[0:(len_to_read *4)]
                #print(len_of_length)
                #print(len_of_length)
                #len_of_length = ''.join('{:08b}'.format(x) for x in bytes.fromhex(len_of_length))
                len_of_length = int(len_of_length,2)
                len_of_length_bits = len_of_length * 4
                #print(len_of_length, len_of_length_bits)
                #print("len_of_length",len_of_length )
                #print(aux, (aux[len_to_read:(len_to_read+len_of_length)]), len(aux[len_to_read:(len_to_read+len_of_length)]))
                #print(aux_hex[len_to_read:(len_to_read+len_of_length)])
                if (len(aux_hex[len_to_read:(len_to_read+len_of_length)]) == len_of_length) or len(aux) >= (len_of_length_bits +4):
                    counter = len_of_length
                    total_length = ''
                    #print("aqui", counter)
                    retrieved = False
                    while counter != 0 and not retrieved:
                        if counter % 2 != 0:
                            total_length += len_of_length_tmp[(len_to_read*4):]
                            counter -= 1
                            #print("h",len_to_read, len_of_length_tmp,total_length, len_of_length*4, len(total_length),(len(total_length)+(len_to_read *4)) )
                        elif counter %2 == 0 and counter != 0 and len(total_length) != (len_of_length * 4):
                            aux_hex = hex(int(aux[(len(total_length)+(len_to_read *4)):((len_of_length*4)+len(total_length))],2))[2:]
                            if len(aux_hex) %2 != 0:
                                aux_hex = '0' + aux_hex
                            #print(aux_hex,aux,aux[(len(total_length)+(len_to_read *4)):((len_of_length*4)+len(total_length))])
                            total_length_tmp = cipherfunctions.ecipherChaCha20(aux_hex, auxChaCha20key, "hex")
                            total_length_tmp = '{:08b}'.format(int(total_length_tmp,16))
                            total_length += total_length_tmp
                            counter -= 2
                            #print("o", total_length)
                        if len(total_length) == (len_of_length * 4):
                            print("Retrieved")
                            retrieved = True
                    #print("Firstafter", len_of_length,total_length)
                    total_length = int(total_length,2)
                    total_length_bits = total_length * 8
                    rest = (len_of_length*4) +(len_to_read *4)
                    #print("rest", rest, (len_of_length*4), (len_to_read *4))
                    in_next_transaction = False
                    first_low_length =False
                    #print(total_length,total_length_bits)
                else:
                    #print("here")
                    total_length = ''
                    total_length = len_of_length_tmp[
                    (len_to_read*4):] + aux[8:]
                    #print(total_length,aux_hex[(len_to_read +1):(len_to_read+1+len_of_length)])
                    in_next_transaction = True
                    first_low_length =False
        elif in_next_transaction:
            #print("next", len_of_length,total_length)
            rest = (len_of_length*4) - len(total_length)
            #print(rest, aux)
            #tmp = aux[:(rest*4)]
            tmp = aux[:rest]
            #print(tmp,tmp[:8])
            if len(total_length) > 4:
                tmp = total_length[4:]+ tmp
            #print(tmp)
            #aux = copy.deepcopy(tmp)
            #New three delete if not working
            #aux = copy.deepcopy(tmp[8:])
            #rest = 0
            #print(aux)
            tmp = int(tmp[:8],2)
            #print(tmp)
            aux_hex = hex(tmp)[2:]
            if len(aux_hex) %2 != 0:
                aux_hex = '0' + aux_hex
            #total_length += aux_hex[:rest]
            #print("next2", aux_hex)
            if len(aux_hex)== 2:
                if ChaCha20key!= "":
                    auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
                    total_length_tmp = cipherfunctions.ecipherChaCha20(aux_hex, auxChaCha20key, "hex")
                    if len(total_length) > 4:
                        total_length = total_length[:4] + '{:08b}'.format(int(total_length_tmp,16))
                    else:
                        total_length += '{:08b}'.format(int(total_length_tmp,16))
                    #print(total_length_tmp, total_length)
                    total_length = int(total_length,2)
                    total_length_bits = total_length * 8
                    #print(total_length_tmp, total_length)
                else:
                    total_length += aux[:int((rest/2)*8)]
                    total_length = int(total_length,2)
                    total_length_bits = total_length * 8
                in_next_transaction = False
            else:
                in_next_transaction = True
                aux = ''
            #print(total_length, message)
        #print(rest)
        if rest != 0:
            print("ups", aux,aux[8:], aux[rest:] )
            #message += aux[8:]
            message += aux[rest:]
            print("message", message)
            rest = 0
        else:
            message += aux
            print(message)
        #print(message)
    #print(message)
    if len(message)>= (total_length * 8):
        all_extracted = True
    #message_bits = ''.join('{:08b}'.format(x) for x in bytes.fromhex(message))
    # message_bits = message[:total_length_bits]
    if all_extracted:
        total_length_bits = total_length * 8
        message = message[:total_length_bits]
        print("all",total_length,total_length_bits)
    # message = bytearray([int(message_bits[i:i+8], 2) for i in range(0, len(message_bits), 8)]).hex()
    #print(message)
    return message,len_of_length, total_length, message, in_next_transaction,all_extracted,first_low_length

def checkifsinglefullfield (data_in_transactions,len_to_read,ChaCha20key, field = ""):
    """
    Check if the hash passed is part of a single-transaction message,
    is one of the transactions that contain the message or it does not
    seem to contain a message
    """
    single = False
    withmessage = True
    data_array = copy.deepcopy(data_in_transactions)
    total = 0
    aux_total = ''
    total_length = 0
    in_next_transaction = False
    first_low_length = False
    aux = ''.join('{:08b}'.format(x) for x in bytes.fromhex(data_in_transactions[0]))
    #print("aux", aux)
    message = ''
    if len(aux) >=8:
        # if ChaCha20key != "":
        #     totalaux = cipherfunctions.ecipherChaCha20 (data_in_transactions[0][:2], ChaCha20key, "hex")
        #     print(totalaux)
        #     total = int(totalaux[1], 16)
        # else:
        #     total = int(data_in_transactions[0][1], 16
        #     data_in_transactions[0] = copy.deepcopy(hex(element)[2:])
        # if len(data_in_transactions[0])%2 !=0:
        #     data_in_transactions[0] = '0'+ data_in_transactions[0]
        if ChaCha20key != "":
            toread = aux[:4]
            #Number in bytes
            #print(aux, toread)
            # toread = int(toread,2)
            # to_read_bits = toread * 8
            aux_hex = hex(int(aux[:8],2))[2:]
            if len(aux_hex) %2 != 0:
                aux_hex = '0' + aux_hex
            #len
            #print("aux",aux_hex)
            auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
            #recover the whole byte to decipher
            #print("aux2",aux_hex[0:(len_to_read+1)])
            len_of_length_tmp = cipherfunctions.ecipherChaCha20(aux_hex[0:(len_to_read+1)], auxChaCha20key, "hex")
            #print("after",len_of_length_tmp)
            len_of_length_tmp = '{:08b}'.format(int(len_of_length_tmp,16))
            #print("bits",len_of_length_tmp)
            len_of_length = len_of_length_tmp[0:(len_to_read *4)]
            #print(len_of_length)
            #print(len_of_length)
            #len_of_length = ''.join('{:08b}'.format(x) for x in bytes.fromhex(len_of_length))
            len_of_length = int(len_of_length,2)
            len_of_length_bits = len_of_length * 4
            #print(len_of_length, len_of_length_bits)
            #print("len_of_length",len_of_length )
            #print(aux, (aux[len_to_read:(len_to_read+len_of_length)]), len(aux[len_to_read:(len_to_read+len_of_length)]))
            #print(aux_hex[len_to_read:(len_to_read+len_of_length)])
            if (len(aux_hex[len_to_read:(len_to_read+len_of_length)]) == len_of_length) or len(aux) >= (len_of_length_bits +4):
                counter = len_of_length
                total_length = ''
                #print("aqui", counter)
                retrieved = False
                while counter != 0 and not retrieved:
                    if counter % 2 != 0:
                        total_length += len_of_length_tmp[(len_to_read*4):]
                        counter -= 1
                        #print("h",len_to_read, len_of_length_tmp,total_length, len_of_length*4, len(total_length),(len(total_length)+(len_to_read *4)) )
                    elif counter %2 == 0 and counter != 0 and len(total_length) != (len_of_length * 4):
                        aux_hex = hex(int(aux[(len(total_length)+(len_to_read *4)):((len_of_length*4)+len(total_length))],2))[2:]
                        if len(aux_hex) %2 != 0:
                            aux_hex = '0' + aux_hex
                        #print(aux_hex,aux,aux[(len(total_length)+(len_to_read *4)):((len_of_length*4)+len(total_length))])
                        total_length_tmp = cipherfunctions.ecipherChaCha20(aux_hex, auxChaCha20key, "hex")
                        #print("y", total_length_tmp)
                        total_length_tmp = '{:08b}'.format(int(total_length_tmp,16))
                        while len(total_length_tmp) % 4 != 0:
                            total_length_tmp = '0' + total_length_tmp
                        total_length += total_length_tmp
                        counter -= 2
                        #print("o", total_length)
                    if len(total_length) == (len_of_length * 4):
                        print("Retrieved")
                        retrieved = True
                total_length = int(total_length,2)
                total_length_bits = total_length * 8
                #print(total_length,total_length_bits)
            else:
                #print("here")
                total_length = ''
                total_length = len_of_length_tmp[
                (len_to_read*4):] + aux[8:]
                #print(total_length,aux_hex[(len_to_read +1):(len_to_read+1+len_of_length)])
                in_next_transaction = True

        else:
            len_of_length = aux[0:len_to_read]
            len_of_length = '{:04b}'.format(int(len_of_length,16))
            #len_of_length = ''.join('{:08b}'.format(x) for x in bytes.fromhex(len_of_length))
            len_of_length = int(len_of_length,2)
            len_of_length_bits = len_of_length * 4
            if (len(aux[len_to_read:(len_to_read+len_of_length)]) == len_of_length):
                total_length = int(aux[len_to_read:(len_to_read+len_of_length)], 2)
                total_length_bits = total_length * 8
                #print("aquí")
            else:
                total_length = aux[len_to_read:(len_to_read+len_of_length)]
                in_next_transaction = True
    else:
        len_of_length = 0
        total_length = aux
        in_next_transaction = True
        first_low_length = True
    #print("lens", len_of_length, total_length)
    if not first_low_length:
        message += aux[((len_to_read*4)+len_of_length_bits):]
    #print(message, len_of_length, total_length,in_next_transaction,first_low_length )
    return len_of_length, total_length, message, in_next_transaction,first_low_length



def preparemessagesolidity (data, ChaCha20key):
    print(data)
    data_length_hex_bytes = hex(int(len(data)/2))[2:]
    if len(data_length_hex_bytes) % 2 != 0:
        data_length_hex_bytes = '0' + data_length_hex_bytes
    if len(data_length_hex_bytes) < 4:
        data_length_hex_bytes = '0'* (4-len(data_length_hex_bytes)) + data_length_hex_bytes
    if len(data_length_hex_bytes) > 4:
        print("Message too big")
        exit(1)
    if ChaCha20key != "":
        print(data_length_hex_bytes)
        auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
        data_length_hex_ciphered = cipherfunctions.cipherChaCha20(data_length_hex_bytes, auxChaCha20key, "hex")
        print(data_length_hex_ciphered)
        data = data_length_hex_ciphered + data
    else:
        data = data_length_hex_bytes + data
    return data

def extractlengthsolidity (data_bin, ChaCha20key):
    TextV = bytearray([int(data_bin[i:i+8], 2) for i in range(0, len(data_bin), 8)])
    data = TextV.hex()
    auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
    #recover the whole byte to decipher
    #print("aux2",aux_hex[0:(len_to_read+1)])
    length= cipherfunctions.ecipherChaCha20(data[0:4], auxChaCha20key, "hex")
    length_bin = len(data_bin)/8
    message = data_bin[(2*8):]
    if int(length,16) >length_bin-2:
        in_next_transaction = True
    else:
        in_next_transaction = False
    return int(length,16),in_next_transaction,message


def preparemessagefullfield(data, length, ChaCha20key):
    message_prepared = []
    data_length_hex_bytes = hex(int(len(data)/2))[2:]
    lengthsmin = []
    #to_read = hex(len(data_length_hex_bytes))[2:]
    if len(data_length_hex_bytes) % 2 != 0:
        data_length_hex_bytes = '0' + data_length_hex_bytes
    # if len(to_read) % 2 != 0:
    #     to_read = '0' + to_read
    #len_bits = ''.join('{:04b}'.format(x) for x in bytes.fromhex(data_length_hex_bytes))
    len_bits = bin(int(len(data)/2))[2:]
    while len(len_bits) % 4 != 0:
        len_bits = '0' + len_bits
    #print("antes de nada", data_length_hex_bytes, len_bits)
    len_bits_hex = hex(int(len(len_bits)/4))[2:]
    if len(len_bits_hex) % 2 != 0:
        len_bits_hex = '0' + len_bits_hex
    to_read =  ''.join('{:04b}'.format(x) for x in bytes.fromhex(len_bits_hex))
    if ChaCha20key != "":
        if len(len_bits) % 8 == 0 :
            len_bits = '0' * 4 + len_bits
            len_bits_hex = hex(int(len(len_bits)/4))[2:]
            if len(len_bits_hex) % 2 != 0:
                len_bits_hex = '0' + len_bits_hex
            to_read =  ''.join('{:04b}'.format(x) for x in bytes.fromhex(len_bits_hex))
        length_control_info = to_read + len_bits
        #print(to_read, len_bits, length_control_info)
        #print(cipherfunctions.nonce)
        auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
        #print(cipherfunctions.nonce)
        #auxChaCha20key = ChaCha20key
        len_data_length = hex(int(length_control_info[:8],2))[2:]
        if len(len_data_length) % 2 != 0:
            len_data_length = '0' + len_data_length
        #print(cipherfunctions.nonce,auxChaCha20key )
        len_data_length = cipherfunctions.cipherChaCha20(len_data_length, auxChaCha20key, "hex")
        #print(cipherfunctions.nonce,auxChaCha20key )
        if length_control_info[8:] != '':
            data_length_hex = hex(int(length_control_info[8:],2))[2:]
            if len(data_length_hex)% 2 != 0:
                data_length_hex = '0' + data_length_hex
            #print("control", length_control_info,length_control_info[8:],data_length_hex)
            #print(cipherfunctions.nonce,auxChaCha20key )
            data_length_hex =  cipherfunctions.cipherChaCha20(data_length_hex, auxChaCha20key, "hex")
            if len(data_length_hex)% 2 != 0:
                data_length_hex = '0' + data_length_hex
            #print(cipherfunctions.nonce,auxChaCha20key )
            #print("control", length_control_info,length_control_info[8:],data_length_hex,len_data_length + data_length_hex)
            data = len_data_length + data_length_hex + data
        else:
            data = len_data_length + data
    else:
        length_control_info = to_read + len_bits
        #print("controlnocipher", length_control_info, to_read , len_bits,  hex(int(length_control_info,2))[2:])
        hex_control_info =  hex(int(length_control_info,2))[2:]
        if len(hex_control_info)% 2 != 0:
            hex_control_info = '0' + hex_control_info
        data =  hex_control_info + data
    #to bits:
    data = ''.join('{:08b}'.format(x) for x in bytes.fromhex(data))
    data_old = copy.deepcopy(data)
    #sizes_number = [sizes[i%len(sizes)] for i in range(blocks)]
    #sum_sizes = [sum(x) for x in sizes_number]
    completed = False
    i = 0
    while i < 255 and not completed:
        #sizes_number = sizes[i%len(sizes)]
        #sum_sizes = sum(sizes_number)
        #print("sizes and sum", sizes_number, sum_sizes)
        while len(data) < (length*8):
            rest = (length*8) - len(data)
            data += random.choice(['0', '1'])
        message = data[:(length*8)]
        message_hex = bytearray([int(message[i:i+8], 2) for i in range(0, len(message), 8)]).hex()
        while len(message_hex) < (length * 2):
            message_hex = '0' + message_hex
        message_prepared.append(message_hex)
        data = data[(length*8):]
        if len(data) == 0:
            completed = True
        i+=1
    if len(message_prepared)>255 or not completed:
        print("Please, reduce the size of the message and try again\n")
        exit(1)
    return message_prepared

def checkifsinglevalue (data_in_transactions, ChaCha20key, field = ""):
    """
    Check if the hash passed is part of a single-transaction message,
    is one of the transactions that contain the message or it does not
    seem to contain a message
    """
    single = False
    withmessage = True
    data_array = copy.deepcopy(data_in_transactions)
    total = 0
    # if ChaCha20key != "":
    #     totalaux = cipherfunctions.ecipherChaCha20 (data_in_transactions[0][:2], ChaCha20key, "hex")
    #     print(totalaux)
    #     total = int(totalaux[1], 16)
    # else:
    #     total = int(data_in_transactions[0][1], 16
    if len(data_in_transactions[0])%2 !=0:
        data_in_transactions[0] = '0'+ data_in_transactions[0]
    if ChaCha20key != "":
        totalaux = cipherfunctions.ecipherChaCha20 (data_in_transactions[0][:len(hex(globalsettings.max_number_transactions)[2:])*2], cipherfunctions.upgradeChaCha20key(ChaCha20key), "hex")
        index = int(totalaux[:len(hex(globalsettings.max_number_transactions)[2:])], 16)
        total = int(totalaux[len(hex(globalsettings.max_number_transactions)[2:]):], 16)
    else:
        index = int(data_in_transactions[0][:len(hex(globalsettings.max_number_transactions)[2:])], 16)
        total = int(data_in_transactions[0][len(hex(globalsettings.max_number_transactions)[2:]):len(hex(globalsettings.max_number_transactions)[2:])*2], 16)
    if index < 1 or index > total or total < 1:
        withmessage = False
        print("This transaction hash does not seem to contain a message\n")
    else:
        if total == 1:
            single = True
            data_array[(index-1)] = data_in_transactions[0]
        else:
            data_array = ['0'] * total
            data_array[(index-1)] = data_in_transactions[0]
        print("Transaction {} of {}".format(index,total))
    return single, data_array, total, index, withmessage

def checkifpartofmessagevalueMethod_old (data_to_retrieve, nonces_tx, data_in_transactions, cumulative_nonces, dict_valuespossible, bits_per_length, ChaCha20key, field = ""):
    i = 0
    added= 0
    print(data_to_retrieve)
    while i < len(data_to_retrieve):
        if type(data_to_retrieve[i]) == int:
            data_to_retrieve[i] = hex(data_to_retrieve[i])[2:]
        tmp_full = str(int(data_to_retrieve[i],16))
        print(tmp_full)
        length = len(tmp_full)
        tmp = tmp_full.rstrip('0')
        zeros = len(tmp_full) - len(tmp)
        print(length, zeros,tmp)
        values = dict_valuespossible[length][zeros]
        #print(values)
        number_bits = bits_per_length[length][zeros]
        print(number_bits)
        element = values.index(int(tmp))
        print(element)
            #data_in_transactions[i] = copy.deepcopy(hex(element)[2:])
        # if len((data_to_retrieve[i]))%2 !=0:
        #     data_to_retrieve[i] = '0'+ data_to_retrieve[i]
        if ChaCha20key != "":
            xtmp = '{0:08b}'.format(int(element))
            print(number_bits)
            if len(xtmp) < number_bits:
                necessary = number_bits -len(xtmp)
                xtmp = '0' * necessary + xtmp
            print(xtmp)
            xhex = xtmp[:len(hex(globalsettings.max_number_transactions)[2:])*8]
            xhex= hex(int(xhex, 2))[2:]
            print(xhex)
            if len(xhex)%2 !=0:
                xhex = '0'+ xhex
            print(xhex)
            if len(xhex) < len(hex(globalsettings.max_number_transactions)[2:])*2:
                xhex = '00'+ xhex
            totalaux = cipherfunctions.ecipherChaCha20 (xhex, cipherfunctions.upgradeChaCha20key(ChaCha20key), "hex")
            print(totalaux)
            index = int(totalaux[:len(hex(globalsettings.max_number_transactions)[2:])], 16)
            total = int(totalaux[len(hex(globalsettings.max_number_transactions)[2:]):], 16)
            print(index,total)
        else:
            xtmp = '{0:08b}'.format(int(element))
            print(number_bits)
            if len(xtmp) < number_bits:
                necessary = number_bits -len(xtmp)
                xtmp = '0' * necessary + xtmp
            print(xtmp)
            if len(xhex) < len(hex(globalsettings.max_number_transactions)[2:])*2:
                xhex = '00'+ xhex
            xhex = xtmp[:len(hex(globalsettings.max_number_transactions)[2:])*8]
            xhex= hex(int(xhex, 2))[2:]
            print(xhex)
            if len(xhex)%2 !=0:
                xhex = '0'+ xhex
            print(xhex)
            index = int(xhex[:len(hex(globalsettings.max_number_transactions)[2:])], 16)
            total = int(xhex[len(hex(globalsettings.max_number_transactions)[2:]):len(hex(globalsettings.max_number_transactions)[2:])*2], 16)
        if total == len(data_in_transactions) and index > 0 and index <= len(data_in_transactions):
            data_in_transactions[(index-1)] = xtmp
            print((index-1))
            cumulative_nonces[(index-1)] = nonces_tx[i]
            print("Transaction {} of {}".format(index,total))
            added += 1
        i += 1
    print("check", xtmp, data_in_transactions)
    return data_in_transactions ,cumulative_nonces, added

def checkifpartofmessagevalueMethod (data_in_transactions, len_to_read,len_of_length, total_length, message, in_next_transaction, first_low_length, dict_valuespossible, bits_per_length, ChaCha20key, field = ""):
    i = 0
    added= 0
    #print(data_in_transactions)
    rest = 0
    all_extracted = False
    if ChaCha20key != "":
        auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
    for x in range(len(data_in_transactions)):
        if type(data_in_transactions[x]) == int:
            data_in_transactions[x] = hex(data_in_transactions[x])[2:]
        tmp_full = str(int(data_in_transactions[x],16))
        #print(tmp_full)
        length = len(tmp_full)
        tmp = tmp_full.rstrip('0')
        zeros = len(tmp_full) - len(tmp)
        #print(length, zeros,tmp)
        values = dict_valuespossible[length][zeros]
        #print(values)
        number_bits = bits_per_length[length][zeros]
        #print(number_bits)
        element = values.index(int(tmp))
        #print(element)
        aux = '{0:08b}'.format(int(element))
        #print(number_bits)
        if len(aux) < number_bits:
            necessary = number_bits -len(aux)
            aux = '0' * necessary + aux
            #data_in_transactions[i] = copy.deepcopy(hex(element)[2:])
        # if len((data_to_retrieve[i]))%2 !=0:
        #     data_to_retrieve[i] = '0'+ data_to_retrieve[i]
        #print("aux", aux, message,first_low_length,in_next_transaction, data_in_transactions)
        if first_low_length and len(total_length + aux)>= 8:
            #print("First minor 8")
            aux = total_length + aux
            #print("aux", aux, message,data_in_transactions)
            if ChaCha20key != "":
                toread = aux[:4]
                #Number in bytes
                #print(aux, toread)
                # toread = int(toread,2)
                # to_read_bits = toread * 8
                aux_hex = hex(int(aux[:8],2))[2:]
                if len(aux_hex) %2 != 0:
                    aux_hex = '0' + aux_hex
                #len
                #print("aux",aux_hex)
                #recover the whole byte to decipher
                #print("aux2",aux_hex[0:(len_to_read+1)])
                len_of_length_tmp = cipherfunctions.ecipherChaCha20(aux_hex[0:(len_to_read+1)], auxChaCha20key, "hex")
                #print("after",len_of_length_tmp)
                len_of_length_tmp = '{:08b}'.format(int(len_of_length_tmp,16))
                #print("bits",len_of_length_tmp)
                len_of_length = len_of_length_tmp[0:(len_to_read *4)]
                #print(len_of_length)
                #print(len_of_length)
                #len_of_length = ''.join('{:08b}'.format(x) for x in bytes.fromhex(len_of_length))
                len_of_length = int(len_of_length,2)
                len_of_length_bits = len_of_length * 4
                #print(len_of_length, len_of_length_bits)
                #print("len_of_length",len_of_length )
                #print(aux, (aux[len_to_read:(len_to_read+len_of_length)]), len(aux[len_to_read:(len_to_read+len_of_length)]))
                #print(aux_hex[len_to_read:(len_to_read+len_of_length)])
                if (len(aux_hex[len_to_read:(len_to_read+len_of_length)]) == len_of_length) or len(aux) >= (len_of_length_bits +4):
                    counter = len_of_length
                    total_length = ''
                    #print("aqui", counter)
                    retrieved = False
                    while counter != 0 and not retrieved:
                        if counter % 2 != 0:
                            total_length += len_of_length_tmp[(len_to_read*4):]
                            counter -= 1
                            #print("h",len_to_read, len_of_length_tmp,total_length, len_of_length*4, len(total_length),(len(total_length)+(len_to_read *4)) )
                        elif counter %2 == 0 and counter != 0 and len(total_length) != (len_of_length * 4):
                            aux_hex = hex(int(aux[(len(total_length)+(len_to_read *4)):((len_of_length*4)+len(total_length))],2))[2:]
                            if len(aux_hex) %2 != 0:
                                aux_hex = '0' + aux_hex
                            #print(aux_hex,aux,aux[(len(total_length)+(len_to_read *4)):((len_of_length*4)+len(total_length))])
                            total_length_tmp = cipherfunctions.ecipherChaCha20(aux_hex, auxChaCha20key, "hex")
                            total_length_tmp = '{:08b}'.format(int(total_length_tmp,16))
                            total_length += total_length_tmp
                            counter -= 2
                            #print("o", total_length)
                        if len(total_length) == (len_of_length * 4):
                            print("Retrieved")
                            retrieved = True
                    #print("Firstafter", len_of_length,total_length)
                    total_length = int(total_length,2)
                    total_length_bits = total_length * 8
                    rest = (len_of_length*4) +(len_to_read *4)
                    #print("rest", rest, (len_of_length*4), (len_to_read *4))
                    in_next_transaction = False
                    first_low_length =False
                    #print(total_length,total_length_bits)
                else:
                    #print("here")
                    total_length = ''
                    total_length = len_of_length_tmp[
                    (len_to_read*4):] + aux[8:]
                    #print(total_length,aux_hex[(len_to_read +1):(len_to_read+1+len_of_length)])
                    aux = ''
                    in_next_transaction = True
                    first_low_length =False
        elif in_next_transaction and not first_low_length:
            #print("next", len_of_length,total_length)
            if (len_of_length*4) <= len(total_length):
                rest = (len_of_length*4) - len(total_length)
                to_add = total_length[rest:]
                aux = to_add + aux
                if len(total_length) > 4:
                    tmp = total_length[4:rest]
                rest = 0
            else:
                rest = (len_of_length*4) - len(total_length)
                tmp = aux[:rest]
                #print(rest, aux)
                #tmp = aux[:(rest*4)]
                tmp = aux[:rest]
                #print(tmp,tmp[:8])
                if len(total_length) > 4:
                    tmp = total_length[4:]+ tmp
            #print(tmp)
            #aux = copy.deepcopy(tmp)
            #New three delete if not working
            #aux = copy.deepcopy(tmp[8:])
            #rest = 0
            #print(aux)
            tmp2 = int(tmp[:8],2)
            #print(tmp2)
            aux_hex = hex(tmp2)[2:]
            if len(aux_hex) %2 != 0:
                aux_hex = '0' + aux_hex
            #total_length += aux_hex[:rest]
            #print("next2", aux_hex)
            if len(aux_hex)== 2 and len(tmp)>=8:
                if ChaCha20key!= "":
                    total_length_tmp = cipherfunctions.ecipherChaCha20(aux_hex, auxChaCha20key, "hex")
                    if len(total_length) > 4:
                        total_length = total_length[:4] + '{:08b}'.format(int(total_length_tmp,16))
                    else:
                        total_length += '{:08b}'.format(int(total_length_tmp,16))
                    #print(total_length_tmp, total_length)
                    total_length = int(total_length,2)
                    total_length_bits = total_length * 8
                    #print(total_length_tmp, total_length)
                else:
                    total_length += aux[:int((rest/2)*8)]
                    total_length = int(total_length,2)
                    total_length_bits = total_length * 8
                in_next_transaction = False
            else:
                total_length += aux
                in_next_transaction = True
                aux = ''
            #print(total_length, message)
        #print(rest)
        if not first_low_length and not in_next_transaction:
            if rest != 0:
                #print("ups", aux,aux[8:], aux[rest:] )
                #message += aux[8:]
                message += aux[rest:]
                #print("message", message)
                rest = 0
            else:
                message += aux
                #print(message)
        else:
            total_length += aux
            print("still collecting", total_length)
        #print(message)
    #print(message)
    if type(total_length) != str and len(message)>= (total_length * 8):
        all_extracted = True
    #message_bits = ''.join('{:08b}'.format(x) for x in bytes.fromhex(message))
    # message_bits = message[:total_length_bits]
    if all_extracted:
        total_length_bits = total_length * 8
        message = message[:total_length_bits]
        print("all",total_length,total_length_bits)
    # message = bytearray([int(message_bits[i:i+8], 2) for i in range(0, len(message_bits), 8)]).hex()
    #print(message)
    return message,len_of_length, total_length, message, in_next_transaction,all_extracted,first_low_length

def checkifpartofmessagevalue (data_to_retrieve, nonces_tx, data_in_transactions, cumulative_nonces, ChaCha20key, field = ""):
    i = 0
    added= 0
    while i < len(data_to_retrieve):
        if type(data_to_retrieve[i]) == int:
            data_to_retrieve[i] = hex(data_to_retrieve[i])[2:]
        if len((data_to_retrieve[i]))%2 !=0:
            data_to_retrieve[i] = '0'+ data_to_retrieve[i]
        if ChaCha20key != "":
            totalaux = cipherfunctions.ecipherChaCha20 (data_to_retrieve[i][:len(hex(globalsettings.max_number_transactions)[2:])*2], cipherfunctions.upgradeChaCha20key(ChaCha20key), "hex")
            index = int(totalaux[:len(hex(globalsettings.max_number_transactions)[2:])], 16)
            total = int(totalaux[len(hex(globalsettings.max_number_transactions)[2:]):], 16)
        else:
            index = int(data_to_retrieve[i][:len(hex(globalsettings.max_number_transactions)[2:])], 16)
            total = int(data_to_retrieve[i][len(hex(globalsettings.max_number_transactions)[2:]):len(hex(globalsettings.max_number_transactions)[2:])*2], 16)
        if total == len(data_in_transactions) and index > 0 and index <= len(data_in_transactions):
            data_in_transactions[(index-1)] = data_to_retrieve[i]
            cumulative_nonces[(index-1)] = nonces_tx[i]
            print("Transaction {} of {}".format(index,total))
            added += 1
        i += 1
    return data_in_transactions ,cumulative_nonces, added

def orderblockscombined (swarms, constructors_after, ChaCha20key):
    """
    Orders the transactions passed with hex information for the combined method
    """
    completed = False
    swarms_ordered = copy.deepcopy(swarms)
    constructors_ordered = copy.deepcopy(constructors_after)
    cont = 0
    # if ChaCha20key != "":
    #     totalaux = cipherfunctions.ecipherChaCha20 (data_in_transactions[0][:2], ChaCha20key, "hex")
    #     print(totalaux)
    #     total = int(totalaux[1], 16)
    # else:
    #     total = int(data_in_transactions[0][1], 16)
    for x in range(len(swarms)):
        if ChaCha20key != "":
            totalaux = cipherfunctions.ecipherChaCha20 (swarms[x][:2], cipherfunctions.upgradeChaCha20key(ChaCha20key), "hex")
            index = int(totalaux[0], 16)
            total = int(totalaux[1], 16)
            print("Transaction {} of {}".format(index,total))
        else:
            index = int(swarms[x][0], 16)
            total = int(swarms[x][1], 16)
        swarms_ordered[(index-1)] = swarms[x]
        constructors_ordered[(index-1)] = constructors_after[x]
        cont += 1
    if cont != total:
        print("Some transactions are missing\n")
        exit(1)
    return swarms_ordered,constructors_ordered

def orderblocksbin (data_in_transactions,ChaCha20key):
    """
    Orders the transaction passed with binary information
    """
    completed = False
    data_ordered = copy.deepcopy(data_in_transactions)
    cont = 0
    #print(data_in_transactions)
    # if ChaCha20key != "":
    #     totalaux = hex(int(data_in_transactions[0][-8:], 2))[2:]
    #     while len(totalaux) % 2 != 0:
    #         totalaux = '0' + totalaux
    #     totalaux = cipherfunctions.ecipherChaCha20 (totalaux, ChaCha20key, "hex")
    #     total = int(totalaux[-1],16)
    #     print(total)
    # else:
    #     total = int(data_in_transactions[0][-4:], 2)
    for x in data_in_transactions:
        #print(x)
        if ChaCha20key != "":
            totalaux = hex(int(x[-len(hex(globalsettings.max_number_transactions)[2:])*8:], 2))[2:]
            #print(totalaux,x[-len(hex(globalsettings.max_number_transactions)[2:])*8:],len(hex(globalsettings.max_number_transactions)[2:]*2))
            while len(totalaux) != len(hex(globalsettings.max_number_transactions)[2:]*2):
                totalaux = '0' + totalaux
            #print(totalaux)
            totalaux = cipherfunctions.ecipherChaCha20 (totalaux, cipherfunctions.upgradeChaCha20key(ChaCha20key), "hex")
            #print(totalaux)
            index = int(totalaux[:len(hex(globalsettings.max_number_transactions)[2:])],16)
            total = int(totalaux[len(hex(globalsettings.max_number_transactions)[2:]):],16)
        else:
            index = int(x[-len(hex(globalsettings.max_number_transactions)[2:])*8:-len(hex(globalsettings.max_number_transactions)[2:])//2 *8], 2)
            total = int(x[0][-len(hex(globalsettings.max_number_transactions)[2:])//2*8:], 2)
        print("Transaction {} of {}".format(index,total))
        data_ordered[(index-1)] = x
        cont += 1
    if cont != total:
        print("Some transactions are missing\n")
        exit(1)
    return data_ordered

def checkifpartofmessagebin (data_to_retrieve, nonces_tx, data_in_transactions, cumulative_nonces, ChaCha20key):
    i = 0
    added= 0
    while i < len(data_to_retrieve):
        if ChaCha20key != "":
            totalaux = hex(int(data_to_retrieve[i][-len(hex(globalsettings.max_number_transactions)[2:])*8:], 2))[2:]
            while len(totalaux) != len(hex(globalsettings.max_number_transactions)[2:]*2):
                totalaux = '0' + totalaux
            totalaux = cipherfunctions.ecipherChaCha20 (totalaux, cipherfunctions.upgradeChaCha20key(ChaCha20key), "hex")
            index = int(totalaux[:len(hex(globalsettings.max_number_transactions)[2:])],16)
            total = int(totalaux[len(hex(globalsettings.max_number_transactions)[2:]):],16)
        else:
            index = int(data_to_retrieve[i][-len(hex(globalsettings.max_number_transactions)[2:])*8:-len(hex(globalsettings.max_number_transactions)[2:])//2 *8], 2)
            total = int(data_to_retrieve[i][0][-len(hex(globalsettings.max_number_transactions)[2:])//2*8:], 2)
        if total == len(data_in_transactions) and index > 0 and index <= len(data_in_transactions):
            data_in_transactions[(index-1)] = data_to_retrieve[i]
            cumulative_nonces[(index-1)] = nonces_tx[i]
            print("Transaction {} of {}".format(index,total))
            added += 1
        i += 1
    return data_in_transactions ,cumulative_nonces, added

def checkifsinglebin (data_in_transactions, ChaCha20key):
    """
    Check if the hash passed is part of a single-transaction message,
    is one of the transactions that contain the message or it does not
    seem to contain a message
    """
    single = False
    withmessage = True
    data_array = copy.deepcopy(data_in_transactions)
    total = 0
    # if ChaCha20key != "":
    #     totalaux = cipherfunctions.ecipherChaCha20 (data_in_transactions[0][:2], ChaCha20key, "hex")
    #     print(totalaux)
    #     total = int(totalaux[1], 16)
    # else:
    #     total = int(data_in_transactions[0][1], 16)

    if ChaCha20key != "":
        #print(-len(hex(globalsettings.max_number_transactions)[2:])*8, data_in_transactions[0][-len(hex(globalsettings.max_number_transactions)[2:])*8:])
        totalaux = hex(int(data_in_transactions[0][-len(hex(globalsettings.max_number_transactions)[2:])*8:], 2))[2:]
        #print(totalaux)
        while len(totalaux) != len(hex(globalsettings.max_number_transactions)[2:]*2):
            totalaux = '0' + totalaux
        totalaux = cipherfunctions.ecipherChaCha20 (totalaux, cipherfunctions.upgradeChaCha20key(ChaCha20key), "hex")
        #print(totalaux)
        index = int(totalaux[:len(hex(globalsettings.max_number_transactions)[2:])],16)
        total = int(totalaux[len(hex(globalsettings.max_number_transactions)[2:]):],16)
    else:
        index = int(data_in_transactions[0][-len(hex(globalsettings.max_number_transactions)[2:])*8:-len(hex(globalsettings.max_number_transactions)[2:])//2 *8], 2)
        total = int(data_in_transactions[0][0][-len(hex(globalsettings.max_number_transactions)[2:])//2*8:], 2)
    if index < 1 or index > total or total < 1:
        withmessage = False
        print("This transaction hash does not seem to contain a message\n")
    else:
        if total == 1:
            single = True
            data_array[(index-1)] = data_in_transactions[0]
        else:
            data_array = ['0'] * total
            data_array[(index-1)] = data_in_transactions[0]
        print("Transaction {} of {}".format(index,total))
    return single, data_array, total, index, withmessage

def orderblocksbincom (data_in_transactions,ChaCha20key):
    """
    Orders the transaction passed with binary information
    """
    completed = False
    data_ordered = copy.deepcopy(data_in_transactions)
    cont = 0
    #print(data_in_transactions)
    # if ChaCha20key != "":
    #     totalaux = hex(int(data_in_transactions[0][-8:], 2))[2:]
    #     while len(totalaux) % 2 != 0:
    #         totalaux = '0' + totalaux
    #     totalaux = cipherfunctions.ecipherChaCha20 (totalaux, ChaCha20key, "hex")
    #     total = int(totalaux[-1],16)
    #     print(total)
    # else:
    #     total = int(data_in_transactions[0][-4:], 2)
    for x in data_in_transactions:
        if ChaCha20key != "":
            totalaux = hex(int(x[:8], 2))[2:]
            while len(totalaux) % 2 != 0:
                totalaux = '0' + totalaux
            totalaux = cipherfunctions.ecipherChaCha20 (totalaux, cipherfunctions.upgradeChaCha20key(ChaCha20key), "hex")
            index = int(totalaux[0],16)
            total = int(totalaux[-1],16)
        else:
            index = int(x[:4], 2)
            total = int(x[4:8], 2)
        print("Transaction {} of {}".format(index,total))
        data_ordered[(index-1)] = x
        cont += 1
    if cont != total:
        print("Some transactions are missing\n")
        exit(1)
    return data_ordered

def orderblocksbin2 (data_in_transactions, ChaCha20key):
    """
    Orders the transaction passed with binary information
    """
    completed = False
    data_ordered = copy.deepcopy(data_in_transactions)
    cont = 0
    nonce_first = 0
    #print(data_in_transactions)
    # if ChaCha20key != "":
    #     totalaux = hex(int(data_in_transactions[0][0:8], 2))[2:]
    #     while len(totalaux) % 2 != 0:
    #         totalaux = '0' + totalaux
    #     totalaux = cipherfunctions.ecipherChaCha20 (totalaux, ChaCha20key, "hex")
    #     total = int(totalaux[-1],16)
    # else:
    #     total = int(data_in_transactions[0][4:8], 2)
    for x in data_in_transactions:
        if ChaCha20key != "":
            cipherfunctions.nonce = 0
            auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
            nonce = hex(int(x[0:8], 2))[2:]
            while len(nonce) % 2 != 0:
                nonce = '0' + nonce
            nonce = cipherfunctions.ecipherChaCha20 (nonce, auxChaCha20key, "hex")
            cipherfunctions.nonce = int(nonce,16)
            auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
            totalaux = hex(int(x[8:16], 2))[2:]
            while len(totalaux) % 2 != 0:
                totalaux = '0' + totalaux
            totalaux = cipherfunctions.ecipherChaCha20 (totalaux, auxChaCha20key, "hex")
            index = int(totalaux[0],16)
            if index == 1:
                nonce_first = int(nonce,16)
            total = int(totalaux[-1],16)
        else:
            total = int(x[4:8], 2)
            index = int(x[0:4], 2)
        print("Transaction {} of {}".format(index,total))
        data_ordered[(index-1)] = x
        cont += 1
    #print(cont)
    if cont != total:
        print("Some transactions are missing\n")
        exit(1)
    return data_ordered, nonce_first
