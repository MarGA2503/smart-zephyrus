#!/usr/bin/env python
"""
Ethereum bytecode dissassembler and assembler
"""

import math

__author__ = "Mar Gimenez Aguilar"
__version__ = "1"
__maintainer__ = "Mar Gimenez Aguilar"
__email__ = "mar.gimeneza@gmail.com"

# value = mnemonic, pc , gas, removedfromstack, addedtostack
def expGas (exp):
    if exp == 0:
        return 10
    else:
        return  (10 + 10 * (1 + math.log(exp,256)))

def sha3Gas(sizeInputInWords):
    return (30 + 6 * (sizeInputInWords))

def DataCopyGas(numberOfWords):
    numberOfWords = math.ceil(numberOfWords)
    return (2 + 3 * (numberOfWords))

def extCodeCopyGas(numberOfWords):
    numberOfWords = math.ceil(numberOfWords)
    return (700 + 3 * (numberOfWords))

def sstoreGas(values, storage):
    cost = 0
    for x in range(len(values)):
        if values[x] == storage[x] or values[x] == "0":
            cost += 5000
        else:
            cost += 20000
        return cost
def logGas (numberLog, numberBytesData):
    return (375 + 8 * (numberBytesData) + numberLog * 375)


vadissa = {
    "00" : [["STOP"], 1 , 0, 0, 0],
    "01" : [["ADD"], 1 , 3, 2, 1],
    "02" : [["MUL"], 1 , 5, 2, 1],
    "03" : [["SUB"], 1 , 3, 2, 1],
    "04" : [["DIV"], 1 , 5, 2, 1],
    "05" : [["SDIV"], 1 , 5, 2, 1],
    "06" : [["MOD"], 1 , 5, 2, 1],
    "07" : [["SMOD"], 1 , 5, 2, 1],
    "08" : [["ADDMOD"], 1 , 8, 3, 1],
    "09" : [["MULMOD"], 1 , 8, 3, 1],
    "0a" : [["EXP"], 1, expGas, 2, 1],
    "09" : [["SIGNEXTEND"], 1 , 5, 2, 1],
    "10" : [["LT"], 1 , 3, 2, 1],
    "11" : [["GT"], 1 , 3, 2, 1],
    "12" : [["SLT"], 1 , 3, 2, 1],
    "13" : [["SGT"], 1 , 3, 2, 1],
    "14" : [["EQ"], 1 , 3, 2, 1],
    "15" : [["ISZERO"], 1 , 3, 1, 1],
    "16" : [["AND"], 1 , 3, 2, 1],
    "17" : [["OR"], 1 , 3, 2, 1],
    "18" : [["XOR"], 1 , 3, 2, 1],
    "19" : [["NOT"], 1 , 3, 1, 1],
    "1a" : [["BYTE"], 1 , 3, 2, 1],
    "20" : [["SHA3"], 1 , sha3Gas, 2, 1],
    "30" : [["ADDRESS"], 1 , 2, 0, 1],
    "31" : [["BALANCE"], 1 , 400, 1, 1],
    "32" : [["ORIGIN"], 1 , 2, 0, 1],
    "33" : [["CALLER"], 1 , 2, 0, 1],
    "34" : [["CALLVALUE"], 1 , 2, 0, 1],
    "35" : [["CALLDATALOAD"], 1 , 3, 1, 1],
    "36" : [["CALLDATASIZE"], 1 , 2, 0, 1],
    "37" : [["CALLDATACOPY"], 1 , DataCopyGas, 3, 0],
    "38" : [["CODESIZE"], 1 , 2, 0, 1],
    "39" : [["CODECOPY"], 1 , DataCopyGas, 3, 0],
    "3a" : [["GASPRICE"], 1 , 2, 0, 1],
    "3b" : [["EXTCODESIZE"], 1 , 700, 1, 1],
    "3c" : [["EXTCODECOPY"], 1 , extCodeCopyGas, 4, 0],
    "40" : [["BLOCKHASH"], 1 , 20, 1, 1],
    "41" : [["COINBASE"], 1 , 2, 0, 1],
    "42" : [["TIMESTAMP"], 1 , 2, 0, 1],
    "43" : [["NUMBER"], 1 , 2, 0, 1],
    "44" : [["DIFFICULTY"], 1 , 2, 0, 1],
    "45" : [["GASLIMIT"], 1 , 2, 0, 1],
    "50" : [["POP"], 1 , 2, 1, 0],
    "51" : [["MLOAD"], 1 , 3, 1, 1],
    "52" : [["MSTORE"], 1 , 3, 2, 0],
    "53" : [["MSTORE8"], 1 , 3, 2, 0],
    "54" : [["SLOAD"], 1 , 200, 1, 1],
    "55" : [["SSTORE"], 1 , sstoreGas, 1, 1],
    "56" : [["JUMP"], 1 , 8, 1, 0],
    "57" : [["JUMPI"], 1 , 10, 2, 0],
    "58" : [["PC"], 1 , 2, 0, 1],
    "59" : [["MSIZE"], 1 , 2, 0, 1],
    "5a" : [["GAS"], 1 , 2, 0, 1],
    "5b" : [["JUMPDEST"], 1 , 1, 0, 0],
    "f0" : [["CREATE"], 1 , 32000, 3, 1],
    "f1" : [["CALL"], 1 , "COM", 7, 1],
    "f2" : [["CALLCODE"], 1 , "COM", 7, 1],
    "f3" : [["RETURN"], 1 , 0, 2, 0],
    "f4" : [["DELEGATECALL"], 1 , "COM", 6, 1],
    "fe" : [["ASSERT"], 1 , 0, 0, 0],
    "ff" : [["SELFDESTRUCT"], 1 , "COM", 1, 0],
}
vaassem = {
    "STOP" : ["00", 1 , 0, 0, 0],
    "ADD" : ["01", 1 , 3, 2, 1],
    "MUL" : ["02", 1 , 5, 2, 1],
    "SUB" : ["03", 1 , 3, 2, 1],
    "DIV" : ["04", 1 , 5, 2, 1],
    "SDIV" : ["05", 1 , 5, 2, 1],
    "MOD" : ["06", 1 , 5, 2, 1],
    "SMOD" : ["07", 1 , 5, 2, 1],
    "ADDMOD" : ["08", 1 , 8, 3, 1],
    "MULMOD" : ["09", 1 , 8, 3, 1],
    "EXP" : ["0a", 1, expGas, 2, 1],
    "SIGNEXTEND" : ["0b", 1 , 5, 2, 1],
    "LT" : ["10", 1 , 3, 2, 1],
    "GT" : ["11", 1 , 3, 2, 1],
    "SLT" : ["12", 1 , 3, 2, 1],
    "SGT" : ["13", 1 , 3, 2, 1],
    "EQ" : ["14", 1 , 3, 2, 1],
    "ISZERO" : ["15", 1 , 3, 1, 1],
    "AND" : ["16", 1 , 3, 2, 1],
    "OR" : ["17", 1 , 3, 2, 1],
    "XOR" : ["18", 1 , 3, 2, 1],
    "NOT" : ["19", 1 , 3, 1, 1],
    "BYTE" : ["1a", 1 , 3, 2, 1],
    "SHA3" : ["20", 1 , sha3Gas, 2, 1],
    "ADDRESS" : ["30", 1 , 2, 0, 1],
    "BALANCE" : ["31", 1 , 400, 1, 1],
    "ORIGIN" : ["32", 1 , 2, 0, 1],
    "CALLER" : ["33", 1 , 2, 0, 1],
    "CALLVALUE" : ["34", 1 , 2, 0, 1],
    "CALLDATALOAD" : ["35", 1 , 3, 1, 1],
    "CALLDATASIZE" : ["36", 1 , 2, 0, 1],
    "CALLDATACOPY" : ["37", 1 , DataCopyGas, 3, 0],
    "CODESIZE" : ["38", 1 , 2, 0, 1],
    "CODECOPY" : ["39", 1 , DataCopyGas, 3, 0],
    "GASPRICE" : ["3a", 1 , 2, 0, 1],
    "EXTCODESIZE" : ["3b", 1 , 700, 1, 1],
    "EXTCODECOPY" : ["3c", 1 , extCodeCopyGas, 4, 0],
    "BLOCKHASH" : ["40", 1 , 20, 1, 1],
    "COINBASE" : ["41", 1 , 2, 0, 1],
    "TIMESTAMP" : ["42", 1 , 2, 0, 1],
    "NUMBER" : ["43", 1 , 2, 0, 1],
    "DIFFICULTY" : ["44", 1 , 2, 0, 1],
    "GASLIMIT" : ["45", 1 , 2, 0, 1],
    "POP" : ["50", 1 , 2, 1, 0],
    "MLOAD" : ["51", 1 , 3, 1, 1],
    "MSTORE" : ["52", 1 , 3, 2, 0],
    "MSTORE8" : ["53", 1 , 3, 2, 0],
    "SLOAD" : ["54", 1 , 200, 1, 1],
    "SSTORE" : ["55", 1 , sstoreGas, 1, 1],
    "JUMP" : ["56", 1 , 8, 1, 0],
    "JUMPI" : ["57", 1 , 10, 2, 0],
    "PC" : ["58", 1 , 2, 0, 1],
    "MSIZE" : ["59", 1 , 2, 0, 1],
    "GAS" : ["5a", 1 , 2, 0, 1],
    "JUMPDEST" : ["5b", 1 , 1, 0, 0],
    "CREATE" : ["f0", 1 , 32000, 3, 1],
    "CALL" : ["f1", 1 , "COM", 7, 1],
    "CALLCODE" : ["f2", 1 , "COM", 7, 1],
    "RETURN" : ["f3", 1 , 0, 2, 0],
    "DELEGATECALL" : ["f4", 1 , "COM", 6, 1],
    "ASSERT" : ["fe", 1 , 0, 0, 0],
    "SELFDESTRUCT" : ["ff", 1 , "COM", 1, 0],
}

def assembly (opcodes):
    """
    Return bytecode from opcode
    """
    bytecode = ""
    #First check the PUSH, DUP, SWAP and LOG groups
    for i in opcodes:
        if i[0][:4] == "PUSH":
            number = i[0][4:]
            bytecode += hex(95 + int(number)).split('x')[-1]
            bytecode += i[1]
        elif i[0][:3] == "DUP":
            number = i[0][3:]
            bytecode += hex(127 + int(number)).split('x')[-1]
        elif i[0][:4] == "SWAP":
            number = i[0][4:]
            bytecode += hex(143 + int(number)).split('x')[-1]
        elif i[0][:3] == "LOG":
            number = i[0][3:]
            bytecode += hex(160 + int(number)).split('x')[-1]
        elif i[0] in vaassem:
            bytecode += vaassem[i[0]][0]
        else:
            bytecode += i[1]
    return bytecode

def disassembly(bytecode):
    """
    Return bytecode originally in hex in opcode form and memory positions
    """
    #First check the PUSH, DUP, SWAP and LOG groups
    opcodes = []
    memory = ["00000000"]
    i = 0
    while (i) < len(bytecode):
        byte = bytecode[i:(i + 2)]
        #PUSH
        if int(byte, 16) >= 96 and int(byte, 16) <= 127:
            number = int(byte, 16) - 95
            i += 2
            pushed = bytecode[i:(i + number * 2)]
            i += number * 2
            opcodes.append(["PUSH" + str(number), pushed])
            memory.append("0" * (8 - len(hex(int(memory[-1], 16) + (1 + number) ).split('x')[-1]) ) + hex(int(memory[-1], 16) + (1 + number) ).split('x')[-1])
        #DUP
        elif int(byte, 16) >= 128 and int(byte, 16) <= 143:
            number = int(byte, 16) - 127
            i += 2
            opcodes.append(["DUP" + str(number)])
            memory.append("0" * (8 - len(hex(int(memory[-1], 16) + (1) ).split('x')[-1] )) + hex(int(memory[-1], 16) + (1) ).split('x')[-1])
        #SWAP
        elif int(byte, 16) >= 144 and int(byte, 16) <= 159:
            number = int(byte, 16) - 143
            i += 2
            opcodes.append(["SWAP" + str(number)])
            memory.append("0" * (8 - len(hex(int(memory[-1], 16) + (1) ).split('x')[-1] )) + hex(int(memory[-1], 16) + (1) ).split('x')[-1])
        #LOG
        elif int(byte, 16) >= 160 and int(byte, 16) <= 164:
            number = int(byte, 16) - 160
            i += 2
            opcodes.append(["LOG" + str(number)])
            memory.append("0" * (8 - len(hex(int(memory[-1], 16) + (1) ).split('x')[-1] )) + hex(int(memory[-1], 16) + (1) ).split('x')[-1])
        #Check dictionary
        elif byte in vadissa:
            opcodes.append(vadissa[byte][0])
            i += 2
            memory.append("0" * (8 - len(hex(int(memory[-1], 16) + (1) ).split('x')[-1] )) + hex(int(memory[-1], 16) + (1) ).split('x')[-1])
        else:
            opcodes.append(["NOTOPCODE", byte ])
            i += 2
            memory.append("0" * (8 - len(hex(int(memory[-1], 16) + (1) ).split('x')[-1] )) + hex(int(memory[-1], 16) + (1) ).split('x')[-1])


        #Check if we are in the metadata
        # if len(bytecode) - 68 == i:
        #     if opcodes[-3][0] == "LOG1" and opcodes[-2][0] == "PUSH6" and opcodes[-2][1] == "627a7a723058" and opcodes[-1][0] == "SHA3":
        #         opcodes.append(bytecode[-68:-4])
        #         memory.append("0" * (8 - len(hex(int(memory[-1], 16) + (1) ).split('x')[-1] )) + hex(int(memory[-1], 16) + (1) ).split('x')[-1])
        #         i += 64
        #         opcodes.append(bytecode[-4:-2])
        #         memory.append("0" * (8 - len(hex(int(memory[-1], 16) + (1) ).split('x')[-1] )) + hex(int(memory[-1], 16) + (1) ).split('x')[-1])
        #         opcodes.append(bytecode[-2:])
        #         memory.append("0" * (8 - len(hex(int(memory[-1], 16) + (1) ).split('x')[-1] )) + hex(int(memory[-1], 16) + (1) ).split('x')[-1])
        #         i += 4
    return opcodes , memory[:-1]


# bytecode = "6060604052341561000f57600080fd5b60ba8061001d6000396000f300606060405260043610603f576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff168063771602f7146044575b600080fd5b3415604e57600080fd5b606b60048080359060200190919080359060200190919050506081565b6040518082815260200191505060405180910390f35b60008183019050929150505600a165627a7a723058206cd0d2416766777794260e766c7000326f47035656f6d665541429622667fd220029"
# opcodes, memory = disassembly(bytecode)
# print(opcodes)
# print(memory)
# newbytecode = assembly (opcodes)
# print(newbytecode)
# print(bytecode)
# assert(bytecode == newbytecode)
