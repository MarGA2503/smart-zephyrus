#!/usr/bin/env python
"""
Contains all functions related with generating bytecode based of the message
"""

import random
import copy
import sys
from .. auxiliar import cipherfunctions
from .. auxiliar import globalsettings

__author__ = "Mar Gimenez Aguilar"
__version__ = "1"
__maintainer__ = "Mar Gimenez Aguilar"
__email__ = "mar.gimeneza@gmail.com"

push0 = [['PUSH1', 1]]
push1 = [['PUSH2', 1]]
#value0 = [['PUSH1', 1], ['ADD', -1], ['MUL', -1], ['MOD', -1],['OR', -1], ['NOT', 0], ['DUP1', 1], ['DUP5', 1],['SWAP1', 0], ['SWAP2', 0], ['SWAP4', 0] ]
value0sin = {'PUSH1': ['PUSH1', 1], 'SWAP1':['SWAP1', 0], 'DUP2': ['DUP2', 1], 'SWAP2':['SWAP2', 0],'DUP1':['DUP1', 1],'EXP':['EXP',-1],'MSTORE':['MSTORE', -2], 'MLOAD':['MLOAD',0],'SSTORE':['SSTORE', -2], 'OR': ['OR', -1]}
value0sindictweights ={'PUSH1': 92994, 'DUP2':25282, 'SWAP2':24376, 'DUP1':20804, 'EXP':16338, 'MSTORE':10496, 'MLOAD':8389, 'SSTORE':6459, 'OR':5072}
value1sindictweights ={'POP':131842,'AND':34553, 'SWAP3':21787,'SLOAD':20618,'SUB':15297, 'ADD':8393, 'DUP4':6676, 'SWAP4':5561, 'NOT':5265 }
initialdic = {'00': [['PUSH1', 1], ['SLOAD', 0]], '01':[['POP', -1],['POP', -1]], '10': [['PUSH1', 1],['PUSH1', 1]],'11':[['PUSH1', 1],['DUP1', 0]]}
finaldic = {'11':[['POP', -1], ['POP', -1]], '10':[['AND', -1],['DUP2', 1]], '00':[['SWAP1', 0],['SSTORE', -2]], '01':[['POP', -1],['SWAP1', 0]]}
#value1 = [['PUSH2', 1], ['SUB', -1], ['DIV', -1], ['AND', -1], ['XOR', -1], ['DUP2', 1], ['DUP3', 1],['DUP4', 1], ['SWAP3', 0],['SWAP5', 0]]
value1sin = {'POP':['POP', -1], 'AND':['AND', -1], 'SWAP3': ['SWAP3', 0],'SLOAD':['SLOAD', 0], 'SUB':['SUB', -1],'ADD':['ADD', -1],'DUP4':['DUP4', 1],'SWAP4':['SWAP4', 0],'NOT':['NOT', 0]}
final0 = [['SWAP1', 0],['SWAP2', 0], ['SWAP4', 0]]
final1 = [['POP', -1], ['SWAP3', 0],['SWAP5', 0]]
push1_values = {'00':'00', '01':'01', '10':'02', '11':'a0'}
push1_values8 = {'000':'00', '001':'01', '010':'02', '011':'a0','100':'40','101':'20','110':'05','111':'ff'}
push1_bogus = {'40':8736, '20':6848, 'ff':2960}
length_select = {9:7830, 13:3974}
final_pop_select = {2: 22312, 4:4179}

value0_non_select = {}
eliminate1 = [['SUB', -1], ['DIV', -1], ['AND', -1], ['XOR', -1]]
dup0 = [['DUP1', 1]]
dup1 = [['DUP2', 1]]
eliminate0 = [['ADD', -1], ['MUL', -1], ['MOD', -1],['OR', -1]]
initialdic_extract = {
"['PUSH1', 'SLOAD']" : '00',
"['POP', 'POP']" : '01',
"['PUSH1', 'PUSH1']" : '10',
"['PUSH1', 'DUP1']" : '11'
}
finaldic_extract = {
"['POP', 'POP']" : '11',
"['AND', 'DUP2']" : '10',
"['SWAP1', 'SSTORE']" : '00',
"['POP', 'SWAP1']" : '01'

}
dicpush1_values = {
'00':'00',
'01':'01',
'02':'10',
'a0':'11'
}

dicpush1_values8 ={
'00':'000',
'01':'001',
'02':'010',
'a0':'011',
'40':'100',
'20':'101',
'05':'110',
'ff':'111'
}


dic0 = {
'PUSH1': '0',
'SWAP1': '0',
'DUP2': '0',
'SWAP2': '0',
'DUP1': '0',
'EXP':'0',
'MSTORE':'0',
'MLOAD':'0',
'SSTORE':'0',
'OR': '0'
}

dic1 = {
'POP': '1',
'AND': '1',
'SWAP3': '1',
'SLOAD':'1',
'SUB': '1',
'ADD': '1',
'DUP4': '1',
'SWAP4': '1',
'NOT': '1'
}

dic0_old = {
'PUSH1': '0',
'ADD': '0',
'MUL': '0',
'MOD': '0',
'OR': '0',
'NOT': '0',
'DUP1': '0',
'DUP5': '0',
'SWAP1': '0',
'SWAP2': '0',
'SWAP4': '0'
}

dic1_old = {
'PUSH2': '1',
'SUB': '1',
'DIV': '1',
'AND': '1',
'XOR': '1',
'POP': '1',
'DUP2': '1',
'DUP3': '1',
'DUP4': '1',
'SWAP3': '1',
'SWAP5': '1'
}

key_words = ['STOP', 'ASSERT']
#TODO = verificar si efectivamente mod resta 1 en la pila
#TODO introducir jumps
def generateBytecodeFunction ( message, key = ""):
    """
    Generate de bytecode equivalent to the message which is executed in a function
    """
    bytecode = []
    size = 0
    cont = 0
    posjump = 0
    if key != "":
        random.seed(key)
        orderMessage = random.sample(list(range(0,len(message))),len(message))
    else:
        orderMessage = list(range(0,len(message)))
    ##print(orderMessage)
    random.seed(a=None)
    for i in orderMessage:
        value = message[i]
        if posjump >= 4:
            if value == '0':
                instruction = random.choice(eliminate0)
            else:
                instruction = random.choice(eliminate1)
        elif cont < 2 :
            if value == '0':
                instruction = random.choice(push0)
            else:
                instruction = random.choice(push1)
        else:
            if value == '0':
                instruction = random.choice(value0)
            else:
                instruction = random.choice(value1)
        if instruction[0][0:4] == 'SWAP':
            if int(instruction[0][-1]) >= posjump:
                if value == '0':
                    instruction = random.choice(dup0)
                else:
                    instruction = random.choice(dup1)
            bytecode.append([instruction[0]])
            size += 1
        elif instruction[0][0:3] == 'DUP':
            if int(instruction[0][-1]) > posjump:
                if value == '0':
                    instruction = random.choice(dup0)
                else:
                    instruction = random.choice(dup1)
            bytecode.append([instruction[0]])
            size += 1
        elif instruction[0][0:4] == 'PUSH':
            maxim = 'f' * int(instruction[0][-1]) * 2
            number = hex(random.randint(0, int(maxim, 16)))[2:]
            if len(number) != len(maxim):
                zeros = (len(maxim) - len(number)) * '0'
                number = zeros + number
            bytecode.append([instruction[0], number])
            size = size + 1 + int(len(number)/2)
        else:
            bytecode.append([instruction[0]])
            size += 1
        cont += instruction[1]
        posjump += instruction[1]
        ##print("posjump",posjump)
        ##print(i)
    while bytecode[-1][0] == "POP":
        bytecode[-1] = random.choice(eliminate1)
    while  posjump != 0:
        bytecode.append(['POP'])
        posjump -= 1
        size += 1
    ##print(cont)

    ##print(size)
    return bytecode, size

def generateBytecodeFunctionpush ( message, key = ""):
    """
    Generate de bytecode equivalent to the message which is executed in a function
    """
    bytecode = []
    size = 0
    cont = 0
    posjump = 0
    if key != "":
        random.seed(key)
        orderMessage = random.sample(list(range(0,len(message))),len(message))
    else:
        orderMessage = list(range(0,len(message)))
    ##print(orderMessage)
    random.seed(a=None)
    left = random.randint(2,5)
    i = 0
    while i < len(message):
        random.seed(a=None)
        value = message[i]
        if posjump >= 4:
            if value == '0':
                instruction = random.choice(eliminate0)
            else:
                instruction = random.choice(eliminate1)
        elif cont < 2 :
            if value == '0':
                instruction = random.choice(push0)
            else:
                instruction = random.choice(push1)
        else:
            if value == '0':
                instruction = random.choice(value0)
            else:
                instruction = random.choice(value1)
        if instruction[0][0:4] == 'SWAP':
            if int(instruction[0][-1]) >= posjump:
                if value == '0':
                    instruction = random.choice(dup0)
                else:
                    instruction = random.choice(dup1)
            bytecode.append([instruction[0]])
            size += 1
        elif instruction[0][0:3] == 'DUP':
            if int(instruction[0][-1]) > posjump:
                if value == '0':
                    instruction = random.choice(dup0)
                else:
                    instruction = random.choice(dup1)
            bytecode.append([instruction[0]])
            size += 1
        elif instruction[0][0:4] == 'PUSH':
            maxim = 'f' * int(instruction[0][-1]) * 2
            indibits = 2 + int(instruction[0][-1]) +1
            auxbits = ''.join('{:08b}'.format(x) for x in bytes.fromhex(maxim))
            len_to_embed = len(auxbits)-(indibits)
            rest = len(message[(i+1):-left])
            #print("cont",cont, rest)
            if rest == 0 and cont >= 2:
                if value == '0':
                    instruction = random.choice(dup0)
                else:
                    instruction = random.choice(dup1)
                bytecode.append([instruction[0]])
                size += 1
            else:
                rest = len(message[(i+1):-left])
                #print(len_to_embed, rest,(len(auxbits) -1 ))
                if (len(auxbits) -1 ) <= rest:
                    len_to_embed = (len(auxbits) -1 )
                    len_to_embed_bits = '1'
                elif len_to_embed > rest:
                    len_to_embed = rest
                    len_to_embed_bits = '0' + bin(len_to_embed)[2:].zfill(indibits-1)
                else:
                    len_to_embed = len(auxbits)-(indibits)
                    len_to_embed_bits = '0' + bin(len_to_embed)[2:].zfill(indibits-1)
                #print(len_to_embed, rest)
                messabitsinpush = ""
                j = 1
                while j < (len_to_embed + 1) and rest != 0:
                    messabitsinpush += message[i+j]
                    j += 1
                #print(messabitsinpush)
                messagepush = len_to_embed_bits + messabitsinpush
                #number = hex(random.randint(0, int(maxim, 16)))[2:]
                #print(messagepush)
                if len(messagepush) < len(auxbits):
                    rest = len(auxbits) -len(messagepush)
                    messagepush += ''.join([random.choice(['0','1']) for x in range (rest)])
                    #print(messagepush)
                #print(messagepush)
                number0 = int(messagepush[0:indibits],2)
                number_l =int(messagepush[-indibits:],2)
                number0 = number0 ^ number_l
                #print(number0)
                number = bin(number0)[2:].zfill(indibits) + messagepush[indibits:]
                #print(number)
                random.seed(key+value.encode())
                orderMessage = random.sample(list(range(0,len(auxbits))),len(auxbits))
                newmessage = ""
                for z in orderMessage:
                    newmessage += messagepush[z]
                    z += 1
                number = bytearray([int(newmessage[i:i+8], 2) for i in range(0, len(messagepush), 8)]).hex()
                bytecode.append([instruction[0], number])
                #print([instruction[0], number])
                size = size + 1 + int(len(number)/2)
                i += len_to_embed
        else:
            bytecode.append([instruction[0]])
            size += 1
        cont += instruction[1]
        posjump += instruction[1]
        i+=1
        ##print("posjump",posjump)
        ##print(i)
    while bytecode[-1][0] == "POP":
        bytecode[-1] = random.choice(eliminate1)
    while  posjump != 0:
        bytecode.append(['POP'])
        posjump -= 1
        size += 1
    ##print(cont)

    ##print(size)
    return bytecode, size

# def generateBytecodepush_rewrite ( message, length_block, generated_bytecode, key = ""):
#     bytecode = []
#     bytecode.append(["JUMPDEST"])
#     i = 0
#     if  sys.version_info <= (3, 6):
#         opcodes_0 = []
#         opcodes_1  = []
#         push1_values = []
#         for key in value0sindictweights:
#             tmp = [key] * value0sindictweights[key]
#             opcodes_0 += tmp
#         for key in value1sindictweights:
#             tmp = [key] * value1sindictweights
#             opcodes_1 += tmp
#         for key in push1_bogus:
#             tmp = [key] * push1_bogus
#             push1_values = tmp
#     for i in range(length_block):
#         if value == '0':
#             if  sys.version_info >= (3, 6):
#                 instruction = random.choices (value0sindictweights.keys(), value0sindictweights.values())
#             else:
#                 instruction = random.choice(opcodes_0)
#         elif value == '1':
#             if  sys.version_info >= (3, 6):
#                 instruction = random.choices (value1sindictweights.keys(), value1sindictweights.values())
#             else:
#                 instruction = random.choice(opcodes_1)



def generateBytecodepushPOP ( message, key = ""):
    """
    Generate de bytecode equivalent to the message
    """
    bytecode = []
    bytecode.append(["JUMPDEST"])
    #size = 1
    size = 0
    #get lenght to use:
    if  sys.version_info >= (3, 6):
        length = random.choices(list(length_select.keys()), list(length_select.values()))[0]
        left = random.choices(list(final_pop_select.keys()), list(final_pop_select.values()))[0]
        #opcodes_0 = random.choices (value0sindictweights.keys(), value0sindictweights.values())
        #opcodes_1 = random.choices(value1sindictweights.keys(), value1sindictweights.values())
    else:
        all_options = []
        pop_options = []
        opcodes_0 = []
        opcodes_1  = []
        for key in length_select:
            tmp = [key] * length_select[key]
            all_options += tmp
        for key in final_pop_select:
            tmp = [key] * final_pop_select[key]
            pop_options += tmp
        for key in value0sindictweights:
            tmp = [key] * value0sindictweights[key]
            opcodes_0 += tmp
        for key in value1sindictweights:
            tmp = [key] * value1sindictweights[key]
            opcodes_1 += tmp
        length = random.choice(all_options)
        left = random.choice(pop_options)
    cont = 0
    ##print(orderMessage)
    random.seed(a=None)
    #left = random.randint(2,5)
    #print("selected length", length, left,message)
    size  = 0
    size_total = 1
    #we select the first two:
    while len(message) < 2:
        message += random.choice(['0', '1'])
    firsttwo = message[:2]
    tmp = initialdic[firsttwo]
    i = 2
    for instruction in tmp:
        size += 1
        size_total += 1
        if instruction[0] == 'PUSH1':
            while len(message[i:(i+2)]) < 2:
                message += random.choice(['0', '1'])
            number = push1_values[message[i:(i+2)]]
            bytecode.append([instruction[0], number])
            size_total+=int(len(number)/2)
            i += 2
        else:
            bytecode.append([instruction[0]])
    message = message[i:]
    i = 0
    #print(size, bytecode, length, left)
    while size < (length -left):
        random.seed(a=None)
        #print(i, message)
        if message[i:] == '':
            message += random.choice(['0', '1'])
        value = message[i]
        #print(i, message, value)
        i+=1
        # if i >= (len(length) - left):
        #     if value == '0':
        #         instruction = random.choice(final0)
        #     else:
        #         instruction = random.choice(final1)
        # elif cont < 1 :
        #     if value == '0':
        #         instruction = random.choice(push0)
        #     else:
        #         instruction = random.choice(push1)
        # else:
        #     if value == '0':
        #         instruction = random.choice(value0)
        #     else:
        #         instruction = random.choice(value1)
        if value == '0':
            if  sys.version_info >= (3, 6):
                instruction = random.choices (list(value0sindictweights.keys()), list(value0sindictweights.values()))[0]
            else:
                instruction = random.choice(opcodes_0)
            instruction = value0sin[instruction]
        elif value == '1':
            if  sys.version_info >= (3, 6):
                instruction = random.choices (list(value1sindictweights.keys()), list(value1sindictweights.values()))[0]
            else:
                instruction = random.choice(opcodes_1)
            instruction = value1sin[instruction]
        if instruction[0][0:4] == 'PUSH':
            #TODO: I dont want to end in PUsH
            size += 1
            while len(message[i:(i+2)]) < 2:
                message += random.choice(['0', '1'])
            number = push1_values[message[i:(i+2)]]
            bytecode.append([instruction[0], number])
            size_total = size_total + 1 + int(len(number)/2)
            i += 2
        else:
            if bytecode[-1][0:4] == "PUSH":
                while  instruction[0] == 'POP':
                    if  sys.version_info >= (3, 6):
                        instruction = random.choices (list(value1sindictweights.keys()), list(value1sindictweights.values()))[0]
                    else:
                        instruction = random.choice(opcodes_1)
                    instruction = value1sin[instruction]
            bytecode.append([instruction[0]])
            size += 1
            size_total += 1
        cont += instruction[1]
    ##print(cont)
    while bytecode[-1][0] == "POP":
        if  sys.version_info >= (3, 6):
            instruction = random.choices (list(value1sindictweights.keys()), list(value1sindictweights.values()))[0]
        else:
            instruction = random.choice(opcodes_1)
        instruction = value1sin[instruction]
        bytecode[-1] = [instruction[0]]
    while  left != 0:
        bytecode.append(['POP'])
        left -= 1
        size_total += 1
    message = message[i:]
    bytecode.append(["JUMP"])
    size_total += 1
    #print(bytecode, message)
    return bytecode, length, message, size_total

def generateBytecodepush ( message, key = ""):
    """
    Generate de bytecode equivalent to the message
    """
    bytecode = []
    bytecode.append(["JUMPDEST"])
    #size = 1
    size = 0
    left = 2
    #get lenght to use:
    if  sys.version_info >= (3, 6):
        length = random.choices(list(length_select.keys()), list(length_select.values()))[0]
        #left = random.choices(list(final_pop_select.keys()), list(final_pop_select.values()))[0]
        #opcodes_0 = random.choices (value0sindictweights.keys(), value0sindictweights.values())
        #opcodes_1 = random.choices(value1sindictweights.keys(), value1sindictweights.values())
    else:
        all_options = []
        pop_options = []
        opcodes_0 = []
        opcodes_1  = []
        for key in length_select:
            tmp = [key] * length_select[key]
            all_options += tmp
        for key in final_pop_select:
            tmp = [key] * final_pop_select[key]
            pop_options += tmp
        for key in value0sindictweights:
            tmp = [key] * value0sindictweights[key]
            opcodes_0 += tmp
        for key in value1sindictweights:
            tmp = [key] * value1sindictweights[key]
            opcodes_1 += tmp
        length = random.choice(all_options)
        #left = random.choice(pop_options)
    cont = 0
    ##print(orderMessage)
    random.seed(a=None)
    #left = random.randint(2,5)
    #print("selected length", length, left,message)
    size  = 0
    size_total = 1
    #we select the first two:
    while len(message) < 2:
        message += random.choice(['0', '1'])
    firsttwo = message[:2]
    tmp = initialdic[firsttwo]
    i = 2
    for instruction in tmp:
        size += 1
        size_total += 1
        if instruction[0] == 'PUSH1':
            while len(message[i:(i+3)]) < 3:
                message += random.choice(['0', '1'])
            number = push1_values8[message[i:(i+3)]]
            bytecode.append([instruction[0], number])
            size_total+=int(len(number)/2)
            i += 3
        else:
            bytecode.append([instruction[0]])
    message = message[i:]
    i = 0
    #print(size, bytecode, length, left)
    while size < (length -left):
        random.seed(a=None)
        #print(i, message)
        if message[i:] == '':
            message += random.choice(['0', '1'])
        value = message[i]
        #print(i, message, value)
        i+=1
        # if i >= (len(length) - left):
        #     if value == '0':
        #         instruction = random.choice(final0)
        #     else:
        #         instruction = random.choice(final1)
        # elif cont < 1 :
        #     if value == '0':
        #         instruction = random.choice(push0)
        #     else:
        #         instruction = random.choice(push1)
        # else:
        #     if value == '0':
        #         instruction = random.choice(value0)
        #     else:
        #         instruction = random.choice(value1)
        if value == '0':
            if  sys.version_info >= (3, 6):
                instruction = random.choices (list(value0sindictweights.keys()), list(value0sindictweights.values()))[0]
            else:
                instruction = random.choice(opcodes_0)
            instruction = value0sin[instruction]
        elif value == '1':
            if  sys.version_info >= (3, 6):
                instruction = random.choices (list(value1sindictweights.keys()), list(value1sindictweights.values()))[0]
            else:
                instruction = random.choice(opcodes_1)
            instruction = value1sin[instruction]
        if instruction[0][0:4] == 'PUSH':
            #TODO: I dont want to end in PUsH
            size += 1
            while len(message[i:(i+3)]) < 3:
                message += random.choice(['0', '1'])
            number = push1_values8[message[i:(i+3)]]
            bytecode.append([instruction[0], number])
            size_total = size_total + 1 + int(len(number)/2)
            i += 3
        else:
            if bytecode[-1][0:4] == "PUSH":
                while  instruction[0] == 'POP':
                    if  sys.version_info >= (3, 6):
                        instruction = random.choices (list(value1sindictweights.keys()), list(value1sindictweights.values()))[0]
                    else:
                        instruction = random.choice(opcodes_1)
                    instruction = value1sin[instruction]
            bytecode.append([instruction[0]])
            size += 1
            size_total += 1
        cont += instruction[1]
    ##print(cont)
    while len(message[i:]) < 2:
        #print("before random", message)
        message += random.choice(['0', '1'])
        #print("after random", message)
    # while bytecode[-1][0] == "POP":
    #     if  sys.version_info >= (3, 6):
    #         instruction = random.choices (list(value1sindictweights.keys()), list(value1sindictweights.values()))[0]
    #     else:
    #         instruction = random.choice(opcodes_1)
    #     instruction = value1sin[instruction]
    #     bytecode[-1] = [instruction[0]]
    lasttwo = message[i:(i+2)]
    #print(message, i , lasttwo)
    tmp = finaldic[lasttwo]
    for instruction in tmp:
        bytecode.append([instruction[0]])
        i += 1
        size_total += 1
    # while  left != 0:
    #     bytecode.append(['POP'])
    #     left -= 1
    #     size_total += 1
    message = message[i:]
    bytecode.append(["JUMP"])
    size_total += 1
    #print(bytecode, message)
    return bytecode, length, message, size_total

def prepareforgenerateBytecodepush(data, ChaCha20key):
    #print("data", data)
    bits_to_block_counter= int(len(hex(globalsettings.max_number_transactions)[2:])/2) *8
    data_length_hex_bytes = hex(int(len(data)/2))[2:]
    #to_read = hex(len(data_length_hex_bytes))[2:]
    if len(data_length_hex_bytes) % 2 != 0:
        data_length_hex_bytes = '0' + data_length_hex_bytes
    # if len(to_read) % 2 != 0:
    #     to_read = '0' + to_read
    #len_bits = ''.join('{:04b}'.format(x) for x in bytes.fromhex(data_length_hex_bytes))
    len_bits = bin(int(len(data)/2))[2:]
    while len(len_bits) % 4 != 0:
        len_bits = '0' + len_bits
    #print("antes de nada", data_length_hex_bytes, len_bits)
    len_bits_hex = hex(int(len(len_bits)/4))[2:]
    if len(len_bits_hex) % 2 != 0:
        len_bits_hex = '0' + len_bits_hex
    to_read =  ''.join('{:04b}'.format(x) for x in bytes.fromhex(len_bits_hex))
    if ChaCha20key != "":
        if len(len_bits) % 8 == 0 :
            len_bits = '0' * 4 + len_bits
            len_bits_hex = hex(int(len(len_bits)/4))[2:]
            if len(len_bits_hex) % 2 != 0:
                len_bits_hex = '0' + len_bits_hex
            to_read =  ''.join('{:04b}'.format(x) for x in bytes.fromhex(len_bits_hex))
        length_control_info = to_read + len_bits
        #print(to_read, len_bits, length_control_info)
        #print(cipherfunctions.nonce)
        auxChaCha20key = cipherfunctions.upgradeChaCha20key(ChaCha20key)
        #print(cipherfunctions.nonce)
        #auxChaCha20key = ChaCha20key
        len_data_length = hex(int(length_control_info[:8],2))[2:]
        if len(len_data_length) % 2 != 0:
            len_data_length = '0' + len_data_length
        #print(cipherfunctions.nonce,auxChaCha20key )
        len_data_length = cipherfunctions.cipherChaCha20(len_data_length, auxChaCha20key, "hex")
        #print(cipherfunctions.nonce,auxChaCha20key )
        if length_control_info[8:] != '':
            data_length_hex = hex(int(length_control_info[8:],2))[2:]
            if len(data_length_hex)% 2 != 0:
                data_length_hex = '0' + data_length_hex
            #print("control", length_control_info,length_control_info[8:],data_length_hex)
            #print(cipherfunctions.nonce,auxChaCha20key )
            data_length_hex =  cipherfunctions.cipherChaCha20(data_length_hex, auxChaCha20key, "hex")
            if len(data_length_hex)% 2 != 0:
                data_length_hex = '0' + data_length_hex
            #print(cipherfunctions.nonce,auxChaCha20key )
            #print("control", length_control_info,length_control_info[8:],data_length_hex,len_data_length + data_length_hex)
            data = len_data_length + data_length_hex + data
        else:
            data = len_data_length + data
    else:
        length_control_info = to_read + len_bits
        #print("control", length_control_info, to_read , len_bits,  hex(int(length_control_info,2))[2:])
        hex_control_info =  hex(int(length_control_info,2))[2:]
        if len(hex_control_info)% 2 != 0:
            hex_control_info = '0' + hex_control_info
        data =  hex_control_info + data
    #bogus = '0' * bits_to_block_counter
    data = ''.join('{:08b}'.format(x) for x in bytes.fromhex(data))
    data_old = copy.deepcopy(data)
    #data = bogus + data
    message = []
    lengths = []
    sizes_total = []
    #we simulate bogus block for first transaction
    while data != '' and len(message)<= globalsettings.max_number_transactions:
        bytecode, length, data, size_total = generateBytecodepush (data)
        message.append(bytecode)
        lengths.append(bytecode)
        sizes_total.append(size_total)
    if len(message) >  globalsettings.max_number_transactions or data != '':
        print("Please, reduce the size of the message or try again\n")
        exit(1)
    # block = hex(len(message))[2:]
    # if len(block) %2 != 0:
    #     block = '0' + block
    # if ChaCha20key != "":
    #     block_bits = ''.join('{:08b}'.format(x) for x in bytes.fromhex(auxtotal))
    return message,sizes_total


def generateBytecodepush_old ( message, key = ""):
    """
    Generate de bytecode equivalent to the message
    """
    bytecode = []
    bytecode.append(["JUMPDEST"])
    size = 1
    cont = 0
    ##print(orderMessage)
    random.seed(a=None)
    left = random.randint(2,5)
    i = 0
    while i < len(message):
        random.seed(a=None)
        value = message[i]
        if i >= (len(message) - left):
            if value == '0':
                instruction = random.choice(final0)
            else:
                instruction = random.choice(final1)
        elif cont < 1 :
            if value == '0':
                instruction = random.choice(push0)
            else:
                instruction = random.choice(push1)
        else:
            if value == '0':
                instruction = random.choice(value0)
            else:
                instruction = random.choice(value1)
        if instruction[0][0:4] == 'PUSH':
            #TODO: I dont want to end in PUsH
            maxim = 'f' * int(instruction[0][-1]) * 2
            indibits = 2 + int(instruction[0][-1]) + 1
            auxbits = ''.join('{:08b}'.format(x) for x in bytes.fromhex(maxim))
            len_to_embed = len(auxbits)-(indibits)
            rest = len(message[(i+1):-left])
            #print("cont", rest)
            if rest == 0:
                if value == '0':
                    instruction = random.choice(dup0)
                else:
                    instruction = random.choice(dup1)
                bytecode.append([instruction[0]])
                size += 1
            else:
                #print(len_to_embed, rest)
                #1 not extra, 0 extra
                if (len(auxbits) -1 ) <= rest:
                    len_to_embed = (len(auxbits) -1 )
                    len_to_embed_bits = '1'
                elif len_to_embed > rest:
                    len_to_embed = rest
                    len_to_embed_bits = '0' + bin(len_to_embed)[2:].zfill(indibits-1)
                else:
                    len_to_embed = len(auxbits)-(indibits)
                    len_to_embed_bits = '0' + bin(len_to_embed)[2:].zfill(indibits-1)
                #print(len_to_embed, rest)
                messabitsinpush = ""
                j = 1
                while j < (len_to_embed + 1) and rest != 0:
                    messabitsinpush += message[i+j]
                    j += 1
                #print(messabitsinpush)
                messagepush = len_to_embed_bits + messabitsinpush
                #number = hex(random.randint(0, int(maxim, 16)))[2:]
                #print(messagepush)
                if len(messagepush) < len(auxbits):
                    rest = len(auxbits) -len(messagepush)
                    messagepush += ''.join([random.choice(['0','1']) for x in range (rest)])

                #print(messagepush)
                number0 = int(messagepush[0:indibits],2)
                number_l =int(messagepush[-indibits:],2)
                number0 = number0 ^ number_l
                #print(number0)
                number = bin(number0)[2:].zfill(indibits) + messagepush[indibits:]
                #print(number)
                ##print(key+value.encode())
                random.seed(key+value.encode())
                orderMessage = random.sample(list(range(0,len(auxbits))),len(auxbits))
                newmessage = ""
                for z in orderMessage:
                    newmessage += messagepush[z]
                    z += 1
                number = bytearray([int(newmessage[i:i+8], 2) for i in range(0, len(messagepush), 8)]).hex()
                bytecode.append([instruction[0], number])
                #print([instruction[0], number])
                size = size + 1 + int(len(number)/2)
                i += len_to_embed
        else:
            bytecode.append([instruction[0]])
            size += 1
        cont += instruction[1]
        i += 1

    ##print(cont)
    bytecode.append(["JUMP"])
    size += 1
    #print(bytecode)
    return bytecode, size


def generateBytecodepushi ( message, key = ""):
    """
    Generate de bytecode equivalent to the message
    """
    bytecode = []
    size = 1
    cont = 0
    ##print(orderMessage)
    random.seed(a=None)
    left = random.randint(0,2)
    i = 0
    while i < len(message):
        random.seed(a=None)
        value = message[i]
        if i >= (len(message) - left):
            if value == '0':
                instruction = random.choice(final0)
            else:
                instruction = random.choice(final1)
        elif cont < 1 :
            if value == '0':
                instruction = random.choice(push0)
            else:
                instruction = random.choice(push1)
        else:
            if value == '0':
                instruction = random.choice(value0)
            else:
                instruction = random.choice(value1)
        if instruction[0][0:4] == 'PUSH':
            #TODO: I dont want to end in PUsH
            maxim = 'f' * int(instruction[0][-1]) * 2
            indibits = 2 + int(instruction[0][-1]) + 1
            auxbits = ''.join('{:08b}'.format(x) for x in bytes.fromhex(maxim))
            len_to_embed = len(auxbits)-(indibits)
            rest = len(message[(i+1):-left])
            #print("cont", rest)
            if rest == 0:
                if value == '0':
                    instruction = random.choice(dup0)
                else:
                    instruction = random.choice(dup1)
                bytecode.append([instruction[0]])
                size += 1
            else:
                #print(len_to_embed, rest)
                #1 not extra, 0 extra
                if (len(auxbits) -1 ) <= rest:
                    len_to_embed = (len(auxbits) -1 )
                    len_to_embed_bits = '1'
                elif len_to_embed > rest:
                    len_to_embed = rest
                    len_to_embed_bits = '0' + bin(len_to_embed)[2:].zfill(indibits-1)
                else:
                    len_to_embed = len(auxbits)-(indibits)
                    len_to_embed_bits = '0' + bin(len_to_embed)[2:].zfill(indibits-1)
                #print(len_to_embed, rest)
                messabitsinpush = ""
                j = 1
                while j < (len_to_embed + 1) and rest != 0:
                    messabitsinpush += message[i+j]
                    j += 1
                #print(messabitsinpush)
                messagepush = len_to_embed_bits + messabitsinpush
                #number = hex(random.randint(0, int(maxim, 16)))[2:]
                #print(messagepush)
                if len(messagepush) < len(auxbits):
                    rest = len(auxbits) -len(messagepush)
                    messagepush += ''.join([random.choice(['0','1']) for x in range (rest)])

                #print(messagepush)
                number0 = int(messagepush[0:indibits],2)
                number_l =int(messagepush[-indibits:],2)
                number0 = number0 ^ number_l
                #print(number0)
                number = bin(number0)[2:].zfill(indibits) + messagepush[indibits:]
                #print(number)
                ##print(key+value.encode())
                random.seed(key+value.encode())
                orderMessage = random.sample(list(range(0,len(auxbits))),len(auxbits))
                newmessage = ""
                for z in orderMessage:
                    newmessage += messagepush[z]
                    z += 1
                number = bytearray([int(newmessage[i:i+8], 2) for i in range(0, len(messagepush), 8)]).hex()
                bytecode.append([instruction[0], number])
                #print([instruction[0], number])
                size = size + 1 + int(len(number)/2)
                i += len_to_embed
        else:
            bytecode.append([instruction[0]])
            size += 1
        cont += instruction[1]
        i += 1

    ##print(cont)
    bytecode.append(["JUMPI"])
    size += 1
    #print(bytecode)
    return bytecode, size


def generateBytecode ( message, key = ""):
    """
    Generate de bytecode equivalent to the message
    """
    bytecode = []
    bytecode.append(["JUMPDEST"])
    size = 1
    cont = 0
    if key != "":
        random.seed(key)
        orderMessage = random.sample(list(range(0,len(message))),len(message))
    else:
        orderMessage = list(range(0,len(message)))
    ##print(orderMessage)
    random.seed(a=None)
    left = random.randint(1,4)
    for i in orderMessage:
        value = message[i]
        if len(bytecode) >= (len(message) - left):
            if value == '0':
                instruction = random.choice(final0)
            else:
                instruction = random.choice(final1)
        elif cont < 1 :
            if value == '0':
                instruction = random.choice(push0)
            else:
                instruction = random.choice(push1)
        else:
            if value == '0':
                instruction = random.choice(value0)
            else:
                instruction = random.choice(value1)
        if instruction[0][0:4] == 'PUSH':
            maxim = 'f' * int(instruction[0][-1]) * 2
            number = hex(random.randint(0, int(maxim, 16)))[2:]
            if len(number) != len(maxim):
                zeros = (len(maxim) - len(number)) * '0'
                number = zeros + number
            bytecode.append([instruction[0], number])
            size = size + 1 + int(len(number)/2)
        else:
            bytecode.append([instruction[0]])
            size += 1
        cont += instruction[1]
    ##print(cont)
    bytecode.append(["JUMP"])
    size += 1
    ##print(bytecode)
    return bytecode, size
#TODO: add JUMPS
def getmessage (bytecode, key = ""):
    """
    Retrieves the message from the bytecode
    """
    message = ['0'] * len(bytecode)
    if key != "":
        random.seed(key)
        orderMessage = random.sample(list(range(0,len(message))),len(message))
    else:
        orderMessage = list(range(0,len(message)))
    ##print(key)
    ##print(orderMessage)
    x = 0
    for i in orderMessage:
        if bytecode[x][0] in dic1:
            message[i] = '1'
        x += 1
    return ''.join(message)

def getmessagepush (bytecode, key = ""):
    """
    Retrieves the message from the bytecode
    """
    message = ''
    ##print(key)
    ##print(orderMessage)
    found = False
    end =len(bytecode)
    #TODO:check if correct instruction
    ##print(bytecode)
    # while end >=0 and not found:
    #     print(bytecode[end])
    #     if bytecode[end][0] == "POP":
    #         end -= 1
    #     else:
    #         found = True
    #         end += 1
    found = False
    bytecode = bytecode[:end]
    print(bytecode,end,bytecode[:end])
    i = 0
    tmp = ''
    values = []
    instruction = []
    for i in range(2):
        instruction.append( bytecode[i][0])
        if bytecode[i][0][0:4] == 'PUSH':
            tmp = dicpush1_values8[bytecode[i][1]]
            values.append(tmp)
    message += initialdic_extract[str(instruction)]
    for y in values:
        message += y
    i = 2
    #print("before", message)
    while i < (len(bytecode)-2):
        print(bytecode[i], message)
        if bytecode[i][0] in dic1:
            message += '1'
        elif bytecode[i][0] in dic0:
            message += '0'
        if bytecode[i][0][0:4] == 'PUSH':
            #print(bytecode[i])
            tmp = dicpush1_values8[bytecode[i][1]]
            message+= tmp
        i += 1
    #print("i", i, len(bytecode))
    instruction = []
    while i <(len(bytecode)):
        instruction.append( bytecode[i][0])
        i+=1
    message += finaldic_extract[str(instruction)]
    #print(message)
    return (message)

def getmessagepushPOP (bytecode, key = ""):
    """
    Retrieves the message from the bytecode
    """
    message = ''
    ##print(key)
    ##print(orderMessage)
    found = False
    end =len(bytecode) - 1
    #TODO:check if correct instruction
    ##print(bytecode)
    while end >=0 and not found:
        #print(bytecode[end])
        if bytecode[end][0] == "POP":
            end -= 1
        else:
            found = True
            end += 1
    found = False
    bytecode = bytecode[:end]
    #print(bytecode)
    i = 0
    tmp = ''
    values = []
    instruction = []
    for i in range(2):
        instruction.append( bytecode[i][0])
        if bytecode[i][0][0:4] == 'PUSH':
            tmp = dicpush1_values[bytecode[i][1]]
            values.append(tmp)
    message += initialdic_extract[str(instruction)]
    for y in values:
        message += y
    i = 2
    #print("before", message)
    while i < (len(bytecode)):
        #print(bytecode[i], message)
        if bytecode[i][0] in dic1:
            message += '1'
        elif bytecode[i][0] in dic0:
            message += '0'
        if bytecode[i][0][0:4] == 'PUSH':
            #print(bytecode[i])
            tmp = dicpush1_values[bytecode[i][1]]
            message+= tmp
        #print(message)
        i += 1
    #print(message)
    return (message)

def getmessagepush_old (bytecode, key = ""):
    """
    Retrieves the message from the bytecode
    """
    message = ''
    ##print(key)
    ##print(orderMessage)
    x = 0
    #first two:
    for i in range(len(bytecode)):
        if bytecode[i][0] in dic1:
            message += '1'
        elif bytecode[i][0] in dic0:
            message += '0'
        if bytecode[i][0][0:4] == 'PUSH':
            #print(bytecode[i])
            indibits = 2 + int(bytecode[i][0][-1]) + 1
            bytes_hex = bytes.fromhex(bytecode[i][1])
            bits = ''.join('{:08b}'.format(x) for x in bytes_hex)
            #print('bits', bits)
            ##print(key+message[-1].encode())
            random.seed(key+message[-1].encode())
            orderMessage = random.sample(list(range(0,len(bits))),len(bits))
            z = 0
            newmessage = list(copy.deepcopy(bits))
            for j in orderMessage:
                newmessage[j] = bits[z]
                z += 1
            bits = ''.join(newmessage)
            #Check if full or only parts
            #print('bits', bits)
            if bits[0] == '1':
                message += bits[1:]
            else:
                #print ('else')
                #print (bits)
                to_read = int(bits [1: indibits],2)
                #print (to_read)
                bits = bits[indibits:]
                #print(bits[0:to_read])
                message += bits[0:to_read]
        #print(message)
        i += 1
    #print(message)
    return (message)

def findlastjump(bytecode):
    found_jump = False
    found_jumpi = False
    i =len(bytecode) - 1
    in_keywords = False
    #TODO:check if correct instruction
    ##print(bytecode)
    while i >=0 and not found_jump and not found_jumpi:
        #print(bytecode[i][0])
        if bytecode[i][0] == "JUMP" :
            print(bytecode[(i+1)][0])
            if  bytecode[(i+1)][0] in key_words:
                in_keywords = True
            found_jump = True
        elif bytecode[i][0] == "JUMPI":
            if  bytecode[(i+1)][0] in key_words:
                in_keywords = True
            found_jumpi = True
        elif bytecode[i][0] == "JUMPDEST":
            i = 0
        else:
            i -= 1
    #print(i, found_jump, found_jumpi, in_keywords)
    return i, found_jump, found_jumpi, in_keywords

def findlaststop(bytecode):
    found = False
    i =len(bytecode) - 1
    #TODO:check if correct instruction
    ##print(bytecode)
    while i >=0 and not found:
        if bytecode[i][0] == "STOP" and bytecode[(i+1)][0] in key_words:
            found = True
        elif bytecode[i][0] == "JUMPI" or bytecode[i][0] == "JUMP" or bytecode[i][0] == "JUMPDEST":
            i = 0
        else:
            i -= 1
    return i, found


# def changesizecontract (originalbytecode, messagebytecodesize):
#     """
#     Change the size of the contract
#     """
#     found = False
#     i = 0
#     #TODO:check if correct instruction
#     while i < len(originalbytecode) and not found:
#         if originalbytecode[i][0] == "STOP":
#             found = True
#         i += 1
#     i -= 1
#     #now I find the instruction to change
#     found = False
#     while i >= 0  and not found:
#         if originalbytecode[i][0] == "DUP1":
#             found = True
#         i -= 1
#     #Check sizes
#     original_size = len(originalbytecode[i][1])
#     ##print(originalbytecode[i][1])
#     newsize = hex(int(originalbytecode[i][1], 16) + messagebytecodesize)[2:]
#     ##print(len(newsize), original_size)
#     if len(newsize) > original_size:
#         print("Message too large, reduce it or divide between contracts\n")
#         exit(1)
#     if len(newsize) != original_size:
#         rest = original_size - len(newsize)
#         newsize = ('0'*rest) + newsize
#         ##print(newsize)
#     originalbytecode[i][1] = newsize
#     return originalbytecode

def changesizecontract (originalbytecode, messagebytecodesize):
    """
    Change the size of the contract
    """
    found = False
    i = 0
    #TODO:check if correct instruction
    while i < len(originalbytecode) and not found:
        if originalbytecode[i][0] == "CODECOPY":
            found = True
        i += 1
    i -= 1
    #now I find the instruction to change
    found = False
    while i >= 0  and not found:
        if originalbytecode[i][0] == "DUP1":
            found = True
        i -= 1
    #Check sizes
    original_size = len(originalbytecode[i][1])
    ##print(originalbytecode[i][1])
    newsize = hex(int(originalbytecode[i][1], 16) + messagebytecodesize)[2:]
    ##print(len(newsize), original_size)
    if len(newsize) > original_size:
        print("Message too large, reduce it or divide between contracts\n")
        exit(1)
    if len(newsize) != original_size:
        rest = original_size - len(newsize)
        newsize = ('0'*rest) + newsize
        ##print(newsize)
    originalbytecode[i][1] = newsize
    return originalbytecode

def extractBytecodeFunction(bytecode, key = ""):
    """
    Retrieves the message for the bytecode inserted in a function
    """
    found = False
    end =len(bytecode) - 1
    #TODO:check if correct instruction
    ##print(bytecode)
    while end >=0 and not found:
        ##print(bytecode[end])
        if bytecode[end][0] == "POP":
            end -= 1
        else:
            found = True
    i = copy.deepcopy(end)
    found = False
    while i >=0 and not found:
        ##print(bytecode[i])
        if bytecode[i][0] == "JUMP" or bytecode[i][0] == "JUMPDEST":
            found = True
        else:
            i -= 1
    ##print(bytecode[(i+1):end])
    message = getmessagepush(bytecode[(i+1):(end +1)], key)
    return message

def extractBytecodeNoJumps(bytecode, key = ""):
    """
    Extract all bytecode before the JUMPDEST
    """
    found = False
    i = len(bytecode) -1
    #TODO:check if correct instruction
    ##print(bytecode)
    while i >=0 and not found:
        ##print(bytecode[i])
        if bytecode[i][0] == "JUMPDEST":
            found = True
        else:
            i -= 1
    print(bytecode[(i+1):])
    message = getmessagepush(bytecode[(i+1):], key)
    return message

def extractBytecodeNoJumps_old(bytecode, key = ""):
    """
    Extract all bytecode before the JUMPDEST
    """
    found = False
    i = len(bytecode) -1
    #TODO:check if correct instruction
    ##print(bytecode)
    while i >=0 and not found:
        ##print(bytecode[i])
        if bytecode[i][0] == "JUMPDEST":
            found = True
        else:
            i -= 1
    message = getmessagepush(bytecode[(i+1):], key)
    return message

def extractBytecodeNoJumpsi(bytecode, key = ""):
    """
    Extract all bytecode before the JUMPDEST
    """
    found = False
    i = len(bytecode) -1
    #TODO:check if correct instruction
    ##print(bytecode)
    while i >=0 and not found:
        ##print(bytecode[i])
        if bytecode[i][0] == "JUMPI":
            found = True
        else:
            i -= 1
    message = getmessagepush(bytecode[(i+1):], key)
    return message
# def addnewbytecode (originalbytecode, newbytecode):
#     found = False
#     i = len(originalbytecode) -1
#     while i >= 0 and not found:

# import disassembler
# message = '10001111111111100000000101010010101010010000101111000100000111100010100101010010010010100101010100101010101010101010010100'
# # bytecode = "6060604052341561000f57600080fd5b61011c8061001e6000396000f3006060604052600436106049576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff168063165c4a1614604e5780638c12d8f014608b575b600080fd5b3415605857600080fd5b6075600480803590602001909190803590602001909190505060cf565b6040518082815260200191505060405180910390f35b3415609557600080fd5b60b2600480803590602001909190803590602001909190505060dc565b604051808381526020018281526020019250505060405180910390f35b6000818302905092915050565b6000808284019150828402905092509290505600a165627a7a72305820bd6b5df937fcf534e13a518b40f94d72fcaadf91fcbdf7bf7df1a81913e30e9a0029"
# # opcode, f = disassembler.disassembly(bytecode[:-90])
# # #print(bytecode[:-90])
# # #print(opcode)
# # newopcode, size = generateBytecodeFunction( message, key = "")
# # #print(newopcode)
# # changesizecontract (opcode, size)
# # opcode_modified = opcode + newopcode
# # newbytecode = disassembler.assembly(opcode_modified) + bytecode[-90:]
# # #print(newbytecode)
# bytecode = generateBytecodeFunctionpush(message)
# #print(bytecode)
# newmessage = getmessagepush(bytecode[0])
# #print(newmessage)
# assert (message == newmessage[:-1])
# bytecode = generateBytecodeFunction(message, "hola")
# #print(bytecode)
# #print(bytecode[:-1])
# bytecode = bytecode[:-1]
# key = "hola"
# random.seed(key)
# #print(len(bytecode))
# #print(len(message))
# orderMessage = random.sample(list(range(0,len(message))),len(message))
# #print(orderMessage)
# newmessage = getmessage(bytecode, "hola")
# #print(newmessage)
# assert (message == newmessage)
