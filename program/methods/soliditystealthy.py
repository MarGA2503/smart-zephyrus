import os
import copy
import math
from natsort import natsorted
import itertools
from itertools import combinations
import random
from collections import OrderedDict
import string

def get_random_string(length):
    # choose from all lowercase letter
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    ##print("Random string of length", length, "is:", result_str)
    return result_str



def retrievefolder (folder_name):
    sub_folders = os.listdir(folder_name)
    return natsorted(sub_folders)

def formatname(name):
    if '-' in name:
        name = name.split ('-')[1]
    if "." in name:
        name = name.split ('.')[0]
    return name

def readwholefile (file):
    with open(file, encoding="utf-8") as f:
         read_data = f.read()
    return read_data

def readlines (file):
    with open(file, encoding="utf-8") as f:
        lines=f.readlines()
    return lines

def writecontract (contract_name, contractmessage):
    with open('./' + contract_name + ".sol", "w", encoding="utf-8") as f:
        f.write(contractmessage)


def buildToken (message, herency,appended):
    dictionary  =  './program/resources/dictionarycleaned.txt'
    token_folder = './program/resources/TokenContract'
    lines = readlines(dictionary)
    length = int(math.log2(len(lines)))
    #print(length)
    message_aux = copy.deepcopy(message)
    #Token name
    if len(message_aux) < length:
        tmp = ''.join(random.choices(['0','1'], k=(length-len(message_aux))))
        message_aux += tmp
    mess_tmp = int(message_aux[:length],2)
    #print(mess_tmp)
    message_aux = message_aux[length:]
    contract_name= lines[mess_tmp].rstrip().capitalize()
    token_name= lines[mess_tmp].rstrip().capitalize() + " "
    #print(contract_name)
    #print(token_name)
    del lines [mess_tmp]
    if len(message_aux) < length:
        tmp = ''.join(random.choices(['0','1'], k=(length-len(message_aux))))
        message_aux += tmp
    mess_tmp = int(message_aux[:length],2)
    #print(mess_tmp)
    message_aux = message_aux[length:]
    contract_name += lines[mess_tmp].rstrip().capitalize()
    token_name += lines[mess_tmp].rstrip().capitalize()
    #print(contract_name)
    #print(token_name)
    #Symbol length
    if len(message_aux) < 1:
        tmp = ''.join(random.choices(['0','1'], k=(1-len(message_aux))))
        message_aux += tmp
    mess_tmp = int(message_aux[:1],2)
    #print(mess_tmp)
    message_aux = message_aux[1:]
    length_symbol =[3,4][mess_tmp]
    #print(length_symbol)
    symbol_list = [''.join(l) for l in combinations(contract_name, length_symbol)]
    symbol_list = [x.lower() for x in symbol_list]
    symbol_list = list(OrderedDict.fromkeys(symbol_list))
    length = int(math.log2(len(symbol_list)))
    #print(length,len(symbol_list))
    if len(message_aux) < length:
        tmp = ''.join(random.choices(['0','1'], k=(length-len(message_aux))))
        message_aux += tmp
    mess_tmp = message_aux[:length]
    #print(mess_tmp)
    message_aux = message_aux[length:]
    index_chosen = int(mess_tmp, 2)
    #print(index_chosen)
    token_symbol = symbol_list[index_chosen].upper()
    #print(token_symbol)
    #print(symbol_list)
    #Weigthed choice with Token, just for fun
    random.seed(a=None)
    result=random.choices(population=["", " Token"], weights=[0.88, 0.12])[0]
    contract_name+=result[1:]
    token_name +=result
    #Start constructing the token
    #header
    token_contract = ""
    header_path = os.path.join(token_folder, "header")
    files = os.listdir(header_path)
    #print(files)
    if "comments.txt" in files:
        lines = readlines(os.path.join(token_folder, "header","comments.txt"))
        #print(lines)
        lines [1] = lines[1].replace("MyToken", contract_name)
        #print(lines[1])
        token_contract += "".join(lines)
        del files[files.index("comments.txt")]
    lines = readlines(os.path.join(token_folder, "header", files[0]))
    #print(lines)
    header_line = lines[0].replace("MyToken", contract_name)[:-1] + ", ".join(herency) + " {\n"
    #print(header_line)
    token_contract += header_line
    #token_contract += "\n"
    #Now we add the name and symbol
    variables_path = os.path.join(token_folder, "variables")
    files = os.listdir(variables_path)
    #print(files)
    for i in files:
        if os.path.isfile(os.path.join (variables_path, i)):
            # lines = readlines(os.path.join(token_folder, "variables", i))
            # #print(lines)
            # variable_line = lines[0].replace("MyToken", token_name)
            # #print(variable_line)
            # token_contract += variable_line
            # variable_line = lines[1].replace("MyToken", token_symbol)
            # #print(variable_line)
            # token_contract += variable_line
            # #print(token_contract)
            continue
        elif ( eval(i +  " in herency)")):
            sub_files = os.listdir(os.path.join(variables_path, i))
            lines = readlines(os.path.join(token_folder, "variables", i, sub_files[0]))
            for x in lines:
                token_contract += x
            #print(token_contract)
    #constructor
    token_contract += "\n"
    constructor_path = os.path.join(token_folder, "constructor")
    files = os.listdir(constructor_path)
    #print(files)
    if "to_delete.txt" in files:
        lines = readlines(os.path.join(token_folder, "constructor","to_delete.txt"))
        to_delete = eval(lines[0])
        del files[files.index("to_delete.txt")]
        #Checking which we have to delete
        list_keys = list(to_delete.keys())
        #print(list_keys)
        for x  in list_keys:
            if x in herency:
                del to_delete[x]
    lines = readlines(os.path.join(token_folder, "constructor", "constructor.txt"))
    #print(lines, "+++++", to_delete, herency)
    constructor_line = lines[0]
    for x in to_delete.keys():
        if to_delete[x] in constructor_line:
            constructor_line = constructor_line.replace(to_delete[x], "")
    constructor_line = constructor_line.replace ("_name", '"'+token_name+'"' )
    constructor_line  = constructor_line.replace (" _symbol", '"'+token_symbol+'"')
    token_contract += constructor_line
    del files[files.index("constructor.txt")]
    for i in files:
        #print(i)
        ##print(eval(i))
        if ( eval(i +  " in herency)")):
            sub_files = os.listdir(os.path.join(constructor_path, i))
            lines = readlines(os.path.join(token_folder, "constructor", i, sub_files[0]))
            for x in lines:
                token_contract += x
    token_contract += "     }\n"
    token_contract += "\n"
    #print(token_contract)
    #modifier
    modifiers_path = os.path.join(token_folder, "modifiers")
    files = os.listdir(modifiers_path)
    if "to_delete.txt" in files:
        lines = readlines(os.path.join(token_folder, "modifiers","to_delete.txt"))
        to_delete = eval(lines[0])
        del files[files.index("to_delete.txt")]
        #Checking which we have to delete
        list_keys = list(to_delete.keys())
        #print(list_keys)
        for x  in list_keys:
            if x in herency:
                del to_delete[x]
    for i in files:
        if ( eval(i +  " in herency)")):
            sub_files = os.listdir(os.path.join(modifiers_path, i))
            lines = readlines(os.path.join(token_folder, "modifiers", i, sub_files[0]))
            for x in lines:
                token_contract += x
    #print(token_contract)
    #functions
    functions_path = os.path.join(token_folder, "functions")
    files = os.listdir(functions_path)
    lines = readlines(os.path.join(token_folder, "functions", "functions.txt"))
    #print(lines)
    del files[files.index("functions.txt")]
    for i in range(len(lines)):
        for x in to_delete.keys():
            for z in to_delete[x]:
                if z in lines[i]:
                    #print(z,lines[i])
                    lines[i] = lines[i].replace(" "+z, "")
            if "MinterRole" in herency and ("onlyMinter" in lines[i] and "onlyOwner" in lines[i]):
                lines[i] = lines[i].replace(" onlyOwner", "")
            if "MinterRole" in herency and "Forwarder" in herency and ("onlyMinter" in lines[i] and "onlyParent" in lines[i]):
                lines[i] = lines[i].replace(" onlyParent", "")
            if "Forwarder" in herency and not "Ownable" in herency and ("onlyParent" in lines[i] and "onlyOwner" in lines[i]):
                lines[i] = lines[i].replace(" onlyOwner", "")
        token_contract += lines[i]
    #print(token_contract)
    token_contract += "\n"
    for y in files:
        #print(y)
        y_formatted = formatname(y)
        if (( eval(y_formatted +  "in herency"))or ( eval(y_formatted +  "in appended"))):
            if os.path.isfile(os.path.join(functions_path, y)):
                sub_files = [y]
                y = ""
            else:
                sub_files = os.listdir(os.path.join(functions_path, y))
            for b in range(len(sub_files)):
                if eval(sub_files[b][:-4]):
                    lines = readlines(os.path.join(token_folder, "functions", y, sub_files[b]))
                    for i in range(len(lines)):
                        for x in to_delete.keys():
                            for z in to_delete[x]:
                                if z in lines[i]:
                                    #print(z,lines[i])
                                    lines[i] = lines[i].replace(" "+z, "")
                            if len(list(filter(lambda x: "ERC20" in x, herency))) <= 1 and "override (ERC20,ERC20Snapshot)" in lines[i]:
                                lines[i] = lines[i].replace("(ERC20,ERC20Snapshot)", "(ERC20Snapshot)")
                            if "MinterRole" in herency and ("onlyMinter" in lines[i] and "onlyOwner" in lines[i]):
                                lines[i] = lines[i].replace(" onlyOwner", "")
                            if not "Pausable" in herency and "whenNotPaused" in lines[i]:
                                lines[i] = lines[i].replace(" whenNotPaused", "")
                            if "MinterRole" in herency and "Forwarder" in herency and ("onlyMinter" in lines[i] and "onlyParent" in lines[i]):
                                lines[i] = lines[i].replace(" onlyParent", "")
                            if "Forwarder" in herency and not "Ownable" in herency and ("onlyParent" in lines[i] and "onlyOwner" in lines[i]):
                                lines[i] = lines[i].replace(" onlyOwner", "")
                        token_contract += lines[i]
                    token_contract += "\n"
    token_contract += "}\n"
    #print(token_contract)
    return token_contract,contract_name,message_aux



def insertstealthy (message):
    contractmessage = ""
    ERC20_folder = './program/resources/ERC20/stealthy'
    folder_stealthy = './program/resources/stealhy'
    #dictionary  =  '/home/mar/Descargas/dictionary.txt'
    contract_list = retrievefolder(folder_stealthy)
    erc_20_order = retrievefolder(ERC20_folder)
    #print(contract_list, erc_20_order)
    message_aux = copy.deepcopy(message)
    #Pragma
    pragma = "pragma solidity 0.8."
    if len(message_aux) < 4:
        tmp = ''.join(random.choices(['0','1'], k=(4-len(message_aux))))
        message_aux += tmp
    mess_tmp = message_aux[:4]
    #print(mess_tmp)
    #print(contract_list)
    message_aux = message_aux[4:]
    pragma_version = int(mess_tmp, 2)
    pragma += str(pragma_version) +";\n"
    contractmessage += "// SPDX-License-Identifier: MIT\n"
    contractmessage += "\n"
    contractmessage += pragma
    contractmessage += "\n"
    herency = []
    appended = []
    for i in range(len(erc_20_order)):
        contractmessage += readwholefile(os.path.join(ERC20_folder, erc_20_order[i])) + "\n"
        #print(formatname( erc_20_order[i]))
        appended.append(formatname( erc_20_order[i]))
    herency.append("ERC20")
    #print(herency)
    #print(appended)
    #We will save the chosen in a dict to embed information in the sorted
    chosen = {}
    for i in range(2):
        if len(message_aux) < 3:
            tmp = ''.join(random.choices(['0','1'], k=(3-len(message_aux))))
            message_aux += tmp
        mess_tmp = message_aux[:3]
        #print(mess_tmp)
        #print(contract_list)
        message_aux = message_aux[3:]
        index_chosen = int(mess_tmp, 2)
        #print(index_chosen,len(contract_list))
        contract_chosen = contract_list[index_chosen]
        #chosen[formatname(contract_chosen)] = ""
        del contract_list[index_chosen]
        files_in_contract = retrievefolder(os.path.join(folder_stealthy, contract_chosen))
        for x in range(len(files_in_contract)):
            if formatname(files_in_contract[x]) not in appended:
                data = readwholefile(os.path.join(folder_stealthy, contract_chosen, files_in_contract[x]))
                contractmessage += data + "\n"
                #chosen[formatname(contract_chosen)] += data + "\n"
                appended.append(formatname(files_in_contract[x]))
        if herency[0] == "ERC20" and "ERC20" in contract_chosen:
            del herency[0]
        if formatname(files_in_contract[-1])!="TokenTimelock":
            herency.append(formatname(files_in_contract[-1]))
    #print(contractmessage)
    ##print(chosen, chosen.keys())
    # #insert in order
    # chosen_sorted = sorted(list(chosen.keys()))
    # z = list(itertools.permutations(chosen_sorted))
    # length = int(math.log2(len(z)))
    # #print(length,len(z))
    # mess_tmp = message_aux[:length]
    # #print(mess_tmp)
    # message_aux = message_aux[length:]
    # index_chosen = int(mess_tmp, 2)
    # #print(index_chosen)
    # chosen_sorted = list(z[index_chosen])
    # #print(chosen_sorted)
    # #append to messgage in order
    # for i in chosen_sorted:
    #     contractmessage += chosen[i]
    # #print(contractmessage)
    #print("***",herency)
    #herency in order
    #herency = sorted(herency)
    z = list(itertools.permutations(herency))
    length = int(math.log2(len(z)))
    #print(length,len(z))
    if len(message_aux) < length:
        tmp = ''.join(random.choices(['0','1'], k=(length-len(message_aux))))
        message_aux += tmp
    mess_tmp = message_aux[:length]
    #print(mess_tmp)
    message_aux = message_aux[length:]
    if length > 0:
        index_chosen = int(mess_tmp, 2)
    else:
        index_chosen = 0
    #print(index_chosen)
    herency = list(z[index_chosen])
    #print(herency)
    #print(appended)
    #print(message_aux)
    token_contract, contract_name, message_aux = buildToken (message_aux, herency,appended)
    contractmessage += token_contract
    ##print(contractmessage)
    return contractmessage, contract_name, message_aux,pragma_version
def insertmessage (message):
    message = bytes.fromhex(message)
    message =''.join('{:08b}'.format(x) for x in message)
    contracts_names = []
    pragmas = []
    while len(message) != 0:
        contractmessage, contract_name,message_aux, pragma_version = insertstealthy (message)
        if contract_name not in contracts_names:
            contracts_names.append(contract_name)
            #print("hhhhh")
            #print(contracts_names)
        else:
            i=1

            while contract_name in contracts_names:
                contract_name += str(i)
                i+=1
            contracts_names.append(contract_name)
            #print("oooooooo")
            #print(contracts_names)
        writecontract (contract_name,contractmessage)
        message = copy.deepcopy(message_aux)
        pragmas.append(pragma_version)
    return contracts_names, pragmas


def buildTokenstealthy (message, herency,appended):
    dictionary  =  './program/resources/little_dic.txt'
    token_folder = './program/resources/TokenContract'
    lines = readlines(dictionary)
    length = int(math.log2(len(lines)))
    #print(length)
    message_aux = copy.deepcopy(message)
    #Token name
    if len(message_aux) < length:
        tmp = ''.join(random.choices(['0','1'], k=(length-len(message_aux))))
        message_aux += tmp
    mess_tmp = int(message_aux[:length],2)
    #print(mess_tmp)
    message_aux = message_aux[length:]
    contract_name= lines[mess_tmp].rstrip().capitalize()
    token_name= lines[mess_tmp].rstrip().capitalize() + " "
    #print(contract_name)
    #print(token_name)
    del lines [mess_tmp]
    if len(message_aux) < length:
        tmp = ''.join(random.choices(['0','1'], k=(length-len(message_aux))))
        message_aux += tmp
    mess_tmp = int(message_aux[:length],2)
    #print(mess_tmp)
    message_aux = message_aux[length:]
    contract_name += lines[mess_tmp].rstrip().capitalize()
    token_name += lines[mess_tmp].rstrip().capitalize()
    #print(contract_name)
    #print(token_name)
    #Symbol length
    if len(message_aux) < 1:
        tmp = ''.join(random.choices(['0','1'], k=(1-len(message_aux))))
        message_aux += tmp
    mess_tmp = int(message_aux[:1],2)
    #print(mess_tmp)
    message_aux = message_aux[1:]
    length_symbol =[3,4][mess_tmp]
    #print(length_symbol)
    symbol_list = [''.join(l) for l in combinations(contract_name, length_symbol)]
    symbol_list = [x.lower() for x in symbol_list]
    symbol_list = list(OrderedDict.fromkeys(symbol_list))
    length = int(math.log2(len(symbol_list)))
    #print(length,len(symbol_list))
    # if len(message_aux) < length:
    #     tmp = ''.join(random.choices(['0','1'], k=(length-len(message_aux))))
    #     message_aux += tmp
    # mess_tmp = message_aux[:length]
    # #print(mess_tmp)
    # message_aux = message_aux[length:]
    random.seed(a=None)
    rnd = random.randint(0,100)
    if rnd <= 80:
        mess_tmp = message_aux[:length]
        if len(mess_tmp) < length:
            tmp = ''.join(random.choices(['0','1'], k=(length-len(mess_tmp))))
            mess_tmp += tmp
        index_chosen = int(mess_tmp, 2)
        #print(index_chosen)
        token_symbol = symbol_list[index_chosen].upper()
    else:
        #print
        token_symbol = get_random_string(length_symbol).upper()
    #print(token_symbol)
    #print(symbol_list)
    #Weigthed choice with Token, just for fun
    random.seed(a=None)
    result=random.choices(population=["", " Token"], weights=[0.88, 0.12])[0]
    contract_name+=result[1:]
    token_name +=result
    #Start constructing the token
    #header
    token_contract = ""
    header_path = os.path.join(token_folder, "header")
    files = os.listdir(header_path)
    #print(files)
    if "comments.txt" in files:
        lines = readlines(os.path.join(token_folder, "header","comments.txt"))
        #print(lines)
        lines [1] = lines[1].replace("MyToken", contract_name)
        #print(lines[1])
        token_contract += "".join(lines)
        del files[files.index("comments.txt")]
    lines = readlines(os.path.join(token_folder, "header", files[0]))
    #print(lines)
    header_line = lines[0].replace("MyToken", contract_name)[:-1] + ", ".join(herency) + " {\n"
    #print(header_line)
    token_contract += header_line
    #token_contract += "\n"
    #Now we add the name and symbol
    variables_path = os.path.join(token_folder, "variables")
    files = os.listdir(variables_path)
    #print(files)
    for i in files:
        if os.path.isfile(os.path.join (variables_path, i)):
            # lines = readlines(os.path.join(token_folder, "variables", i))
            # #print(lines)
            # variable_line = lines[0].replace("MyToken", token_name)
            # #print(variable_line)
            # token_contract += variable_line
            # variable_line = lines[1].replace("MyToken", token_symbol)
            # #print(variable_line)
            # token_contract += variable_line
            # #print(token_contract)
            continue
        elif ( eval(i +  " in herency)")):
            sub_files = os.listdir(os.path.join(variables_path, i))
            lines = readlines(os.path.join(token_folder, "variables", i, sub_files[0]))
            for x in lines:
                token_contract += x
            #print(token_contract)
    #constructor
    token_contract += "\n"
    constructor_path = os.path.join(token_folder, "constructor")
    files = os.listdir(constructor_path)
    #print(files)
    if "to_delete.txt" in files:
        lines = readlines(os.path.join(token_folder, "constructor","to_delete.txt"))
        to_delete = eval(lines[0])
        del files[files.index("to_delete.txt")]
        #Checking which we have to delete
        list_keys = list(to_delete.keys())
        #print(list_keys)
        for x  in list_keys:
            if x in herency:
                del to_delete[x]
    lines = readlines(os.path.join(token_folder, "constructor", "constructor.txt"))
    #print(lines, "+++++", to_delete, herency)
    constructor_line = lines[0]
    for x in to_delete.keys():
        if to_delete[x] in constructor_line:
            constructor_line = constructor_line.replace(to_delete[x], "")
    constructor_line = constructor_line.replace ("_name", '"'+token_name+'"' )
    constructor_line  = constructor_line.replace (" _symbol", '"'+token_symbol+'"')
    token_contract += constructor_line
    del files[files.index("constructor.txt")]
    for i in files:
        #print(i)
        ##print(eval(i))
        if ( eval(i +  " in herency)")):
            sub_files = os.listdir(os.path.join(constructor_path, i))
            lines = readlines(os.path.join(token_folder, "constructor", i, sub_files[0]))
            for x in lines:
                token_contract += x
    token_contract += "     }\n"
    token_contract += "\n"
    #print(token_contract)
    #modifier
    modifiers_path = os.path.join(token_folder, "modifiers")
    files = os.listdir(modifiers_path)
    if "to_delete.txt" in files:
        lines = readlines(os.path.join(token_folder, "modifiers","to_delete.txt"))
        to_delete = eval(lines[0])
        del files[files.index("to_delete.txt")]
        #Checking which we have to delete
        list_keys = list(to_delete.keys())
        #print(list_keys)
        for x  in list_keys:
            if x in herency:
                del to_delete[x]
    for i in files:
        if ( eval(i +  " in herency)")):
            sub_files = os.listdir(os.path.join(modifiers_path, i))
            lines = readlines(os.path.join(token_folder, "modifiers", i, sub_files[0]))
            for x in lines:
                token_contract += x
    #print(token_contract)
    #functions
    functions_path = os.path.join(token_folder, "functions")
    files = os.listdir(functions_path)
    lines = readlines(os.path.join(token_folder, "functions", "functions.txt"))
    #print(lines)
    del files[files.index("functions.txt")]
    for i in range(len(lines)):
        for x in to_delete.keys():
            for z in to_delete[x]:
                if z in lines[i]:
                    #print(z,lines[i])
                    lines[i] = lines[i].replace(" "+z, "")
            if "MinterRole" in herency and ("onlyMinter" in lines[i] and "onlyOwner" in lines[i]):
                lines[i] = lines[i].replace(" onlyOwner", "")
            if "MinterRole" in herency and "Forwarder" in herency and ("onlyMinter" in lines[i] and "onlyParent" in lines[i]):
                lines[i] = lines[i].replace(" onlyParent", "")
            if "Forwarder" in herency and not "Ownable" in herency and ("onlyParent" in lines[i] and "onlyOwner" in lines[i]):
                lines[i] = lines[i].replace(" onlyOwner", "")
        token_contract += lines[i]
    #print(token_contract)
    token_contract += "\n"
    for y in files:
        #print(y)
        y_formatted = formatname(y)
        if (( eval(y_formatted +  "in herency"))or ( eval(y_formatted +  "in appended"))):
            if os.path.isfile(os.path.join(functions_path, y)):
                sub_files = [y]
                y = ""
            else:
                sub_files = os.listdir(os.path.join(functions_path, y))
            for b in range(len(sub_files)):
                if eval(sub_files[b][:-4]):
                    lines = readlines(os.path.join(token_folder, "functions", y, sub_files[b]))
                    for i in range(len(lines)):
                        for x in to_delete.keys():
                            for z in to_delete[x]:
                                if z in lines[i]:
                                    #print(z,lines[i])
                                    lines[i] = lines[i].replace(" "+z, "")
                            if len(list(filter(lambda x: "ERC20" in x, herency))) <= 1 and "override (ERC20,ERC20Snapshot)" in lines[i]:
                                lines[i] = lines[i].replace("(ERC20,ERC20Snapshot)", "(ERC20Snapshot)")
                            if "MinterRole" in herency and ("onlyMinter" in lines[i] and "onlyOwner" in lines[i]):
                                lines[i] = lines[i].replace(" onlyOwner", "")
                            if not "Pausable" in herency and "whenNotPaused" in lines[i]:
                                lines[i] = lines[i].replace(" whenNotPaused", "")
                            if "MinterRole" in herency and "Forwarder" in herency and ("onlyMinter" in lines[i] and "onlyParent" in lines[i]):
                                lines[i] = lines[i].replace(" onlyParent", "")
                            if "Forwarder" in herency and not "Ownable" in herency and ("onlyParent" in lines[i] and "onlyOwner" in lines[i]):
                                lines[i] = lines[i].replace(" onlyOwner", "")
                        token_contract += lines[i]
                    token_contract += "\n"
    token_contract += "}\n"
    #print(token_contract)
    return token_contract,contract_name,message_aux



def insertstealthystealthy (message):
    contractmessage = ""
    ERC20_folder = './program/resources/ERC20/stealthy'
    folder_stealthy = './program/resources/stealhy'
    #dictionary  =  '/home/mar/Descargas/dictionary.txt'
    contract_list = retrievefolder(folder_stealthy)
    erc_20_order = retrievefolder(ERC20_folder)
    #print(contract_list, erc_20_order)
    message_aux = copy.deepcopy(message)
    #Pragma
    pragma = "pragma solidity 0.8."
    if len(message_aux) < 4:
        tmp = ''.join(random.choices(['0','1'], k=(4-len(message_aux))))
        message_aux += tmp
    mess_tmp = message_aux[:4]
    #print(mess_tmp)
    #print(contract_list)
    message_aux = message_aux[4:]
    pragma_version = int(mess_tmp, 2)
    pragma += str(pragma_version) +";\n"
    contractmessage += "// SPDX-License-Identifier: MIT\n"
    contractmessage += "\n"
    contractmessage += pragma
    contractmessage += "\n"
    herency = []
    appended = []
    for i in range(len(erc_20_order)):
        contractmessage += readwholefile(os.path.join(ERC20_folder, erc_20_order[i])) + "\n"
        #print(formatname( erc_20_order[i]))
        appended.append(formatname( erc_20_order[i]))
    herency.append("ERC20")
    #print(herency)
    #print(appended)
    #We will save the chosen in a dict to embed information in the sorted
    chosen = {}
    for i in range(2):
        if len(message_aux) < 3:
            tmp = ''.join(random.choices(['0','1'], k=(3-len(message_aux))))
            message_aux += tmp
        mess_tmp = message_aux[:3]
        #print(mess_tmp)
        #print(contract_list)
        message_aux = message_aux[3:]
        index_chosen = int(mess_tmp, 2)
        #print(index_chosen,len(contract_list))
        contract_chosen = contract_list[index_chosen]
        #chosen[formatname(contract_chosen)] = ""
        del contract_list[index_chosen]
        files_in_contract = retrievefolder(os.path.join(folder_stealthy, contract_chosen))
        for x in range(len(files_in_contract)):
            if formatname(files_in_contract[x]) not in appended:
                data = readwholefile(os.path.join(folder_stealthy, contract_chosen, files_in_contract[x]))
                contractmessage += data + "\n"
                #chosen[formatname(contract_chosen)] += data + "\n"
                appended.append(formatname(files_in_contract[x]))
        if herency[0] == "ERC20" and "ERC20" in contract_chosen:
            del herency[0]
        if formatname(files_in_contract[-1])!="TokenTimelock":
            herency.append(formatname(files_in_contract[-1]))
    #print(contractmessage)
    ##print(chosen, chosen.keys())
    # #insert in order
    # chosen_sorted = sorted(list(chosen.keys()))
    # z = list(itertools.permutations(chosen_sorted))
    # length = int(math.log2(len(z)))
    # #print(length,len(z))
    # mess_tmp = message_aux[:length]
    # #print(mess_tmp)
    # message_aux = message_aux[length:]
    # index_chosen = int(mess_tmp, 2)
    # #print(index_chosen)
    # chosen_sorted = list(z[index_chosen])
    # #print(chosen_sorted)
    # #append to messgage in order
    # for i in chosen_sorted:
    #     contractmessage += chosen[i]
    # #print(contractmessage)

    # #print("***",herency)
    random.seed(a=None)
    rnd = random.randint(0,100)
    if rnd <= 10:
        random.shuffle(herency)

    #herency in order
    #herency = sorted(herency)
    # z = list(itertools.permutations(herency))
    # length = int(math.log2(len(z)))
    # #print(length,len(z))
    # if len(message_aux) < length:
    #     tmp = ''.join(random.choices(['0','1'], k=(length-len(message_aux))))
    #     message_aux += tmp
    # mess_tmp = message_aux[:length]
    # #print(mess_tmp)
    # message_aux = message_aux[length:]
    # index_chosen = int(mess_tmp, 2)
    # #print(index_chosen)
    # herency = list(z[index_chosen])
    #print(herency)
    #print(appended)
    #print(message_aux)
    token_contract, contract_name, message_aux = buildTokenstealthy (message_aux, herency,appended)
    contractmessage += token_contract
    ##print(contractmessage)
    return contractmessage, contract_name, message_aux,pragma_version

def insertmessagestealthy (message):
    message = bytes.fromhex(message)
    message =''.join('{:08b}'.format(x) for x in message)
    contracts_names = []
    pragmas = []
    while len(message) != 0:
        contractmessage, contract_name,message_aux, pragma_version = insertstealthystealthy (message)
        if contract_name not in contracts_names:
            contracts_names.append(contract_name)
        else:
            i=1
            while contract_name in contracts_names:
                contract_name += str(i)
                i+=1
            contracts_names.append(contract_name)
        writecontract (contract_name,contractmessage)
        message = copy.deepcopy(message_aux)
        pragmas.append(pragma_version)
    return contracts_names, pragmas

#message = '11111111111111111111111111111111111111111111111111111111111111111111111'
# message = '011010000110111101101100011000010010000001110001011101010110010100100000011101000110000101101100'
# #message = '011001010111001101110100011001010010000001100101011100110010000001100101011011000010000001101110011101010110010101110110011011110010000001110100011001010111001101110100'
# message="0111011001100001011011010110111101110011001000000110000100100000011000110110000101101101011000100110100101100001011100100010000001110100011011110110010001110000"
# message = '76616D6F7320612063616D6269617220746F6470'
# insertmessage(message)
