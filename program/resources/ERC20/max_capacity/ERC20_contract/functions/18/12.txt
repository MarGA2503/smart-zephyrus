  function _spendAllowance( address owner, address _spender,  uint256 amount) internal virtual {
       uint256 _currentAllowance = allowance(owner, _spender);
       if (_currentAllowance != type(uint256).max) {
           require(_currentAllowance >= amount, "ERC20: insufficient allowance");
           unchecked {
               _approve(owner, _spender, _currentAllowance - amount);
           }
       }
   }
