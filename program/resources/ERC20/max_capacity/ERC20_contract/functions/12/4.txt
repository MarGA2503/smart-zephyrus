function _approve( address _owner, address _spender, uint256 amount) internal virtual {
    require(_owner != address(0), "ERC20: approve from the zero address");
    require(_spender != address(0), "ERC20: approve to the zero address");

    _allowances[_owner][_spender] = amount;
    emit Approval(_owner, _spender, amount);
}

