/**
 * @title MyToken
 * @dev Standard ERC20 token 
 * For full specification of ERC-20 standard see:
 * https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20.md
 */
