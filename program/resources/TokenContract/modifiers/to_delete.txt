{'Pausable': ['whenNotPaused', 'whenPaused'], 'ReentrancyGuard': ['nonReentrant'], 'MinterRole': ['onlyMinter'], 'Utils': ['greaterThanZero(amount)', 'greaterThanZero(addedValue)', 'greaterThanZero(subtractedValue)'], 'Forwarder': ['onlyParent']}

