    /**
     * @dev Creates a new snapshot and returns its snapshot id.
     *
     * Emits a {Snapshot} event that contains the same id.
     *
     * {_snapshot} is `internal` and you have to decide how to expose it externally. Its usage may be restricted to a
     * set of accounts, for example using {AccessControl}, or it may be open to the public.
     *
     */
     function snapshot() public onlyOwner onlyParent returns (uint256) {
         return super._snapshot();
     }

    /**
     * @dev Get the current snapshotId
     */
     function getCurrentSnapshotId() public view returns (uint256) {
        return super._getCurrentSnapshotId();
        }
