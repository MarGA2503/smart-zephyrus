    /**
     * @dev Destroys `amount` tokens from the caller.
     *
     * See {ERC20-_burn}.
     */
     function burn(uint256 amount) public override onlyMinter onlyOwner onlyParent nonReentrant {
        _burn(_msgSender(), amount);
    } 
    
    /**
     * @dev Destroys `amount` tokens from `account`, deducting from the caller's
     * allowance.
     *
     * See {ERC20-_burn} and {ERC20-allowance}.
     *
     * Requirements:
     *
     * - the caller must have allowance for ``accounts``'s tokens of at least
     * `amount`.
     */
    function burnFrom(address account, uint256 amount) public override onlyMinter onlyOwner onlyParent nonReentrant {
        _spendAllowance(account, _msgSender(), amount);
        _burn(account, amount);
    }
